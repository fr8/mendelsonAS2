head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.52.22;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2007.10.09.10.53.50;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b19/de/mendelson/comm/as2/message/AS2MessageParser.java,v 1.1 2007/10/09 10:53:50 heller Exp $
package de.mendelson.comm.as2.message;
import de.mendelson.comm.as2.AS2Exception;
import de.mendelson.comm.as2.cert.CertificateManager;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.partner.PartnerAccessDB;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.util.MecResourceBundle;
import de.mendelson.util.security.BCCryptoHelper;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.bouncycastle.cms.RecipientId;
import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.RecipientInformationStore;
import org.bouncycastle.mail.smime.SMIMECompressed;
import org.bouncycastle.mail.smime.SMIMEEnveloped;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Analyzes and builds AS2 messages
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class AS2MessageParser{
    
    private Logger logger = Logger.getLogger( "de.mendelson.as2" );
    
    /**Access to the settings of this product*/
    private PreferencesAS2 preferences = new PreferencesAS2();
    
    /**Access to all certificates*/
    private CertificateManager certificateManager;
    
    private MecResourceBundle rb = null;
    private MecResourceBundle rbMessage = null;
    
    public AS2MessageParser( CertificateManager certificateManager ){
        //Load resourcebundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleAS2MessageParser.class.getName());
            this.rbMessage = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleAS2Message.class.getName());
        }
        //load up  resourcebundle
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
        this.certificateManager = certificateManager;
    }
    
    /**Analyzes and creates passed message data
     */
    public AS2Message createMessageFromRequest( byte[] data, Properties header, String contentType ) throws Exception{
        AS2Message message = new AS2Message();
        AS2MessageInfo info = message.getMessageInfo();
        info.setMessageId( header.getProperty( "message-id" ));
        //MDN: server is in "server"
        //AS2 msg: server is in "user-agent"
        info.setUserAgent( header.getProperty( "server" ));
        if( info.getUserAgent() == null ){
            info.setUserAgent( header.getProperty( "user-agent" ));
        }
        MDNParser mdnParser = new MDNParser();
        mdnParser.parseMDNData( info, data, contentType );
        if( info.isMDN() ){
            this.logger.log( Level.FINE, this.rb.getResourceString( "mdn.incoming", info.getMessageId()), info );
            //check if related message id exists in db
            MessageAccessDB messageAccess = new MessageAccessDB( "localhost" );
            if( !messageAccess.messageIdExists( info.getRelatedMessageId() )){
                messageAccess.close();
                throw new Exception( this.rb.getResourceString( "mdn.unexpected",
                        new Object[]{
                    info.getMessageId(), info.getRelatedMessageId()
                }));
            }
            messageAccess.close();
            this.logger.log( Level.INFO, this.rb.getResourceString( "mdn.answerto",
                    new Object[]{
                info.getMessageId(), info.getRelatedMessageId()
            }), info );
            if( mdnParser.getDispositionState() != null ){
                if( mdnParser.getDispositionState().toLowerCase().indexOf( "failed" ) >= 0
                        || mdnParser.getDispositionState().toLowerCase().indexOf( "error" ) >= 0 ){
                    this.logger.log( Level.SEVERE,
                            this.rb.getResourceString( "mdn.state",
                            new Object[]{
                        info.getMessageId(), mdnParser.getDispositionState()
                    }), info );
                    this.logger.log( Level.SEVERE,
                            this.rb.getResourceString( "mdn.details",
                            new Object[]{
                        info.getMessageId(), mdnParser.getMdnDetails()
                    }), info );
                    info.setState( AS2Message.STATE_STOPPED );
                } else{
                    this.logger.log( Level.FINE,
                            this.rb.getResourceString( "mdn.state",
                            new Object[]{
                        info.getMessageId(), mdnParser.getDispositionState()
                    }), info );
                    this.logger.log( Level.FINE,
                            this.rb.getResourceString( "mdn.details",
                            new Object[]{
                        info.getMessageId(), mdnParser.getMdnDetails()
                    }), info );
                    info.setState( AS2Message.STATE_FINISHED );
                }
            }
        } else{
            //inbound AS2 message, no MDN
            this.logger.log( Level.FINE, this.rb.getResourceString( "msg.incoming",
                    new Object[]{
                info.getMessageId(),
                AS2Message.getDataSizeDisplay( data.length )}
            ), info );
            info.getDispositionNotificationOptions().setHeaderValue(
                    header.getProperty( "disposition-notification-options" ));
        }
        String senderId = header.getProperty( "as2-from" );
        String receiverId = header.getProperty( "as2-to" );
        info.setSenderId( senderId );
        info.setReceiverId( receiverId );
        info.setSenderEMail(header.getProperty( "from" ));
        //indicates if a sync or async mdn is requested
        info.setAsyncMDNURL( header.getProperty( "receipt-delivery-option" ) );
        info.setRequestsSyncMDN( header.getProperty( "receipt-delivery-option" ) == null
                || header.getProperty( "receipt-delivery-option" ).trim().length() == 0 );
        message.setRawData( data );
        //check for existing partners
        PartnerAccessDB partnerAccess = new PartnerAccessDB( "localhost" );
        Partner sender = partnerAccess.getPartner( senderId );
        Partner receiver = partnerAccess.getPartner( receiverId );
        partnerAccess.close();
        if( sender == null ){
            throw new AS2Exception( AS2Exception.UNKNOWN_TRADING_PARTNER_ERROR,
                    "Sender AS2 id " + senderId + " is unknown.", message );
        }
        if( receiver == null ){
            throw new AS2Exception( AS2Exception.UNKNOWN_TRADING_PARTNER_ERROR,
                    "Receiver AS2 id " + receiverId + " is unknown.", message );
        }
        if( !receiver.isLocalStation() ){
            throw new AS2Exception( AS2Exception.PROCESSING_ERROR,
                    "Receiver of message (" + receiver.getAS2Identification() + ") is not defined as the local station.",
                    message);
        }
        byte[] decryptedData = this.decryptData( message, data, contentType, sender, receiver );
        message.setDecryptedRawData( decryptedData );
        MimeBodyPart payloadPart = this.verifySignature( message, sender, receiver, contentType );
        if( !info.isMDN()){
            this.computeReceivedContentMIC( message, payloadPart, contentType );
        }
        //decompress the data if it has been sent compressed
        payloadPart = this.decompressData( payloadPart, message );
        this.writePayloadToMessage( payloadPart, message );
        return( message );
    }
    
    /**Writes a passed payload part to the passed message object
     */
    private void writePayloadToMessage( Part payloadPart, AS2Message message ) throws Exception{
        AS2MessageInfo info = message.getMessageInfo();
        //ups, attachments found. This is a AS2 1.2 feature
        if( !info.isMDN() && payloadPart.isMimeType( "multipart/*")){
            ByteArrayOutputStream mem = new ByteArrayOutputStream();
            payloadPart.writeTo( mem );
            mem.flush();
            mem.close();
            MimeMultipart multipart = new MimeMultipart(
                    new ByteArrayDataSource(mem.toByteArray(), payloadPart.getContentType()));
            int attachmentCount = multipart.getCount();
            this.logger.log( Level.INFO, this.rb.getResourceString( "found.attachments",
                    new Object[]{ info.getMessageId(), String.valueOf( attachmentCount ) }),
                    info);
            //add all attachments to the message
            for( int i = 0; i < multipart.getCount(); i++ ){
                this.writePayloadToMessage( multipart.getBodyPart(i), message );
            }
        }else{
            Object payload = payloadPart.getContent();
            if( payload instanceof InputStream ){
                ByteArrayOutputStream payloadOut = new ByteArrayOutputStream();
                this.copyStreams( (InputStream)payload, payloadOut);
                payloadOut.flush();
                payloadOut.close();
                AS2Payload as2Payload = new AS2Payload();
                as2Payload.setData( payloadOut.toByteArray() );
                as2Payload.setOriginalFilename( payloadPart.getFileName() );
                message.addPayload( as2Payload );
            }else if( payload instanceof String ){
                AS2Payload as2Payload = new AS2Payload();
                as2Payload.setData( ((String)payload).getBytes());
                as2Payload.setOriginalFilename( payloadPart.getFileName() );
                message.addPayload( as2Payload );
            }
        }
    }
    
    
    /**Computes the received content MIC and writes it to the message info object
     */
    private void computeReceivedContentMIC( AS2Message message, MimeBodyPart payloadPart, String contentType ) throws Exception{
        BCCryptoHelper helper = new BCCryptoHelper();
        ByteArrayInputStream memIn = new ByteArrayInputStream( message.getDecryptedRawData());
        MimeBodyPart signedPart = new MimeBodyPart( memIn );
        //if the message was not encrypted the content type should be taken from the header
        if( message.getMessageInfo().getEncryptionType() == AS2Message.ENCRYPTION_NONE )
            signedPart.setHeader( "Content-Type", contentType );
        memIn.close();
        //unsigned, unencoded: The message is not enwrapped in MIME structures
        if( message.getMessageInfo().getEncryptionType() == AS2Message.ENCRYPTION_NONE
                && message.getMessageInfo().getSignType() == AS2Message.SIGNATURE_NONE ){
            String digestOID = helper.convertAlgorithmNameToOID( helper.ALGORITHM_SHA1 );
            String mic = helper.calculateMIC( message.getRawData(), digestOID );
            message.getMessageInfo().setReceivedContentMIC( mic + ", sha1" );
        }else if( message.getMessageInfo().getSignType() != AS2Message.SIGNATURE_NONE ){
            String digestOID = helper.getDigestAlgOIDFromSignature( signedPart );
            String mic = helper.calculateMIC( payloadPart, digestOID, true );
            String digestAlgorithmName = helper.convertOIDToAlgorithmName( digestOID );
            message.getMessageInfo().setReceivedContentMIC( mic + ", " + digestAlgorithmName );
        } else{
            String digestOID = helper.convertAlgorithmNameToOID( helper.ALGORITHM_SHA1 );
            String mic = helper.calculateMIC( payloadPart, digestOID, false );
            message.getMessageInfo().setReceivedContentMIC( mic + ", sha1" );
        }
    }
    
    /**Returns a compressed part of this container if it exists, else null. If the container itself
     *is compressed it is returned.
     */
    public Part getCompressedEmbeddedPart( Part part ) throws MessagingException, IOException{
        if( part.getContentType().toLowerCase().indexOf( "compressed-data") >= 0 ){
            return( part );
        }
        if( part.isMimeType( "multipart/*" )) {
            Multipart multiPart = (Multipart)part.getContent();
            int count = multiPart.getCount();
            for (int i = 0; i < count; i++){
                BodyPart bodyPart = multiPart.getBodyPart(i);
                Part compressedEmbeddedPart = this.getCompressedEmbeddedPart( bodyPart );
                if( compressedEmbeddedPart != null )
                    return( compressedEmbeddedPart );
            }
        }
        return( null );
    }
    
    
    /**Verifies the signature of the passed message. If the transfer mode is unencrypted/unsigned, a new Bodypart will be constructed
     *@@return the payload part, this is important to compute the MIC later
     */
    private MimeBodyPart verifySignature( AS2Message message, Partner sender, Partner receiver, String contentType ) throws Exception{
        AS2MessageInfo info = message.getMessageInfo();
        BCCryptoHelper helper = new BCCryptoHelper();
        String signatureTransferEncoding = null;
        //use MimeBodyPart at this point because the message could be a non-mime plain text message, too if it is not
        //signed and encrypted
        ByteArrayInputStream memIn = new ByteArrayInputStream( message.getDecryptedRawData() );
        MimeBodyPart possibleSignedPart = new MimeBodyPart( memIn );
        memIn.close();
        //if the message was not encrypted the content type should be taken from the header
        if( info.getEncryptionType() == AS2Message.ENCRYPTION_NONE ){
            possibleSignedPart.setHeader( "Content-Type", contentType );
        }
        Part signedPart = helper.getSignedEmbeddedPart( possibleSignedPart );
        //part is NOT signed but is defined to be signed
        if( signedPart == null ){
            if( info.isMDN() ){
                this.logger.log( Level.INFO, this.rb.getResourceString( "mdn.notsigned", info.getMessageId() ), info);
                //MDN is not signed but should be signed'
                if( sender.isSignedMDN() ){
                    this.logger.log( Level.SEVERE, this.rb.getResourceString( "mdn.unsigned.error",
                            new Object[]{
                        info.getMessageId(),
                        sender.getName(),
                    }), info);
                }
            } else{
                this.logger.log( Level.INFO, this.rb.getResourceString( "msg.notsigned", info.getMessageId() ), info);
            }
            if( !info.isMDN() && sender.getSignType() != AS2Message.SIGNATURE_NONE ){
                throw new AS2Exception( AS2Exception.INSUFFICIENT_SECURITY_ERROR,
                        "Incoming messages from AS2 partner " + sender.getAS2Identification() + " are defined to be signed.",
                        message );
            }
            info.setSignType( AS2Message.SIGNATURE_NONE );
            //if the message has been unsigned it is required to set a new datasource
            MimeBodyPart unsignedPart = new MimeBodyPart();
            unsignedPart.setHeader( "content-type", contentType );
            unsignedPart.setDataHandler(new DataHandler(new ByteArrayDataSource(message.getDecryptedRawData(), contentType)));            
            return( unsignedPart );
        } else{
            //it is definitly a signed message
            try{
                MimeMultipart checkPart = (MimeMultipart)signedPart.getContent();
                //it is sure that it is a signed part: set the type to multipart if the
                //parser has problems parsing it. Don't know why sometimes a parsing fails for
                //MimeBodyPart. This check looks if the parser is able to find more than one subpart
                if( checkPart.getCount() == 1 ){
                    MimeMultipart multipart = new MimeMultipart(
                            new ByteArrayDataSource(message.getDecryptedRawData(), contentType));
                    MimeMessage possibleSignedMessage = new MimeMessage( Session.getInstance(System.getProperties(), null));
                    possibleSignedMessage.setContent( multipart, multipart.getContentType() );
                    possibleSignedMessage.saveChanges();
                    //overwrite the formerly found signed part
                    signedPart = helper.getSignedEmbeddedPart( possibleSignedMessage );
                }
                info.setSignType( this.getDigestFromSignature(signedPart) );
                //get the content encoding of the signature
                MimeMultipart signedMultiPart = (MimeMultipart)signedPart.getContent();
                //body part 1 is always the signature
                String encodingHeader[] = signedMultiPart.getBodyPart(1).getHeader( "Content-Transfer-Encoding");
                if( encodingHeader != null ){
                    signatureTransferEncoding = encodingHeader[0];
                }
            } catch( Exception e ){
                throw new AS2Exception( AS2Exception.AUTHENTIFICATION_ERROR,
                        info.getMessageId() + ": " + e.getMessage(), message );
            }
            if( info.isMDN() ){
                this.logger.log( Level.INFO, this.rb.getResourceString( "mdn.signed", info.getMessageId() ), info);
                //MDN is signed but shouldn't be signed'
                if( !sender.isSignedMDN() ){
                    this.logger.log( Level.SEVERE, this.rb.getResourceString( "mdn.signed.error",
                            new Object[]{
                        info.getMessageId(),
                        sender.getName(),
                    }), info);
                }
            } else{
                this.logger.log( Level.INFO, this.rb.getResourceString( "msg.signed", info.getMessageId() ), info);
            }
        }
        this.logger.log( Level.INFO, this.rb.getResourceString( "signature.using.alias",
                new Object[]{
            info.getMessageId(), sender.getCertAlias()
        }), info );
        X509Certificate certificate
                = this.certificateManager.getX509Certificate( sender.getCertAlias() );
        MimeBodyPart payloadPart = null;
        try{
            payloadPart = helper.verify( signedPart, signatureTransferEncoding, certificate );
        } catch( Exception e ){
            throw new AS2Exception( AS2Exception.AUTHENTIFICATION_ERROR,
                    "Error verifying the senders digital signature: " + e.getMessage() + ".",
                    message );
        }
        this.logger.log( Level.INFO, this.rb.getResourceString( "signature.ok", info.getMessageId() ), info );
        return( payloadPart );
    }
    
    /*Returns the digest of the signature, as constant of AS2Message
     */
    private int getDigestFromSignature( Part signedPart ) throws Exception{
        BCCryptoHelper helper = new BCCryptoHelper();
        String digestOID = helper.getDigestAlgOIDFromSignature( signedPart );
        String as2Digest = helper.convertOIDToAlgorithmName( digestOID );
        if( as2Digest.equalsIgnoreCase( helper.ALGORITHM_SHA1))
            return( AS2Message.SIGNATURE_SHA1 );
        if( as2Digest.equalsIgnoreCase( helper.ALGORITHM_MD5))
            return( AS2Message.SIGNATURE_MD5 );
        //should never happen because unknown algorithms are thrown already by the conversion method
        return( -1 );
    }
    
    /**Decrypts the passed data and returns it. Will return the original data
     *if it is not marked as encrypted
     */
    private byte[] decryptData( AS2Message message, byte[] data, String contentType, Partner sender, Partner receiver )throws AS2Exception{
        try{
            AS2MessageInfo info = message.getMessageInfo();
            //first check if the message is decrypted.
            if( contentType.toLowerCase().indexOf( "application/pkcs7-mime" ) < 0 ){
                if( info.isMDN() ){
                    this.logger.log( Level.INFO, this.rb.getResourceString( "mdn.notencrypted", info.getMessageId() ), info);
                } else{
                    this.logger.log( Level.INFO, this.rb.getResourceString( "msg.notencrypted", info.getMessageId() ), info);
                }
                info.setEncryptionType( AS2Message.ENCRYPTION_NONE );
                //encryption expected?
                if( !info.isMDN() && sender.getEncryptionType() != AS2Message.SIGNATURE_NONE ){
                    throw new AS2Exception( AS2Exception.INSUFFICIENT_SECURITY_ERROR,
                            "Incoming messages from AS2 partner " + sender.getAS2Identification() + " are defined to be encrypted.",
                            message );
                }
                return( data );
            }
            if( info.isMDN() ){
                this.logger.log( Level.INFO, this.rb.getResourceString( "mdn.encrypted", info.getMessageId() ), info);
            } else{
                this.logger.log( Level.INFO, this.rb.getResourceString( "msg.encrypted", info.getMessageId() ), info);
            }
            MimeBodyPart encryptedBody = new MimeBodyPart();
            encryptedBody.setHeader( "content-type", contentType );
            encryptedBody.setDataHandler(new DataHandler(new ByteArrayDataSource(data, contentType)));
            //receiver cert
            X509Certificate certificate = this.certificateManager.getX509Certificate( receiver.getCertAlias() );
            //receiver priv key
            PrivateKey privateKey = this.certificateManager.getPrivateKey( receiver.getCertAlias() );
            RecipientId recipientId = new RecipientId();
            recipientId.setSerialNumber(certificate.getSerialNumber());
            recipientId.setIssuer(certificate.getIssuerX500Principal().getEncoded());
            SMIMEEnveloped enveloped = new SMIMEEnveloped(encryptedBody);
            BCCryptoHelper helper = new BCCryptoHelper();
            String algorithm = helper.convertOIDToAlgorithmName(enveloped.getEncryptionAlgOID());
            if( algorithm.equals( BCCryptoHelper.ALGORITHM_3DES )){
                info.setEncryptionType( AS2Message.ENCRYPTION_3DES );
            } else if( algorithm.equals( BCCryptoHelper.ALGORITHM_RC2 )){
                info.setEncryptionType( AS2Message.ENCRYPTION_RC2_UNKNOWN );
            } else if( algorithm.equals( BCCryptoHelper.ALGORITHM_AES_128 )){
                info.setEncryptionType( AS2Message.ENCRYPTION_AES_128 );
            } else if( algorithm.equals( BCCryptoHelper.ALGORITHM_AES_192 )){
                info.setEncryptionType( AS2Message.ENCRYPTION_AES_192 );
            } else if( algorithm.equals( BCCryptoHelper.ALGORITHM_AES_256 )){
                info.setEncryptionType( AS2Message.ENCRYPTION_AES_256 );
            } else {
                info.setEncryptionType( AS2Message.ENCRYPTION_UNKNOWN_ALGORITHM );
            }
            RecipientInformationStore recipients = enveloped.getRecipientInfos();
            RecipientInformation recipient = recipients.get(recipientId);
            if(recipient == null)
                throw new AS2Exception( AS2Exception.AUTHENTIFICATION_ERROR,
                        "Recipient certificate does not match part signature.", message );
            byte[] decryptedData = recipient.getContent(privateKey, "BC");
            this.logger.log( Level.INFO, this.rb.getResourceString( "decryption.done.alias",
                    new Object[]{
                info.getMessageId(),
                receiver.getCertAlias(),
                this.rbMessage.getResourceString( "encryption." + info.getEncryptionType() )
            } ),
                    info);
            return( decryptedData );
        } catch( AS2Exception e ){
            throw e;
        } catch( Throwable e ){
            throw new AS2Exception( AS2Exception.DECRYPTION_ERROR, e.getMessage(), message );
        }
    }
    
    /**Looks if the data is compressed and decompresses it if necessary
     */
    private MimeBodyPart decompressData( MimeBodyPart part, AS2Message message ) throws Exception{
        MimeBodyPart compressedPart = (MimeBodyPart)this.getCompressedEmbeddedPart(part);
        if( compressedPart == null ){
            return( part );
        }
        InputStream inStream = compressedPart.getInputStream();
        SMIMECompressed compressed = new SMIMECompressed( compressedPart );
        byte[] decompressedData = compressed.getContent();
        this.logger.log( Level.INFO, this.rb.getResourceString( "data.compressed.expanded",
                new Object[]{
            message.getMessageInfo().getMessageId(), AS2Message.getDataSizeDisplay( part.getSize() ),
            AS2Message.getDataSizeDisplay( decompressedData.length )
        }), message.getMessageInfo() );
        ByteArrayInputStream memIn = new ByteArrayInputStream( decompressedData );
        MimeBodyPart uncompressedPayload = new MimeBodyPart( memIn );
        memIn.close();
        return( uncompressedPayload );
    }
    
    
    /**Copies all data from one stream to another*/
    private void copyStreams( InputStream in, OutputStream out ) throws IOException{
        BufferedInputStream inStream = new BufferedInputStream( in );
        BufferedOutputStream outStream = new BufferedOutputStream( out );
        //copy the contents to an output stream
        byte[] buffer = new byte[1024];
        int read = 1024;
        //a read of 0 must be allowed, sometimes it takes time to
        //extract data from the input
        while( read != -1 ){
            read = inStream.read( buffer );
            if( read > 0 )
                outStream.write( buffer, 0, read );
        }
        outStream.flush();
    }
    
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/message/AS2MessageParser.java 71    29.05.07 11:17 Heller $
d49 1
a49 1
 * @@version $Revision: 71 $
@

