head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.52.21;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2007.10.09.10.53.50;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b19/de/mendelson/comm/as2/message/AS2Message.java,v 1.1 2007/10/09 10:53:50 heller Exp $
package de.mendelson.comm.as2.message;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Logger;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Stores a rosettaNet message
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */

public class AS2Message implements Serializable{
    
    
    public static final int ENCRYPTION_UNKNOWN = 0;
    public static final int ENCRYPTION_NONE = 1;
    public static final int ENCRYPTION_3DES = 2;
    public static final int ENCRYPTION_RC2_40 = 3;
    public static final int ENCRYPTION_RC2_64 = 4;
    public static final int ENCRYPTION_RC2_128 = 5;
    public static final int ENCRYPTION_RC2_196 = 6;
    public static final int ENCRYPTION_RC2_UNKNOWN = 7;
    public static final int ENCRYPTION_AES_128 = 8;
    public static final int ENCRYPTION_AES_192 = 9;
    public static final int ENCRYPTION_AES_256 = 10;
    public static final int ENCRYPTION_UNKNOWN_ALGORITHM = 99;
    
    public static final int SIGNATURE_UNKNOWN = 0;
    public static final int SIGNATURE_NONE = 1;
    public static final int SIGNATURE_SHA1 = 2;
    public static final int SIGNATURE_MD5 = 3;
    
    public static final int COMPRESSION_UNKNOWN = 0;
    public static final int COMPRESSION_NONE = 1;
    public static final int COMPRESSION_ZLIB = 2;
    
    public static final int STATE_FINISHED = 1;
    public static final int STATE_PENDING = 2;
    public static final int STATE_STOPPED = 3;
        
    /**Stores all details about the message*/
    private AS2MessageInfo messageInfo = new AS2MessageInfo();
    
    /**Stores the raw message data*/
    private byte[] rawData = null;
    
    /**Stores the raw message data, decrypted. Contains the same data as the raw data
     *if the message has been sent unencrypted
     */
    private byte[] decryptedRawData = null;
    
    /**Payload of the as2 message, will be only one if the AS2 version is < AS2 1.2
     */
    private ArrayList<AS2Payload> payload = new ArrayList<AS2Payload>();
    
    private Properties header = new Properties();
    
    private String contentType;
            
    
    /**Constructor to create a new message, empty message object
     */
    public AS2Message( AS2MessageInfo info ) {
        this.messageInfo = info;
    }
    
    /**Constructor to create a new message, empty message object
     */
    public AS2Message() {
    }
    
    /**Returns the number of attachments of the AS2 message. This will mainly be 1 if the AS2 version is < AS2 1.2
     */
    public int getPayloadCount(){
       return( this.payload.size() ) ;
    }
    
    
    /**Displays the passed data size in a proper format
     */
    public static String getDataSizeDisplay( long size ){
        Logger logger = Logger.getLogger( "de.mendelson.as2" );
       StringBuilder builder = new StringBuilder();
       Formatter formatter = new Formatter(builder);
       if( size > 1.048E6 ){
           formatter.format( Locale.getDefault(), "%.2f", new Float( size/1.048E6));
           builder.append( " ").append( "MB" );
           return( builder.toString());
       } else if( size > 1024L ){
           formatter.format( Locale.getDefault(), "%.2f", new Float( size/1024));
           builder.append( " ").append( "KB" );
           return( builder.toString());
       }
       return( String.valueOf( size ) + " Byte" );
    }
    
    /**Writes the raw data of the message to a specified outFile*/
    public void writeDecryptedRawTo(File outFile) throws IOException {
        FileOutputStream outStream = new FileOutputStream( outFile );
        ByteArrayInputStream inStream = new ByteArrayInputStream( this.getDecryptedRawData() );
        this.copyStreams( inStream, outStream );
        inStream.close();
        outStream.flush();
        outStream.close();
    }
    
    
    public AS2MessageInfo getMessageInfo() {
        return messageInfo;
    }
    
    public void setMessageInfo(AS2MessageInfo messageInfo) {
        this.messageInfo = messageInfo;
    }
    
    /**Copies all data from one stream to another*/
    private void copyStreams( InputStream in, OutputStream out ) throws IOException{
        BufferedInputStream inStream = new BufferedInputStream( in );
        BufferedOutputStream outStream = new BufferedOutputStream( out );
        //copy the contents to an output stream
        byte[] buffer = new byte[2048];
        int read = 2048;
        //a read of 0 must be allowed, sometimes it takes time to
        //extract data from the input
        while( read != -1 ){
            read = inStream.read( buffer );
            if( read > 0 )
                outStream.write( buffer, 0, read );
        }
        outStream.flush();
    }
    
    public byte[] getRawData() {
        return rawData;
    }
    
    public void setRawData(byte[] rawData) {
        this.rawData = rawData;
    }
    
    public byte[] getDecryptedRawData() {
        return decryptedRawData;
    }
    
    public void setDecryptedRawData(byte[] decryptedRawData) {
        this.decryptedRawData = decryptedRawData;
    }
    
    public Properties getHeader() {
        return header;
    }
    
    public void setHeader(Properties header) {
        this.header = header;
    }
    
    /**Will return the payload of the passed index. The index should be 0 if the AS2 version is < AS2 1.2
     */
    public AS2Payload getPayload( int index ) {
        return( this.payload.get(index));
    }
    
    public void addPayload(AS2Payload data ) {
        this.payload.add( data );
    }
    
    /**Will return the payloads of the message
     */
    public AS2Payload[] getPayloads(){
        AS2Payload[] payloadArray = new AS2Payload[this.payload.size()];
        this.payload.toArray( payloadArray );        
        return( payloadArray );
    }    
    
    /**Writes the payload to the message to the passed file*/
    public void writeRawDecryptedTo( File file ) throws Exception{
        FileOutputStream outStream = new FileOutputStream( file );
        ByteArrayInputStream inStream = new ByteArrayInputStream(this.decryptedRawData);
        this.copyStreams( inStream, outStream );
        outStream.flush();
        outStream.close();
        inStream.close();
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/message/AS2Message.java 24    4.05.07 11:35 Heller $
d28 1
a28 1
 * @@version $Revision: 24 $
@

