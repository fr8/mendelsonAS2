head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.52.22;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2007.10.09.10.53.51;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b19/de/mendelson/comm/as2/message/store/MessageStoreHandler.java,v 1.1 2007/10/09 10:53:51 heller Exp $
package de.mendelson.comm.as2.message.store;
import de.mendelson.comm.as2.AS2ServerVersion;
import de.mendelson.comm.as2.message.AS2Message;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.AS2Payload;
import de.mendelson.comm.as2.message.MessageAccessDB;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.util.MecResourceBundle;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Stores messages in specified directories
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */

public class MessageStoreHandler{
    
    /**products preferences*/
    private PreferencesAS2 preferences = new PreferencesAS2();
    
    private Logger logger = Logger.getLogger( "de.mendelson.as2" );
    
    /**localize the output*/
    private MecResourceBundle rb = null;
    
    private final String CRLF = new String( new byte[]{ 0x0d, 0x0a });
    
    public MessageStoreHandler(){
        //Load resourcebundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleMessageStoreHandler.class.getName());
        }
        //load up  resourcebundle
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
    }
    
    /**Stores incoming data for the server without analyzing it, raw
     *Returns the raw filename and the header filename
     */
    public String[] storeRawIncomingData( byte[] data, Properties header, String remoteHost ) throws IOException{
        String[] filenames = new String[2];
        File inRawDir
                = new File( new File( this.preferences.get( PreferencesAS2.DIR_MSG )).getAbsolutePath()
                + File.separator + "_rawincoming" );
        //ensure the directory exists
        if( !inRawDir.exists() ){
            boolean created = inRawDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        inRawDir.getAbsolutePath() ));
        }
        DateFormat format = new SimpleDateFormat( "yyyyMMddHHmmssSSS" );
        StringBuffer rawFileName = new StringBuffer();
        rawFileName.append( inRawDir.getAbsolutePath() ).append( File.separator )
        .append( format.format( new Date() ) ).append( "_" );
        if( remoteHost != null ){
            rawFileName.append( remoteHost );
        } else{
            rawFileName.append( "unknownhost" );
        }
        rawFileName.append( ".as2" );
        File rawDataFile = new File( rawFileName.toString() );
        //write raw data
        FileOutputStream outStream = new FileOutputStream( rawDataFile );
        ByteArrayInputStream inStream = new ByteArrayInputStream( data );
        this.copyStreams( inStream, outStream );
        outStream.flush();
        outStream.close();
        inStream.close();
        //write header
        File headerFile = new File( rawDataFile.getAbsolutePath() + ".header" );
        FileOutputStream outStreamHeader = new FileOutputStream( headerFile );
        Enumeration enumeration = header.keys();
        while( enumeration.hasMoreElements() ){
            String key = (String)enumeration.nextElement();
            outStreamHeader.write( new String( key + " = " + header.getProperty( key ) + CRLF).getBytes() );
        }
        outStreamHeader.flush();
        outStreamHeader.close();
        filenames[0] = rawDataFile.getAbsolutePath();
        filenames[1] = headerFile.getAbsolutePath();
        return( filenames );
    }
    
    /**Copies all data from one stream to another*/
    private final void copyStreams( InputStream in, OutputStream out ) throws IOException{
        BufferedInputStream inStream = new BufferedInputStream( in );
        BufferedOutputStream outStream = new BufferedOutputStream( out );
        //copy the contents to an output stream
        byte[] buffer = new byte[2048];
        int read = 0;
        //a read of 0 must be allowed, sometimes it takes time to
        //extract data from the input
        while( read != -1 ){
            read = inStream.read( buffer );
            if( read > 0 ){
                outStream.write( buffer, 0, read );
            }
        }
        outStream.flush();
    }
    
    /**If a message state is OK the payload has to be moved to the right directory
     */
    public void movePayloadToInbox( String messageId, Partner localstation, Partner senderstation ) throws Exception{
        StringBuffer inBoxDirPath = new StringBuffer();
        inBoxDirPath.append( new File( this.preferences.get( PreferencesAS2.DIR_MSG )).getAbsolutePath() );
        inBoxDirPath.append( File.separator );
        inBoxDirPath.append( this.convertToValidFilename(localstation.getName()) );
        inBoxDirPath.append( File.separator );
        inBoxDirPath.append( "inbox" );
        if( this.preferences.getBoolean( PreferencesAS2.RECEIPT_PARTNER_SUBDIR )){
            inBoxDirPath.append( File.separator );
            inBoxDirPath.append( this.convertToValidFilename(senderstation.getName()) );
        }
        //store incoming message
        File inboxDir = new File( inBoxDirPath.toString() );
        //ensure the directory exists
        if( !inboxDir.exists() ){
            boolean created = inboxDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        inboxDir.getAbsolutePath() ));
        }
        //load message overview from database
        MessageAccessDB messageAccess = new MessageAccessDB( "localhost" );
        AS2Payload[] payload = messageAccess.getPayload( messageId );
        AS2MessageInfo messageInfo = messageAccess.getMessage( messageId );
        if( payload != null ){
            for( int i = 0; i < payload.length; i++ ){
                if( payload[i].getPayloadFilename() == null ){
                    continue;
                }
                File inFile = new File( payload[i].getPayloadFilename());
                StringBuffer outFilename = new StringBuffer();
                outFilename.append( inboxDir.getAbsolutePath() );
                outFilename.append( File.separator );
                outFilename.append( new File( payload[i].getPayloadFilename()).getName() );
                File outFile = new File( outFilename.toString() );
                inFile.renameTo( outFile );
                payload[i].setPayloadFilename( outFilename.toString() );
                this.logger.log( Level.FINE, this.rb.getResourceString( "comm.success",
                        new Object[]{
                    messageInfo.getMessageId(),
                    String.valueOf( i+1 ),
                    outFilename.toString()
                }), messageInfo );
            }
            messageAccess.insertPayloadFilenames( messageId, payload );
        }
        messageAccess.close();
    }
    
    
    /**Stores an incoming message payload to the right partners mailbox, the decrypted message to the raw directory
     *The filenames of the files where the data has been stored in is written to the message object
     */
    public void storeParsedIncomingMessage( AS2Message message, Properties header,
            Partner localstation, Partner sender ) throws Exception{
        StringBuffer inBoxDirPath = new StringBuffer();
        inBoxDirPath.append( new File( this.preferences.get( PreferencesAS2.DIR_MSG )).getAbsolutePath() );
        inBoxDirPath.append( File.separator );
        inBoxDirPath.append( this.convertToValidFilename( localstation.getName() ));
        inBoxDirPath.append( File.separator );
        inBoxDirPath.append( "inbox" );
        //store incoming message
        File inboxDir = new File( inBoxDirPath.toString() );
        //ensure the directory exists
        if( !inboxDir.exists() ){
            boolean created = inboxDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        inboxDir.getAbsolutePath() ));
        }
        //store the payload to the pending directory. It resists there as long as no positive MDN comes in
        File pendingDir = new File(inboxDir.getAbsolutePath() + File.separator + "pending" );
        if( !pendingDir.exists() ){
            boolean created = pendingDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        pendingDir.getAbsolutePath() ));
        }
        //do not store signals payload in pending dir
        if( !message.getMessageInfo().isMDN() ){
            for( int i = 0; i < message.getPayloadCount(); i++ ){
                AS2Payload payload = message.getPayload(i);
                StringBuffer pendingFilename = new StringBuffer();
                pendingFilename.append( pendingDir.getAbsolutePath() );
                pendingFilename.append( File.separator );
                pendingFilename.append( message.getMessageInfo().getMessageId() );
                if( message.getPayloadCount() > 1 ){
                    pendingFilename.append( "_" ).append( String.valueOf(i) );
                }
                File pendingFile = new File( pendingFilename.toString() );
                payload.writeTo(pendingFile);
                payload.setPayloadFilename( pendingFile.getAbsolutePath() );
            }
            MessageAccessDB messageAccess = new MessageAccessDB( "localhost" );
            messageAccess.insertPayloadFilenames( message.getMessageInfo().getMessageId(), message.getPayloads() );
            messageAccess.close();
        }
        File descryptedRawFile = new File( message.getMessageInfo().getRawFilename() + ".decrypted" );
        FileOutputStream outStream = new FileOutputStream( descryptedRawFile );
        ByteArrayInputStream inStream = new ByteArrayInputStream( message.getDecryptedRawData());
        this.copyStreams( inStream, outStream );
        outStream.flush();
        outStream.close();
        message.getMessageInfo().setRawFilenameDecrypted( descryptedRawFile.getAbsolutePath() );
    }
    
    /**Stores the message if an error occured during creation
     *or sending the message
     */
    public void storeSentErrorMessage( AS2Message message, Partner receiver )throws Exception{
        StringBuffer errorDirName = new StringBuffer();
        errorDirName.append( new File( this.preferences.get( PreferencesAS2.DIR_MSG )).getAbsolutePath() );
        errorDirName.append( File.separator );
        errorDirName.append( this.convertToValidFilename( receiver.getName() ));
        errorDirName.append( File.separator );
        errorDirName.append( "error" );
        //store sent message
        File errorDir = new File( errorDirName.toString() );
        //ensure the directory exists
        if( !errorDir.exists() ){
            boolean created = errorDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        errorDir.getAbsolutePath() ));
        }
        //write out the payload(s)
        for( int i = 0; i < message.getPayloadCount(); i++ ){
            File payloadFile = File.createTempFile( "AS2Message", ".as2", errorDir );
            message.getPayload(i).writeTo( payloadFile );
            message.getPayload(i).setPayloadFilename( payloadFile.getAbsolutePath() );
            this.logger.log( Level.SEVERE, this.rb.getResourceString( "message.error.stored",
                    new Object[]{
                message.getMessageInfo().getMessageId(),
                payloadFile.getAbsolutePath()
            }), message.getMessageInfo() );
        }
        //write raw file to error/raw
        File errorRawDir
                = new File( errorDir.getAbsolutePath() + File.separator + "raw" );
        //ensure the directory exists
        if( !errorRawDir.exists() ){
            boolean created = errorRawDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        errorRawDir.getAbsolutePath() ));
        }
        File errorRawFile = File.createTempFile( "error", ".raw", errorRawDir );
        message.writeRawDecryptedTo( errorRawFile );
        this.logger.log( Level.SEVERE, this.rb.getResourceString( "message.error.raw.stored",
                new Object[]{
            message.getMessageInfo().getMessageId(), errorRawFile.getAbsolutePath()
        }), message.getMessageInfo());
        message.getMessageInfo().setRawFilenameDecrypted( errorRawFile.getAbsolutePath() );
        //update the filenames in the db
        MessageAccessDB messageAccess = new MessageAccessDB( "localhost" );
        messageAccess.updateFilenames( message.getMessageInfo() );
        messageAccess.insertPayloadFilenames( message.getMessageInfo().getMessageId(), message.getPayloads() );
        messageAccess.close();
    }
    
    /**This is for integration purpose. It writes a single file that contains the final state of the message
     *to a file, also in the sent directory. This state file is only written for outbound messages
     */
    public void storeSentMessageState( AS2MessageInfo messageInfo, AS2Payload[] payload, Partner receiver, int state ) throws Exception{
        String receiverName = "unidentified";
        if( receiver != null )
            receiverName = this.convertToValidFilename( receiver.getName());
        //store sent message
        File sentDir
                = new File( new File( this.preferences.get( PreferencesAS2.DIR_MSG )).getAbsolutePath()
                + File.separator + receiverName + File.separator + "sent" );
        //ensure the directory exists
        if( !sentDir.exists() ){
            boolean created = sentDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        sentDir.getAbsolutePath() ));
        }
        //write a state file for every attachment of the AS2 message
        for( int i = 0; i < payload.length; i++ ){
            String originalFilename = payload[i].getOriginalFilename();
            if( originalFilename == null ){
                originalFilename = "unknown";
            }
            String requestType = "";
            if( messageInfo.isMDN())
                requestType = "_MDN";
            StringBuffer rawFilename = new StringBuffer();
            rawFilename.append( sentDir.getAbsolutePath() );
            rawFilename.append( File.separator );
            rawFilename.append( originalFilename );
            rawFilename.append( "_" );
            rawFilename.append(messageInfo.getMessageId());
            rawFilename.append( requestType );
            rawFilename.append( ".sent.state" );
            File stateFile = new File( rawFilename.toString() );
            FileOutputStream outStream = new FileOutputStream( stateFile );
            outStream.write( "product=".getBytes() );
            outStream.write( AS2ServerVersion.getProductName().getBytes() );
            outStream.write( " ".getBytes() );
            outStream.write( AS2ServerVersion.getVersion().getBytes() );
            outStream.write( " ".getBytes() );
            outStream.write( AS2ServerVersion.getBuild().getBytes() );
            outStream.write( "\n".getBytes());
            outStream.write( "originalfile=".getBytes() );
            outStream.write( payload[i].getOriginalFilename().getBytes() );
            outStream.write( "\n".getBytes());
            outStream.write( "messageid=".getBytes() );
            outStream.write( messageInfo.getMessageId().getBytes() );
            outStream.write( "\n".getBytes());
            outStream.write( "state=".getBytes() );
            if( state == AS2Message.STATE_FINISHED ){
                outStream.write( "OK".getBytes() );
            }else{
                outStream.write( "ERROR".getBytes() );
            }
            outStream.flush();
            outStream.close();
            this.logger.log( Level.INFO, this.rb.getResourceString( "message.state.stored",
                    new Object[]{
                messageInfo.getMessageId(), stateFile.getAbsolutePath()
            }), messageInfo);
        }
        //update the filenames in the db
        MessageAccessDB messageAccess = new MessageAccessDB( "localhost" );
        messageAccess.insertPayloadFilenames( messageInfo.getMessageId(), payload );
        messageAccess.close();
    }
    
    /**Stores an outgoing message in a sent directory
     */
    public void storeSentMessage( AS2Message message, Partner receiver, Properties header ) throws Exception{
        String receiverName = "unidentified";
        if( receiver != null )
            receiverName = this.convertToValidFilename( receiver.getName());
        //store sent message
        File sentDir
                = new File( new File( this.preferences.get( PreferencesAS2.DIR_MSG )).getAbsolutePath()
                + File.separator + receiverName + File.separator + "sent" );
        //ensure the directory exists
        if( !sentDir.exists() ){
            boolean created = sentDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        sentDir.getAbsolutePath() ));
        }
        AS2MessageInfo messageInfo = message.getMessageInfo();
        String requestType = "";
        if( messageInfo.isMDN()){
            requestType = "_MDN";
        }
        StringBuffer rawFilename = new StringBuffer();
        rawFilename.append( sentDir.getAbsolutePath() );
        rawFilename.append( File.separator );
        rawFilename.append(message.getMessageInfo().getMessageId());
        rawFilename.append( requestType );
        rawFilename.append( ".as2" );
        File headerFile = new File( rawFilename.toString() + ".header" );
        FileOutputStream outStream = new FileOutputStream( headerFile );
        Enumeration enumeration = header.keys();
        while( enumeration.hasMoreElements() ){
            String key = (String)enumeration.nextElement();
            outStream.write( new String( key + " = " + header.getProperty( key ) + CRLF).getBytes() );
        }
        outStream.close();
        outStream.flush();
        message.getMessageInfo().setHeaderFilename( headerFile.getAbsolutePath() );
        File rawFile = new File( rawFilename.toString() );
        outStream = new FileOutputStream( rawFile );
        ByteArrayInputStream inStream = new ByteArrayInputStream( message.getDecryptedRawData());
        this.copyStreams( inStream, outStream );
        inStream.close();
        outStream.flush();
        outStream.close();
        File rawFileDecrypted = new File( rawFilename.toString() + ".decrypted" );
        outStream = new FileOutputStream( rawFileDecrypted );
        inStream = new ByteArrayInputStream( message.getDecryptedRawData());
        this.copyStreams( inStream, outStream );
        inStream.close();
        outStream.flush();
        outStream.close();
        for( int i = 0; i < message.getPayloadCount(); i++ ){
            StringBuffer payloadFilename = new StringBuffer();
            payloadFilename.append( sentDir.getAbsolutePath() ).append( File.separator );
            String originalFilename = message.getPayload(i).getOriginalFilename();
            if( originalFilename == null ){
                originalFilename = "unknown";
            }
            payloadFilename.append( message.getMessageInfo().getMessageId() );
            payloadFilename.append( ".payload" );
            File payloadFile = new File( payloadFilename.toString() );
            message.getPayload(i).writeTo( payloadFile );
            message.getPayload(i).setPayloadFilename( payloadFile.getAbsolutePath() );
        }
        //set all filenames to the message object
        message.getMessageInfo().setRawFilename( rawFile.getAbsolutePath() );
        message.getMessageInfo().setRawFilenameDecrypted( rawFileDecrypted.getAbsolutePath() );
        message.getMessageInfo().setHeaderFilename( headerFile.getAbsolutePath() );
        //update the filenames in the db
        MessageAccessDB messageAccess = new MessageAccessDB( "localhost" );
        messageAccess.updateFilenames( message.getMessageInfo() );
        messageAccess.insertPayloadFilenames( message.getMessageInfo().getMessageId(), message.getPayloads() );
        messageAccess.close();
    }
    
    /**Converts a suggested filename to a valid filename. This may be necessary if as2 ids contain chars that are not allowed in
     *the current file system
     */
    public static String convertToValidFilename( String filename){
        File file = new File( filename );
        //no trouble, file already exists on the file system
        if( file.exists()){
            return( filename);
        }
        //seems not to be a valid filename, replace some chars
        StringBuffer buffer = new StringBuffer();
        for( int i = 0; i < filename.length(); i++ ){
            char c = filename.charAt(i);
            int type = Character.getType( c );
            if( type == Character.DECIMAL_DIGIT_NUMBER
                    || type == Character.LETTER_NUMBER
                    || type == Character.LOWERCASE_LETTER
                    || type == Character.OTHER_LETTER
                    || type == Character.OTHER_NUMBER
                    || type == Character.TITLECASE_LETTER
                    || type == Character.UPPERCASE_LETTER ){
                buffer.append( c );
            } else{
                buffer.append( '_' );
            }
        }
        return( buffer.toString() );
    }
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/message/store/MessageStoreHandler.java 35    4.05.07 9:59 Heller $
d39 1
a39 1
 * @@version $Revision: 35 $
@

