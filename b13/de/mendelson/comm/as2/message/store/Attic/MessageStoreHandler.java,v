head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.56.59;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2007.01.24.15.57.46;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b13/de/mendelson/comm/as2/message/store/MessageStoreHandler.java,v 1.1 2007/01/24 15:57:46 heller Exp $
package de.mendelson.comm.as2.message.store;
import de.mendelson.comm.as2.message.AS2Message;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.MessageAccessDB;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.util.MecResourceBundle;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Stores messages in specified directories
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */

public class MessageStoreHandler{
    
    /**products preferences*/
    private PreferencesAS2 preferences = new PreferencesAS2();
    
    private Logger logger = Logger.getLogger( "de.mendelson.as2" );
    
    /**localize the output*/
    private MecResourceBundle rb = null;
    
    public MessageStoreHandler(){
        //Load resourcebundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleMessageStoreHandler.class.getName());
        }
        //load up  resourcebundle
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
    }
    
    /**Stores incoming data for the server without analyzing it, raw
     */
    public File storeRawIncomingData( byte[] data, Properties header ) throws IOException{
        File inRawDir
                = new File( new File( this.preferences.get( PreferencesAS2.DIR_MSG )).getAbsolutePath()
                + File.separator + "_rawincoming" );
        //ensure the directory exists
        if( !inRawDir.exists() ){
            boolean created = inRawDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        inRawDir.getAbsolutePath() ));
        }
        DateFormat format = new SimpleDateFormat( "yyyyMMddHHmmssSSS" );
        File uniqueFile = File.createTempFile( format.format( new Date() ) + "_", ".as2", inRawDir );
        FileOutputStream outStream = new FileOutputStream( uniqueFile );
        outStream.write( "HTTP Header:\n".getBytes());
        outStream.write( "------------\n".getBytes());
        Enumeration enumeration = header.keys();
        while( enumeration.hasMoreElements() ){
            String key = (String)enumeration.nextElement();
            outStream.write( new String( key + " = " + header.getProperty( key ) + "\n").getBytes() );
        }
        outStream.write( "\n\nHTTP Content:\n".getBytes());
        outStream.write( "-----------------\n".getBytes());
        ByteArrayInputStream inStream = new ByteArrayInputStream( data );
        this.copyStreams( inStream, outStream );
        outStream.flush();
        outStream.close();
        inStream.close();
        return( uniqueFile );
    }
    
    /**Copies all data from one stream to another*/
    private void copyStreams( InputStream in, OutputStream out ) throws IOException{
        BufferedInputStream inStream = new BufferedInputStream( in );
        BufferedOutputStream outStream = new BufferedOutputStream( out );
        //copy the contents to an output stream
        byte[] buffer = new byte[1024];
        int read = 1024;
        //a read of 0 must be allowed, sometimes it takes time to
        //extract data from the input
        while( read != -1 ){
            read = inStream.read( buffer );
            if( read > 0 )
                outStream.write( buffer, 0, read );
        }
        outStream.flush();
    }
    
    /**If a message state is OK the payload has to be moved to the right directory
     */
    public void movePayloadToInbox( String messageId, Partner localstation ) throws Exception{
        StringBuffer inBoxDirPath = new StringBuffer(
                new File( this.preferences.get( PreferencesAS2.DIR_MSG )).getAbsolutePath()
                + File.separator + localstation.getName() + File.separator + "inbox" );
        //store incoming message
        File inboxDir = new File( inBoxDirPath.toString() );
        //ensure the directory exists
        if( !inboxDir.exists() ){
            boolean created = inboxDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        inboxDir.getAbsolutePath() ));
        }
        //load message overview from database
        MessageAccessDB messageAccess = new MessageAccessDB( "localhost" );
        AS2MessageInfo messageInfo = messageAccess.getMessage(messageId);        
        FileInputStream inStream 
                = new FileInputStream( messageInfo.getPayloadFilename());  
        String outFilename = inboxDir.getAbsolutePath()
                + File.separator + new File( messageInfo.getPayloadFilename()).getName();
        FileOutputStream outStream = new FileOutputStream( outFilename );
        this.copyStreams( inStream, outStream );
        inStream.close();
        outStream.flush();
        outStream.close();
        new File( messageInfo.getPayloadFilename() ).delete();
        messageInfo.setPayloadFilename( outFilename );
        messageAccess.updateFilenames( messageInfo );
        messageAccess.close();
        this.logger.log( Level.FINE, this.rb.getResourceString( "comm.success",
                new Object[]{
            messageInfo.getMessageId(), outFilename
        }), messageInfo );
    }
    
    
    /**Stores an incoming message to the right partners mailbox
     *The filenames of the files where the data has been stored in is written to the message object
     */
    public void storeIncomingMessageToMailBox( AS2Message message, Properties header,
            Partner localstation, Partner sender ) throws Exception{
        StringBuffer inBoxDirPath = new StringBuffer(
                new File( this.preferences.get( PreferencesAS2.DIR_MSG )).getAbsolutePath()
                + File.separator + localstation.getName() + File.separator + "inbox" );
        //store incoming message
        File inboxDir = new File( inBoxDirPath.toString() );
        //ensure the directory exists
        if( !inboxDir.exists() ){
            boolean created = inboxDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        inboxDir.getAbsolutePath() ));
        }
        //store the payload to the pending directory. It resists there as long as no positive MDN comes in
        File pendingDir = new File(inboxDir.getAbsolutePath() + File.separator + "pending" );
        if( !pendingDir.exists() ){
            boolean created = pendingDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        pendingDir.getAbsolutePath() ));
        }
        File uniqueFile = new File( pendingDir.getAbsolutePath()
        + File.separator + message.getMessageInfo().getMessageId() );
        //do not store signals payload
        if( !message.getMessageInfo().isMDN() ){
            message.writePayloadTo(uniqueFile);
            message.getMessageInfo().setPayloadFilename( uniqueFile.getAbsolutePath() );
        }
        //store the raw request, messages and signals
        File rawDir = new File(inboxDir.getAbsolutePath() + File.separator + "raw" );
        if( !rawDir.exists() ){
            boolean created = rawDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        rawDir.getAbsolutePath() ));
        }
        String requestType = "";
        AS2MessageInfo messageInfo = message.getMessageInfo();
        if( messageInfo.isMDN()){
            requestType = "_MDN";
        }
        File rawFile = new File( rawDir.getAbsolutePath() + File.separator
                + uniqueFile.getName()
                + requestType + ".as2" );
        FileOutputStream outStream = new FileOutputStream( rawFile );
        outStream.write( "HTTP Header:\n".getBytes());
        outStream.write( "------------\n".getBytes());
        Enumeration enumeration = header.keys();
        while( enumeration.hasMoreElements() ){
            String key = (String)enumeration.nextElement();
            outStream.write( new String( key + " = " + header.getProperty( key ) + "\n").getBytes() );
        }
        outStream.write( "\n\nHTTP Content:\n".getBytes());
        outStream.write( "-------------\n".getBytes());
        ByteArrayInputStream inStream = new ByteArrayInputStream( message.getDecryptedRawData());
        this.copyStreams( inStream, outStream );
        outStream.flush();
        outStream.close();
        if( message.getMessageInfo().getRawFilename() != null )
            new File( message.getMessageInfo().getRawFilename() ).delete();
        message.getMessageInfo().setRawFilename( rawFile.getAbsolutePath() );
    }
    
    /**Stores the message if an error occured during creation
     *or sending the message
     */
    public void storeSentErrorMessage( AS2Message message, Partner receiver )throws Exception{
        //store sent message
        File errorDir
                = new File( new File( this.preferences.get( PreferencesAS2.DIR_MSG )).getAbsolutePath()
                + File.separator + receiver.getName() + File.separator + "error" );
        //ensure the directory exists
        if( !errorDir.exists() ){
            boolean created = errorDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        errorDir.getAbsolutePath() ));
        }
        File payloadFile = File.createTempFile( "AS2Message", ".as2", errorDir );
        message.writePayloadTo( payloadFile );
        //write raw file to error/raw
        File errorRawDir
                = new File( errorDir.getAbsolutePath() + File.separator + "raw" );
        //ensure the directory exists
        if( !errorRawDir.exists() ){
            boolean created = errorRawDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        errorRawDir.getAbsolutePath() ));
        }
        String originalFilename = message.getMessageInfo().getOriginalFilename();
        if( originalFilename == null ){
            originalFilename = "unknown";
        }
        File errorRawFile 
                = new File( errorRawDir.getAbsolutePath() 
                + File.separator 
                + originalFilename + "_"
                + payloadFile.getName() + ".raw");
        message.writeRawDecryptedTo( errorRawFile );
        this.logger.log( Level.SEVERE, this.rb.getResourceString( "message.error.stored",
                new Object[]{
            message.getMessageInfo().getMessageId(),
                    payloadFile.getAbsolutePath()
        }), message.getMessageInfo() );
        this.logger.log( Level.SEVERE, this.rb.getResourceString( "message.error.raw.stored",
                new Object[]{
            message.getMessageInfo().getMessageId(), errorRawFile.getAbsolutePath()
        }), message.getMessageInfo());
        message.getMessageInfo().setRawFilename( errorRawFile.getAbsolutePath() );
    }
    
    
    /**Stores an outgoing message in a sent directory
     */
    public void storeSentMessage( AS2Message message, Partner receiver, Properties header ) throws Exception{        
        String receiverName = "unidentified";
        if( receiver != null )
            receiverName = receiver.getName();
        String originalFilename = message.getMessageInfo().getOriginalFilename();
        if( originalFilename == null ){
            originalFilename = "unknown";
        }
        //store sent message
        File sentDir
                = new File( new File( this.preferences.get( PreferencesAS2.DIR_MSG )).getAbsolutePath()
                + File.separator + receiverName + File.separator + "sent" );
        //ensure the directory exists
        if( !sentDir.exists() ){
            boolean created = sentDir.mkdirs();
            if( !created )
                this.logger.warning( this.rb.getResourceString( "dir.createerror",
                        sentDir.getAbsolutePath() ));
        }
        AS2MessageInfo messageInfo = message.getMessageInfo();
        String requestType = "";
        if( messageInfo.isMDN())
            requestType = "_MDN";
        File uniqueFile = new File( sentDir.getAbsolutePath()
        + File.separator 
        + originalFilename + "_"
        + message.getMessageInfo().getMessageId()
        + requestType + ".as2" );
        FileOutputStream outStream = new FileOutputStream( uniqueFile );
        outStream.write( "HTTP Header:\n".getBytes());
        outStream.write( "------------\n".getBytes());
        Enumeration enumeration = header.keys();
        while( enumeration.hasMoreElements() ){
            String key = (String)enumeration.nextElement();
            outStream.write( new String( key + " = " + header.getProperty( key ) + "\n").getBytes() );
        }
        outStream.write( "\n\nHTTP Content:\n".getBytes());
        outStream.write( "-------------\n".getBytes());
        ByteArrayInputStream inStream = null;
        if( messageInfo.getEncryptionType() != AS2Message.ENCRYPTION_NONE ){
            inStream = new ByteArrayInputStream( message.getDecryptedRawData());
        } else{
            inStream = new ByteArrayInputStream( message.getRawData() );
        }
        this.copyStreams( inStream, outStream );
        inStream.close();
        outStream.flush();
        outStream.close();
        message.getMessageInfo().setRawFilename( uniqueFile.getAbsolutePath() );
    }
    
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /mec_as2/de/mendelson/comm/as2/message/store/MessageStoreHandler.java 21    30.11.06 13:31 Heller $
d38 1
a38 1
 * @@version $Revision: 21 $
@

