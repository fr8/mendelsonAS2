head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.56.58;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2007.01.24.15.57.46;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b13/de/mendelson/comm/as2/message/loggui/DialogMessageDetails.java,v 1.1 2007/01/24 15:57:46 heller Exp $
package de.mendelson.comm.as2.message.loggui;
import de.mendelson.comm.as2.log.LogAccessDB;
import de.mendelson.comm.as2.log.LogEntry;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.MessageAccessDB;
import de.mendelson.util.log.IRCColors;
import java.text.SimpleDateFormat;
import javax.swing.*;
import de.mendelson.util.MecResourceBundle;
import java.awt.Color;
import java.io.File;
import java.text.DateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Dialog to show the about info
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class DialogMessageDetails extends JDialog implements ListSelectionListener{
    
    private Logger logger = Logger.getLogger( "de.mendelson.as2");
    
    /**Localize the GUI*/
    private MecResourceBundle rb = null;
    
    /**Stores information about the message
     */
    private AS2MessageInfo overviewInfo = null;
    
    private JPanelFileDisplay jPanelFileDisplayRaw = new JPanelFileDisplay();
    
    private LogAccessDB logAccess;
    
    /** Creates new form AboutDialog */
    public DialogMessageDetails( JFrame parent, AS2MessageInfo overviewInfo ) {
        super(parent, true);
        //load resource bundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleMessageDetails.class.getName());
        } catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
        this.overviewInfo = overviewInfo;
        this.setTitle( this.rb.getResourceString( "title" ));
        this.initComponents();
        this.jLabelPIPInfo.setText( overviewInfo.getMessageId() );
        this.getRootPane().setDefaultButton( this.jButtonOk );
        this.jTableMessageDetails.getTableHeader().setReorderingAllowed( false );
        //first icon
        TableColumn column = this.jTableMessageDetails.getColumnModel().getColumn(0);
        column.setMaxWidth( 20 );
        column.setResizable( false );
        column = this.jTableMessageDetails.getColumnModel().getColumn(2);
        column.setMaxWidth( 20 );
        column.setResizable( false );
        this.displayData( overviewInfo );
        this.jTabbedPane.addTab( this.rb.getResourceString( "raw.message" ), jPanelFileDisplayRaw );
        this.jTableMessageDetails.getSelectionModel().addListSelectionListener(this);
        try{
            this.logAccess = new LogAccessDB( "localhost" );
        } catch( Exception e ){
            this.logger.severe( e.getMessage() );
        }
        this.displayPIPLog();
        this.jTableMessageDetails.getSelectionModel().setSelectionInterval(0,0);
    }
    
    /**Displays the PIP log*/
    private void displayPIPLog(){
        StyledDocument document = (StyledDocument)this.jTextPaneLog.getDocument();
        StyleContext context  = StyleContext.getDefaultStyleContext();
        Style currentStyle = context.getStyle( StyleContext.DEFAULT_STYLE );
        Color defaultColor = (Color)currentStyle.getAttribute( StyleConstants.Foreground );
        LogEntry[] entries = this.logAccess.getLog( overviewInfo.getMessageId() );
        StringBuffer buffer = new StringBuffer();
        DateFormat format = SimpleDateFormat.getDateTimeInstance( DateFormat.MEDIUM, DateFormat.MEDIUM );
        for( int i = 0; i < entries.length; i++ ){
            currentStyle.removeAttribute(StyleConstants.Foreground);
            currentStyle.addAttribute(StyleConstants.Foreground, defaultColor );
            buffer.append( "[" + format.format( entries[i].getMillis() ) + "] ");
            try{
                document.insertString( document.getLength(), buffer.toString(), currentStyle );
            } catch( Throwable ignore ){
                //nop
            }
            buffer.setLength( 0 );
            if( entries[i].getLevel().equals( Level.WARNING )){
                currentStyle.addAttribute(StyleConstants.Foreground, IRCColors.indexedColors[2].brighter() );
            } else if( entries[i].getLevel().equals( Level.SEVERE )){
                currentStyle.addAttribute(StyleConstants.Foreground, IRCColors.indexedColors[4] );
            } else if( entries[i].getLevel().equals( Level.FINE )){
                currentStyle.addAttribute(StyleConstants.Foreground, IRCColors.indexedColors[3] );
            } else{
                currentStyle.addAttribute(StyleConstants.Foreground, defaultColor );
            }
            buffer.append( entries[i].getMessage() + "\n");
            try{
                document.insertString( document.getLength(), buffer.toString(), currentStyle );
            } catch( Throwable ignore ){
                //nop
            }
            buffer.setLength( 0 );
            currentStyle.removeAttribute(StyleConstants.Foreground);
            currentStyle.addAttribute(StyleConstants.Foreground, defaultColor );
        }
    }
    
    /**Displays all messages that contain to the passed overview object*/
    private void displayData( AS2MessageInfo overviewRow ){
        try{
            MessageAccessDB messageAccess = new MessageAccessDB( "localhost" );
            AS2MessageInfo[] details = messageAccess.getMessageDetails( overviewRow.getMessageId() );
            messageAccess.close();
            ((TableModelMessageDetails)this.jTableMessageDetails.getModel()).passNewData(
                    new ArrayList( Arrays.asList(details )));
        } catch( Exception e ){
            JFrame parent = (JFrame)SwingUtilities.getAncestorOfClass( JFrame.class, this );
            JOptionPane.showMessageDialog( parent, e.getMessage() );
        }
    }
    
    /**ListSelectionListener*/
    public void valueChanged(ListSelectionEvent listSelectionEvent) {
        int selectedRow = this.jTableMessageDetails.getSelectedRow();
        if( selectedRow >= 0 ){
            AS2MessageInfo info = ((TableModelMessageDetails)this.jTableMessageDetails.getModel()).getRow(selectedRow);
            File rawFile = null;
            if(info.getRawFilename() != null )
                rawFile = new File(info.getRawFilename() );
            this.jPanelFileDisplayRaw.displayFile( rawFile );
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelMain = new javax.swing.JPanel();
        jPanelHeader = new javax.swing.JPanel();
        jLabelPIPInfo = new javax.swing.JLabel();
        jPanelInfo = new javax.swing.JPanel();
        jSplitPane = new javax.swing.JSplitPane();
        jScrollPaneList = new javax.swing.JScrollPane();
        jTableMessageDetails = new javax.swing.JTable();
        jTabbedPane = new javax.swing.JTabbedPane();
        jPanelPIPLog = new javax.swing.JPanel();
        jScrollPaneLog = new javax.swing.JScrollPane();
        jTextPaneLog = new javax.swing.JTextPane();
        jPanelButton = new javax.swing.JPanel();
        jButtonOk = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanelMain.setLayout(new java.awt.GridBagLayout());

        jPanelHeader.setLayout(new java.awt.GridBagLayout());

        jLabelPIPInfo.setText("<Info>");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanelHeader.add(jLabelPIPInfo, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelMain.add(jPanelHeader, gridBagConstraints);

        jPanelInfo.setLayout(new java.awt.GridBagLayout());

        jSplitPane.setDividerLocation(200);
        jSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jTableMessageDetails.setModel(new TableModelMessageDetails());
        jTableMessageDetails.setShowHorizontalLines(false);
        jTableMessageDetails.setShowVerticalLines(false);
        jScrollPaneList.setViewportView(jTableMessageDetails);

        jSplitPane.setLeftComponent(jScrollPaneList);

        jPanelPIPLog.setLayout(new java.awt.GridBagLayout());

        jScrollPaneLog.setViewportView(jTextPaneLog);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelPIPLog.add(jScrollPaneLog, gridBagConstraints);

        jTabbedPane.addTab(this.rb.getResourceString( "tab.log"), jPanelPIPLog);

        jSplitPane.setRightComponent(jTabbedPane);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelInfo.add(jSplitPane, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelMain.add(jPanelInfo, gridBagConstraints);

        jButtonOk.setFont(new java.awt.Font("Dialog", 0, 12));
        jButtonOk.setText(this.rb.getResourceString( "button.ok" ));
        jButtonOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOkActionPerformed(evt);
            }
        });

        jPanelButton.add(jButtonOk);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        jPanelMain.add(jPanelButton, gridBagConstraints);

        getContentPane().add(jPanelMain, java.awt.BorderLayout.CENTER);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-820)/2, (screenSize.height-624)/2, 820, 624);
    }// </editor-fold>//GEN-END:initComponents
    
    private void jButtonOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOkActionPerformed
        this.setVisible( false );
        this.dispose();
    }//GEN-LAST:event_jButtonOkActionPerformed
    
    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        this.setVisible(false);
        this.dispose();
    }//GEN-LAST:event_closeDialog
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonOk;
    private javax.swing.JLabel jLabelPIPInfo;
    private javax.swing.JPanel jPanelButton;
    private javax.swing.JPanel jPanelHeader;
    private javax.swing.JPanel jPanelInfo;
    private javax.swing.JPanel jPanelMain;
    private javax.swing.JPanel jPanelPIPLog;
    private javax.swing.JScrollPane jScrollPaneList;
    private javax.swing.JScrollPane jScrollPaneLog;
    private javax.swing.JSplitPane jSplitPane;
    private javax.swing.JTabbedPane jTabbedPane;
    private javax.swing.JTable jTableMessageDetails;
    private javax.swing.JTextPane jTextPaneLog;
    // End of variables declaration//GEN-END:variables
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /mec_as2/de/mendelson/comm/as2/message/loggui/DialogMessageDetails.java 5     16.06.06 11:34 Heller $
d35 1
a35 1
 * @@version $Revision: 5 $
@

