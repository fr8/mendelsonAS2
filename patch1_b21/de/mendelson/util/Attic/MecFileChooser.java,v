head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2009.03.16.12.28.10;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2008.02.07.10.25.21;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/patch1_b21/de/mendelson/util/MecFileChooser.java,v 1.1 2008/02/07 10:25:21 heller Exp $
package de.mendelson.util;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;
import javax.swing.filechooser.*;
import java.io.*;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.swing.text.JTextComponent;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */


/**
 * This shows a file chooser, it is possible to select a native or the
 * swing file chooser.
 * @@author  S.Heller
 * @@version $Revision: 1.1 $
 */

public class MecFileChooser extends JFileChooser{
    
    //supports only one type of choosers at the moment
    public static int TYPE_SWING = 1;
    
    /**ResourceBundle to store localized informations*/
    private static MecResourceBundle rb = null;
    
    /**ParentFrame of this component*/
    private Frame parent = null;
    
    /**indicates if the user canceled the dialog*/
    private boolean canceled = false;
    
    /** Creates new MecFileChooser
     * Creates a new FileChooser with the given default directory
     * @@param defaultDirectory Directory to start by default, may be a file
     * @@param dialogTitle Title to show at the chooser
     * @@param parent parent component
     * @@param TYPE type of the dialog to choose as defined in the class
     *@@deprecated use the same method without the type settings
     */
    public MecFileChooser( Frame parent, String defaultDirectory,
            String dialogTitle, final int TYPE ){
        this( parent, dialogTitle );
        this.setPreselectedFile( new File( defaultDirectory ));
    }
    
    /** Creates new MecFileChooser
     * Creates a new FileChooser with the given default directory
     * @@param defaultDirectory Directory to start by default, may be a file
     * @@param dialogTitle Title to show at the chooser
     * @@param parent parent component
     */
    public MecFileChooser( Frame parent, String dialogTitle ){
        super( FileSystemView.getFileSystemView() );
        //Load default resourcebundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleMecFileChooser.class.getName());
        }
        //load up default english resourcebundle
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found" );
        }
        this.parent = parent;
        this.setDialogTitle( dialogTitle );
        this.setMultiSelectionEnabled( false );        
    }
    
    /**Set a new file view to the chooser, e.g. to display new icons etc*/
    public void setFileView( FileView fileview ){
        super.setFileView( fileview );
    }
    
    
    /**Sets the type of the chooser: load*/
    public void setTypeLoad(){
        this.setDialogType( JFileChooser.OPEN_DIALOG );
    }
    
    /**Sets the type of the chooser: save*/
    public void setTypeSave(){
        this.setDialogType( JFileChooser.SAVE_DIALOG );
    }
    
    /**Creates a chooser dialog and displays it*/
    private void showChooserDialog(){
        this.setApproveButtonText(this.rb.getResourceString( "button.select" ));
        final JDialog dialog = this.createDialog(this.parent);
        // Add listener for approve and cancel events
        this.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent evt) {
                JFileChooser chooser = (JFileChooser)evt.getSource();
                if (JFileChooser.CANCEL_SELECTION.equals(evt.getActionCommand())){
                    canceled = true;
                    dialog.setVisible( false );
                } else if (JFileChooser.APPROVE_SELECTION.equals(evt.getActionCommand())){
                    dialog.setVisible( false );
                }
            }
        });
        // Add listener for window closing events
        dialog.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                canceled = true;
            }
        });
        dialog.setVisible(true);
    }
    
    
    /**Browses for a filename and returns it
     * @@param parent Parent component
     * @@return null if the user cancels the action!
     */
    public String browseFilename(){
        this.showChooserDialog();
        if( this.canceled )
            return( null );
        File file = this.getSelectedFile();
        return( file.getAbsolutePath() );
    }
    
    /**Browses the directory for a filename
     * @@param component JComponent where the chosen filename will displayed
     * @@param filter FileFilters that are accepted
     */
    public String browseFilename( JComponent component, String[] filter){
        if( filter != null )
            this.addChoosableFileFilter( new MecFileFilter( filter ));
        //component is a text field
        if( component instanceof JTextField ){
            JTextField textField = (JTextField)component;
            if( textField.getText().length() > 0 )
                this.setPreselectedFile( new File( textField.getText() ));
        }
        this.showChooserDialog();
        if( this.canceled )
            return( null );
        File file = this.getSelectedFile();
        if( component != null ){
            if( component instanceof JTextComponent )
                ((JTextComponent)component).setText( file.getAbsolutePath());
            if( component instanceof JComboBox )
                this.setItem( (JComboBox)component, file.getAbsolutePath() );
        }
        return( file.getAbsolutePath() );
    }
    
    /**Sets the preselected file in the directory chooser*/
    public void setPreselectedFile( File preselectedFile ){
        if( !preselectedFile.isDirectory() ){
            String parent = preselectedFile.getParent();
            if( parent != null )
                this.setCurrentDirectory( new File( parent ) );
            this.setSelectedFile( preselectedFile );
        } else
            this.setCurrentDirectory( preselectedFile );
    }
    
    /**Adds an item to a comboBox. If the item already exists,
     * it is set as selected
     * @@param comboBox Component to set the item
     * @@param item Object to write into the ComboBox
     */
    private void setItem( JComboBox comboBox, Object item ){
        //Check if element exists. if the item exists, set it and return
        for( int i = 0; i < comboBox.getItemCount(); i++ )
            if( comboBox.getItemAt(i).equals( item )){
            comboBox.setSelectedIndex(i);
            return;
            }
        comboBox.addItem( item );
        comboBox.setSelectedItem( item );
    }
    
    /**Browses the directory for a filename, all files are accepted. If there is
     * already a file name displayed in the component, its path will set as
     * default path.
     * @@param component JComponent where the chosen filename will displayed
     *@@return null if the user cancels the action!
     */
    public String browseFilename( JComponent component ){        
        return( this.browseFilename( component, null ) );
    }
    
    /**Browses directories ONLY, no file selection allowed
     * @@param component TextComponent where the chosen filename will displayed
     */
    public String browseDirectory(JComponent component){
        this.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY );
        return( this.browseFilename( component ) );
    }
    
    /**Browses directories ONLY, no file selection allowed
     */
    public String browseDirectory(){
        this.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY );
        return( this.browseFilename());
    }
    
    /**Filefilter for the chooser*/
    public class MecFileFilter extends javax.swing.filechooser.FileFilter{
        
        private String filePath = "";
        /**Stores the possible file filter extentions*/
        protected String[] filter = null;
        
        public MecFileFilter( String[] filter ){
            super();
            this.filter = filter;
        }
        
        public boolean accept( File file ){
            if( file.isDirectory() )
                return( true );
            
            this.filePath = file.getPath().toLowerCase();
            //check accept
            if( this.filter != null ){
                boolean accept = false;
                for( int i = 0; i < this.filter.length; i++ )
                    if( this.filePath.endsWith( this.filter[i] )){
                    accept = true;
                    break;
                    }
                return( accept );
            }
            return( true );
        }
        
        /**return descriptions of choosable file extentions*/
        public String getDescription(){
            if( this.filePath.endsWith( ".xsl" ))
                return( "Format conversion (*.xsl)" );
            if( this.filePath.endsWith( ".xml" ))
                return( "Extended Markup Language (*.xml)" );
            if( this.filePath.endsWith( ".properties" ))
                return( "Properties File (*.properties)" );
            return( "" );
        }
    }
    
    
}@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /converteride/de/mendelson/util/MecFileChooser.java 12    15.12.05 12:19 Heller $
d27 1
a27 1
 * @@version $Revision: 12 $
@

