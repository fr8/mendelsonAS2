head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2009.03.16.12.28.10;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2008.02.07.10.24.54;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/patch1_b21/de/mendelson/comm/as2/send/DirPollManager.java,v 1.1 2008/02/07 10:24:54 heller Exp $
package de.mendelson.comm.as2.send;
import de.mendelson.comm.as2.cert.CertificateManager;
import de.mendelson.comm.as2.jms.JMSMessageSender;
import de.mendelson.comm.as2.message.AS2Message;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.store.MessageStoreHandler;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.partner.PartnerAccessDB;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.util.FileFilterRegexpMatch;
import de.mendelson.util.MecResourceBundle;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Manager that polls the outbox directories of the partners, creates messages
 * and sends them
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class DirPollManager{
    
    private Logger logger = Logger.getLogger( "de.mendelson.as2.server" );
    
    private PreferencesAS2 preferences = new PreferencesAS2();
    
    private CertificateManager certificateManager;
    
    /**The sender of all messages*/
    private Partner localStation;
    
    /**Stores all poll threads
     *key: partner DB id, value: pollThread
     */
    private HashMap mapPollThread = new HashMap();
    
    /**Localize the GUI*/
    private MecResourceBundle rb = null;
    
    public DirPollManager( CertificateManager certificateManager ){
        //Load default resourcebundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleDirPollManager.class.getName());
        }
        //load up resourcebundle
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
        this.certificateManager = certificateManager;
        this.logger.info( this.rb.getResourceString( "manager.started" ));
    }
    
    /**Indicates that the partner configuration has been changed: This should stop now
     *unued tasks and start other
     */
    public void partnerConfigurationChanged(){
        Partner[] partner = null;
        try{
            PartnerAccessDB access = new PartnerAccessDB( "localhost" );
            partner = access.getPartner();
            this.localStation = access.getLocalStation();
            access.close();
        } catch( Exception e ){
            this.logger.severe( e.getMessage() );
            return;
        }
        if( partner == null ){
            this.logger.severe( "partnerConfigurationChanged: Unable to load partner" );
            return;
        }
        for( int i = 0; i < partner.length; i++ ){
            Integer id = new Integer( partner[i].getDBId() );
            //add partner task if it does not exist so far
            if( !this.mapPollThread.containsKey( id )
            && !partner[i].isLocalStation() ){
                this.addPartnerPollThread( partner[i] );
            } else if( this.mapPollThread.containsKey( id )){
                DirPollThread thread = (DirPollThread)this.mapPollThread.get(id);
                if( !partner[i].isLocalStation() ){
                    //task exists: update its information
                    thread.setPartner( partner[i]);
                } else{
                    //its a local station now: stop the task and remove it
                    thread.requestStop();
                    this.mapPollThread.remove( id );
                }
            }
        }
        //still running task that is not in the configuration any more: stop and remove
        ArrayList<Integer> idList = new ArrayList<Integer>();
        Iterator iterator = this.mapPollThread.keySet().iterator();
        while( iterator.hasNext() ){
            idList.add( (Integer)iterator.next() );
        }
        for( int i = 0; i < idList.size(); i++ )   {
            int id = ((Integer)idList.get(i)).intValue();
            boolean idFound = false;
            for( int ii = 0; ii < partner.length; ii++ ){
                if( partner[ii].getDBId() == id ){
                    idFound = true;
                    break;
                }
            }
            //old still running taks, has been deleted in the config: stop and remove
            if( !idFound ){
                Integer idObj = new Integer(id);
                DirPollThread thread = (DirPollThread)this.mapPollThread.get(idObj);
                thread.requestStop();
                this.mapPollThread.remove( idObj );
            }
        }
    }
    
    /**Adds a new partner to the poll thread list
     **/
    private void addPartnerPollThread( Partner partner ){
        DirPollThread thread = new DirPollThread();
        thread.setPartner( partner );
        this.mapPollThread.put( new Integer( partner.getDBId()), thread );
        thread.start();
        String pollIgnoreList = partner.getPollIgnoreListAsString();
        if( pollIgnoreList == null ){
            pollIgnoreList = "--";
        }
        logger.info( rb.getResourceString( "poll.started", new Object[]{
            partner.getName(), pollIgnoreList, partner.getPollInterval()
        }));
    }
    
    /**Worker class that listens on the queue and performs a http send
     *if a send order has been found
     */
    public class DirPollThread extends Thread{
        
        /**Polls all 10s*/
        private long pollInterval = 10000;
        
        private boolean stopRequested = false;
        
        private Partner receiver = null;
        
        /**Asks the thread to stiop*/
        public void requestStop(){
            logger.info( rb.getResourceString( "poll.stopped", this.receiver.getName() ));
            this.stopRequested = true;
        }
        
        /**Extracts the right directory to poll for the passed partner
         */
        public void setPartner( Partner receiver ){
            //partner renamed, this results in a new poll directory
            if( this.receiver != null && !this.receiver.getName().equals( receiver.getName() )){
                logger.info( rb.getResourceString( "poll.stopped", this.receiver.getName() ));
                String pollIgnoreList = receiver.getPollIgnoreListAsString();
                if( pollIgnoreList == null ){
                    pollIgnoreList = "--";
                }
                logger.info( rb.getResourceString( "poll.started", new Object[]{
                    receiver.getName(), pollIgnoreList, receiver.getPollInterval()
                }));
            }
            this.receiver = receiver;
            this.pollInterval = receiver.getPollInterval()*1000;
            this.setName( "Dir poll for receiver " + receiver.getName() );
        }
        
        /**Runs this thread
         */
        public void run(){
            while( !stopRequested ){
                try{
                    this.sleep( this.pollInterval );
                } catch( InterruptedException e ){
                    //nop
                }
                Partner localStation = null;
                //get data about the local station
                try{
                    if( localStation == null ){
                        PartnerAccessDB access = new PartnerAccessDB( "localhost" );
                        localStation = access.getLocalStation();
                        access.close();
                    }
                }catch( Exception e ){
                    logger.severe( "(" + this.getName() + ")-" + e.getMessage() );
                    continue;
                }
                StringBuffer outboxDirName = new StringBuffer();
                outboxDirName.append( new File( preferences.get( PreferencesAS2.DIR_MSG )).getAbsolutePath() );
                outboxDirName.append( File.separator );
                outboxDirName.append( MessageStoreHandler.convertToValidFilename( this.receiver.getName() ));
                outboxDirName.append( File.separator );
                outboxDirName.append( "outbox" );                
                File outboxDir = new File( outboxDirName.toString() );
                if( !outboxDir.exists() )
                    outboxDir.mkdirs();
                FileFilterRegexpMatch fileFilter = new FileFilterRegexpMatch();
                if( this.receiver.getPollIgnoreList() != null ){
                    for( String ignoreEntry:this.receiver.getPollIgnoreList() ){
                        fileFilter.addNonMatchingPattern( ignoreEntry );
                    }
                }
                File[] files = outboxDir.listFiles( fileFilter );
                for( int i = 0; i < files.length; i++ ){
                    if( files[i].isDirectory() )
                        continue;
                    if( !files[i].canWrite()){
                        logger.warning( rb.getResourceString( "warning.ro", files[i].getAbsolutePath() ));
                        continue;
                    }
                    this.processFile( files[i] );
                }
            }
        }
        
        /**Processes a single, found file
         */
        private void processFile( File file ){
            AS2MessageInfo messageInfo = null;
            try{
                logger.fine( rb.getResourceString( "processing.file",
                        new Object[]{
                    file.getName(),
                    this.receiver.getName()
                })
                );
                JMSMessageSender messageSender = new JMSMessageSender();
                AS2Message message = messageSender.send( certificateManager, localStation, this.receiver, file );
                boolean deleted = file.delete();
                if( deleted ){
                    logger.log( Level.INFO,
                            rb.getResourceString( "messagefile.deleted",
                            new Object[]{
                        message.getMessageInfo().getMessageId(),
                        file.getName(),
                    }),
                            message.getMessageInfo() );
                }
            } catch( Throwable e ){
                logger.severe( rb.getResourceString( "processing.file.error",
                        new Object[]{ file.getName(), this.receiver, e.getMessage() }));
            }
        }
        
    }
}@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/send/DirPollManager.java 16    21.11.07 10:13 Heller $
d32 1
a32 1
 * @@version $Revision: 16 $
@

