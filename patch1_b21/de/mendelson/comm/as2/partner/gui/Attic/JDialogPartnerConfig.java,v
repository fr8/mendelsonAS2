head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2009.03.16.12.28.10;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2008.02.07.10.24.51;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/patch1_b21/de/mendelson/comm/as2/partner/gui/JDialogPartnerConfig.java,v 1.1 2008/02/07 10:24:51 heller Exp $
package de.mendelson.comm.as2.partner.gui;
import de.mendelson.comm.as2.cert.CertificateManager;
import de.mendelson.comm.as2.clientserver.message.PartnerConfigurationChanged;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.partner.PartnerAccessDB;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.util.ImageUtil;
import javax.swing.*;
import java.util.*;
import de.mendelson.util.*;
import de.mendelson.util.clientserver.GUIClient;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Dialog to configure the partner of the rosettanet server
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class JDialogPartnerConfig extends JDialog{
    
    /**Resource to locaize the GUI*/
    private MecResourceBundle rb = null;
    
    /**List of all available partner*/
    private ArrayList<Partner> partnerList = new ArrayList<Partner>();
    
    private JPanelPartner panelEditPartner = null;
    
    private JTreePartner jTreePartner = null;
    
    private PreferencesAS2 preferences = new PreferencesAS2();
    
    private CertificateManager certificateManager = null;
    
    private GUIClient guiClient;
    
    /** Creates new form JDialogMessageMapping */
    public JDialogPartnerConfig(JFrame parent, GUIClient guiClient ) {
        super(parent, true);
        this.guiClient = guiClient;
        //load resource bundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundlePartnerConfig.class.getName());
        } catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
        this.jTreePartner = new JTreePartner();
        this.initComponents();
        this.jScrollPaneTree.setViewportView(this.jTreePartner);
        this.certificateManager = new CertificateManager();
        this.certificateManager.loadKeystoreCertificates( "certificates.p12",
                this.preferences.get( PreferencesAS2.KEYSTORE_PASS).toCharArray() );
        this.panelEditPartner = new JPanelPartner( this.jTreePartner,
                certificateManager, this.jButtonOk );
        this.jPanelPartner.add( this.panelEditPartner );
        this.getRootPane().setDefaultButton( this.jButtonOk );
        try{
            Partner[] partner = this.jTreePartner.buildTree();
            this.partnerList.addAll( Arrays.asList( partner ));
        } catch( Exception e ){
            JOptionPane.showMessageDialog( parent, e.getMessage() );
            return;
        }
        //mix up the add/edit/delete icons
        ImageUtil imageUtil = new ImageUtil();
        ImageIcon iconBase
                = new ImageIcon( this.getClass().
                getResource( "/de/mendelson/comm/as2/partner/gui/singlepartner16x16.gif" ));
        
        ImageIcon iconAdd
                = new ImageIcon( this.getClass().
                getResource( "/de/mendelson/comm/as2/partner/gui/mini_add.gif" ));
        ImageIcon iconDelete
                = new ImageIcon( this.getClass().
                getResource( "/de/mendelson/comm/as2/partner/gui/mini_delete.gif" ));
        ImageIcon iconMixedAdd = imageUtil.mixImages( iconBase, iconAdd );
        ImageIcon iconMixedDelete = imageUtil.mixImages( iconBase, iconDelete );
        
        this.jButtonNewPartner.setIcon( iconMixedAdd );
        this.jButtonDeletePartner.setIcon( iconMixedDelete );
        this.jTreePartner.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent evt) {
                displayPartnerValues();
            }
        });
        this.displayPartnerValues();
    }
    
    private void displayPartnerValues(){
        DefaultMutableTreeNode selectedNode = this.jTreePartner.getSelectedNode();
        if( selectedNode == null )
            return;
        Partner selectedPartner = (Partner)selectedNode.getUserObject();
        this.panelEditPartner.setPartner(selectedPartner, selectedNode);
    }
    
    /**Sets the button states of this GUI*/
    private void setButtonState(){
    }
    
    
    private void deleteSelectedPartner(){
        Partner partner = this.jTreePartner.getSelectedPartner();
        if( partner != null ){
            //ask the user if the partner should be really deleted, all data is lost
            int requestValue = JOptionPane.showConfirmDialog(
                    this, this.rb.getResourceString( "dialog.partner.delete.message", partner.getName() ),
                    this.rb.getResourceString( "dialog.partner.delete.title" ),
                    JOptionPane.YES_NO_OPTION );
            if( requestValue != JOptionPane.YES_OPTION ){
                return;
            }
            partner = this.jTreePartner.deleteSelectedPartner();
            if( partner != null ){
                this.partnerList.remove( partner );
            }
        }
    }
    
    private boolean checkLocalStationHasPrivateKey(){
        Partner localStation = this.jTreePartner.getLocalStation();
        //no local station? should not happen
        if( localStation == null ){
            return( false );            
        }
        String signAlias = localStation.getSignAlias();
        String cryptAlias = localStation.getCryptAlias();
        try{
            this.certificateManager.getPrivateKey( signAlias );
            this.certificateManager.getPrivateKey( cryptAlias );
        }
        catch( Exception e ){
            return( false );
        }
        return( true );
    }
    
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jToolBar = new javax.swing.JToolBar();
        jButtonNewPartner = new javax.swing.JButton();
        jButtonDeletePartner = new javax.swing.JButton();
        jPanelMain = new javax.swing.JPanel();
        jPanelPartnerMain = new javax.swing.JPanel();
        jSplitPane = new javax.swing.JSplitPane();
        jScrollPaneTree = new javax.swing.JScrollPane();
        jPanelPartner = new javax.swing.JPanel();
        jPanelButton = new javax.swing.JPanel();
        jButtonOk = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();

        setTitle(this.rb.getResourceString( "title" ));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jToolBar.setFloatable(false);
        jToolBar.setRollover(true);
        jButtonNewPartner.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/partner/gui/singlepartner16x16.gif")));
        jButtonNewPartner.setText(this.rb.getResourceString( "button.new"));
        jButtonNewPartner.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNewPartnerActionPerformed(evt);
            }
        });

        jToolBar.add(jButtonNewPartner);

        jButtonDeletePartner.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/partner/gui/singlepartner16x16.gif")));
        jButtonDeletePartner.setText(this.rb.getResourceString( "button.delete"));
        jButtonDeletePartner.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeletePartnerActionPerformed(evt);
            }
        });

        jToolBar.add(jButtonDeletePartner);

        getContentPane().add(jToolBar, java.awt.BorderLayout.NORTH);

        jPanelMain.setLayout(new java.awt.GridBagLayout());

        jPanelPartnerMain.setLayout(new java.awt.GridBagLayout());

        jSplitPane.setDividerLocation(150);
        jSplitPane.setLeftComponent(jScrollPaneTree);

        jPanelPartner.setLayout(new java.awt.BorderLayout());

        jSplitPane.setRightComponent(jPanelPartner);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelPartnerMain.add(jSplitPane, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelMain.add(jPanelPartnerMain, gridBagConstraints);

        jPanelButton.setLayout(new java.awt.GridBagLayout());

        jButtonOk.setText(this.rb.getResourceString( "button.ok" ));
        jButtonOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOkActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelButton.add(jButtonOk, gridBagConstraints);

        jButtonCancel.setText(this.rb.getResourceString( "button.cancel" ));
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelButton.add(jButtonCancel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelMain.add(jPanelButton, gridBagConstraints);

        getContentPane().add(jPanelMain, java.awt.BorderLayout.CENTER);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-696)/2, (screenSize.height-475)/2, 696, 475);
    }// </editor-fold>//GEN-END:initComponents
    
    private void jButtonDeletePartnerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeletePartnerActionPerformed
        this.deleteSelectedPartner();
    }//GEN-LAST:event_jButtonDeletePartnerActionPerformed
    
    private void jButtonNewPartnerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNewPartnerActionPerformed
        Partner partner = this.jTreePartner.createNewPartner( this.certificateManager );
        this.partnerList.add( partner );
    }//GEN-LAST:event_jButtonNewPartnerActionPerformed
    
    private void jTreePartnerValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_jTreePartnerValueChanged
        
    }//GEN-LAST:event_jTreePartnerValueChanged
    
    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
        this.setVisible( false );
        this.dispose();
    }//GEN-LAST:event_jButtonCancelActionPerformed
    
    private void jButtonOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOkActionPerformed
        //check if a local station is set
        if( !this.jTreePartner.localStationIsSet()){
            JOptionPane.showMessageDialog( this,
                    this.rb.getResourceString( "nolocalstation.message" ),
                    this.rb.getResourceString( "nolocalstation.title" ),
                    JOptionPane.INFORMATION_MESSAGE );
            return;
        }
        //check if the localstation contains a private key in security settings
        boolean localStationHasPrivateKey = this.checkLocalStationHasPrivateKey();
        if( !localStationHasPrivateKey ){
            JOptionPane.showMessageDialog( this,
                    this.rb.getResourceString( "localstation.noprivatekey.message" ),
                    this.rb.getResourceString( "localstation.noprivatekey.title" ),
                    JOptionPane.INFORMATION_MESSAGE );
            return;
        }
        try{
            PartnerAccessDB access = new PartnerAccessDB( "localhost" );
            Partner[] newPartner = new Partner[this.partnerList.size()];
            this.partnerList.toArray(newPartner);
            access.updatePartner( newPartner );
            access.close();
        } catch( Exception e ){
            JFrame parent = (JFrame)SwingUtilities.getAncestorOfClass( JFrame.class, this );
            JOptionPane.showMessageDialog( parent, e.getMessage() );
            return;
        }
        //inform the server that the configuration has been changed
        PartnerConfigurationChanged signal = new PartnerConfigurationChanged();
        this.guiClient.sendAsync( signal );
        this.setVisible( false );
        this.dispose();
    }//GEN-LAST:event_jButtonOkActionPerformed
    
    /** Closes the dialog */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        setVisible(false);
        this.dispose();
    }//GEN-LAST:event_closeDialog
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonDeletePartner;
    private javax.swing.JButton jButtonNewPartner;
    private javax.swing.JButton jButtonOk;
    private javax.swing.JPanel jPanelButton;
    private javax.swing.JPanel jPanelMain;
    private javax.swing.JPanel jPanelPartner;
    private javax.swing.JPanel jPanelPartnerMain;
    private javax.swing.JScrollPane jScrollPaneTree;
    private javax.swing.JSplitPane jSplitPane;
    private javax.swing.JToolBar jToolBar;
    // End of variables declaration//GEN-END:variables
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/partner/gui/JDialogPartnerConfig.java 8     21.11.07 10:13 Heller $
d27 1
a27 1
 * @@version $Revision: 8 $
@

