head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.37.42;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2010.01.26.16.28.12;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b31/de/mendelson/comm/as2/cert/KeystoreCertificate.java,v 1.1 2010/01/26 16:28:12 heller Exp $
package de.mendelson.comm.as2.cert;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.cert.CertPath;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertPathValidatorResult;
import java.security.cert.CertStore;
import java.security.cert.CertStoreParameters;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.PKIXCertPathValidatorResult;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERString;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyUsage;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Object that stores a single configuration certificate/key
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class KeystoreCertificate implements Comparable {

    private String alias = "";
    private X509Certificate certificate = null;
    private boolean isKeyPair = false;

    public KeystoreCertificate() {
    }

    /**Returns the extension value "extended key usage", OID 2.5.29.37
     * 
     */
    public ArrayList<String> getExtendedKeyUsage() {
        HashMap<String, String> oidMap = new HashMap<String, String>();
        oidMap.put("1.3.6.1.5.5.7.3.2", "Client authentication");
        oidMap.put("1.3.6.1.5.5.7.3.1", "Webserver authentication");
        oidMap.put("1.3.6.1.5.5.7.3.5", "IPSec end system");
        oidMap.put("1.3.6.1.5.5.7.3.6", "IPSec tunnel");
        oidMap.put("1.3.6.1.5.5.7.3.3", "Code signing");
        oidMap.put("1.3.6.1.5.5.7.3.7", "IPSec user");
        oidMap.put("1.3.6.1.5.5.7.3.4", "Email protection");
        oidMap.put("1.3.6.1.5.5.7.3.8", "Timestamping");
        oidMap.put("2.16.840.1.113733.1.8.1", "Verisign Server Gated Crypto");
        oidMap.put("2.16.840.1.113730.4.1", "Netscape Server Gated Crypto");
        oidMap.put("1.3.6.1.4.1.311.10.3.3", "Microsoft Server Gated Crypto");

        ArrayList<String> extendedKeyUsage = new ArrayList<String>();
        byte[] extensionValue = this.certificate.getExtensionValue("2.5.29.37");
        if (extensionValue == null) {
            return (extendedKeyUsage);
        }
        try {
            byte[] octedBytes = ((ASN1OctetString) ASN1Object.fromByteArray(extensionValue)).getOctets();
            ASN1Sequence asn1Sequence = (ASN1Sequence) ASN1Object.fromByteArray(octedBytes);
            for (int i = 0; i < asn1Sequence.size(); i++) {
                String oid = (asn1Sequence.getObjectAt(i).getDERObject().toString());
                if (oidMap.containsKey(oid)) {
                    extendedKeyUsage.add(oidMap.get(oid));
                } else {
                    extendedKeyUsage.add(oid);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (extendedKeyUsage);
    }

    /**Returns the subject alternative name of this cert, OID 2.5.29.15*/
    public ArrayList<String> getKeyUsages() {
        ArrayList<String> keyUsages = new ArrayList<String>();
        byte[] extensionValue = this.certificate.getExtensionValue("2.5.29.15");
        if (extensionValue == null) {
            return (keyUsages);
        }
        try {
            byte[] octedBytes = ((ASN1OctetString) ASN1Object.fromByteArray(extensionValue)).getOctets();
            //bit encoded values for the key usage
            int val = KeyUsage.getInstance(ASN1Object.fromByteArray(octedBytes)).intValue();
            if ((val & KeyUsage.cRLSign) == KeyUsage.cRLSign) {
                keyUsages.add("CRL signing");
            }
            if ((val & KeyUsage.dataEncipherment) == KeyUsage.dataEncipherment) {
                keyUsages.add("Data encipherment");
            }
            if ((val & KeyUsage.decipherOnly) == KeyUsage.decipherOnly) {
                keyUsages.add("Decipher");
            }
            if ((val & KeyUsage.digitalSignature) == KeyUsage.digitalSignature) {
                keyUsages.add("Digital signature");
            }
            if ((val & KeyUsage.encipherOnly) == KeyUsage.encipherOnly) {
                keyUsages.add("Encipher");
            }
            if ((val & KeyUsage.keyAgreement) == KeyUsage.keyAgreement) {
                keyUsages.add("Key agreement");
            }
            if ((val & KeyUsage.keyCertSign) == KeyUsage.keyCertSign) {
                keyUsages.add("Key certificate signing");
            }
            if ((val & KeyUsage.keyEncipherment) == KeyUsage.keyEncipherment) {
                keyUsages.add("Key encipherment");
            }
            if ((val & KeyUsage.nonRepudiation) == KeyUsage.nonRepudiation) {
                keyUsages.add("Non repudiation");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return (keyUsages);
    }

    /**Returns the subject alternative name of this cert, OID 2.5.29.17*/
    public ArrayList<String> getSubjectAlternativeNames() {
        ArrayList<String> alternativeNames = new ArrayList<String>();
        byte[] extensionValue = this.certificate.getExtensionValue("2.5.29.17");
        if (extensionValue == null) {
            return (alternativeNames);
        }
        try {
            byte[] octedBytes = ((ASN1OctetString) ASN1Object.fromByteArray(extensionValue)).getOctets();
            GeneralName[] names = (GeneralNames.getInstance(ASN1Object.fromByteArray(octedBytes))).getNames();
            for (GeneralName name : names) {                
                alternativeNames.add(((DERString) name.getName()).getString() + " (" + this.generalNameTagNoToString(name) + ")" );
            }
        } catch (Exception e) {
            //nop
        }
        return (alternativeNames);
    }


    /**Converts the tag no of a general name to a human readable value*/
    private String generalNameTagNoToString(GeneralName name){
        if( name.getTagNo() == GeneralName.dNSName ){
            return( "DNS name");
        }
        if( name.getTagNo() == GeneralName.directoryName ){
            return( "Directory name");
        }
        if( name.getTagNo() == GeneralName.ediPartyName ){
            return( "EDI party name");
        }
        if( name.getTagNo() == GeneralName.iPAddress ){
            return( "IP address");
        }
        if( name.getTagNo() == GeneralName.otherName ){
            return( "Other name");
        }
        if( name.getTagNo() == GeneralName.registeredID ){
            return( "Registered ID");
        }
        if( name.getTagNo() == GeneralName.rfc822Name ){
            return( "RFC822 name");
        }
        if( name.getTagNo() == GeneralName.uniformResourceIdentifier ){
            return( "URI" );
        }
        if( name.getTagNo() == GeneralName.x400Address ){
            return( "x.400 address");
        }
        return( "" );
    }


    /**
     * Get extension values for CRL Distribution Points as a string list or an empty list if an exception occured or
     * the extension doesnt exist
     * OID 2.5.29.31
     */
    public ArrayList<String> getCrlDistributionURLs() {
        ArrayList<String> ulrList = new ArrayList<String>();
        //CRL destribution points has OID 2.5.29.31
        byte[] extensionValue = this.certificate.getExtensionValue("2.5.29.31");
        if (extensionValue == null) {
            return (ulrList);
        }
        try {
            byte[] octedBytes = ((ASN1OctetString) ASN1Object.fromByteArray(extensionValue)).getOctets();
            CRLDistPoint distPoint = CRLDistPoint.getInstance(ASN1Object.fromByteArray(octedBytes));

            DistributionPoint[] points = distPoint.getDistributionPoints();
            for (DistributionPoint point : points) {
                DistributionPointName distributionPointName = point.getDistributionPoint();
                if (distributionPointName != null) {
                    if (distributionPointName.getType() == DistributionPointName.FULL_NAME) {
                        GeneralNames generalNames = (GeneralNames) distributionPointName.getName();
                        for (GeneralName generalName : generalNames.getNames()) {
                            //generalName.getTagNo() is GeneralName.uniformResourceIdentifier in this case
                            ulrList.add(((DERString) generalName.getName()).getString());
                        }
                    }
                }
            }
        } catch (Exception e) {
            //nop
        }
        return (ulrList);
    }

    /**Returns the enwrapped certificate version*/
    public int getVersion() {
        return (this.certificate.getVersion());
    }

    public String getSigAlgName() {
        return (this.certificate.getSigAlgName());
    }

    public String getPublicKeyAlgorithm() {
        return (this.certificate.getPublicKey().getAlgorithm());
    }

    /**Valid date start*/
    public Date getNotBefore() {
        return (this.certificate.getNotBefore());
    }

    /**Valid date end*/
    public Date getNotAfter() {
        return (this.certificate.getNotAfter());
    }

    public String getSubjectDN() {
        return (this.certificate.getSubjectDN().toString());
    }

    public String getIssuerDN() {
        return (this.certificate.getIssuerDN().toString());
    }

    /**Returns the serial number as decimal*/
    public String getSerialNumberDEC() {
        return (this.certificate.getSerialNumber().toString());
    }

    /**Returns the serial number as decimal*/
    public String getSerialNumberHEX() {
        return (this.certificate.getSerialNumber().toString(16).toUpperCase());
    }

    public void setAlias(String alias) {
        if (alias == null) {
            alias = "";
        }
        this.alias = alias;
    }

    public void setCertificate(X509Certificate certificate) {
        this.certificate = certificate;
    }

    public void setIsKeyPair(boolean isKeyPair) {
        this.isKeyPair = isKeyPair;
    }

    public X509Certificate getX509Certificate() {
        return (this.certificate);
    }

    public boolean getIsKeyPair() {
        return (this.isKeyPair);
    }

    public boolean isRootCertificate() {
        return (this.certificate.getBasicConstraints() != -1);
    }

    public String getAlias() {
        return (this.alias);
    }

    public int getPublicKeyLength() {
        PublicKey key = this.certificate.getPublicKey();
        if (key instanceof RSAPublicKey) {
            RSAPublicKey rsaKey = (RSAPublicKey) key;
            return (rsaKey.getModulus().bitLength());
        }
        return (0);
    }

    /**@@param digest to create the hash value, please use SHA1 or MD5 only
     * 
     */
    public String getFingerPrint(String digest) throws CertificateEncodingException, NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance(digest);
        byte[] bytes = messageDigest.digest(this.certificate.getEncoded());
        StringBuilder fingerprint = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            if (fingerprint.length() > 0) {
                fingerprint.append(":");
            }
            fingerprint.append(Integer.toHexString(bytes[i] & 0xFF).toUpperCase());
        }
        return fingerprint.toString();
    }

    /**Returns the cert path for this certificate as it exists in the keystore
     * @@return null if no cert path could be found
     */
    public PKIXCertPathBuilderResult getPKIXCertPathBuilderResult(KeyStore keystore, ArrayList<X509Certificate> certificateList) {
        try {
            X509CertSelector selector = new X509CertSelector();
            selector.setCertificate(this.getX509Certificate());
            boolean selected = selector.match(this.getX509Certificate());
            if (!selected) {
                return (null);
            }
            CertPathBuilder builder = CertPathBuilder.getInstance("PKIX", "BC");
            PKIXBuilderParameters pkix = new PKIXBuilderParameters(keystore, selector);
            pkix.setRevocationEnabled(false);
            CertStoreParameters params = new CollectionCertStoreParameters(certificateList);
            CertStore certstore = CertStore.getInstance("Collection", params, "BC");
            pkix.addCertStore(certstore);
            PKIXCertPathBuilderResult result = (PKIXCertPathBuilderResult) builder.build((PKIXBuilderParameters) pkix);
            return (result);
        } catch (NoSuchProviderException e) {
        } catch (KeyStoreException e) {
        } catch (NoSuchAlgorithmException e) {
        } catch (InvalidAlgorithmParameterException e) {
        } catch (CertPathBuilderException e) {
        }
        return (null);
    }

    /**Validates the certificate and returns the trust anchor certificate if the cert path is valid and the
     * full path could be validated
     * @@return null if the certificate could not be trusted or an other failure like nosuchalg exception etc occurs
     */
    public X509Certificate validateCertPath(KeyStore keystore, ArrayList<X509Certificate> certificateList) {
        CertPath certPath = this.getPKIXCertPathBuilderResult(keystore, certificateList).getCertPath();
        if (certPath == null) {
            return (null);
        }
        try {
            // Validator params
            PKIXParameters params = new PKIXParameters(keystore);
            // Disable CRL checking since we are not supplying any CRLs
            params.setRevocationEnabled(false);
            //use BC here else PKCS#12 is not supported as keystore
            CertPathValidator certPathValidator = CertPathValidator.getInstance("PKIX", "BC");
            CertPathValidatorResult result = certPathValidator.validate(certPath, params);
            // Get the CA used to validate this path
            PKIXCertPathValidatorResult pkixResult = (PKIXCertPathValidatorResult) result;
            TrustAnchor ta = pkixResult.getTrustAnchor();
            X509Certificate taCert = ta.getTrustedCert();
            return (taCert);
        } catch (NoSuchProviderException e) {
        } catch (KeyStoreException e) {
        } catch (NoSuchAlgorithmException e) {
        } catch (InvalidAlgorithmParameterException e) {
        } catch (CertPathValidatorException e) {
            // Validation failed
            }
        return (null);
    }

    @@Override
    public String toString() {
        return (this.alias);
    }

    public int compareTo(Object object) {
        KeystoreCertificate otherCert = (KeystoreCertificate) object;
        return (this.alias.toUpperCase().compareTo(otherCert.alias.toUpperCase()));
    }
}@


1.1
log
@
Committed on the Free edition of March Hare Software CVSNT Server.
Upgrade to CVS Suite for more features and support:
http://march-hare.com/cvsnt/
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/cert/KeystoreCertificate.java 13    20.05.09 16:42 Heller $
d53 1
a53 1
 * @@version $Revision: 13 $
@

