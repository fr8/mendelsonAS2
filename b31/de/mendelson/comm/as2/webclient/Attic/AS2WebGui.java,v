head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.37.50;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2010.01.26.16.28.25;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b31/de/mendelson/comm/as2/webclient/AS2WebGui.java,v 1.1 2010/01/26 16:28:25 heller Exp $
package de.mendelson.comm.as2.webclient;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
import de.mendelson.comm.as2.AS2ServerVersion;
import de.mendelson.comm.as2.database.DBDriverManager;
import de.mendelson.comm.as2.message.MessageOverviewFilter;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.partner.PartnerAccessDB;
import de.mendelson.comm.as2.webclient.about.AboutDialog;
import de.mendelson.comm.as2.webclient.state.StateDialog;
import org.wings.SIcon;
import org.wings.SURLIcon;
import org.wings.SFrame;
import org.wings.SLabel;
import org.wings.SPanel;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import org.wings.SBorderLayout;
import org.wings.SBoxLayout;
import org.wings.SButton;
import org.wings.SComboBox;
import org.wings.SConstants;
import org.wings.SGridLayout;
import org.wings.SMenu;
import org.wings.SMenuBar;
import org.wings.SMenuItem;
import org.wings.SOptionPane;
import org.wings.SResourceIcon;
import org.wings.SToolBar;
import org.wings.border.SBorder;
import org.wings.border.SLineBorder;
import org.wings.header.FaviconHeader;

/**
 * Log frame for the web interface
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class AS2WebGui {

    private Color backgroundColor = new Color(0xEE, 0xEE, 0xEE);
    private SMenuBar menuBar;
    private SFrame rootFrame;
    private SButton sButtonDetails;
    private SComboBox sComboBoxFilterLocalStation;
    private SPanelTransactionOverview overviewPanel = null;
    //DB connection
    private Connection connection;

    /** This is the entry point of you application as denoted in your web.xml */
    public AS2WebGui() {
        //register the database drivers for the VM
        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (Exception e) {
            e.printStackTrace();
            SOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        try {
            this.connection = DBDriverManager.getConnectionWithoutErrorHandling("localhost");
        } catch (Exception e) {
            e.printStackTrace();
            SOptionPane.showMessageDialog(null, e.getClass().getName() + ": " + e.getMessage());
        }
        // the root application frame
        this.rootFrame = new SFrame();
        this.rootFrame.addHeader(new FaviconHeader("../images/mendelson_favicon.png"));
        this.rootFrame.setBackground(this.backgroundColor);
        this.rootFrame.setTitle(AS2ServerVersion.getProductName() + " web GUI");
        this.menuBar = this.createMenuBar();
        SPanel mainPanel = this.buildMainPanel();
        rootFrame.getContentPane().add(mainPanel);
        // show application
        rootFrame.setVisible(true);
    }

    /**creates the main panel*/
    private SPanel buildMainPanel() {
        SPanel panel = new SPanel(new SBorderLayout());
        SIcon sIconLogo = new SURLIcon("../images/logo.jpg");
        SLabel sLabelLogo = new SLabel(sIconLogo);
        panel.add(sLabelLogo, SBorderLayout.NORTH);
        SPanel contentPanel = this.buildContentPanel();
        panel.add(contentPanel, SBorderLayout.CENTER);
        return panel;
    }

    /**creates the main panel*/
    private SPanel buildContentPanel() {
        SPanel panel = new SPanel(new SGridLayout(3, 1, 10, 10));
        panel.add(this.menuBar);
        SToolBar sToolBar = new SToolBar();
        SBoxLayout layout = new SBoxLayout(sToolBar, SBoxLayout.HORIZONTAL);
        layout.setHgap(5);
        sToolBar.setLayout(layout);
        this.sButtonDetails = new SButton();
        this.sButtonDetails.setToolTipText("Display all available information about the selected transaction");
        this.sButtonDetails.setIcon(new SResourceIcon("/de/mendelson/comm/as2/client/messagedetails16x16.gif"));
        this.sButtonDetails.setText("Message details");
        sToolBar.add(this.sButtonDetails);
        //ask for available local stations
        PartnerAccessDB partnerAccess = new PartnerAccessDB(this.connection);
        Partner[] localStations = partnerAccess.getLocalStations();
        //add filter for local station if the number of local stations is > 1
        if (localStations != null && localStations.length > 1) {
            SLabel sLabelFilterLocalStation = new SLabel("Local station: ");
            sToolBar.add(sLabelFilterLocalStation);
            this.sComboBoxFilterLocalStation = new SComboBox();
            this.sComboBoxFilterLocalStation.addItem("Display all");
            for (Partner localStation : localStations) {
                this.sComboBoxFilterLocalStation.addItem(localStation);
            }
            this.sComboBoxFilterLocalStation.addActionListener(new ActionListener() {

                @@Override
                public void actionPerformed(ActionEvent evt) {
                    sComboBoxFilterLocalStationActionPerformed(evt);
                }
            });
            sToolBar.add(this.sComboBoxFilterLocalStation);
        }
        sToolBar.setHorizontalAlignment(SConstants.LEFT_ALIGN);
        panel.add(sToolBar);
        this.overviewPanel = new SPanelTransactionOverview(this.sButtonDetails, this.connection);
        panel.add(this.overviewPanel);
        return panel;
    }

    private SMenuBar createMenuBar() {
        SMenuBar localMenuBar = new SMenuBar();
        SBorder border = new SLineBorder(Color.WHITE, 0);
        border.setThickness(1, SConstants.TOP);
        localMenuBar.setBorder(border);
        SMenu serverMenu = new SMenu("AS2 server");
        SMenuItem serverStateMenuItem = new SMenuItem("State");
        serverStateMenuItem.addActionListener(new ActionListener() {

            @@Override
            public void actionPerformed(ActionEvent evt) {
                serverStateMenuItemActionPerformed(evt);
            }
        });
        serverMenu.add(serverStateMenuItem);
        localMenuBar.add(serverMenu);
        SMenu helpMenu = new SMenu("Help");
        SMenuItem helpMenuItem = new SMenuItem("About");
        helpMenuItem.addActionListener(new ActionListener() {

            @@Override
            public void actionPerformed(ActionEvent evt) {
                helpMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(helpMenuItem);
        localMenuBar.add(helpMenu);
        return localMenuBar;
    }

    /**Action listener for an event*/
    private void helpMenuItemActionPerformed(ActionEvent evt) {
        AboutDialog dialog = new AboutDialog(this.rootFrame, "About");
        dialog.setVisible(true);
    }

    /**Action listener for an event*/
    private void serverStateMenuItemActionPerformed(ActionEvent evt) {
        StateDialog dialog = new StateDialog(this.rootFrame, "AS2 Server state");
        dialog.setVisible(true);
    }

    /**Action listener for an event*/
    private void sComboBoxFilterLocalStationActionPerformed(ActionEvent evt) {
        if (this.overviewPanel != null) {
            MessageOverviewFilter filter = new MessageOverviewFilter();
            if (this.sComboBoxFilterLocalStation.getSelectedIndex() > 0) {
                filter.setShowLocalStation((Partner) this.sComboBoxFilterLocalStation.getSelectedItem());
            }
            this.overviewPanel.refreshData(filter);
        }
    }
}
@


1.1
log
@
Committed on the Free edition of March Hare Software CVSNT Server.
Upgrade to CVS Suite for more features and support:
http://march-hare.com/cvsnt/
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/webclient/AS2WebGui.java 9     6.08.09 11:43 Heller $
d46 1
a46 1
 * @@version $Revision: 9 $
@

