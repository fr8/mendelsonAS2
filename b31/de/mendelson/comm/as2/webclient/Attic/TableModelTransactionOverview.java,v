head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.37.50;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2010.01.26.16.28.25;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b31/de/mendelson/comm/as2/webclient/TableModelTransactionOverview.java,v 1.1 2010/01/26 16:28:25 heller Exp $
package de.mendelson.comm.as2.webclient;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
import de.mendelson.comm.as2.message.AS2Message;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.AS2Payload;
import de.mendelson.comm.as2.message.MessageAccessDB;
import de.mendelson.comm.as2.message.MessageOverviewFilter;
import de.mendelson.comm.as2.message.ResourceBundleAS2Message;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.partner.PartnerAccessDB;
import de.mendelson.util.MecResourceBundle;
import java.sql.Connection;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.wings.SIcon;
import org.wings.SResourceIcon;
import org.wingx.table.XTableModel;

/**
 * Table model for the transaction overview
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class TableModelTransactionOverview extends XTableModel {

    public static final SResourceIcon ICON_IN = new SResourceIcon("/de/mendelson/comm/as2/message/loggui/in16x16.gif");
    public static final SResourceIcon ICON_OUT = new SResourceIcon("/de/mendelson/comm/as2/message/loggui/out16x16.gif");
    public static final SResourceIcon ICON_PENDING = new SResourceIcon("/de/mendelson/comm/as2/message/loggui/state_pending16x16.gif");
    public static final SResourceIcon ICON_STOPPED = new SResourceIcon("/de/mendelson/comm/as2/message/loggui/state_stopped16x16.gif");
    public static final SResourceIcon ICON_FINISHED = new SResourceIcon("/de/mendelson/comm/as2/message/loggui/state_finished16x16.gif");
    /**ResourceBundle to localize the enc/signature stuff*/
    private MecResourceBundle rbMessage = null;
    /**Stores all partner ids and the corresponding partner objects*/
    private HashMap<String, Partner> partnerMap = new HashMap<String, Partner>();
    /**Data to display*/
    private ArrayList<AS2Message> data = new ArrayList<AS2Message>();
    /**Format the date display*/
    private DateFormat format = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
    /**Filter that filter the transaction overview data fro display*/
    private MessageOverviewFilter filter = new MessageOverviewFilter();
    //DB connection
    private Connection connection;

    public TableModelTransactionOverview(Connection connection) {
        this.connection = connection;
        //load resource bundle
        try {
            this.rbMessage = (MecResourceBundle) ResourceBundle.getBundle(
                    ResourceBundleAS2Message.class.getName());
        } //load up  resourcebundle
        catch (MissingResourceException e) {
            throw new RuntimeException("Oops..resource bundle " + e.getClassName() + " not found.");
        }
        //init filter
        filter.setShowFinished(true);
        filter.setShowPending(true);
        filter.setShowStopped(true);
        filter.setShowPartner(null);
        this.refresh();
    }

    @@Override
    public int getColumnCount() {
        return (10);
    }

    @@Override
    public String getColumnName(int col) {
        switch (col) {
            case 0:
                return (" ");
            case 1:
                return ("  ");
            case 2:
                return ("Timestamp");
            case 3:
                return ("Local station");
            case 4:
                return ("Partner");
            case 5:
                return ("Message id");
            case 6:
                return ("Payload");
            case 7:
                return ("Encryption");
            case 8:
                return ("Signature");
            case 9:
                return ("MDN");
        }
        return (null);
    }

    @@Override
    public int getRowCount() {
        return (this.data.size());
    }

    @@Override
    public Object getValueAt(int row, int col) {
        AS2Message overviewRow = this.data.get(row);
        AS2MessageInfo info = overviewRow.getMessageInfo();
        switch (col) {
            case 0:
                if (info.getState() == AS2Message.STATE_FINISHED) {
                    return (ICON_FINISHED);
                } else if (info.getState() == AS2Message.STATE_STOPPED) {
                    return (ICON_STOPPED);
                }
                return (ICON_PENDING);
            case 1:
                if (info.getDirection() == AS2MessageInfo.DIRECTION_IN) {
                    return (ICON_IN);
                } else {
                    return (ICON_OUT);
                }
            case 2:
                return (this.format.format(info.getMessageDate()));
            case 3:
                if (info.getDirection() != AS2MessageInfo.DIRECTION_IN) {
                    String id = info.getSenderId();
                    Partner sender = this.partnerMap.get(id);
                    if (sender != null) {
                        return (sender.getName());
                    } else {
                        return (id);
                    }
                } else {
                    String id = info.getReceiverId();
                    Partner receiver = this.partnerMap.get(id);
                    if (receiver != null) {
                        return (receiver.getName());
                    } else {
                        return (id);
                    }
                }
            case 4:
                if (info.getDirection() == AS2MessageInfo.DIRECTION_IN) {
                    String id = info.getSenderId();
                    Partner sender = this.partnerMap.get(id);
                    if (sender != null) {
                        return (sender.getName());
                    } else {
                        return (id);
                    }
                } else {
                    String id = info.getReceiverId();
                    Partner receiver = this.partnerMap.get(id);
                    if (receiver != null) {
                        return (receiver.getName());
                    } else {
                        return (id);
                    }
                }
            case 5:
                return (info.getMessageId());
            case 6:
                if (overviewRow.getPayloadCount() == 0 ||
                        (overviewRow.getPayloadCount() == 1 && overviewRow.getPayload(0).getOriginalFilename() == null)) {
                    return ("--");
                } else if (overviewRow.getPayloadCount() == 1) {
                    return (overviewRow.getPayload(0).getOriginalFilename());
                } else {
                    return ("Number of attachments: " + String.valueOf(overviewRow.getPayloadCount()));
                }
            case 7:
                return (this.rbMessage.getResourceString("encryption." + info.getEncryptionType()));
            case 8:
                return (this.rbMessage.getResourceString("signature." + info.getSignType()));
            case 9:
                if (info.requestsSyncMDN()) {
                    return ("SYNC");
                } else {
                    return ("ASYNC");
                }

        }
        return (null);
    }

    public AS2Message getRow(int row) {
        return (this.data.get(row));
    }

    @@Override
    public Class getColumnClass(int columnIndex) {
        return (new Class[]{
                    SIcon.class,
                    SIcon.class,
                    String.class,
                    String.class,
                    String.class,
                    String.class,
                    String.class,
                    String.class,
                    String.class,
                    String.class
                }[columnIndex]);
    }

    @@Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void refreshData(MessageOverviewFilter filter) {
        this.filter = filter;
        this.refresh();
    }

    @@Override
    public void refresh() {
        //clear existing data
        this.data.clear();
        this.partnerMap.clear();

        //load partner data
        try {
            PartnerAccessDB partnerAccess = new PartnerAccessDB(this.connection);
            Partner[] partner = partnerAccess.getPartner();
            for (Partner singlePartner : partner) {
                partnerMap.put(singlePartner.getAS2Identification(), singlePartner);
            }
        } catch (Exception e) {
            //nop
        }
        //load message data
        try {
            MessageAccessDB access = new MessageAccessDB(this.connection);
            AS2MessageInfo[] info = access.getMessageOverview(this.filter);
            for (AS2MessageInfo messageInfo : info) {
                AS2Message message = new AS2Message();
                message.setMessageInfo(messageInfo);
                AS2Payload[] payloads = access.getPayload(messageInfo.getMessageId());
                for (AS2Payload payload : payloads) {
                    message.addPayload(payload);
                }
                this.data.add(message);
            }
        } catch (Exception e) {
            //nop
        }
        this.fireTableDataChanged();
    }
}
@


1.1
log
@
Committed on the Free edition of March Hare Software CVSNT Server.
Upgrade to CVS Suite for more features and support:
http://march-hare.com/cvsnt/
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/webclient/TableModelTransactionOverview.java 4     6.08.09 11:43 Heller $
d33 1
a33 1
 * @@version $Revision: 4 $
@

