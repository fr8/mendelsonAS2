head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.37.47;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2010.01.26.16.28.15;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b31/de/mendelson/comm/as2/message/ExecuteShellCommand.java,v 1.1 2010/01/26 16:28:15 heller Exp $
package de.mendelson.comm.as2.message;

import de.mendelson.comm.as2.log.LogAccessDB;
import de.mendelson.comm.as2.log.LogEntry;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.util.Exec;
import de.mendelson.util.MecResourceBundle;
import java.io.File;
import java.io.PrintStream;
import java.sql.Connection;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Allows to execute a shell command. This is used to execute a shell command on
 *message receipt
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class ExecuteShellCommand {

    private Logger logger = Logger.getLogger("de.mendelson.as2.server");
    private MessageAccessDB messageAccess;
    /**Localize your GUI!*/
    private MecResourceBundle rb = null;
    //DB connection
    private Connection connection;

    public ExecuteShellCommand(Connection connection) {
        this.messageAccess = new MessageAccessDB( connection );
        this.connection = connection;
        //Load resourcebundle
        try {
            this.rb = (MecResourceBundle) ResourceBundle.getBundle(
                    ResourceBundleExecuteShellCommand.class.getName());
        } //load up  resourcebundle
        catch (MissingResourceException e) {
            throw new RuntimeException("Oops..resource bundle " + e.getClassName() + " not found.");
        }
    }

    /**Executes a shell command for an inbound AS2 message if this has been defined in the partner settings
     */
    public void executeShellCommandOnSend(Partner messageSender, Partner messageReceiver, AS2MessageInfo messageInfo, int messageState) {
        AS2Payload[] payload = this.messageAccess.getPayload(messageInfo.getMessageId());
        String rawCommand = null;
        if (messageState == AS2Message.STATE_FINISHED) {
            if (!messageReceiver.useCommandOnSendSuccess()) {
                return;
            } else {
                rawCommand = messageReceiver.getCommandOnSendSuccess();
                //empty command, huh?
                if (rawCommand == null || rawCommand.trim().length() == 0) {
                    return;
                }
            }
        } else if (messageState == AS2Message.STATE_STOPPED) {
            if (!messageReceiver.useCommandOnSendError()) {
                return;
            } else {
                rawCommand = messageReceiver.getCommandOnSendError();
                //empty command, huh?
                if (rawCommand == null || rawCommand.trim().length() == 0) {
                    return;
                }
            }
        } else {
            //state:pending should not execute anything
            return;
        }
        if (payload != null) {
            this.logger.log(Level.INFO, this.rb.getResourceString("executing.send", messageInfo.getMessageId()), messageInfo);
            for (AS2Payload singlePayload : payload) {
                if (singlePayload.getPayloadFilename() == null) {
                    continue;
                }
                String filename = singlePayload.getOriginalFilename();
                String command = this.replace(rawCommand, "${filename}", filename);
                command = this.replace( command, "${sender}", messageSender.getName());
                command = this.replace( command, "${receiver}", messageReceiver.getName());
                command = this.replace( command, "${messageid}", messageInfo.getMessageId());
                //add log?
                if (command.contains("${log}")) {
                    try {
                        LogAccessDB logAccess = new LogAccessDB(this.connection);
                        LogEntry[] entries = logAccess.getLog(messageInfo.getMessageId());
                        StringBuilder logBuffer = new StringBuilder();
                        for( LogEntry logEntry:entries){
                            logBuffer.append( logEntry.getMessage() + "\\n");
                        }
                        //dont use single and double quotes, this is used in command line environment
                        String logText = this.replace( logBuffer.toString(), "\"", "" );
                        logText = this.replace( logText, "'", "" );
                        command = this.replace(command, "${log}", logText);
                    } catch (Exception e) {
                        //nop
                    }
                }                
                this.logger.log(Level.INFO, this.rb.getResourceString("executing.command",
                        new Object[]{messageInfo.getMessageId(), command}), messageInfo);
                Exec exec = new Exec();
                try {
                    int returnCode = exec.start(command, new PrintStream(new AS2LoggerOutputStream(this.logger, messageInfo)),
                            new PrintStream(new AS2LoggerOutputStream(this.logger, messageInfo)));
                    this.logger.log(Level.INFO, this.rb.getResourceString("executed.command",
                            new Object[]{messageInfo.getMessageId(), String.valueOf(returnCode)}), messageInfo);
                } catch (Exception e) {
                    this.logger.warning(e.getMessage());
                }
            }
        }
    }

    /**Executes a shell command for an inbound AS2 message if this has been defined in the partner settings
     */
    public void executeShellCommandOnReceipt(Partner messageSender, Partner messageReceiver, AS2MessageInfo messageInfo) {
        if (!messageSender.useCommandOnReceipt()) {
            return;
        }
        if (messageSender.getCommandOnReceipt() == null || messageSender.getCommandOnReceipt().trim().length() == 0) {
            return;
        }
        AS2Payload[] payload = this.messageAccess.getPayload(messageInfo.getMessageId());
        if (payload != null) {
            this.logger.log(Level.INFO, this.rb.getResourceString("executing.receipt", messageInfo.getMessageId()), messageInfo);
            for (int i = 0; i < payload.length; i++) {
                if (payload[i].getPayloadFilename() == null) {
                    continue;
                }
                String filename = payload[i].getPayloadFilename();
                String command = this.replace(messageSender.getCommandOnReceipt(), "${filename}",
                        new File(filename).getAbsolutePath());
                command = this.replace( command, "${sender}", messageSender.getName());
                command = this.replace( command, "${receiver}", messageReceiver.getName());
                command = this.replace( command, "${messageid}", messageInfo.getMessageId());
                this.logger.log(Level.INFO, this.rb.getResourceString("executing.command",
                        new Object[]{messageInfo.getMessageId(), command}), messageInfo);
                Exec exec = new Exec();
                try {
                    int returnCode = exec.start(command, new PrintStream(new AS2LoggerOutputStream(this.logger, messageInfo)),
                            new PrintStream(new AS2LoggerOutputStream(this.logger, messageInfo)));
                    this.logger.log(Level.INFO, this.rb.getResourceString("executed.command",
                            new Object[]{messageInfo.getMessageId(), String.valueOf(returnCode)}), messageInfo);
                } catch (Exception e) {
                    this.logger.warning(e.getMessage());
                }
            }
        }
    }

    /** Replaces the string tag by the string replacement in the sourceString
     * @@param source Source string
     * @@param tag	String that will be replaced
     * @@param replacement String that will replace the tag
     * @@return String that contains the replaced values
     */
    private String replace(String source, String tag, String replacement) {
        if (source == null) {
            return null;
        }
        StringBuilder buffer = new StringBuilder();
        while (true) {
            int index = source.indexOf(tag);
            if (index == -1) {
                buffer.append(source);
                return (buffer.toString());
            }
            buffer.append(source.substring(0, index));
            buffer.append(replacement);
            source = source.substring(index + tag.length());
        }
    }
}
@


1.1
log
@
Committed on the Free edition of March Hare Software CVSNT Server.
Upgrade to CVS Suite for more features and support:
http://march-hare.com/cvsnt/
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/message/ExecuteShellCommand.java 15    21.12.09 11:30 Heller $
d28 1
a28 1
 * @@version $Revision: 15 $
@

