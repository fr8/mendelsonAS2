head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2006.06.20.13.55.37;	author heller;	state Exp;
branches;
next	;


desc
@@


1.1
log
@*** empty log message ***
@
text
@//$Header: /mec_as2/de/mendelson/comm/as2/client/AS2Gui.java 2     31.05.06 11:23 Heller $
package de.mendelson.comm.as2.client;

import de.mendelson.comm.as2.AS2ServerVersion;
import de.mendelson.comm.as2.client.about.AboutDialog;
import de.mendelson.comm.as2.client.log.LogConsolePanel;
import de.mendelson.comm.as2.message.AS2Message;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.MessageAccessDB;
import de.mendelson.comm.as2.message.MessageOverviewFilter;
import de.mendelson.comm.as2.message.loggui.DialogMessageDetails;
import de.mendelson.comm.as2.message.loggui.TableModelMessageOverview;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.partner.PartnerAccessDB;
import de.mendelson.comm.as2.partner.gui.JDialogPartnerConfig;
import de.mendelson.comm.as2.preferences.JDialogPreferences;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.comm.as2.timing.MessageDeleteController;
import de.mendelson.util.ImageUtil;
import de.mendelson.util.MecResourceBundle;
import de.mendelson.util.Splash;
import de.mendelson.util.table.ColumnFitAdapter;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import oracle.help.CSHManager;
import oracle.help.Help;
import oracle.help.library.helpset.HelpSet;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Main GUI for the control of the mendelson rosettanet server
 * @@author S.Heller
 * @@version $Revision: 2 $
 */
public class AS2Gui extends JFrame implements ListSelectionListener{
    
    /**Preferences of the application*/
    private PreferencesAS2 preferences = new PreferencesAS2();
    
    private Logger logger = Logger.getLogger( "de.mendelson.as2");
    
    /**Resourcebundle to localize the GUI*/
    private MecResourceBundle rb = null;
    
    /**Helpbroker to invoke java help*/
    private CSHManager cshManager = null;
    
    /**actual loaded helpset*/
    private HelpSet helpSet = null;
    
    /**Actual help component*/
    private Help help = null;
    
    /**Flag to show/hide the filter panel*/
    private boolean showFilterPanel = false;
    
    /** Creates new form NewJFrame */
    public AS2Gui( Splash splash ) {
        //Set System default look and feel
        try{
            //support the command line option -Dswing.defaultlaf=...
            if( System.getProperty( "swing.defaultlaf" ) == null ){
                UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
            }
        } catch( Exception e ){
            this.logger.warning( this.getClass().getName() + ":" + e.getMessage() );
        }
        //load resource bundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleAS2Gui.class.getName());
        } catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
        initComponents();
        //set preference values to the GUI
        this.setBounds(
                this.preferences.getInt( PreferencesAS2.FRAME_X ),
                this.preferences.getInt( PreferencesAS2.FRAME_Y ),
                this.preferences.getInt( PreferencesAS2.FRAME_WIDTH ),
                this.preferences.getInt( PreferencesAS2.FRAME_HEIGHT ));
        LogConsolePanel consolePanel = new LogConsolePanel();
        this.jPanelServerLog.add( consolePanel );
        this.setTitle( AS2ServerVersion.getProductName() + " " + AS2ServerVersion.getVersion());
        //initialize the help system if available
        this.initializeJavaHelp();
        this.jTableMessageOverview.getSelectionModel().addListSelectionListener(this);
        this.jTableMessageOverview.getTableHeader().setReorderingAllowed( false );
        //icon columns
        TableColumn column = this.jTableMessageOverview.getColumnModel().getColumn(0);
        column.setMaxWidth( 20 );
        column.setResizable( false );
        column = this.jTableMessageOverview.getColumnModel().getColumn(1);
        column.setMaxWidth( 20 );
        column.setResizable( false );        
        this.jPanelFilterOverview.setVisible( this.showFilterPanel );
        this.jPanelFilterOverview.setVisible( this.showFilterPanel );
        this.jTableMessageOverview.getTableHeader().addMouseListener(new ColumnFitAdapter());
        this.setButtonState();
    }
           
    /**Stores the actual GUIs preferences to restore the GUI at
     *the next program start
     */
    private void savePreferences(){
        this.preferences.putInt( PreferencesAS2.FRAME_X,
                (int)this.getBounds().getX() );
        this.preferences.putInt( PreferencesAS2.FRAME_Y,
                (int)this.getBounds().getY() );
        this.preferences.putInt( PreferencesAS2.FRAME_WIDTH,
                (int)this.getBounds().getWidth() );
        this.preferences.putInt( PreferencesAS2.FRAME_HEIGHT,
                (int)this.getBounds().getHeight() );
    }
    
    /**Initialized a help set by a given name
     */
    private void initializeJavaHelp() {
        try{
            //At the moment only english and german help systems are implemented.
            String filename = null;
            //If the found default is none of them, set the english help as
            //default!
            if( !Locale.getDefault().getLanguage().equals( Locale.GERMANY.getLanguage() )
            && !Locale.getDefault().getLanguage().equals( Locale.US.getLanguage() )){
                this.logger.warning( "Sorry, there is no specific HELPSET available for your language, " );
                this.logger.warning( "the english help will be displayed." );
                filename = "as2help/as2_en.hs";
            } else
                filename = "as2help/as2_" + Locale.getDefault().getLanguage() + ".hs";
            
            File helpSetFile = new File( filename );
            URL helpURL = helpSetFile.toURL();
            this.helpSet = new HelpSet( helpURL );
            this.help = new Help( true, false, true );
            Help.setHelpLocale( Locale.getDefault() );
            help.setIconImage( new ImageIcon(getClass().
                    getResource("/de/mendelson/comm/as2/client/questionmark16x16.gif")).getImage() );
            help.addBook( helpSet );
            help.setDefaultTopicID( "topic_as2_main" );
            try{
                URL url = new File( "./as2_help_favories.xml" ).toURL();
                help.enableFavoritesNavigator( url );
            } catch( MalformedURLException ignore ){
            }
            this.cshManager = new CSHManager( help );
        } catch( Exception e ){
            // could not find it! Disable menu item
            this.logger.warning( "Helpset not found, helpsystem is disabled!" );
            this.jMenuItemHelpSystem.setEnabled( false );
        }
    }
    
    /**Reloads the partner ids with their names and passes these information
     *to the overview table*/
    public void refreshTablePartnerData(){
        try{
            PartnerAccessDB partnerAccess = new PartnerAccessDB( "localhost" );
            Partner[] partner = partnerAccess.getPartner();
            HashMap<String,Partner> partnerMap = new HashMap<String,Partner>();
            for( int i = 0; i < partner.length; i++ )
                partnerMap.put( partner[i].getAS2Identification(), partner[i] );
            ((TableModelMessageOverview)this.jTableMessageOverview.getModel()).passPartner( partnerMap );
            partnerAccess.close();
        } catch( Exception e ){
            //nop
        }
    }
    
    /**Refreshes the message overview list from the database*/
    public void refreshMessageOverviewList(){
        try{
            MessageAccessDB access = new MessageAccessDB( "localhost" );
            MessageOverviewFilter filter = new MessageOverviewFilter();
            filter.setShowFinished( this.jCheckBoxFilterShowOk.isSelected());
            filter.setShowPending( this.jCheckBoxFilterShowPending.isSelected());
            filter.setShowStopped( this.jCheckBoxFilterShowStopped.isSelected());
            AS2MessageInfo[] info = access.getMessageOverview( filter );
            TableModelMessageOverview tableModel
                    = (TableModelMessageOverview)this.jTableMessageOverview.getModel();
            tableModel.passNewData( new ArrayList( Arrays.asList( info )));
            access.close();
        } catch( Exception e ){
            this.logger.severe( "refreshMessageOverviewList: " + e.getMessage() );
        }
    }
    
    /**Displays details for the selected msg row
     */
    private void showSelectedRowDetails(){
        int selectedRow = this.jTableMessageOverview.getSelectedRow();
        if( selectedRow >= 0 ){
            AS2MessageInfo overviewRow
                    = ((TableModelMessageOverview)this.jTableMessageOverview.getModel()).getRow(selectedRow);
            DialogMessageDetails dialog = new DialogMessageDetails( this, overviewRow );
            dialog.setVisible( true );
        }
    }
    
    /**Enables/disables the buttons of this GUI*/
    private void setButtonState(){
        this.jButtonMessageDetails.setEnabled( this.jTableMessageOverview.getSelectedRow() >= 0 );
        this.jButtonDeleteMessage.setEnabled( this.jTableMessageOverview.getSelectedRow() >= 0 );
        //check if min one of the selected rows has the state "stopped" or "finished"
        int[] selectedRows = this.jTableMessageOverview.getSelectedRows();
        AS2MessageInfo[] overviewRows
                = ((TableModelMessageOverview)this.jTableMessageOverview.getModel()).getRows(selectedRows);
        boolean deletableRowSelected = false;
        for( int i = 0; i < overviewRows.length; i++ ){
            if( overviewRows[i].getState() == AS2Message.STATE_FINISHED
                    || overviewRows[i].getState() == AS2Message.STATE_STOPPED ){
                deletableRowSelected = true;
                break;
            }
        }
        this.jButtonDeleteMessage.setEnabled( deletableRowSelected );
        ImageIcon iconBase
                = new ImageIcon( this.getClass().
                getResource( "/de/mendelson/comm/as2/client/filter16x16.gif" ));
        if( this.filterIsSet() ){
            ImageUtil imageUtil = new ImageUtil();
            ImageIcon iconActive
                    = new ImageIcon( this.getClass().
                    getResource( "/de/mendelson/comm/as2/client/mini_filteractive.gif" ));
            this.jButtonFilter.setIcon( imageUtil.mixImages( iconBase, iconActive ));
        } else
            this.jButtonFilter.setIcon( iconBase );
        
    }
    
    /**Returns if a filter is set on the message overview entries*/
    private boolean filterIsSet(){
        return( !this.jCheckBoxFilterShowOk.isSelected()
        || !this.jCheckBoxFilterShowPending.isSelected()
        || !this.jCheckBoxFilterShowStopped.isSelected() );
    }
    
    
    /**Makes this a ListSelectionListener
     */
    public void valueChanged(ListSelectionEvent listSelectionEvent) {
        this.setButtonState();
    }
    
    /**Copies all data from one stream to another*/
    private void copyStreams( InputStream in, OutputStream out ) throws IOException{
        BufferedInputStream inStream = new BufferedInputStream( in );
        BufferedOutputStream outStream = new BufferedOutputStream( out );
        //copy the contents to an output stream
        byte[] buffer = new byte[1024];
        int read = 1024;
        //a read of 0 must be allowed, sometimes it takes time to
        //extract data from the input
        while( read != -1 ){
            read = inStream.read( buffer );
            if( read > 0 )
                outStream.write( buffer, 0, read );
        }
        outStream.flush();
    }
    
    /** Deletes the actual selected PIP rows from the database, filesystem etc
     */
    private void deleteSelectedMessages(){
        int requestValue = JOptionPane.showConfirmDialog(
                this, this.rb.getResourceString( "dialog.msg.delete.message" ),
                this.rb.getResourceString( "dialog.msg.delete.title" ), 
                JOptionPane.YES_NO_OPTION );
        if( requestValue != JOptionPane.YES_OPTION )
            return;
        int[] selectedRows = this.jTableMessageOverview.getSelectedRows();
        AS2MessageInfo[] overviewRows
                = ((TableModelMessageOverview)this.jTableMessageOverview.getModel()).getRows(selectedRows);
        MessageDeleteController controller = new MessageDeleteController( this );
        for( int i = 0; i < overviewRows.length; i++ ){
            controller.deleteMessageFromLog( overviewRows[i]);
        }
    }
    
    /**Manually sends adta for a partner*/
    private void performManualSend(){
        JDialogManualSend dialog = new JDialogManualSend( this );
        dialog.setVisible( true );
    }
    
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jToolBar = new javax.swing.JToolBar();
        jButtonPartner = new javax.swing.JButton();
        jButtonMessageDetails = new javax.swing.JButton();
        jButtonFilter = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jButtonDeleteMessage = new javax.swing.JButton();
        jPanelMain = new javax.swing.JPanel();
        jSplitPane = new javax.swing.JSplitPane();
        jPanelMessageLog = new javax.swing.JPanel();
        jPanelFilterOverview = new javax.swing.JPanel();
        jCheckBoxFilterShowOk = new javax.swing.JCheckBox();
        jCheckBoxFilterShowPending = new javax.swing.JCheckBox();
        jCheckBoxFilterShowStopped = new javax.swing.JCheckBox();
        jLabelFilterShowOk = new javax.swing.JLabel();
        jLabelFilterShowPending = new javax.swing.JLabel();
        jLabelFilterShowError = new javax.swing.JLabel();
        jButtonHideFilter = new javax.swing.JButton();
        jScrollPaneMessageOverview = new javax.swing.JScrollPane();
        jTableMessageOverview = new de.mendelson.util.table.JTableSortable();
        jPanelServerLog = new javax.swing.JPanel();
        jPanelInfo = new javax.swing.JPanel();
        jTextFieldInfo = new javax.swing.JTextField();
        jMenuBar = new javax.swing.JMenuBar();
        jMenuFile = new javax.swing.JMenu();
        jMenuItemManualSend = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JSeparator();
        jMenuItemFilePreferences = new javax.swing.JMenuItem();
        jMenuItemPartner = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JSeparator();
        jMenuItemFileExit = new javax.swing.JMenuItem();
        jMenuHelp = new javax.swing.JMenu();
        jMenuItemHelpAbout = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JSeparator();
        jMenuItemHelpSystem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(new ImageIcon( AS2Gui.class.getResource( "/de/mendelson/comm/as2/client/os_logo16x16.gif")).getImage());
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jToolBar.setFloatable(false);
        jToolBar.setRollover(true);
        jButtonPartner.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/partner/gui/singlepartner16x16.gif")));
        jButtonPartner.setText(this.rb.getResourceString( "menu.file.partner" ));
        jButtonPartner.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPartnerActionPerformed(evt);
            }
        });

        jToolBar.add(jButtonPartner);

        jButtonMessageDetails.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/messagedetails16x16.gif")));
        jButtonMessageDetails.setText(this.rb.getResourceString( "details" ));
        jButtonMessageDetails.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonMessageDetailsActionPerformed(evt);
            }
        });

        jToolBar.add(jButtonMessageDetails);

        jButtonFilter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/filter16x16.gif")));
        jButtonFilter.setText(this.rb.getResourceString( "filter"));
        jButtonFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFilterActionPerformed(evt);
            }
        });

        jToolBar.add(jButtonFilter);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setMaximumSize(new java.awt.Dimension(5, 32767));
        jToolBar.add(jSeparator1);

        jButtonDeleteMessage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/delete_16x16.gif")));
        jButtonDeleteMessage.setText(this.rb.getResourceString( "delete.msg"));
        jButtonDeleteMessage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteMessageActionPerformed(evt);
            }
        });

        jToolBar.add(jButtonDeleteMessage);

        getContentPane().add(jToolBar, java.awt.BorderLayout.NORTH);

        jPanelMain.setLayout(new java.awt.GridBagLayout());

        jSplitPane.setDividerLocation(300);
        jSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jPanelMessageLog.setLayout(new java.awt.GridBagLayout());

        jPanelFilterOverview.setLayout(new java.awt.GridBagLayout());

        jPanelFilterOverview.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jCheckBoxFilterShowOk.setSelected(true);
        jCheckBoxFilterShowOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxFilterShowOkActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 0);
        jPanelFilterOverview.add(jCheckBoxFilterShowOk, gridBagConstraints);

        jCheckBoxFilterShowPending.setSelected(true);
        jCheckBoxFilterShowPending.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxFilterShowPendingActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 0);
        jPanelFilterOverview.add(jCheckBoxFilterShowPending, gridBagConstraints);

        jCheckBoxFilterShowStopped.setSelected(true);
        jCheckBoxFilterShowStopped.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxFilterShowStoppedActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 0);
        jPanelFilterOverview.add(jCheckBoxFilterShowStopped, gridBagConstraints);

        jLabelFilterShowOk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/state_finished16x16.gif")));
        jLabelFilterShowOk.setText(this.rb.getResourceString( "filter.showfinished"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelFilterOverview.add(jLabelFilterShowOk, gridBagConstraints);

        jLabelFilterShowPending.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/state_pending16x16.gif")));
        jLabelFilterShowPending.setText(this.rb.getResourceString( "filter.showpending"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelFilterOverview.add(jLabelFilterShowPending, gridBagConstraints);

        jLabelFilterShowError.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/state_stopped16x16.gif")));
        jLabelFilterShowError.setText(this.rb.getResourceString( "filter.showstopped"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelFilterOverview.add(jLabelFilterShowError, gridBagConstraints);

        jButtonHideFilter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/hide_filter.gif")));
        jButtonHideFilter.setMargin(new java.awt.Insets(0, 0, 0, 0));
        jButtonHideFilter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonHideFilterActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHEAST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelFilterOverview.add(jButtonHideFilter, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHWEST;
        gridBagConstraints.weightx = 1.0;
        jPanelMessageLog.add(jPanelFilterOverview, gridBagConstraints);

        jTableMessageOverview.setModel(new TableModelMessageOverview());
        jTableMessageOverview.setShowHorizontalLines(false);
        jTableMessageOverview.setShowVerticalLines(false);
        jTableMessageOverview.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableMessageOverviewMouseClicked(evt);
            }
        });

        jScrollPaneMessageOverview.setViewportView(jTableMessageOverview);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelMessageLog.add(jScrollPaneMessageOverview, gridBagConstraints);

        jSplitPane.setLeftComponent(jPanelMessageLog);

        jPanelServerLog.setLayout(new java.awt.BorderLayout());

        jSplitPane.setRightComponent(jPanelServerLog);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelMain.add(jSplitPane, gridBagConstraints);

        getContentPane().add(jPanelMain, java.awt.BorderLayout.CENTER);

        jPanelInfo.setLayout(new java.awt.GridBagLayout());

        jTextFieldInfo.setEditable(false);
        jTextFieldInfo.setText("For additional EDI software to convert and process your data please contact mendelson-e-commerce GmbH\n");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        jPanelInfo.add(jTextFieldInfo, gridBagConstraints);

        getContentPane().add(jPanelInfo, java.awt.BorderLayout.SOUTH);

        jMenuFile.setText(this.rb.getResourceString( "menu.file" ));
        jMenuItemManualSend.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/send_16x16.gif")));
        jMenuItemManualSend.setText(this.rb.getResourceString( "menu.file.send"));
        jMenuItemManualSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemManualSendActionPerformed(evt);
            }
        });

        jMenuFile.add(jMenuItemManualSend);

        jMenuFile.add(jSeparator2);

        jMenuItemFilePreferences.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/preferences/preferences16x16.gif")));
        jMenuItemFilePreferences.setText(this.rb.getResourceString( "menu.file.preferences"));
        jMenuItemFilePreferences.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemFilePreferencesActionPerformed(evt);
            }
        });

        jMenuFile.add(jMenuItemFilePreferences);

        jMenuItemPartner.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/partner/gui/singlepartner16x16.gif")));
        jMenuItemPartner.setText(this.rb.getResourceString( "menu.file.partner"));
        jMenuItemPartner.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPartnerActionPerformed(evt);
            }
        });

        jMenuFile.add(jMenuItemPartner);

        jMenuFile.add(jSeparator3);

        jMenuItemFileExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/close16x16.gif")));
        jMenuItemFileExit.setText(this.rb.getResourceString( "menu.file.exit" ));
        jMenuItemFileExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemFileExitActionPerformed(evt);
            }
        });

        jMenuFile.add(jMenuItemFileExit);

        jMenuBar.add(jMenuFile);

        jMenuHelp.setText(this.rb.getResourceString( "menu.help"));
        jMenuItemHelpAbout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/os_logo16x16.gif")));
        jMenuItemHelpAbout.setText(this.rb.getResourceString( "menu.help.about"));
        jMenuItemHelpAbout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemHelpAboutActionPerformed(evt);
            }
        });

        jMenuHelp.add(jMenuItemHelpAbout);

        jMenuHelp.add(jSeparator5);

        jMenuItemHelpSystem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/os_logo16x16.gif")));
        jMenuItemHelpSystem.setText(this.rb.getResourceString( "menu.help.helpsystem" ));
        jMenuItemHelpSystem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemHelpSystemActionPerformed(evt);
            }
        });

        jMenuHelp.add(jMenuItemHelpSystem);

        jMenuBar.add(jMenuHelp);

        setJMenuBar(jMenuBar);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-749)/2, (screenSize.height-581)/2, 749, 581);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemManualSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemManualSendActionPerformed
        this.performManualSend();
    }//GEN-LAST:event_jMenuItemManualSendActionPerformed

    private void jButtonDeleteMessageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteMessageActionPerformed
        this.deleteSelectedMessages();
    }//GEN-LAST:event_jButtonDeleteMessageActionPerformed
    
    private void jButtonHideFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonHideFilterActionPerformed
        this.showFilterPanel = !this.showFilterPanel;
        this.jPanelFilterOverview.setVisible( this.showFilterPanel );
    }//GEN-LAST:event_jButtonHideFilterActionPerformed
    
    private void jCheckBoxFilterShowOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxFilterShowOkActionPerformed
        this.refreshMessageOverviewList();
        this.setButtonState();
    }//GEN-LAST:event_jCheckBoxFilterShowOkActionPerformed
    
    private void jCheckBoxFilterShowPendingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxFilterShowPendingActionPerformed
        this.refreshMessageOverviewList();
        this.setButtonState();
    }//GEN-LAST:event_jCheckBoxFilterShowPendingActionPerformed
    
    private void jCheckBoxFilterShowStoppedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxFilterShowStoppedActionPerformed
        this.refreshMessageOverviewList();
        this.setButtonState();
    }//GEN-LAST:event_jCheckBoxFilterShowStoppedActionPerformed
    
    private void jButtonFilterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFilterActionPerformed
        this.showFilterPanel = !this.showFilterPanel;
        this.jPanelFilterOverview.setVisible( this.showFilterPanel );
    }//GEN-LAST:event_jButtonFilterActionPerformed
    
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        this.savePreferences();
    }//GEN-LAST:event_formWindowClosing
            
    private void jButtonPartnerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPartnerActionPerformed
        JDialogPartnerConfig dialog = new JDialogPartnerConfig(this);
        dialog.setVisible( true );
        this.refreshTablePartnerData();
    }//GEN-LAST:event_jButtonPartnerActionPerformed
    
    private void jButtonMessageDetailsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonMessageDetailsActionPerformed
        this.showSelectedRowDetails();
    }//GEN-LAST:event_jButtonMessageDetailsActionPerformed
        
    private void jTableMessageOverviewMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableMessageOverviewMouseClicked
        //double click on a row
        if( evt.getClickCount() == 2 ){
            this.showSelectedRowDetails();
        }
    }//GEN-LAST:event_jTableMessageOverviewMouseClicked
    
    private void jMenuItemFilePreferencesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemFilePreferencesActionPerformed
        JDialogPreferences dialog = new JDialogPreferences( this );
        dialog.setVisible( true );
    }//GEN-LAST:event_jMenuItemFilePreferencesActionPerformed
    
    private void jMenuItemPartnerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPartnerActionPerformed
        JDialogPartnerConfig dialog = new JDialogPartnerConfig(this);
        dialog.setVisible( true );
        this.refreshTablePartnerData();
    }//GEN-LAST:event_jMenuItemPartnerActionPerformed
    
    private void jMenuItemHelpSystemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemHelpSystemActionPerformed
        this.help.showNavigatorWindow();
    }//GEN-LAST:event_jMenuItemHelpSystemActionPerformed
    
    private void jMenuItemHelpAboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemHelpAboutActionPerformed
        AboutDialog dialog = new AboutDialog( this );
        dialog.setVisible( true );
    }//GEN-LAST:event_jMenuItemHelpAboutActionPerformed
    
    private void jMenuItemFileExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemFileExitActionPerformed
        this.savePreferences();
        this.setVisible( false );
        System.exit(0);
    }//GEN-LAST:event_jMenuItemFileExitActionPerformed
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonDeleteMessage;
    private javax.swing.JButton jButtonFilter;
    private javax.swing.JButton jButtonHideFilter;
    private javax.swing.JButton jButtonMessageDetails;
    private javax.swing.JButton jButtonPartner;
    private javax.swing.JCheckBox jCheckBoxFilterShowOk;
    private javax.swing.JCheckBox jCheckBoxFilterShowPending;
    private javax.swing.JCheckBox jCheckBoxFilterShowStopped;
    private javax.swing.JLabel jLabelFilterShowError;
    private javax.swing.JLabel jLabelFilterShowOk;
    private javax.swing.JLabel jLabelFilterShowPending;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu jMenuFile;
    private javax.swing.JMenu jMenuHelp;
    private javax.swing.JMenuItem jMenuItemFileExit;
    private javax.swing.JMenuItem jMenuItemFilePreferences;
    private javax.swing.JMenuItem jMenuItemHelpAbout;
    private javax.swing.JMenuItem jMenuItemHelpSystem;
    private javax.swing.JMenuItem jMenuItemManualSend;
    private javax.swing.JMenuItem jMenuItemPartner;
    private javax.swing.JPanel jPanelFilterOverview;
    private javax.swing.JPanel jPanelInfo;
    private javax.swing.JPanel jPanelMain;
    private javax.swing.JPanel jPanelMessageLog;
    private javax.swing.JPanel jPanelServerLog;
    private javax.swing.JScrollPane jScrollPaneMessageOverview;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSplitPane jSplitPane;
    private de.mendelson.util.table.JTableSortable jTableMessageOverview;
    private javax.swing.JTextField jTextFieldInfo;
    private javax.swing.JToolBar jToolBar;
    // End of variables declaration//GEN-END:variables
    
}
@
