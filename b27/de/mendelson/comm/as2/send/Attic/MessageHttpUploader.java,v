head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.49.12;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2009.03.16.12.06.54;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b27/de/mendelson/comm/as2/send/MessageHttpUploader.java,v 1.1 2009/03/16 12:06:54 heller Exp $
package de.mendelson.comm.as2.send;

import de.mendelson.comm.as2.client.rmi.GenericClient;
import de.mendelson.comm.as2.clientserver.ErrorObject;
import de.mendelson.comm.as2.clientserver.message.RefreshClientMessageOverviewList;
import de.mendelson.comm.as2.clientserver.serialize.CommandObjectIncomingMessage;
import de.mendelson.comm.as2.message.AS2Message;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.MessageAccessDB;
import de.mendelson.comm.as2.partner.HTTPAuthentication;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.comm.as2.statistic.QuotaAccessDB;
import de.mendelson.util.MecResourceBundle;
import de.mendelson.util.clientserver.AbstractServer;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.Security;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpVersion;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;


/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Class to allow HTTP multipart uploads
 * @@author  S.Heller
 * @@version $Revision: 1.1 $
 */
public class MessageHttpUploader {

    private Logger logger = Logger.getLogger("de.mendelson.as2.server");
    private PreferencesAS2 preferences = new PreferencesAS2();
    /**localisze the GUI*/
    private MecResourceBundle rb = null;
    /**The header that has been built fro the request*/
    private Properties requestHeader = new Properties();
    /**remote answer*/
    private byte[] responseData = null;
    /**remote answer header*/
    private Header[] responseHeader = null;
    private AbstractServer abstractServer;

    /** Creates new message uploader instance
     * @@param hostname Name of the host to connect to
     * @@param username Name of the user that will connect to the remote ftp server
     * @@param password password to connect to the ftp server
     */
    public MessageHttpUploader(AbstractServer abstractServer) throws Exception {
        this.abstractServer = abstractServer;
        //Load default resourcebundle
        try {
            this.rb = (MecResourceBundle) ResourceBundle.getBundle(
                    ResourceBundleHttpUploader.class.getName());
        } //load up resourcebundle
        catch (MissingResourceException e) {
            throw new RuntimeException("Oops..resource bundle " + e.getClassName() + " not found.");
        }
        System.setProperty("java.protocol.handler.pkgs",
                "com.sun.net.ssl.internal.www.protocol");
        System.setProperty("javax.net.ssl.trustStore",
                this.preferences.get(PreferencesAS2.KEYSTORE_HTTPS_SEND));
        System.setProperty("javax.net.ssl.trustStorePassword",
                this.preferences.get(PreferencesAS2.KEYSTORE_HTTPS_SEND_PASS));
        System.setProperty("javax.net.ssl.keyStore",
                this.preferences.get(PreferencesAS2.KEYSTORE_HTTPS_SEND));
        System.setProperty("javax.net.ssl.keyStorePassword",
                this.preferences.get(PreferencesAS2.KEYSTORE_HTTPS_SEND_PASS));

        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
    }

    /**Returns the created header for the sent data*/
    public Properties upload(AS2Message message, Partner sender, Partner receiver) throws Exception {
        NumberFormat formatter = new DecimalFormat("0.00");
        AS2MessageInfo info = message.getMessageInfo();
        MessageAccessDB messageAccess = new MessageAccessDB("localhost");
        messageAccess.insertMessage(message);
        this.abstractServer.broadcastToClients(new RefreshClientMessageOverviewList());
        long startTime = System.currentTimeMillis();
        int returnCode = this.performUpload(message, sender, receiver);
        long size = message.getRawData().length;
        long transferTime = System.currentTimeMillis()-startTime;
        float bytePerSec = (float)((float)size*1000f/(float)transferTime);
        float kbPerSec = (float)(bytePerSec/1024f);
        if (returnCode == HttpServletResponse.SC_OK) {
            this.logger.log(Level.INFO,
                    this.rb.getResourceString("returncode.ok",
                    new Object[]{
                        info.getMessageId(),
                        String.valueOf(returnCode), 
                        AS2Message.getDataSizeDisplay(size),
                        AS2Message.getTimeDisplay(transferTime),
                        formatter.format(kbPerSec),
                    }), info);
        } else if (returnCode == HttpServletResponse.SC_ACCEPTED) {
            this.logger.log(Level.INFO,
                    this.rb.getResourceString("returncode.accepted",
                    new Object[]{
                        info.getMessageId(),
                        String.valueOf(returnCode),
                        AS2Message.getDataSizeDisplay(size),
                        AS2Message.getTimeDisplay(transferTime),
                        formatter.format(kbPerSec),
                    }), info);
        } else {
            messageAccess.setMessageState(info.getMessageId(), AS2Message.STATE_STOPPED);
            if (returnCode < 0) {
                throw new Exception(this.rb.getResourceString("error.noconnection", info.getMessageId()));
            }
            throw new Exception(info.getMessageId() + ": HTTP " + returnCode);
        }
        if (message.getMessageInfo().isMDN()) {
            //ASYNC MDN sent: insert an entry into the statistic table
            QuotaAccessDB.incReceivedMessages("localhost", message.getMessageInfo().getSenderId(),
                    message.getMessageInfo().getReceiverId(), message.getMessageInfo().getState(), message.getMessageInfo().getRelatedMessageId());
        }
        //inform the server of the result if a sync MDN has been requested
        if (message.getMessageInfo().requestsSyncMDN()) {
            //perform a check if the answer really contains a MDN or is just an empty HTTP 200 with some header data
            //this check looks for the existance of some key header values
            boolean as2FromExists = false;
            boolean as2ToExists = false;
            for (int i = 0; i < this.responseHeader.length; i++) {
                String key = this.responseHeader[i].getName();
                if (key.toLowerCase().equals("as2-to")) {
                    as2ToExists = true;
                } else if (key.toLowerCase().equals("as2-from")) {
                    as2FromExists = true;
                }
            }
            if (!as2ToExists) {
                throw new Exception(this.rb.getResourceString("answer.no.sync.mdn",
                        new Object[]{info.getMessageId(), "as2-to"}));
            }
            //send the data to the as2 server. It does not care if the MDN has been sync or async anymore
            GenericClient client = new GenericClient();
            CommandObjectIncomingMessage commandObject = new CommandObjectIncomingMessage();
            commandObject.setMessageData(this.responseData);
            for (int i = 0; i < this.responseHeader.length; i++) {
                String key = this.responseHeader[i].getName();
                String value = this.responseHeader[i].getValue();
                commandObject.addHeader(key, value);
                if (key.toLowerCase().equals("content-type")) {
                    commandObject.setContentType(value);
                }
            }
            //compatibility issue: some systems do not send a as2-from in the sync case
            if (!as2FromExists) {
                commandObject.addHeader("as2-from", receiver.getAS2Identification());
            }
            ErrorObject errorObject = client.send(commandObject);
            if (errorObject.getErrors() > 0) {
                messageAccess.setMessageState(info.getMessageId(),
                        AS2Message.STATE_STOPPED);
            }
        }
        messageAccess.close();
        return (this.requestHeader);
    }

    /**Sets necessary HTTP authentication for this partner, depending on if it is an asny MDN that will be sent or an AS2 message.
     *If the partner is not configured to use HTTP authentication in any kind nothing will happen in here
     */
    private void setHTTPAuthentication(HttpClient client, Partner receiver, boolean isMDN) {
        HTTPAuthentication authentication = null;
        if (isMDN) {
            authentication = receiver.getAuthenticationAsyncMDN();
        } else {
            authentication = receiver.getAuthentication();
        }
        if (authentication != null) {
            Credentials defaultcreds = new UsernamePasswordCredentials(authentication.getUser(), authentication.getPassword());
            client.getState().setCredentials(AuthScope.ANY, defaultcreds);
        }
    }

    /**Sets the proxy authentification for the client*/
    private void setProxy(HttpClient client, AS2Message message) {
        //the proxy authentification
        if (this.preferences.getBoolean(PreferencesAS2.PROXY_USE)) {
            Credentials credentials = null;
            String proxyhost = this.preferences.get(PreferencesAS2.PROXY_HOST);
            int proxyport = this.preferences.getInt(PreferencesAS2.PROXY_PORT);
            client.getHostConfiguration().setProxy(proxyhost, proxyport);
            if (this.preferences.getBoolean(PreferencesAS2.AUTH_PROXY_USE)) {
                String user = this.preferences.get(PreferencesAS2.AUTH_PROXY_USER);
                String pass = this.preferences.get(PreferencesAS2.AUTH_PROXY_PASS);
                credentials = new UsernamePasswordCredentials(user, pass);
            }
            client.getState().setProxyCredentials(
                    new AuthScope(proxyhost, proxyport, AuthScope.ANY_REALM), credentials);
            this.logger.log(Level.INFO,
                    this.rb.getResourceString("using.proxy",
                    new Object[]{
                        message.getMessageInfo().getMessageId(),
                        proxyhost, String.valueOf(proxyport),
                    }), message.getMessageInfo());
        }
    }

    /**Uploads the data, returns the HTTP result code*/
    private int performUpload(AS2Message message, Partner sender, Partner receiver) {
        PostMethod filePost = null;
        int status = -1;
        try {
            //find out the URL where to send the message to
            URL receiptURL = null;
            //async MDN requested?
            if (message.getMessageInfo().isMDN()) {
                receiptURL = new URL(message.getMessageInfo().getAsyncMDNURL());
            } else {
                receiptURL = new URL(receiver.getURL());
            }
            filePost = new PostMethod(receiptURL.toExternalForm());
            filePost.getParams().setParameter(HttpMethodParams.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            filePost.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new PostHttpMethodRetryHandler());
            filePost.getParams().setParameter(HttpMethodParams.USE_EXPECT_CONTINUE, Boolean.valueOf(true));
            filePost.getParams().setParameter(HttpMethodParams.USER_AGENT, message.getMessageInfo().getUserAgent());
            filePost.addRequestHeader("AS2-Version", "1.1");
            filePost.addRequestHeader("mime-version", "1.0");
            filePost.addRequestHeader("recipient-address", receiptURL.toExternalForm());
            filePost.addRequestHeader("message-id", "<" + message.getMessageInfo().getMessageId() + ">");
            filePost.addRequestHeader("as2-from", sender.getAS2Identification());
            filePost.addRequestHeader("as2-to", receiver.getAS2Identification());
            String originalFilename = null;
            if (message.getPayloads() != null && message.getPayloads().length > 0) {
                originalFilename = message.getPayloads()[0].getOriginalFilename();
            }
            if (originalFilename != null) {
                filePost.addRequestHeader("subject", this.replace(message.getMessageInfo().getSubject(),
                        "${filename}", originalFilename));
            } else {
                filePost.addRequestHeader("subject", message.getMessageInfo().getSubject());
            }
            filePost.addRequestHeader("from", sender.getEmail());
            filePost.addRequestHeader("connection", "close, TE");
            DateFormat format = new SimpleDateFormat("EE, dd MMM yyyy HH:mm:ss zz");
            filePost.addRequestHeader("date", format.format(new Date()));
            if (message.getMessageInfo().getEncryptionType() != AS2Message.ENCRYPTION_NONE) {
                filePost.addRequestHeader("Content-Type", "application/pkcs7-mime; smime-type=enveloped-data; name=smime.p7m");
            } else {
                filePost.addRequestHeader("Content-Type", message.getContentType());
            }
            //MDN header, this is always the way for async MDNs
            if (message.getMessageInfo().isMDN()) {
                this.logger.log(Level.INFO,
                        this.rb.getResourceString("sending.mdn.async",
                        new Object[]{
                            message.getMessageInfo().getMessageId(),
                            message.getMessageInfo().getAsyncMDNURL()
                        }), message.getMessageInfo());
                filePost.addRequestHeader("Server", message.getMessageInfo().getUserAgent());
            } else {
                //outbound AS2 message
                if (message.getMessageInfo().requestsSyncMDN()) {
                    this.logger.log(Level.INFO,
                            this.rb.getResourceString("sending.msg.sync",
                            new Object[]{
                                message.getMessageInfo().getMessageId(),
                                receiver.getURL()
                            }), message.getMessageInfo());
                } else {
                    //ASYNC
                    this.logger.log(Level.INFO,
                            this.rb.getResourceString("sending.msg.async",
                            new Object[]{
                                message.getMessageInfo().getMessageId(),
                                receiver.getURL(), sender.getMdnURL()
                            }), message.getMessageInfo());
                    filePost.addRequestHeader("receipt-delivery-option", sender.getMdnURL());
                }
                filePost.addRequestHeader("disposition-notification-to", sender.getMdnURL());
                //request a signed MDN if this is set up in the partner configuration
                if (receiver.isSignedMDN()) {
                    filePost.addRequestHeader("disposition-notification-options",
                            message.getMessageInfo().getDispositionNotificationOptions().getHeaderValue());
                }
                if (message.getMessageInfo().getSignType() != AS2Message.SIGNATURE_NONE) {
                    filePost.addRequestHeader("content-disposition", "attachment; filename=\"smime.p7m\"");
                }else if (message.getMessageInfo().getSignType() == AS2Message.SIGNATURE_NONE
                        && message.getMessageInfo().getSignType() == AS2Message.ENCRYPTION_NONE ){
                    filePost.addRequestHeader("content-disposition", "attachment; filename=\""+ message.getPayload(0).getOriginalFilename()+ "\"");
                }
            }
            int port = receiptURL.getPort();
            if (port == -1) {
                port = receiptURL.getDefaultPort();
            }
            filePost.addRequestHeader("host", receiptURL.getHost() + ":" + port);
            filePost.setRequestEntity(new ByteArrayRequestEntity(message.getRawData()));
            HttpClient client = new HttpClient();
            this.setProxy(client, message);
            this.setHTTPAuthentication(client, receiver, message.getMessageInfo().isMDN());
            client.getHttpConnectionManager().getParams().setConnectionTimeout(this.preferences.getInt(PreferencesAS2.HTTP_SEND_TIMEOUT));
            //tcp socket: no timeout
            status = client.executeMethod(filePost);
            this.responseData = filePost.getResponseBody();
            this.responseHeader = filePost.getResponseHeaders();
            for (Header singleHeader : filePost.getRequestHeaders()) {
                this.requestHeader.setProperty(singleHeader.getName(), singleHeader.getValue());
            }
            //HTTP 200 (OK), HTTP 202 (ACCEPTED)
            //HTTP 200 should be enough here but some systems seem to answer with HTTP 202.
            if (status != HttpServletResponse.SC_OK && status != HttpServletResponse.SC_ACCEPTED) {
                this.logger.severe(
                        this.rb.getResourceString("error.httpupload",
                        new Object[]{message.getMessageInfo().getMessageId(),
                            URLDecoder.decode(filePost.getStatusText(), "UTF-8")
                        }));
            }
        } catch (Exception ex) {
            this.logger.severe(message.getMessageInfo().getMessageId() + ": [" + ex.getClass().getSimpleName() + "]@@MessageHttpUploader.performUpload " + ex.getMessage());
        } finally {
            if (filePost != null) {
                filePost.releaseConnection();
            }
        }
        return (status);
    }

    /** Replaces the string tag by the string replacement in the sourceString
     * @@param source Source string
     * @@param tag	String that will be replaced
     * @@param replacement String that will replace the tag
     * @@return String that contains the replaced values
     */
    private String replace(String source, String tag, String replacement) {
        if (source == null) {
            return null;
        }
        StringBuffer buffer = new StringBuffer();
        while (true) {
            int index = source.indexOf(tag);
            if (index == -1) {
                buffer.append(source);
                return (buffer.toString());
            }
            buffer.append(source.substring(0, index));
            buffer.append(replacement);
            source = source.substring(index + tag.length());
        }
    }

    /**Returns the version of this class*/
    public static String getVersion() {
        String revision = "$Revision: 1.1 $";
        return (revision.substring(revision.indexOf(":") + 1,
                revision.lastIndexOf("$")).trim());
    }

    /**Handler class for customized retry behavior*/
    public class PostHttpMethodRetryHandler implements HttpMethodRetryHandler {

        public boolean retryMethod(final HttpMethod method,
                final IOException exception,
                int executionCount) {
//            if (executionCount >= 5) {
//                // Do not retry if over max retry count
//                return false;
//            }
//            if (exception instanceof NoHttpResponseException) {
//                // Retry if the server dropped connection on us
//                return true;
//            }
//            if (!method.isRequestSent()) {
//                // Retry if the request has not been sent fully or
//                // if it's OK to retry methods that have been sent
//                return true;
//            }
            // otherwise do not retry
            return false;
        }
    }
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/send/MessageHttpUploader.java 83    21.01.09 16:35 Heller $
d55 1
a55 1
 * @@version $Revision: 83 $
d374 1
a374 1
        String revision = "$Revision: 83 $";
@

