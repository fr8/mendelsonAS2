head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.49.09;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2009.03.16.12.06.49;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b27/de/mendelson/comm/as2/jms/MessageQueueServer.java,v 1.1 2009/03/16 12:06:49 heller Exp $
package de.mendelson.comm.as2.jms;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.util.MecResourceBundle;

import java.util.logging.*;
import java.io.*;
import java.util.*;
import javax.naming.*;

import fr.dyade.aaa.agent.*;
import java.net.InetAddress;
import javax.jms.JMSException;
import org.objectweb.joram.client.jms.admin.*;
import org.objectweb.joram.client.jms.*;
import org.objectweb.joram.client.jms.tcp.*;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */

/**
 * Server for the message queue
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class MessageQueueServer {
    
    private Logger logger   = Logger.getLogger( "de.mendelson.as2.server" );
    private Properties jndiProperties = null;
    
    /**Server preferences*/
    PreferencesAS2 preferences = new PreferencesAS2();
    
    private MecResourceBundle rb = null;
    
    
    /** Creates a new instance of MessageQueueServer */
    public MessageQueueServer() {
        this.jndiProperties = this.getDefaultJNDIProperties();
        //Load default resourcebundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleMessageQueueServer.class.getName());
        }
        //load up resourcebundle
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
    }
    
    
    /**Sets the directory servive properties, the server will not start up without them
     */
    private Properties getDefaultJNDIProperties(){
        Properties properties = new Properties();
        properties.setProperty( "java.naming.factory.initial",
                "fr.dyade.aaa.jndi2.client.NamingContextFactory" );
        properties.setProperty( "java.naming.factory.host", "localhost" );
        properties.setProperty( "java.naming.factory.port",
                this.preferences.get( PreferencesAS2.JNDI_PORT ));
        return( properties );
    }
    
    /**Shuts down the messaging server*/
    public void shutdownServer() throws Exception{
        javax.naming.Context context
                = new javax.naming.InitialContext( this.jndiProperties );
        try{
            //check if the queue is already running
            System.out.println( "MsgQueue: Running message queue server found, shutting down." );
            int port = Integer.valueOf( this.preferences.get( PreferencesAS2.MQ_PROXY_PORT )).intValue();
            AdminModule.connect( "localhost", port, "root", "root", 60);
            AdminModule.stopServer();
            AdminModule.disconnect();
            context.close();
        } catch( NamingException e ){
            //superb, no running server found
        } finally{
            System.out.println( "MsgQueue: Message queue server stopped." );
        }
        //ensure no server is running
        if( AgentServer.getStatus() != 0 ){
            AgentServer.stop();
        }
    }
    
    /**Returns the JMS provider in the format <provider>_<majorversion>.<minorversion>
     */
    public static String getProviderVersion() throws JMSException{
        ConnectionMetaData metaData = new ConnectionMetaData();
        String provider = metaData.getJMSProviderName();
        String version = metaData.getProviderVersion();
        return( provider + "_" + version );
    }
    
    /**Returns the host address of the mq server*/
    public static String getHostAddress() throws Exception{
        InetAddress host = InetAddress.getLocalHost();
        return( host.getHostAddress() );
    }
    
    /**Starts up the message queue server*/
    public void startup() throws Exception{
        //the persistence directory may not reused if the jms provider has been changed or the ip has been changed,
        //else serialization problems may occur
        String persistenceDir = "./mq_persist_" + getProviderVersion() + "_" + getHostAddress();
        File lockFile = new File( persistenceDir + File.separator + "lock" );
        if( lockFile.exists()){
            boolean success = lockFile.delete();
            if( !success ){
                throw new RuntimeException( "JMS Server: Unable to delete lock file " + lockFile.getAbsolutePath() + ".");
            }
        }
        System.setProperty( AgentServer.CFG_DIR_PROPERTY, "." );
        AgentServer server = new AgentServer();        
        server.main( new String[]{ "0", new File( persistenceDir ).getAbsolutePath()});
        logger.info( this.rb.getResourceString( "queueserver.state",
                new Object[]{ server.getStatusInfo(), server.getHostname((short)0) }));
    }
    
    /**Creates a queue and user on the running server*/
    public void setupQueue() throws Exception{
        javax.naming.Context context
                = new javax.naming.InitialContext( this.jndiProperties );
        int proxyPort = this.preferences.getInt( PreferencesAS2.MQ_PROXY_PORT );
        AdminModule.connect( "localhost", proxyPort, "root", "defaultpass", 60);
        
        org.objectweb.joram.client.jms.Queue messagequeue = (org.objectweb.joram.client.jms.Queue)org.objectweb.joram.client.jms.Queue.create(0);
        User user = User.create("as2user", "messagequeuepasswd", 0);
        messagequeue.setReader( user );
        messagequeue.setWriter( user );
        //has to be bound to adapter - even if this may result in problems if the adapter is disconnected
        String localname = InetAddress.getLocalHost().getCanonicalHostName();
        String ipAddress = InetAddress.getByName( localname ).getHostAddress();
        javax.jms.ConnectionFactory connectionFactory =
                TcpConnectionFactory.create(ipAddress, proxyPort);
        javax.jms.QueueConnectionFactory queueConnectionFactory =
                QueueTcpConnectionFactory.create(ipAddress, proxyPort);
        
        context.rebind("queueConnection", connectionFactory);
        context.rebind("queueFactory", queueConnectionFactory);
        context.rebind("as2_messagequeue", messagequeue);
        context.close();
        AdminModule.disconnect();
        this.logger.info( this.rb.getResourceString( "queue.set.up", messagequeue.getName() ));
    }
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/jms/MessageQueueServer.java 10    13.08.08 14:03 Heller $
d28 1
a28 1
 * @@version $Revision: 10 $
@

