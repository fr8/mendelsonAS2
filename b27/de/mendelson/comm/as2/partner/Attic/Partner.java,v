head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.49.11;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2009.03.16.12.06.51;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b27/de/mendelson/comm/as2/partner/Partner.java,v 1.1 2009/03/16 12:06:51 heller Exp $
package de.mendelson.comm.as2.partner;

import de.mendelson.comm.as2.message.AS2Message;
import java.io.Serializable;
import java.util.StringTokenizer;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */

/**
 * Stores all information about a business partner
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class Partner implements Serializable, Comparable {

    /**Unique id in the database for this partner
     */
    private int dbId = -1;
    private boolean localStation = false;
    /**Found identification in the message*/
    private String as2Identification;
    /**Name of the partner
     */
    private String name;
    private String signAlias;
    private String cryptAlias;
    private int encryptionType = AS2Message.ENCRYPTION_3DES;
    private int signType = AS2Message.SIGNATURE_SHA1;
    private String email = "sender@@as2server.com";
    private String url = this.getDefaultURL();
    private String subject = "AS2 message";
    private String contentType = "application/EDI-Consent";
    private String mdnURL = this.getDefaultURL();
    private boolean syncMDN = true;
    private String[] pollIgnoreList = null;
    /**Direcrtory poll interval in seconds*/
    private int pollInterval = 10;
    /**Compression type for this partner, used if you send messages to the partner*/
    private int compressionType = AS2Message.COMPRESSION_NONE;
    /**MDNs to this partner should be signed?*/
    private boolean signedMDN = true;
    /**Command tht should be executed on message receipt for this partner
     */
    private String commandOnReceipt = null;
    /**Indicates if the command should be executed on receipt of a message
     */
    private boolean useCommandOnReceipt = false;
    /**Stores an async MDN HTTP authentication if requested
     */
    private HTTPAuthentication authenticationAsyncMDN = null;
    /**Stores a send data HTTP authentication if requested
     */
    private HTTPAuthentication authentication = null;
    private boolean keepFilenameOnReceipt = false;
    /**Command that should be executed on message send for this partner / error
     */
    private String commandOnSendError = null;
    /**Indicates if the command should be executed on send of a message / error
     */
    private boolean useCommandOnSendError = false;
    /**Command that should be executed on message send for this partner / success
     */
    private String commandOnSendSuccess = null;
    /**Indicates if the command should be executed on send of a message / success
     */
    private boolean useCommandOnSendSuccess = false;
    /**Contains a commment on the partner. This information is not AS2 protocol relevant
     * 
     */
    private String comment = null;

    public Partner() {
    }

    /**Returns a list of file patterns that will be ignored by the dir poll manager*/
    public String[] getPollIgnoreList() {
        return (this.pollIgnoreList);
    }

    /**Pass the poll ignore list to the partner*/
    public void setPollIgnoreList(String[] pollIgnoreList) {
        this.pollIgnoreList = pollIgnoreList;
    }

    /**Expected is a comma separated list of poll ignores*/
    public void setPollIgnoreListString(String pollIgnoreStr) {
        if (pollIgnoreStr == null) {
            this.pollIgnoreList = null;
            return;
        }
        StringTokenizer tokenizer = new StringTokenizer(pollIgnoreStr, ",");
        this.pollIgnoreList = new String[tokenizer.countTokens()];
        for (int i = 0; tokenizer.hasMoreTokens(); i++) {
            this.pollIgnoreList[i] = tokenizer.nextToken();
        }
    }

    /**Returns a String that contains a comma separated list of dir poll manager ignore patterns*/
    public String getPollIgnoreListAsString() {
        if (this.pollIgnoreList == null) {
            return (null);
        }
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < this.pollIgnoreList.length; i++) {
            buffer.append(this.pollIgnoreList[i]);
            if (i + 1 < this.pollIgnoreList.length) {
                buffer.append(",");
            }
        }
        return (buffer.toString());
    }

    /**Returns the default URL where to connect to
     */
    public String getDefaultURL() {
        return ("http://www.mendelson.de:8080/mec_as2/HttpReceiver");
    }

    public int getDBId() {
        return dbId;
    }

    public void setDBId(int dbId) {
        this.dbId = dbId;
    }

    public boolean isLocalStation() {
        return localStation;
    }

    public void setLocalStation(boolean localStation) {
        this.localStation = localStation;
    }

    public String getAS2Identification() {
        return as2Identification;
    }

    public void setAS2Identification(String as2Identification) {
        this.as2Identification = as2Identification;
    }

    public String getName() {
        if (this.name == null) {
            return (this.getAS2Identification());
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEncryptionType() {
        return encryptionType;
    }

    public void setEncryptionType(int encryptionType) {
        this.encryptionType = encryptionType;
    }

    public int getSignType() {
        return signType;
    }

    public void setSignType(int signType) {
        this.signType = signType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getURL() {
        return url;
    }

    public void setURL(String url) {
        this.url = url;
    }

    public String getMdnURL() {
        return mdnURL;
    }

    public void setMdnURL(String mdnURL) {
        this.mdnURL = mdnURL;
    }

    @@Override
    public String toString() {
        if (this.name != null) {
            return (this.name);
        }
        return (this.as2Identification);
    }

    @@Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.localStation ? 1 : 0);
        hash = 41 * hash + (this.as2Identification != null ? this.as2Identification.hashCode() : 0);
        hash = 41 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 41 * hash + (this.signAlias != null ? this.signAlias.hashCode() : 0);
        hash = 41 * hash + (this.cryptAlias != null ? this.cryptAlias.hashCode() : 0);
        hash = 41 * hash + this.encryptionType;
        hash = 41 * hash + (this.email != null ? this.email.hashCode() : 0);
        hash = 41 * hash + (this.url != null ? this.url.hashCode() : 0);
        hash = 41 * hash + (this.subject != null ? this.subject.hashCode() : 0);
        hash = 41 * hash + (this.contentType != null ? this.contentType.hashCode() : 0);
        hash = 41 * hash + (this.mdnURL != null ? this.mdnURL.hashCode() : 0);
        hash = 41 * hash + (this.syncMDN ? 1 : 0);
        return hash;
    }

    /**Overwrite the equal method of object
     *@@param anObject object ot compare
     */
    @@Override
    public boolean equals(Object anObject) {
        if (anObject == this) {
            return (true);
        }
        if (anObject != null && anObject instanceof Partner) {
            Partner partner = (Partner) anObject;
            return (partner.dbId == this.dbId);
        }
        return (false);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public boolean isSyncMDN() {
        return syncMDN;
    }

    public void setSyncMDN(boolean syncMDN) {
        this.syncMDN = syncMDN;
    }

    public int getPollInterval() {
        return (this.pollInterval);
    }

    public void setPollInterval(int pollInterval) {
        if (pollInterval > 0) {
            this.pollInterval = pollInterval;
        }
    }

    public int getCompressionType() {
        return compressionType;
    }

    public void setCompressionType(int compressionType) {
        this.compressionType = compressionType;
    }

    public boolean isSignedMDN() {
        return signedMDN;
    }

    public void setSignedMDN(boolean signedMDN) {
        this.signedMDN = signedMDN;
    }

    public String getCommandOnReceipt() {
        return commandOnReceipt;
    }

    public void setCommandOnReceipt(String commandOnReceipt) {
        this.commandOnReceipt = commandOnReceipt;
    }

    public boolean useCommandOnReceipt() {
        return useCommandOnReceipt;
    }

    public void setUseCommandOnReceipt(boolean useCommandOnReceipt) {
        this.useCommandOnReceipt = useCommandOnReceipt;
    }

    public HTTPAuthentication getAuthenticationAsyncMDN() {
        return authenticationAsyncMDN;
    }

    public void setAuthenticationAsyncMDN(HTTPAuthentication authenticationAsyncMDN) {
        this.authenticationAsyncMDN = authenticationAsyncMDN;
    }

    public HTTPAuthentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(HTTPAuthentication authentication) {
        this.authentication = authentication;
    }

    public String getSignAlias() {
        return signAlias;
    }

    public void setSignAlias(String signAlias) {
        this.signAlias = signAlias;
    }

    public String getCryptAlias() {
        return cryptAlias;
    }

    public void setCryptAlias(String cryptAlias) {
        this.cryptAlias = cryptAlias;
    }

    public void setKeepOriginalFilenameOnReceipt(boolean keepFilenameOnReceipt) {
        this.keepFilenameOnReceipt = keepFilenameOnReceipt;
    }

    public boolean getKeepOriginalFilenameOnReceipt() {
        return (this.keepFilenameOnReceipt);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        if (comment == null || comment.length() == 0) {
            this.comment = null;
        } else {
            this.comment = comment;
        }
    }

    public int compare(Object one, Object two) {
        Partner obj1 = (Partner) one;
        Partner obj2 = (Partner) two;
        return (obj1.getName().compareToIgnoreCase(obj2.getName()));
    }

    @@Override
    public int compareTo(Object obj) {
        Partner partner = (Partner) obj;
        return (this.name.compareToIgnoreCase(partner.name));
    }

    public int getNotifySend() {
        return (0);
    }

    public void setNotifySend(int notifySend) {
    }

    public int getNotifyReceive() {
        return (0);
    }

    public void setNotifyReceive(int notifyReceive) {
    }

    public int getNotifySendReceive() {
        return (0);
    }

    public void setNotifySendReceive(int notifySendReceive) {
    }

    public boolean isNotifySendEnabled() {
        return (false);
    }

    public void setNotifySendEnabled(boolean notifySendEnabled) {
    }

    public boolean isNotifyReceiveEnabled() {
        return (false);
    }

    public void setNotifyReceiveEnabled(boolean notifyReceiveEnabled) {
    }

    public boolean isNotifySendReceiveEnabled() {
        return (false);
    }

    public void setNotifySendReceiveEnabled(boolean notifySendReceiveEnabled) {
    }

    public String getCommandOnSendError() {
        return commandOnSendError;
    }

    public void setCommandOnSendError(String commandOnSendError) {
        this.commandOnSendError = commandOnSendError;
    }

    public boolean useCommandOnSendError() {
        return useCommandOnSendError;
    }

    public void setUseCommandOnSendError(boolean useCommandOnSendError) {
        this.useCommandOnSendError = useCommandOnSendError;
    }

    public String getCommandOnSendSuccess() {
        return commandOnSendSuccess;
    }

    public void setCommandOnSendSuccess(String commandOnSendSuccess) {
        this.commandOnSendSuccess = commandOnSendSuccess;
    }

    public boolean useCommandOnSendSuccess() {
        return useCommandOnSendSuccess;
    }

    public void setUseCommandOnSendSuccess(boolean useCommandOnSendSuccess) {
        this.useCommandOnSendSuccess = useCommandOnSendSuccess;
    }
    
    public String getDebugDisplay(){
        StringBuffer buffer = new StringBuffer();
        buffer.append( "Name:\t\t" ).append( this.getName());
        buffer.append( "(local station: )").append( this.isLocalStation() ).append( ")\n");
        buffer.append( "AS2 id:\t\t").append( this.getAS2Identification()).append( "\n");
        buffer.append( "UseCommandOnSendError:\t\t" ).append( this.useCommandOnSendError).append( "\n");
        buffer.append( "CommandOnSendError:\t\t" ).append( this.getCommandOnSendError() ).append("\n");
        buffer.append( "UseCommandOnSendSuccess:\t\t" ).append( this.useCommandOnSendSuccess).append( "\n");
        buffer.append( "CommandOnSendSuccess:\t\t" ).append( this.getCommandOnSendSuccess() ).append("\n");
        return( buffer.toString() );
    }
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /mec_as2/de/mendelson/comm/as2/partner/Partner.java 18    11.11.08 10:26 Heller $
d18 1
a18 1
 * @@version $Revision: 18 $
@

