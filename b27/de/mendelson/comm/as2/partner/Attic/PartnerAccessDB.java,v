head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.49.11;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2009.03.16.12.06.51;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b27/de/mendelson/comm/as2/partner/PartnerAccessDB.java,v 1.1 2009/03/16 12:06:51 heller Exp $
package de.mendelson.comm.as2.partner;

import de.mendelson.comm.as2.database.DBDriverManager;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Implementation of a server log for the rosettanet server database
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class PartnerAccessDB {

    /**Logger to log inforamtion to*/
    private Logger logger = Logger.getLogger("de.mendelson.as2.server");
    /**Connection to the database*/
    private Connection connection = null;
    /**Host to conenct to*/
    private String host = null;

    /** Creates new message I/O log and connects to localhost
     *@@param host host to connect to
     */
    public PartnerAccessDB(String host) throws SQLException {
        this.host = host;
        this.connection = DBDriverManager.getConnectionWithoutErrorHandling(host);
    }

    /**Returns all partner stored in the DB, even the local station*/
    public Partner[] getPartner() {
        ArrayList<Partner> partnerList = new ArrayList<Partner>();
        ResultSet result = null;
        try {
            Statement statement = this.connection.createStatement();
            statement.setEscapeProcessing(true);
            //check if the number of entires have changed since last request
            String query = "SELECT * FROM partner";
            result = statement.executeQuery(query);
            while (result.next()) {
                Partner partner = new Partner();
                partner.setAS2Identification(result.getString("as2ident"));
                partner.setName(result.getString("name"));
                partner.setDBId(result.getInt("id"));
                partner.setLocalStation(result.getInt("islocal") == 1);
                partner.setCryptAlias(result.getString("cryptalias"));
                partner.setSignAlias(result.getString("signalias"));
                partner.setSignType(result.getInt("sign"));
                partner.setEncryptionType(result.getInt("encrypt"));
                partner.setEmail(result.getString("email"));
                partner.setURL(result.getString("url"));
                partner.setMdnURL(result.getString("mdnurl"));
                partner.setSubject(result.getString("subject"));
                partner.setContentType(result.getString("contenttype"));
                partner.setSyncMDN(result.getInt("syncmdn") == 1);
                partner.setPollIgnoreListString(result.getString("pollignorelist"));
                partner.setPollInterval(result.getInt("pollinterval"));
                partner.setCompressionType(result.getInt("compression"));
                partner.setSignedMDN(result.getInt("signedmdn") == 1);
                partner.setUseCommandOnReceipt(result.getInt("usecommandonreceipt") == 1);
                partner.setCommandOnReceipt(result.getString("commandonreceipt"));
                partner.setKeepOriginalFilenameOnReceipt(result.getInt("keeporiginalfilenameonreceipt") == 1);
                //http authentication
                if (result.getInt("usehttpauth") == 1) {
                    HTTPAuthentication authentication = new HTTPAuthentication();
                    authentication.setUser(result.getString("httpauthuser"));
                    authentication.setPassword(result.getString("httpauthpass"));
                    partner.setAuthentication(authentication);
                }
                //http async MDN authentication
                if (result.getInt("usehttpauthasyncmdn") == 1) {
                    HTTPAuthentication authentication = new HTTPAuthentication();
                    authentication.setUser(result.getString("httpauthuserasnymdn"));
                    authentication.setPassword(result.getString("httpauthpassasnymdn"));
                    partner.setAuthenticationAsyncMDN(authentication);
                }
                Blob blob = result.getBlob("partnercomment");
                if (result.wasNull()) {
                    partner.setComment(null);
                } else {
                    ObjectInputStream inStream = new ObjectInputStream(blob.getBinaryStream());
                    Object readObject = inStream.readObject();
                    String comment = null;
                    if (readObject != null && readObject instanceof String) {
                        comment = (String) readObject;
                    }
                    partner.setComment(comment);
                }
                partner.setNotifyReceive(result.getInt("notifyreceive"));
                partner.setNotifySend(result.getInt("notifysend"));
                partner.setNotifySendReceive(result.getInt("notifysendreceive"));
                partner.setNotifyReceiveEnabled(result.getInt("notifyreceiveenabled") == 1);
                partner.setNotifySendEnabled(result.getInt("notifysendenabled") == 1);
                partner.setNotifySendReceiveEnabled(result.getInt("notifysendreceiveenabled") == 1);
                partner.setUseCommandOnSendError(result.getInt("usecommandonsenderror") == 1);
                partner.setCommandOnSendError(result.getString("commandonsenderror"));
                partner.setUseCommandOnSendSuccess(result.getInt("usecommandonsendsuccess") == 1);
                partner.setCommandOnSendSuccess(result.getString("commandonsendsuccess"));
                partnerList.add(partner);
            }
            statement.close();
            Partner[] partnerArray = new Partner[partnerList.size()];
            partnerList.toArray(partnerArray);
            return (partnerArray);
        } catch (Exception e) {
            this.logger.severe(e.getMessage());
            return (null);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (Exception e) {
                    //nop
                }
            }
        }
    }

    /**receives a partner configuration and updates the database with them
     */
    public void updatePartner(Partner[] newPartner) {
        //first delete all partners that are in the DB but not in the new list
        Partner[] existingPartner = this.getPartner();
        ArrayList<Partner> newPartnerList = new ArrayList(Arrays.asList(newPartner));
        for (int i = 0; i < existingPartner.length; i++) {
            if (!newPartnerList.contains(existingPartner[i])) {
                this.deletePartner(existingPartner[i]);
            }
        }
        //insert all NEW partners and update the existing
        for (int i = 0; i < newPartner.length; i++) {
            if (newPartner[i].getDBId() < 0) {
                this.insertPartner(newPartner[i]);
            } else {
                this.updatePartner(newPartner[i]);
            }
        }
    }

    /**Updates a single partner in the db*/
    /**Inserts a new partner into the database
     */
    public void updatePartner(Partner partner) {
        try {
            PreparedStatement statement = this.connection.prepareStatement(
                    "UPDATE partner SET as2ident=?,name=?,islocal=?,sign=?,encrypt=?,email=?,signalias=?,cryptalias=?,url=?,mdnurl=?,subject=?,contenttype=?,syncmdn=?,pollignorelist=?,pollinterval=?,compression=?,signedmdn=?,commandonreceipt=?,usecommandonreceipt=?,usehttpauth=?,httpauthuser=?,httpauthpass=?,usehttpauthasyncmdn=?,httpauthuserasnymdn=?,httpauthpassasnymdn=?,keeporiginalfilenameonreceipt=?,partnercomment=?,notifysend=?,notifyreceive=?,notifysendreceive=?,notifysendenabled=?,notifyreceiveenabled=?,notifysendreceiveenabled=?,commandonsenderror=?,usecommandonsenderror=?,commandonsendsuccess=?,usecommandonsendsuccess=? WHERE id=?");
            statement.setEscapeProcessing(true);
            statement.setString(1, partner.getAS2Identification());
            statement.setString(2, partner.getName());
            statement.setInt(3, partner.isLocalStation() ? 1 : 0);
            statement.setInt(4, partner.getSignType());
            statement.setInt(5, partner.getEncryptionType());
            statement.setString(6, partner.getEmail());
            statement.setString(7, partner.getSignAlias());
            statement.setString(8, partner.getCryptAlias());
            statement.setString(9, partner.getURL());
            statement.setString(10, partner.getMdnURL());
            statement.setString(11, partner.getSubject());
            statement.setString(12, partner.getContentType());
            statement.setInt(13, partner.isSyncMDN() ? 1 : 0);
            statement.setString(14, partner.getPollIgnoreListAsString());
            statement.setInt(15, partner.getPollInterval());
            statement.setInt(16, partner.getCompressionType());
            statement.setInt(17, partner.isSignedMDN() ? 1 : 0);
            statement.setString(18, partner.getCommandOnReceipt());
            statement.setInt(19, partner.useCommandOnReceipt() ? 1 : 0);
            if (partner.getAuthentication() != null) {
                statement.setInt(20, 1);
                statement.setString(21, partner.getAuthentication().getUser());
                statement.setString(22, partner.getAuthentication().getPassword());
            } else {
                statement.setInt(20, 0);
                statement.setString(21, null);
                statement.setString(22, null);
            }
            if (partner.getAuthenticationAsyncMDN() != null) {
                statement.setInt(23, 1);
                statement.setString(24, partner.getAuthenticationAsyncMDN().getUser());
                statement.setString(25, partner.getAuthenticationAsyncMDN().getPassword());
            } else {
                statement.setInt(23, 0);
                statement.setString(24, null);
                statement.setString(25, null);
            }
            statement.setInt(26, partner.getKeepOriginalFilenameOnReceipt() ? 1 : 0);
            if (partner.getComment() == null) {
                statement.setNull(27, Types.JAVA_OBJECT);
            } else {
                try {
                    //serialize the comment
                    ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
                    ObjectOutputStream outStream = new ObjectOutputStream(byteOut);
                    outStream.writeObject(partner.getComment());
                    outStream.flush();
                    byteOut.close();
                    statement.setBytes(27, byteOut.toByteArray());
                } catch (Exception e) {
                    statement.setNull(27, Types.JAVA_OBJECT);
                }
            }
            statement.setInt(28, partner.getNotifySend());
            statement.setInt(29, partner.getNotifyReceive());
            statement.setInt(30, partner.getNotifySendReceive());
            statement.setInt(31, partner.isNotifySendEnabled() ? 1 : 0);
            statement.setInt(32, partner.isNotifyReceiveEnabled() ? 1 : 0);
            statement.setInt(33, partner.isNotifySendReceiveEnabled() ? 1 : 0);
            statement.setString(34, partner.getCommandOnSendError());
            statement.setInt(35, partner.useCommandOnSendError() ? 1 : 0);
            statement.setString(36, partner.getCommandOnSendSuccess());
            statement.setInt(37, partner.useCommandOnSendSuccess() ? 1 : 0);
            //where statement
            statement.setInt(38, partner.getDBId());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            this.logger.severe("updatePartner: " + e.getMessage());
        }
    }

    /**Deletes a single partner from the database
     */
    /**Updates a single partner in the db*/
    /**Inserts a new partner into the database
     */
    public void deletePartner(Partner partner) {
        try {
            PreparedStatement statement = this.connection.prepareStatement("DELETE FROM partner WHERE id=?");
            statement.setEscapeProcessing(true);
            statement.setInt(1, partner.getDBId());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            this.logger.severe(e.getMessage());
        }
    }

    /**Inserts a new partner into the database
     */
    public void insertPartner(Partner partner) {
        try {
            PreparedStatement statement = this.connection.prepareStatement(
                    "INSERT INTO partner(as2ident,name,islocal,sign,encrypt,email,signalias,cryptalias,url,mdnurl,subject,contenttype,syncmdn,pollignorelist,pollinterval,compression,signedmdn,commandonreceipt,usecommandonreceipt,usehttpauth,httpauthuser,httpauthpass,usehttpauthasyncmdn,httpauthuserasnymdn,httpauthpassasnymdn,keeporiginalfilenameonreceipt,partnercomment,notifysend,notifyreceive,notifysendreceive,notifysendenabled,notifyreceiveenabled,notifysendreceiveenabled,commandonsenderror,usecommandonsenderror,commandonsendsuccess,usecommandonsendsuccess)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            statement.setEscapeProcessing(true);
            statement.setString(1, partner.getAS2Identification());
            statement.setString(2, partner.getName());
            statement.setInt(3, partner.isLocalStation() ? 1 : 0);
            statement.setInt(4, partner.getSignType());
            statement.setInt(5, partner.getEncryptionType());
            statement.setString(6, partner.getEmail());
            statement.setString(7, partner.getSignAlias());
            statement.setString(8, partner.getCryptAlias());
            statement.setString(9, partner.getURL());
            statement.setString(10, partner.getMdnURL());
            statement.setString(11, partner.getSubject());
            statement.setString(12, partner.getContentType());
            statement.setInt(13, partner.isSyncMDN() ? 1 : 0);
            statement.setString(14, partner.getPollIgnoreListAsString());
            statement.setInt(15, partner.getPollInterval());
            statement.setInt(16, partner.getCompressionType());
            statement.setInt(17, partner.isSignedMDN() ? 1 : 0);
            statement.setString(18, partner.getCommandOnReceipt());
            statement.setInt(19, partner.useCommandOnReceipt() ? 1 : 0);
            if (partner.getAuthentication() != null) {
                statement.setInt(20, 1);
                statement.setString(21, partner.getAuthentication().getUser());
                statement.setString(22, partner.getAuthentication().getPassword());
            } else {
                statement.setInt(20, 0);
                statement.setString(21, null);
                statement.setString(22, null);
            }
            if (partner.getAuthenticationAsyncMDN() != null) {
                statement.setInt(23, 1);
                statement.setString(24, partner.getAuthenticationAsyncMDN().getUser());
                statement.setString(25, partner.getAuthenticationAsyncMDN().getPassword());
            } else {
                statement.setInt(23, 0);
                statement.setString(24, null);
                statement.setString(25, null);
            }
            statement.setInt(26, partner.getKeepOriginalFilenameOnReceipt() ? 1 : 0);
            if (partner.getComment() == null) {
                statement.setNull(27, Types.JAVA_OBJECT);
            } else {
                try {
                    //serialize the comment
                    ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
                    ObjectOutputStream outStream = new ObjectOutputStream(byteOut);
                    outStream.writeObject(partner.getComment());
                    outStream.flush();
                    byteOut.close();
                    statement.setBytes(27, byteOut.toByteArray());
                } catch (Exception e) {
                    statement.setNull(27, Types.JAVA_OBJECT);
                }
            }
            statement.setInt(28, partner.getNotifySend());
            statement.setInt(29, partner.getNotifyReceive());
            statement.setInt(30, partner.getNotifySendReceive());
            statement.setInt(31, partner.isNotifySendEnabled() ? 1 : 0);
            statement.setInt(32, partner.isNotifyReceiveEnabled() ? 1 : 0);
            statement.setInt(33, partner.isNotifySendReceiveEnabled() ? 1 : 0);
            statement.setString(34, partner.getCommandOnSendError());
            statement.setInt(35, partner.useCommandOnSendError() ? 1 : 0);
            statement.setString(36, partner.getCommandOnSendSuccess());
            statement.setInt(37, partner.useCommandOnSendSuccess() ? 1 : 0);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            this.logger.severe(e.getMessage());
        }
    }

    /**Loads a specified partner from the DB
     *@@return null if the partner does not exist
     */
    public Partner getPartner(String as2ident) {
        ResultSet result = null;
        Statement statement = null;
        try {
            statement = this.connection.createStatement();
            statement.setEscapeProcessing(true);
            //check if the number of entires have changed since last request
            String query = "SELECT * FROM partner WHERE as2ident='" + as2ident + "'";
            result = statement.executeQuery(query);
            if (!result.next()) {
                return (null);
            }
            Partner partner = new Partner();
            partner.setAS2Identification(as2ident);
            partner.setName(result.getString("name"));
            partner.setDBId(result.getInt("id"));
            partner.setLocalStation(result.getInt("islocal") == 1);
            partner.setSignAlias(result.getString("signalias"));
            partner.setCryptAlias(result.getString("cryptalias"));
            partner.setSignType(result.getInt("sign"));
            partner.setEncryptionType(result.getInt("encrypt"));
            partner.setEmail(result.getString("email"));
            partner.setURL(result.getString("url"));
            partner.setMdnURL(result.getString("mdnurl"));
            partner.setSubject(result.getString("subject"));
            partner.setContentType(result.getString("contenttype"));
            partner.setSyncMDN(result.getInt("syncmdn") == 1);
            partner.setPollIgnoreListString(result.getString("pollignorelist"));
            partner.setPollInterval(result.getInt("pollinterval"));
            partner.setCompressionType(result.getInt("compression"));
            partner.setSignedMDN(result.getInt("signedmdn") == 1);
            partner.setUseCommandOnReceipt(result.getInt("usecommandonreceipt") == 1);
            partner.setCommandOnReceipt(result.getString("commandonreceipt"));
            //http authentication
            if (result.getInt("usehttpauth") == 1) {
                HTTPAuthentication authentication = new HTTPAuthentication();
                authentication.setUser(result.getString("httpauthuser"));
                authentication.setPassword(result.getString("httpauthpass"));
                partner.setAuthentication(authentication);
            }
            //http async MDN authentication
            if (result.getInt("usehttpauthasyncmdn") == 1) {
                HTTPAuthentication authentication = new HTTPAuthentication();
                authentication.setUser(result.getString("httpauthuserasnymdn"));
                authentication.setPassword(result.getString("httpauthpassasnymdn"));
                partner.setAuthenticationAsyncMDN(authentication);
            }
            partner.setKeepOriginalFilenameOnReceipt(result.getInt("keeporiginalfilenameonreceipt") == 1);
            Blob blob = result.getBlob("partnercomment");
            if (result.wasNull()) {
                partner.setComment(null);
            } else {
                ObjectInputStream inStream = new ObjectInputStream(blob.getBinaryStream());
                Object readObject = inStream.readObject();
                String comment = null;
                if (readObject != null && readObject instanceof String) {
                    comment = (String) readObject;
                }
                partner.setComment(comment);
            }
            partner.setNotifyReceive(result.getInt("notifyreceive"));
            partner.setNotifySend(result.getInt("notifysend"));
            partner.setNotifySendReceive(result.getInt("notifysendreceive"));
            partner.setNotifyReceiveEnabled(result.getInt("notifyreceiveenabled") == 1);
            partner.setNotifySendEnabled(result.getInt("notifysendenabled") == 1);
            partner.setNotifySendReceiveEnabled(result.getInt("notifysendreceiveenabled") == 1);
            partner.setUseCommandOnSendError(result.getInt("usecommandonsenderror") == 1);
            partner.setCommandOnSendError(result.getString("commandonsenderror"));
            partner.setUseCommandOnSendSuccess(result.getInt("usecommandonsendsuccess") == 1);
            partner.setCommandOnSendSuccess(result.getString("commandonsendsuccess"));
            return (partner);
        } catch (Exception e) {
            this.logger.severe(e.getMessage());
            e.printStackTrace();
            return (null);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (Exception e) {
                    //nop
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                    //nop
                }
            }
        }
    }

    /**Returns the stations that are defined as local*/
    public Partner[] getLocalStations() {
        ResultSet result = null;
        Statement statement = null;
        ArrayList<Partner> localStationList = new ArrayList<Partner>();
        try {
            statement = this.connection.createStatement();
            statement.setEscapeProcessing(true);
            String query = "SELECT * FROM partner WHERE islocal=1";
            result = statement.executeQuery(query);
            while (result.next()) {
                Partner partner = new Partner();
                partner.setAS2Identification(result.getString("as2ident"));
                partner.setName(result.getString("name"));
                partner.setDBId(result.getInt("id"));
                partner.setLocalStation(result.getInt("islocal") == 1);
                partner.setSignAlias(result.getString("signalias"));
                partner.setCryptAlias(result.getString("cryptalias"));
                partner.setSignType(result.getInt("sign"));
                partner.setEncryptionType(result.getInt("encrypt"));
                partner.setEmail(result.getString("email"));
                partner.setURL(result.getString("url"));
                partner.setMdnURL(result.getString("mdnurl"));
                partner.setSubject(result.getString("subject"));
                partner.setContentType(result.getString("contenttype"));
                partner.setSyncMDN(result.getInt("syncmdn") == 1);
                partner.setPollIgnoreListString(result.getString("pollignorelist"));
                partner.setPollInterval(result.getInt("pollinterval"));
                partner.setCompressionType(result.getInt("compression"));
                partner.setSignedMDN(result.getInt("signedmdn") == 1);
                partner.setUseCommandOnReceipt(result.getInt("usecommandonreceipt") == 1);
                partner.setCommandOnReceipt(result.getString("commandonreceipt"));
                //http authentication
                if (result.getInt("usehttpauth") == 1) {
                    HTTPAuthentication authentication = new HTTPAuthentication();
                    authentication.setUser(result.getString("httpauthuser"));
                    authentication.setPassword(result.getString("httpauthpass"));
                    partner.setAuthentication(authentication);
                }
                //http async MDN authentication
                if (result.getInt("usehttpauthasyncmdn") == 1) {
                    HTTPAuthentication authentication = new HTTPAuthentication();
                    authentication.setUser(result.getString("httpauthuserasnymdn"));
                    authentication.setPassword(result.getString("httpauthpassasnymdn"));
                    partner.setAuthenticationAsyncMDN(authentication);
                }
                partner.setKeepOriginalFilenameOnReceipt(result.getInt("keeporiginalfilenameonreceipt") == 1);
                Blob blob = result.getBlob("partnercomment");
                if (result.wasNull()) {
                    partner.setComment(null);
                } else {
                    ObjectInputStream inStream = new ObjectInputStream(blob.getBinaryStream());
                    Object readObject = inStream.readObject();
                    String comment = null;
                    if (readObject != null && readObject instanceof String) {
                        comment = (String) readObject;
                    }
                    partner.setComment(comment);
                }
                partner.setNotifyReceive(result.getInt("notifyreceive"));
                partner.setNotifySend(result.getInt("notifysend"));
                partner.setNotifySendReceive(result.getInt("notifysendreceive"));
                partner.setNotifyReceiveEnabled(result.getInt("notifyreceiveenabled") == 1);
                partner.setNotifySendEnabled(result.getInt("notifysendenabled") == 1);
                partner.setNotifySendReceiveEnabled(result.getInt("notifysendreceiveenabled") == 1);
                partner.setUseCommandOnSendError(result.getInt("usecommandonsenderror") == 1);
                partner.setCommandOnSendError(result.getString("commandonsenderror"));
                partner.setUseCommandOnSendSuccess(result.getInt("usecommandonsendsuccess") == 1);
                partner.setCommandOnSendSuccess(result.getString("commandonsendsuccess"));
                localStationList.add(partner);
            }
            Partner[] stationArray = new Partner[localStationList.size()];
            localStationList.toArray(stationArray);
            return (stationArray);
        } catch (Exception e) {
            this.logger.severe("PartnerAccessDB: No local station defined.");
            return (null);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (Exception e) {
                    //nop
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                    //nop
                }
            }
        }
    }

    /**Closes the internal database connection.*/
    public void close() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            this.logger.severe(e.getMessage());
        }
    }
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/partner/PartnerAccessDB.java 23    10.11.08 16:36 Heller $
d29 1
a29 1
 * @@version $Revision: 23 $
@

