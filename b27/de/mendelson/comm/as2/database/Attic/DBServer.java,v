head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.49.09;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2009.03.16.12.06.49;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b27/de/mendelson/comm/as2/database/DBServer.java,v 1.1 2009/03/16 12:06:49 heller Exp $
package de.mendelson.comm.as2.database;

import de.mendelson.comm.as2.AS2ServerVersion;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import java.io.*;
import java.util.*;
import java.sql.*;
import de.mendelson.util.MecResourceBundle;
import java.util.logging.*;
import org.hsqldb.Server;
import org.hsqldb.ServerConstants;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */

/**
 * Class to start a dedicated SQL database server
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 * @@since build 70
 */
public class DBServer implements Runnable {

    /**Resourcebundle to localize messages of the DB server*/
    private static MecResourceBundle rb;
    /**Log messages*/
    private Logger logger = Logger.getLogger("de.mendelson.as2.server");
    /**Database object*/
    private Server server = null;
    private PreferencesAS2 preferences = new PreferencesAS2();
    

    static {
        try {
            rb = (MecResourceBundle) ResourceBundle.getBundle(
                    ResourceBundleDBServer.class.getName());
        } //load up resourcebundle
        catch (MissingResourceException e) {
            throw new RuntimeException("Oops..resource bundle " + e.getClassName() + " not found.");
        }
    }

    /**Start a dedicated database server
     */
    public DBServer() {
        this.createCheck();
    }

    /**Start the server thread*/
    @@Override
    public void run() {
        //available since 1.7.2,
        //alias available since 1.8.0_1
        String serverProps =
                "port=" + this.preferences.get(PreferencesAS2.SERVER_DB_PORT) + ";" + "database.0=file:" + DBDriverManager.DB_NAME + ";" + "dbname.0=" + DBDriverManager.DB_ALIAS + ";" + "silent=true;" + "trace=false;" + "hsqldb.cache_scale=15;" + "hsqldb.cache_file_scale=8;" + "no_system_exit=true;" + "shutdownarg=COMPACT;";
        this.server = new Server();
        server.putPropertiesFromString(serverProps);
        server.setLogWriter(null);
        server.start();
        try {
            //defrag the database
            Connection connection = DBDriverManager.getConnectionWithoutErrorHandling("localhost");
            Statement statement = connection.createStatement();
            statement.execute("CHECKPOINT DEFRAG");
            statement.close();
            connection.close();
        } catch (Exception e) {
            this.logger.warning(e.getMessage());
        }
        DBDriverManager.setupConnectionPool();
        try {
            Connection connection = DBDriverManager.getLocalConnection();
            if (connection == null) {
                return;
            }
            DatabaseMetaData data = connection.getMetaData();
            this.logger.info(rb.getResourceString("server.started",
                    new Object[]{data.getDatabaseProductName() + " " + data.getDatabaseProductVersion()}));
            Statement statement = connection.createStatement();
            statement.execute("SET SCRIPTFORMAT COMPRESSED");
            statement.close();
            //check if a DB update is necessary. If so, update the DB
            this.updateDB(connection);
            connection.close();
        } catch (SQLException e) {
            this.logger.severe(e.getMessage());
        }
    }

    /**Check if db exists and create a new one
     * if it doesnt exist
     */
    private void createCheck() {
        File file = new File(DBDriverManager.DB_NAME + ".script");
        //create new Database
        if (!file.exists()) {
            DBDriverManager.createLocaleDatabase();
        }
    }

    /**Update the database if this is necessary.
     *@@param connection connection to the database
     */
    private void updateDB(Connection connection) {
        int foundVersion = -1;
        Statement statement = null;
        ResultSet result = null;
        try {
            statement = connection.createStatement();
            statement.setEscapeProcessing(true);
            result = statement.executeQuery("SELECT MAX(actualVersion)FROM version");
            result.next();
            //value is always in the first column
            foundVersion = result.getInt(1);
            result.close();
        } catch (SQLException e) {
            this.logger.severe(e.getMessage());
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (Exception ignore) {
                    //nop
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception ignore) {
                    //nop
                }
            }
        }
        //check if this is smaller than the required version!
        if (foundVersion != -1 && foundVersion < AS2ServerVersion.getRequiredDBVersion()) {
            this.logger.info(rb.getResourceString("update.versioninfo",
                    new Object[]{String.valueOf(foundVersion),
                        String.valueOf(AS2ServerVersion.getRequiredDBVersion())
                    }));
            this.logger.info(rb.getResourceString("update.progress"));

            for (int i = foundVersion; i < AS2ServerVersion.getRequiredDBVersion(); i++) {
                if (!this.startDBUpdate(i, connection)) {
                    this.logger.info(rb.getResourceString("update.error",
                            new Object[]{String.valueOf(i), String.valueOf(i + 1)}));
                    System.exit(-1);
                }
                //set new version to the database
                this.setNewDBVersion(connection, i + 1);
                this.logger.info(rb.getResourceString("update.progress.version",
                        String.valueOf(i + 1)));
            }
            this.logger.info((rb.getResourceString("update.successfully")));
        }
    }

    /**Sets the new DB version to the passed number if the update was
     *successfully
     *@@param connection DB connection to use
     *@@param version new DB version the update has updated to
     */
    private void setNewDBVersion(Connection connection, int version) {
        try {
            //request all connections from the database to store them
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO version(actualVersion,updateDate,updateComment)" + "VALUES(?,?,?)");
            statement.setEscapeProcessing(true);
            //fill in values
            statement.setInt(1, version);
            statement.setDate(2, new java.sql.Date(System.currentTimeMillis()));
            statement.setString(3, "by " + AS2ServerVersion.getProductName() + " " + AS2ServerVersion.getBuild() + " auto updater");
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            this.logger.warning("DBServer.setNewDBVersion: " + e);
        }

    }


    /**Sends a shutdown signal to all connections, then shutdown the server*/
    public void shutdown() {
        try {
            this.server.signalCloseAllServerConnections();
        } catch (Exception e) {
            //nop
        }
        try {
            DBDriverManager.shutdownConnectionPool();
        } catch (Exception e) {
            //nop
        }
        System.out.println("DB server shut down.");
        this.server.shutdown();
        while (this.server.getState() != ServerConstants.SERVER_STATE_SHUTDOWN) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
        }
    }

    /**Start the DB update from the startVersion to the startVersion+1
     *@@param startVersion Start version
     *@@param connection Connection to use for the update
     *@@return true if the update was successful
     */
    private boolean startDBUpdate(int startVersion, Connection connection) {
        //either a java class or a sql script MUST exist, else this update
        //will fail!
        boolean updateOK = false;
        boolean updateExists = false;

        //sql file to execute for the update process
        String sqlResource = SQLScriptExecutor.SCRIPT_RESOURCE + "update" + startVersion + "to" + (startVersion + 1) + ".sql";

        SQLScriptExecutor executor = new SQLScriptExecutor();
        if (executor.resourceExists(sqlResource)) {
            updateExists = true;
            executor.executeScript(connection, sqlResource);
            updateOK = true;
        }
        //check if a java file should be executed that changes something in
        //the database, too
        String javaUpdateClass = "sqlscript.Update" + startVersion + "to" + (startVersion + 1);
        try {
            Class cl = Class.forName(javaUpdateClass);
            updateExists = true;
            IUpdater updater = (IUpdater) cl.newInstance();
            updater.startUpdate(connection);
            updateOK = updater.updateWasSuccessfully();
        } catch (ClassNotFoundException e) {
            //ignore if update is already ok
            if (!updateOK) {
                this.logger.info("DBServer.startDBUpdate (ClassNotFoundException):" + e);
            }
        } catch (Throwable e) {
            this.logger.warning(e.getMessage());
        }
        //there exists no update file!
        if (!updateExists) {
            this.logger.info(rb.getResourceString("update.notfound",
                    new Object[]{String.valueOf(startVersion),
                        String.valueOf(startVersion + 1)
                    }));
        }

        return (updateOK);
    }
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/database/DBServer.java 12    5.12.08 14:17 Heller $
d24 1
a24 1
 * @@version $Revision: 12 $
@

