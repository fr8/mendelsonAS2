head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.49.12;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2009.03.16.12.06.54;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b27/de/mendelson/comm/as2/preferences/JDialogPreferences.java,v 1.1 2009/03/16 12:06:54 heller Exp $
package de.mendelson.comm.as2.preferences;

import de.mendelson.util.ImageButtonBar;
import de.mendelson.util.MecResourceBundle;
import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Dialog to configure a single partner
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class JDialogPreferences extends JDialog {

    /**ResourceBundle to localize the GUI*/
    private MecResourceBundle rb = null;
    private PreferencesAS2 preferences = new PreferencesAS2();
    private PreferencesPanelSystemMaintenance jPanelMaintenance = new PreferencesPanelSystemMaintenance();
    private PreferencesPanelNotification jPanelNotification = new PreferencesPanelNotification();

    /** Creates new form JDialogPartnerConfig
     *@@param parameter Parameter to edit, null for a new one
     *@@param parameterList List of available parameter
     */
    public JDialogPreferences(JFrame parent) {
        super(parent, true);
        //load resource bundle
        try {
            this.rb = (MecResourceBundle) ResourceBundle.getBundle(
                    ResourceBundlePreferences.class.getName());
        } catch (MissingResourceException e) {
            throw new RuntimeException("Oops..resource bundle " + e.getClassName() + " not found.");
        }
        initComponents();
        if (this.preferences.get(PreferencesAS2.LANGUAGE).equals("de")) {
            this.jRadioButtonLangDE.setSelected(true);
        } else if (this.preferences.get(PreferencesAS2.LANGUAGE).equals("en")) {
            this.jRadioButtonLangEN.setSelected(true);
        } else if (this.preferences.get(PreferencesAS2.LANGUAGE).equals("fr")) {
            this.jRadioButtonLangFR.setSelected(true);
        }
        PreferencesPanelDirectories jPanelDir = new PreferencesPanelDirectories();
        PreferencesPanelSecurity jPanelSecurity = new PreferencesPanelSecurity();
        PreferencesPanelProxy jPanelProxy = new PreferencesPanelProxy();
        PreferencesPanelMDN jPanelMDN = new PreferencesPanelMDN();
        jPanelDir.loadPreferences();
        jPanelSecurity.loadPreferences();
        jPanelProxy.loadPreferences();
        jPanelMDN.loadPreferences();
        this.jPanelNotification.loadPreferences();
        this.jPanelMaintenance.loadPreferences();


        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.jPanelEdit.add(jPanelDir, gridBagConstraints);
        this.jPanelEdit.add(jPanelSecurity, gridBagConstraints);
        this.jPanelEdit.add(jPanelProxy, gridBagConstraints);
        this.jPanelEdit.add(jPanelMDN, gridBagConstraints);
        this.jPanelEdit.add(this.jPanelMaintenance, gridBagConstraints);
        this.jPanelEdit.add(this.jPanelNotification, gridBagConstraints);

        ImageButtonBar buttonBar = new ImageButtonBar(ImageButtonBar.HORIZONTAL);
        buttonBar.setPreferredButtonSize(85, 80);
        buttonBar.addButton(
                new ImageIcon(this.getClass().getResource("/de/mendelson/comm/as2/preferences/preferences32x32.gif")),
                this.rb.getResourceString("tab.misc"),
                new JComponent[]{jPanelMDN},
                true);
        buttonBar.addButton(
                new ImageIcon(this.getClass().getResource("/de/mendelson/comm/as2/preferences/proxy32x32.gif")),
                this.rb.getResourceString("tab.proxy"),
                new JComponent[]{jPanelProxy},
                false);
        buttonBar.addButton(
                new ImageIcon(this.getClass().getResource("/de/mendelson/comm/as2/preferences/security32x32.gif")),
                this.rb.getResourceString("tab.security"),
                new JComponent[]{jPanelSecurity},
                false);
        buttonBar.addButton(
                new ImageIcon(this.getClass().getResource("/de/mendelson/comm/as2/preferences/folder32x32.gif")),
                this.rb.getResourceString("tab.dir"),
                new JComponent[]{jPanelDir},
                false);
        buttonBar.addButton(
                new ImageIcon(this.getClass().getResource("/de/mendelson/comm/as2/preferences/maintenance32x32.gif")),
                this.rb.getResourceString("tab.maintenance"),
                new JComponent[]{this.jPanelMaintenance},
                false);
        buttonBar.addButton(
                new ImageIcon(this.getClass().getResource("/de/mendelson/comm/as2/preferences/notification32x32.gif")),
                this.rb.getResourceString("tab.notification"),
                new JComponent[]{this.jPanelNotification},
                false);
        buttonBar.addButton(
                new ImageIcon(this.getClass().getResource("/de/mendelson/comm/as2/preferences/language32x32.gif")),
                this.rb.getResourceString("tab.language"),
                new JComponent[]{this.jPanelLanguage},
                false);
        buttonBar.build();
        //add button bar
        this.jPanelButtonBar.setLayout(new BorderLayout());
        this.jPanelButtonBar.add(buttonBar, BorderLayout.CENTER);
        this.getRootPane().setDefaultButton(this.jButtonOk);

    }

    /**Sets the ok and cancel buttons of this GUI*/
    private void setButtonState() {
        this.jButtonOk.setEnabled(true);
    }

    private void captureGUIValues() {
        if (this.jRadioButtonLangDE.isSelected()) {
            this.preferences.put(PreferencesAS2.LANGUAGE, "de");
        } else if (this.jRadioButtonLangEN.isSelected()) {
            this.preferences.put(PreferencesAS2.LANGUAGE, "en");
        } else if (this.jRadioButtonLangFR.isSelected()) {
            this.preferences.put(PreferencesAS2.LANGUAGE, "fr");
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroupLanguage = new javax.swing.ButtonGroup();
        jPanelEdit = new javax.swing.JPanel();
        jPanelLanguage = new javax.swing.JPanel();
        jRadioButtonLangDE = new javax.swing.JRadioButton();
        jRadioButtonLangEN = new javax.swing.JRadioButton();
        jRadioButtonLangFR = new javax.swing.JRadioButton();
        jPanelSpace = new javax.swing.JPanel();
        jPanelButtons = new javax.swing.JPanel();
        jButtonOk = new javax.swing.JButton();
        jPanelButtonBar = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(this.rb.getResourceString( "title"));
        getContentPane().setLayout(new java.awt.GridBagLayout());

        jPanelEdit.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanelEdit.setLayout(new java.awt.GridBagLayout());

        jPanelLanguage.setLayout(new java.awt.GridBagLayout());

        buttonGroupLanguage.add(jRadioButtonLangDE);
        jRadioButtonLangDE.setText("Deutsch");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(20, 10, 5, 5);
        jPanelLanguage.add(jRadioButtonLangDE, gridBagConstraints);

        buttonGroupLanguage.add(jRadioButtonLangEN);
        jRadioButtonLangEN.setText("English");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 5);
        jPanelLanguage.add(jRadioButtonLangEN, gridBagConstraints);

        buttonGroupLanguage.add(jRadioButtonLangFR);
        jRadioButtonLangFR.setText("Fran�ais");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 10, 5, 5);
        jPanelLanguage.add(jRadioButtonLangFR, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelLanguage.add(jPanelSpace, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanelEdit.add(jPanelLanguage, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        getContentPane().add(jPanelEdit, gridBagConstraints);

        jPanelButtons.setLayout(new java.awt.GridBagLayout());

        jButtonOk.setText(this.rb.getResourceString( "button.ok" ));
        jButtonOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOkActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelButtons.add(jButtonOk, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        getContentPane().add(jPanelButtons, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        getContentPane().add(jPanelButtonBar, gridBagConstraints);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-627)/2, (screenSize.height-425)/2, 627, 425);
    }// </editor-fold>//GEN-END:initComponents
    private void jButtonOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOkActionPerformed
        this.jPanelMaintenance.savePreferences();
        jPanelNotification.savePreferences();
        this.setVisible(false);
        this.captureGUIValues();
        this.dispose();
    }//GEN-LAST:event_jButtonOkActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupLanguage;
    private javax.swing.JButton jButtonOk;
    private javax.swing.JPanel jPanelButtonBar;
    private javax.swing.JPanel jPanelButtons;
    private javax.swing.JPanel jPanelEdit;
    private javax.swing.JPanel jPanelLanguage;
    private javax.swing.JPanel jPanelSpace;
    private javax.swing.JRadioButton jRadioButtonLangDE;
    private javax.swing.JRadioButton jRadioButtonLangEN;
    private javax.swing.JRadioButton jRadioButtonLangFR;
    // End of variables declaration//GEN-END:variables
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /mec_as2/de/mendelson/comm/as2/preferences/JDialogPreferences.java 11    2.07.08 12:33 Heller $
d25 1
a25 1
 * @@version $Revision: 11 $
@

