head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2006.09.15.10.41.37;	author heller;	state Exp;
branches;
next	;


desc
@@


1.1
log
@*** empty log message ***
@
text
@//$Header: /mec_as2/de/mendelson/comm/as2/preferences/PreferencesAS2.java 7     16.06.06 13:38 Heller $
package de.mendelson.comm.as2.preferences;
import de.mendelson.comm.as2.AS2ServerVersion;
import de.mendelson.util.MecResourceBundle;
import java.util.prefs.*;
import java.util.*;
import java.awt.*;
import java.io.File;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */

/**
 * Class to manage the preferences of the AS2 server
 * @@author  S.Heller
 * @@version $Revision: 7 $
 */
public class PreferencesAS2{
    
    static MecResourceBundle rb;
    static{
        //load resource bundle
        try{
            rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundlePreferences.class.getName());
        } catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
    }
    
    private final int OS_WIN = 1;
    private final int OS_OTHER = 2;
    
    /**Position of the client frame X*/
    public static final String FRAME_X = "frameguix";
    /**Position of the client frame Y*/
    public static final String FRAME_Y = "frameguiy";
    /**Position of the client frame height*/
    public static final String FRAME_HEIGHT = "frameguiheight";
    /**Position of the IDE frame WIDTH*/
    public static final String FRAME_WIDTH = "frameguiwidth";
        
    /**Language to use for the software localization*/
    public static final String LANGUAGE = "language";
    
    
    /**the actual used server to connect to*/
    public static final String SERVER_HOST = "serverhost";
    /**The RMI connection port*/
    public static final String SERVER_RMI_PORT = "serverrmiport";
    /**RMI service*/
    public static final String SERVER_RMI_SERVICE = "rmiservice";
    /**DB server*/
    public static final String SERVER_DB_PORT = "dbport";
        
    /**Directory the messageparts are stored in*/
    public static final String DIR_MSG = "dirmsg";
    
    public static final String ASYNC_MDN_TIMEOUT = "asyncmdntimeout";
    
    /**keystore for user defined certs in https*/
    public static final String KEYSTORE_HTTPS_SEND = "httpsendkeystore";
    /**password for user defined certs keystore in https*/
    public static final String KEYSTORE_HTTPS_SEND_PASS = "httpsendkeystorepass";
    /**password for the encryption/signature keystore*/
    public static final String KEYSTORE_PASS = "keystorepass";
    
    public static final String AUTH_PROXY_HOST = "proxyhost";
    public static final String AUTH_PROXY_USER = "proxyuser";
    public static final String AUTH_PROXY_PASS = "proxypass";
    public static final String AUTH_PROXY_USE = "proxyuse";
    
    public static final String AUTO_MSG_DELETE = "automsgdelete";
    public static final String AUTO_MSG_DELETE_OLDERTHAN = "automsgdeleteolderthan";
    public static final String AUTO_MSG_DELETE_LOG = "automsgdeletelog";
    
    /**Settings stored for the user*/
    private Preferences preferences = null;
    
    /**name of the operation system*/
    private String os = System.getProperty( "os.name" );
    
    /**Initialize the preferences*/
    public PreferencesAS2(){
        //on windows systems it is common to use as root, the activation will
        //be system wide. On Linux/Unix systems it is ok to activate the
        //IDE for only one user (lets say user account "mendelson" )
        if( this.getOS() == this.OS_WIN ){
            this.preferences
                    = Preferences.systemNodeForPackage( AS2ServerVersion.class );
        } else{
            this.preferences
                    = Preferences.userNodeForPackage( AS2ServerVersion.class );
        }
    }
    
    /**Returns the localized preference*/
    public static String getLocalizedName( final String KEY ){
        return( rb.getResourceString( KEY ));
    }
    
    
    /**Returns the default value for the key
     *@@param KEY key to store properties with in the preferences
     */
    private String getDefaultValue( final String KEY ){
        if( KEY.equals( this.FRAME_X )){
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            Dimension dialogSize
                    = new Dimension(
                    new Integer( this.getDefaultValue( this.FRAME_WIDTH )).intValue(),
                    new Integer( this.getDefaultValue( this.FRAME_HEIGHT )).intValue());
            return( String.valueOf( (screenSize.width-dialogSize.width)/2 ));
        }
        if( KEY.equals( this.FRAME_Y )){
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            Dimension dialogSize
                    = new Dimension(
                    new Integer( this.getDefaultValue( this.FRAME_WIDTH )).intValue(),
                    new Integer( this.getDefaultValue( this.FRAME_HEIGHT )).intValue());
            return( String.valueOf( (screenSize.height-dialogSize.height)/2 ));
        }
        if( KEY.equals( this.FRAME_WIDTH )) return( "800" );
        if( KEY.equals( this.FRAME_HEIGHT )) return( "600" );
        
        //language used for the localization
        if( KEY.equals( this.LANGUAGE )){
            if( Locale.getDefault().equals( Locale.GERMANY ))
                return( "de" );
            //default is always english
            return( "en" );
        }
        
        //RMI port for client-server communication
        if( KEY.equals( this.SERVER_RMI_PORT ))
            return( "1099" );
        //DB port for the server
        if( KEY.equals( this.SERVER_DB_PORT ))
            return( "3333" );
        //RMI service provided by the rmi server
        if( KEY.equals( this.SERVER_RMI_SERVICE ))
            return( "MEC_AS2" );
        //server to connect to by default
        if( KEY.equals( this.SERVER_HOST ))
            return( "localhost" );
        //message part directory
        if( KEY.equals( this.DIR_MSG ))
            return( new File( System.getProperty( "user.dir" ) ).getAbsolutePath()
            + File.separator + "messages" );
        if( KEY.equals( this.KEYSTORE_HTTPS_SEND ))
            return( "jetty/etc/keystore" );
        if( KEY.equals( this.KEYSTORE_HTTPS_SEND_PASS ))
            return( "changeit" );
        if( KEY.equals( this.AUTH_PROXY_HOST ))
            return( "localhost" );
        if( KEY.equals( this.AUTH_PROXY_PASS ))
            return( "mypass" );
        if( KEY.equals( this.AUTH_PROXY_USER ))
            return( "myuser" );
        if( KEY.equals( this.AUTH_PROXY_USE ))
            return( "FALSE" );
        if( KEY.equals( this.KEYSTORE_PASS ))
            return( "test" );
        //30 minutes
        if( KEY.equals( this.ASYNC_MDN_TIMEOUT ))
            return( "30" );
        if( KEY.equals( this.AUTO_MSG_DELETE ))
            return( "TRUE" );
        if( KEY.equals( this.AUTO_MSG_DELETE_LOG ))
            return( "TRUE" );
        if( KEY.equals( this.AUTO_MSG_DELETE_OLDERTHAN ))
            return( "5" );
        throw new IllegalArgumentException( "No defaults defined for prefs key "
                + KEY + " in " + this.getClass().getName() );
    }
    
    
    /**Returns a single string value from the preferences or the default
     *if it is not found
     *@@param key one of the class internal constants
     */
    public String get( final String KEY ){
        return( this.preferences.get( KEY, this.getDefaultValue( KEY )));
    }
    
    /**Stores a value in the preferences. If the passed value is null or an
     *empty string the key-value pair will be deleted from the registry.
     *@@param KEY Key as defined in this class
     *@@param value value to set
     */
    public void put( final String KEY, String value ){
        if( value == null || value.length() == 0 )
            this.preferences.remove( KEY );
        else
            this.preferences.put( KEY, value );
        try{
            this.preferences.flush();
        } catch( BackingStoreException ignore ){
        }
    }
    
    /**Puts a value to the preferences and stores the prefs
     *@@param KEY Key as defined in this class
     *@@param value value to set
     */
    public void putInt( final String KEY, int value ){
        this.preferences.putInt( KEY, value );
        try{
            this.preferences.flush();
        } catch( BackingStoreException ignore ){
        }
    }
    
    /**Returns the value for the asked key, if noen is defined it returns
     *the default value*/
    public int getInt( final String KEY ){
        return( this.preferences.getInt( KEY,
                new Integer( this.getDefaultValue( KEY )).intValue() ));
    }
    
    
    /**Puts a value to the preferences and stores the prefs
     *@@param KEY Key as defined in this class
     *@@param value value to set
     */
    public void putBoolean( final String KEY, boolean value ){
        this.preferences.putBoolean( KEY, value );
        try{
            this.preferences.flush();
        } catch( BackingStoreException ignore ){
        }
    }
    
    /**Returns the value for the asked key, if non is defined it returns
     *the default value*/
    public boolean getBoolean( final String KEY ){
        return( this.preferences.getBoolean( KEY,
                new Boolean( this.getDefaultValue( KEY )).booleanValue() ));
    }
    
    /**Returns the value for the asked key, if noen is defined it returns
     *the second parameters value*/
    public boolean getBoolean( final String KEY, boolean defaultValue ){
        return( this.preferences.getBoolean( KEY, defaultValue));
    }
            
    /**Returns the OS of the actual installation*/
    private int getOS(){
        if( this.os.toLowerCase().startsWith( "win" ))
            return( this.OS_WIN );
        else
            return( this.OS_OTHER );
    }
    
}@
