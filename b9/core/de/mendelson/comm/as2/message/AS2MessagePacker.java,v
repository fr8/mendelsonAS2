head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2006.09.15.10.41.35;	author heller;	state Exp;
branches;
next	;


desc
@@


1.1
log
@*** empty log message ***
@
text
@//$Header: /mec_as2/de/mendelson/comm/as2/message/AS2MessagePacker.java 38    17.08.06 10:06 Leo $
package de.mendelson.comm.as2.message;
import de.mendelson.comm.as2.AS2Exception;
import de.mendelson.comm.as2.AS2ServerVersion;
import de.mendelson.comm.as2.cert.CertificateManager;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.partner.PartnerAccessDB;
import de.mendelson.util.MecResourceBundle;
import de.mendelson.util.security.BCCryptoHelper;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.MissingResourceException;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.bouncycastle.cms.CMSEnvelopedData;
import org.bouncycastle.cms.CMSEnvelopedDataGenerator;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.mail.smime.SMIMEEnvelopedGenerator;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Packs a message with all necessary headers and attachments
 * @@author S.Heller
 * @@version $Revision: 38 $
 */
public class AS2MessagePacker{
    
    private Logger logger = Logger.getLogger( "de.mendelson.as2" );
    
    private MecResourceBundle rb = null;
    
    private CertificateManager certificateManager = null;
    
    /**Randomizer to create ids*/
    private Random random;
    
    public AS2MessagePacker( CertificateManager certificateManager ){
        //Load resourcebundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleAS2MessagePacker.class.getName());
        }
        //load up  resourcebundle
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
        this.certificateManager = certificateManager;
        this.random = new Random(System.currentTimeMillis());
    }
    
    /**Creates an mdn that could be returned to the sender and indicates that
     *everything is ok
     */
    public AS2Message createMDNProcessed( AS2MessageInfo releatedMessageInfo, String senderId, String receiverId) throws Exception{
        AS2Message mdn = this.createMDN( releatedMessageInfo, senderId, receiverId, "processed", "AS2 message received." );
        mdn.getMessageInfo().setState( AS2Message.STATE_FINISHED );
        return( mdn );
    }
    
    /**Creates an mdn that could be returned to the sender and indicates an error
     *by processing the message
     */
    public AS2Message createMDNError( AS2Exception exception ) throws Exception{
        AS2MessageInfo info = exception.getAS2Message().getMessageInfo();
        AS2Message mdn = this.createMDN( info, info.getReceiverId(),
                info.getSenderId(), "processed/error: " + exception.getErrorType(),
                exception.getMessage() );
        this.logger.log( Level.SEVERE, this.rb.getResourceString( "mdn.details", 
                new Object[]{ 
                    info.getMessageId(),
                            exception.getMessage()                            
        }), info );
        mdn.getMessageInfo().setState( AS2Message.STATE_STOPPED );
        return( mdn );
    }
    
    /**Creates a MDN to return
     *@@param dispositionState State that will be written into the disposition header
     */
    private AS2Message createMDN( AS2MessageInfo relatedMessageInfo, String senderId,
            String receiverId, String dispositionState, String additionalText )throws Exception{
        AS2Message message = new AS2Message();
        AS2MessageInfo info = message.getMessageInfo();
        info.setMessageId( this.createId(senderId, receiverId) );
        info.setSenderId( senderId );
        info.setReceiverId( receiverId );
        info.setSenderEMail( relatedMessageInfo.getSenderEMail() );
        info.setRequestsSyncMDN(relatedMessageInfo.requestsSyncMDN() );
        info.setAsyncMDNURL( relatedMessageInfo.getAsyncMDNURL());
        info.setSubject( "Message Delivery Notification" );
        //indicate that is is an MDN
        info.setRelatedMessageId( relatedMessageInfo.getMessageId() );
        MimeMultipart multiPart = new MimeMultipart();
        multiPart.addBodyPart( this.createMDNNotesBody( additionalText ));
        multiPart.addBodyPart( this.createMDNDispositionBody(relatedMessageInfo, dispositionState));
        multiPart.setSubType( "report; report-type=disposition-notification" );
        MimeMessage messagePart = new MimeMessage( Session.getInstance(System.getProperties(), null));
        messagePart.setContent( multiPart, multiPart.getContentType() );
        messagePart.saveChanges();
        ByteArrayOutputStream memOutUnsigned = new ByteArrayOutputStream();
        messagePart.writeTo( memOutUnsigned,
                new String[]{ "Message-ID", "Mime-Version"
        } );
        memOutUnsigned.flush();
        memOutUnsigned.close();
        message.setDecryptedRawData(memOutUnsigned.toByteArray());
        //check if authentification of sender is ok, then sign if possible
        PartnerAccessDB partnerAccess = new PartnerAccessDB( "localhost" );
        Partner sender = partnerAccess.getPartner( senderId );
        partnerAccess.close();
        if( sender != null ){
            MimeMessage signedMessage = this.signMDN( messagePart, sender );
            message.getMessageInfo().setSignType( AS2Message.SIGNATURE_SHA1 );
            message.setContentType( signedMessage.getContentType() );
            ByteArrayOutputStream memOutSigned = new ByteArrayOutputStream();
            signedMessage.writeTo( memOutSigned,
                    new String[]{ "Message-ID", "Mime-Version", "Content-Type"
            } );
            memOutSigned.flush();
            memOutSigned.close();
            message.setRawData(memOutSigned.toByteArray());
        }
        //there occured an authentification error: the system was unable to authentificate the sender,
        //do not sign MDN
        else{
            ByteArrayOutputStream memOut = new ByteArrayOutputStream();
            messagePart.writeTo( memOut,
                    new String[]{ "Message-ID", "Mime-Version", "Content-Type" }
            );
            memOut.flush();
            memOut.close();
            message.getMessageInfo().setSignType( AS2Message.SIGNATURE_NONE );
            message.setContentType( messagePart.getContentType() );
            message.setRawData(memOut.toByteArray());
        }
        if( dispositionState.indexOf( "error" ) >= 0 ){
            this.logger.log( Level.SEVERE, this.rb.getResourceString( "mdn.created",
                    new Object[]{
                info.getMessageId(), dispositionState
            }), info );
        }
        else
            this.logger.log( Level.FINE, this.rb.getResourceString( "mdn.created",
                    new Object[]{
                info.getMessageId(), dispositionState
            }), info );
        return( message );
    }
    
    /**Its necessary to transmit additional notes
     */
    private MimeBodyPart createMDNNotesBody( String text )throws MessagingException{
        MimeBodyPart body = new MimeBodyPart();
        body.setText( text );
        body.setHeader( "Content-type", "text/plain" );
        body.setHeader( "Content-Transfer-Encoding", "7bit" );
        return( body );
    }
    
    /**Creates the MDN body and returns it
     *
     */
    private MimeBodyPart createMDNDispositionBody(AS2MessageInfo relatedMessageInfo, String dispositionState) throws MessagingException{
        MimeBodyPart body = new MimeBodyPart();
        StringBuffer buffer = new StringBuffer();
        buffer.append( "Reporting-UA: " + AS2ServerVersion.getProductName() + "\r\n" );
        buffer.append( "Original-Recipient: rfc822; " + relatedMessageInfo.getReceiverId() + "\r\n" );
        buffer.append( "Final-Recipient: rfc822; " + relatedMessageInfo.getReceiverId() + "\r\n" );
        buffer.append( "Original-Message-ID: <" + relatedMessageInfo.getMessageId() + ">\r\n" );
        buffer.append( "Disposition: automatic-action/MDN-sent-automatically; " + dispositionState + "\r\n" );
        if( relatedMessageInfo.getReceivedContentMIC() != null )
            buffer.append( "Received-Content-MIC: " + relatedMessageInfo.getReceivedContentMIC() + "\r\n" );
        body.setText( buffer.toString() );
        body.setHeader( "Content-type", "message/disposition-notification" );
        body.setHeader( "Content-Transfer-Encoding", "7bit" );
        return( body );
    }
    
    
    
    
    /**Builds up a new message from the passed message parts
     */
    public AS2Message createMessage( Partner sender, Partner receiver,
            File payloadFile ) throws Exception{
        AS2MessageInfo info = new AS2MessageInfo();
        info.setSenderId( sender.getAS2Identification() );
        info.setReceiverId( receiver.getAS2Identification() );
        info.setSenderEMail( sender.getEmail() );
        info.setMessageId( this.createId(sender.getAS2Identification(), receiver.getAS2Identification()) );
        info.setDirection( AS2MessageInfo.DIRECTION_OUT );
        info.setSignType( receiver.getSignType() );
        info.setEncryptionType( receiver.getEncryptionType() );
        info.setRequestsSyncMDN(receiver.isSyncMDN());
        if( !receiver.isSyncMDN() ){
            info.setAsyncMDNURL( sender.getMdnURL());
        }
        info.setSubject( receiver.getSubject() );
        //create message object to return
        AS2Message message = new AS2Message( info );
        InputStream inStream = new FileInputStream( payloadFile );
        ByteArrayOutputStream payloadOut = new ByteArrayOutputStream();
        this.copyStreams( inStream, payloadOut );
        inStream.close();
        payloadOut.flush();
        payloadOut.close();
        message.setPayload( payloadOut.toByteArray());
        //no signature, no encryption, raw transmission
        if( info.getSignType() == AS2Message.SIGNATURE_NONE
                && info.getEncryptionType() == AS2Message.ENCRYPTION_NONE ){
            //payload content type.
            message.setContentType( receiver.getContentType() );
            message.setRawData( payloadOut.toByteArray() );
            message.setDecryptedRawData( payloadOut.toByteArray() );
            this.logger.log( Level.INFO, this.rb.getResourceString( "message.notsigned",
                    new Object[]{
                info.getMessageId()
            }), info );
            this.logger.log( Level.INFO, this.rb.getResourceString( "message.notencrypted",
                    new Object[]{
                info.getMessageId()
            }
            ), info);
            return( message );
        }
        InternetHeaders headers = new InternetHeaders();
        headers.addHeader( "Content-Type", receiver.getContentType() );
        headers.addHeader( "Content-Transfer-Encoding", "binary" );
        headers.addHeader( "Content-Disposition", "attachment; filename=" + payloadFile.getName() );
        MimeBodyPart bodyPart = new MimeBodyPart( headers, message.getPayload() );
        MimeMessage messagePart = new MimeMessage( Session.getInstance(System.getProperties(), null));
        //sign message if this is requested
        if( info.getSignType() != AS2Message.SIGNATURE_NONE ){
            MimeMultipart signedPart = this.signMessage( bodyPart, sender, receiver );
            this.logger.log( Level.INFO, this.rb.getResourceString( "message.signed",
                    new Object[]{
                info.getMessageId(), sender.getCertAlias()
            }
            ), info);
            messagePart.setContent(signedPart);
            messagePart.saveChanges();
        } else{
            //unsigned message
            MimeMultipart unsignedPart = new MimeMultipart();
            unsignedPart.addBodyPart( bodyPart );
            this.logger.log( Level.INFO, this.rb.getResourceString( "message.notsigned",
                    new Object[]{
                info.getMessageId()
            }
            ), info);
            messagePart.setContent(unsignedPart);
            messagePart.saveChanges();
        }
        //store signed or unsigned data
        ByteArrayOutputStream signedOut = new ByteArrayOutputStream();
        messagePart.writeTo( signedOut,
                new String[]{ "Message-ID", "Mime-Version"
        } );
        message.setContentType( messagePart.getContentType() );
        signedOut.flush();
        signedOut.close();
        message.setDecryptedRawData( signedOut.toByteArray() );
        //encryption
        if( info.getEncryptionType() != AS2Message.ENCRYPTION_NONE ){
            X509Certificate certificate = this.certificateManager.getX509Certificate( receiver.getCertAlias());
            CMSEnvelopedDataGenerator dataGenerator =new CMSEnvelopedDataGenerator();
            dataGenerator.addKeyTransRecipient( certificate );
            CMSProcessableByteArray byteArray = new CMSProcessableByteArray( message.getDecryptedRawData() );
            CMSEnvelopedData envelopedData = dataGenerator.generate(byteArray, CMSEnvelopedDataGenerator.DES_EDE3_CBC, "BC");
            byte[] result = envelopedData.getEncoded();
            this.logger.log( Level.INFO, this.rb.getResourceString( "message.encrypted",
                    new Object[]{
                info.getMessageId(), receiver.getCertAlias()
            }
            ), info);
            message.setRawData( result );
        } else{
            message.setRawData( message.getDecryptedRawData() );
            this.logger.log( Level.INFO, this.rb.getResourceString( "message.notencrypted",
                    new Object[]{
                info.getMessageId()
            }
            ), info);
        }
        return( message );
    }
        
    /**Signs the passed message and returns it
     */
    private MimeMultipart signMessage( MimeBodyPart body, Partner sender, Partner receiver ) throws Exception{
        PrivateKey senderKey = this.certificateManager.getPrivateKey(sender.getCertAlias());
        Certificate[] chain = this.certificateManager.getCertificateChain(sender.getCertAlias());
        String digest = null;
        if( receiver.getSignType() == AS2Message.SIGNATURE_SHA1 ){
            digest = "sha1";
        } else if( receiver.getSignType() == AS2Message.SIGNATURE_MD5 ){
            digest = "md5";
        }
        BCCryptoHelper helper = new BCCryptoHelper();
        return( helper.sign( body, chain, senderKey, digest ));
    }
    
    /**Signs the passed mdn and returns it
     */
    private MimeMessage signMDN( MimeMessage message, Partner sender ) throws Exception{
        PrivateKey senderKey = this.certificateManager.getPrivateKey(sender.getCertAlias());
        Certificate[] chain = this.certificateManager.getCertificateChain(sender.getCertAlias());
        String digest = "sha1";
        BCCryptoHelper helper = new BCCryptoHelper();
        return( helper.sign( message, chain, senderKey, digest ));
    }
    
    
    /**Creates a "world-unique" content id according to RFC 2045
     */
    private String createId( String senderId, String receiverId ){
        StringBuffer idBuffer = new StringBuffer();
        idBuffer.append( AS2ServerVersion.getProductNameShortcut());
        idBuffer.append( "-" );
        idBuffer.append( String.valueOf( System.currentTimeMillis()));
        idBuffer.append( "-" );
        idBuffer.append( this.random.nextInt( 99999 ));
        idBuffer.append( "@@");
        idBuffer.append( senderId );
        idBuffer.append( "_" );
        idBuffer.append( receiverId );
        return( idBuffer.toString() );
    }
    
    /**Copies all data from one stream to another*/
    private void copyStreams( InputStream in, OutputStream out ) throws IOException{
        BufferedInputStream inStream = new BufferedInputStream( in );
        BufferedOutputStream outStream = new BufferedOutputStream( out );
        //copy the contents to an output stream
        byte[] buffer = new byte[1024];
        int read = 1024;
        //a read of 0 must be allowed, sometimes it takes time to
        //extract data from the input
        while( read != -1 ){
            read = inStream.read( buffer );
            if( read > 0 )
                outStream.write( buffer, 0, read );
        }
        outStream.flush();
    }
}
@
