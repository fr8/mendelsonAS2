head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2006.09.15.10.41.38;	author heller;	state Exp;
branches;
next	;


desc
@@


1.1
log
@*** empty log message ***
@
text
@//$Header: /mec_as2/de/mendelson/comm/as2/send/MessageHttpUploader.java 37    28.08.06 13:23 Heller $
package de.mendelson.comm.as2.send;
import de.mendelson.comm.as2.AS2ServerVersion;
import de.mendelson.comm.as2.client.AS2Gui;
import de.mendelson.comm.as2.client.rmi.GenericClient;
import de.mendelson.comm.as2.clientserver.ErrorObject;
import de.mendelson.comm.as2.clientserver.serialize.CommandObjectIncomingMessage;
import de.mendelson.comm.as2.message.AS2Message;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.MessageAccessDB;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.util.MecResourceBundle;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.Security;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpMethodRetryHandler;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;


/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Class to allow HTTP multipart uploads
 * @@author  S.Heller
 * @@version $Revision: 37 $
 */
public class MessageHttpUploader{
    
    private Logger logger = Logger.getLogger( "de.mendelson.as2" );
    
    private PreferencesAS2 preferences = new PreferencesAS2();
    
    /**localisze the GUI*/
    private MecResourceBundle rb = null;
    
    /**The header that has been built fro the request*/
    private Properties requestHeader = new Properties();
    
    /**remote answer*/
    private byte[] responseData = null;
    
    /**remote answer header*/
    private Header[] responseHeader = null;
    
    private AS2Gui gui;
    
    /** Creates new FTP
     * @@param hostname Name of the host to connect to
     * @@param username Name of the user that will connect to the remote ftp server
     * @@param password password to connect to the ftp server
     */
    public MessageHttpUploader( AS2Gui gui ) throws Exception {
        this.gui = gui;
        //Load default resourcebundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleHttpUploader.class.getName());
        }
        //load up resourcebundle
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
        System.setProperty("java.protocol.handler.pkgs",
                "com.sun.net.ssl.internal.www.protocol");
        System.setProperty("javax.net.ssl.trustStore",
                this.preferences.get( PreferencesAS2.KEYSTORE_HTTPS_SEND ));
        System.setProperty("javax.net.ssl.trustStorePassword",
                this.preferences.get( PreferencesAS2.KEYSTORE_HTTPS_SEND_PASS ));
        
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
    }
    
    /**Returns the created header for the sent data*/
    public Properties send( AS2Message message, Partner sender, Partner receiver ) throws Exception{
        AS2MessageInfo info = message.getMessageInfo();
        MessageAccessDB messageAccess = new MessageAccessDB( "localhost" );
        messageAccess.insertMessage( info );
        this.gui.refreshMessageOverviewList();
        int returnCode = this.performUpload( message, sender, receiver );
        if( returnCode == HttpServletResponse.SC_OK ){
            this.logger.log( Level.INFO,
                    this.rb.getResourceString( "standard.returncode",
                    new Object[]{
                info.getMessageId(),
                String.valueOf( returnCode )
            }), info );
        }else{
            messageAccess.setMessageState( info.getMessageId(),
                    AS2Message.STATE_STOPPED );
            if( returnCode < 0 ){
                throw new Exception( this.rb.getResourceString( "error.noconnection",
                        info.getMessageId() ));
            }
            throw new Exception( info.getMessageId() + ": HTTP " + returnCode );
        }
        //inform the server of the result if a sync MDN has been requested
        if( message.getMessageInfo().requestsSyncMDN()){
            GenericClient client = new GenericClient();
            CommandObjectIncomingMessage commandObject = new CommandObjectIncomingMessage();
            commandObject.setMessageData( this.responseData );
            for( int i = 0; i < this.responseHeader.length; i++ ){
                String key = this.responseHeader[i].getName();
                String value = this.responseHeader[i].getValue();
                commandObject.addHeader( key, value);
                if( key.toLowerCase().equals( "content-type" ))
                    commandObject.setContentType( value );
            }
            ErrorObject errorObject = client.send( commandObject );
            if( errorObject.getErrors() > 0 ){
                messageAccess.setMessageState( info.getMessageId(),
                        AS2Message.STATE_STOPPED );
            }
        }
        messageAccess.close();
        return( this.requestHeader );
    }
    
    /**Sets the proxy authentification for the client*/
    private void setProxyAuthentification( HttpClient client ){
        //the proxy authentification
        if( this.preferences.getBoolean( PreferencesAS2.AUTH_PROXY_USE )){
            UsernamePasswordCredentials credentials = null;
            String user = this.preferences.get( PreferencesAS2.AUTH_PROXY_USER );
            String pass = this.preferences.get( PreferencesAS2.AUTH_PROXY_PASS );
            String host = this.preferences.get( PreferencesAS2.AUTH_PROXY_HOST );
            if( user != null && user.length() > 0 ){
                credentials = new UsernamePasswordCredentials( user, pass );
            }
            client.getState().setProxyCredentials( AuthScope.ANY_REALM, host, credentials );
        }
    }
    
    
    /**Uploads the data, returns the HTTP result code*/
    private int performUpload( AS2Message message, Partner sender, Partner receiver ){
        PostMethod filePost = null;
        int status = -1; 
        try {
            //find out the URL where to send the message to
            URL receiptURL = null;
            //async MDN?
            if( message.getMessageInfo().isMDN()){
                receiptURL = new URL( message.getMessageInfo().getAsyncMDNURL());
            }
            else{
                receiptURL = new URL( receiver.getURL() );
            }            
            filePost = new PostMethod(receiptURL.toExternalForm());
            filePost.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new PostHttpMethodRetryHandler());
            //filePost.getParams().setParameter(HttpMethodParams.SO_TIMEOUT, new Integer(0) );
            filePost.getParams().setParameter(HttpMethodParams.USE_EXPECT_CONTINUE, new Boolean( true ) );                       
            filePost.addRequestHeader( "AS2-Version", "1.1" );
            filePost.addRequestHeader( "mime-version", "1.0" );
            filePost.addRequestHeader( "User-Agent", AS2ServerVersion.getProductName() + " " 
                        + AS2ServerVersion.getVersion() + " " + AS2ServerVersion.getBuild() + " - www.mendelson-e-c.com");
            filePost.addRequestHeader( "recipient-address", receiptURL.toExternalForm() );
            filePost.addRequestHeader( "message-id", "<" + message.getMessageInfo().getMessageId() + ">");
            filePost.addRequestHeader( "as2-from", sender.getAS2Identification() );
            filePost.addRequestHeader( "as2-to", receiver.getAS2Identification() );
            filePost.addRequestHeader( "subject", message.getMessageInfo().getSubject() );
            filePost.addRequestHeader( "from", sender.getEmail() );
            filePost.addRequestHeader( "connection","close, TE" );
            DateFormat format = new SimpleDateFormat( "EE, dd MMM yyyy HH:mm:ss zz");
            filePost.addRequestHeader( "date", format.format( new Date() ));
            if( message.getMessageInfo().getEncryptionType() != AS2Message.ENCRYPTION_NONE ){
                filePost.addRequestHeader( "Content-Type", "application/pkcs7-mime; smime-type=enveloped-data; name=smime.p7m" );
            } else{
                filePost.addRequestHeader( "Content-Type", message.getContentType() );
            }
            //MDN header, this is always the way for async MDNs
            if( message.getMessageInfo().isMDN()){
                this.logger.log( Level.INFO,
                        this.rb.getResourceString( "sending.mdn.async",
                        new Object[]{
                    message.getMessageInfo().getMessageId(),
                    receiptURL.toExternalForm()
                }), message.getMessageInfo() );
                filePost.addRequestHeader( "Server", AS2ServerVersion.getProductName());
            } else{
                //message header
                if( message.getMessageInfo().requestsSyncMDN()){
                    this.logger.log( Level.INFO,
                            this.rb.getResourceString( "sending.msg.sync",
                            new Object[]{
                        message.getMessageInfo().getMessageId(),
                        receiptURL.toExternalForm()
                    }), message.getMessageInfo() );
                } else{
                    this.logger.log( Level.INFO,
                            this.rb.getResourceString( "sending.msg.async",
                            new Object[]{
                        message.getMessageInfo().getMessageId(),
                        receiptURL.toExternalForm()
                    }), message.getMessageInfo() );
                }
                if( !message.getMessageInfo().requestsSyncMDN()){
                    filePost.addRequestHeader( "receipt-delivery-option", receiptURL.toExternalForm() );
                }
                filePost.addRequestHeader( "disposition-notification-to", sender.getMdnURL() );
                filePost.addRequestHeader( "disposition-notification-options", "signed-receipt-protocol=optional, pkcs7-signature;signed-receipt-micalg=optional, sha1" );
                if( message.getMessageInfo().getSignType() != AS2Message.SIGNATURE_NONE ){
                    filePost.addRequestHeader( "content-disposition", "attachment; filename=\"smime.p7m\"" );
                }
            }
            int port = receiptURL.getPort();
            if( port == -1 )
                port = receiptURL.getDefaultPort();
            filePost.addRequestHeader( "host", receiptURL.getHost() + ":" + port );
            filePost.setRequestEntity( new ByteArrayRequestEntity( message.getRawData() ));
            HttpClient client = new HttpClient();
            this.setProxyAuthentification(client);
            client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
            //tcp socket: no timeout
            status = client.executeMethod(filePost);
            this.responseData = filePost.getResponseBody();
            this.responseHeader = filePost.getResponseHeaders();
            Header[] requestHeader = filePost.getRequestHeaders();
            for( int i = 0; i < requestHeader.length; i++ ){
                this.requestHeader.setProperty( requestHeader[i].getName(), requestHeader[i].getValue() );
            }
            if( status != HttpServletResponse.SC_OK ){
                this.logger.severe(
                        this.rb.getResourceString( "error.httpupload",
                        new Object[]{ message.getMessageInfo().getMessageId(),
                        URLDecoder.decode(filePost.getStatusText(), "UTF-8")
                }));
            }
        } catch (Exception ex) {
            this.logger.severe( message.getMessageInfo().getMessageId() + ": "
                    + ex.getMessage());
        } finally {
            if( filePost != null )
                filePost.releaseConnection();
        }
        return( status );
    }
    
    /**Returns the version of this class*/
    public static String getVersion(){
        String revision = "$Revision: 37 $";
        return( revision.substring( revision.indexOf( ":" )+1,
                revision.lastIndexOf( "$" ) ).trim());
    }
    
    
    /**Handler class for customized retry behavior*/
    public class PostHttpMethodRetryHandler implements HttpMethodRetryHandler {
        
        public boolean retryMethod( final HttpMethod method,
                final IOException exception,
                int executionCount) {
//            if (executionCount >= 5) {
//                // Do not retry if over max retry count
//                return false;
//            }
//            if (exception instanceof NoHttpResponseException) {
//                // Retry if the server dropped connection on us
//                return true;
//            }
//            if (!method.isRequestSent()) {
//                // Retry if the request has not been sent fully or
//                // if it's OK to retry methods that have been sent
//                return true;
//            }
            // otherwise do not retry
            return false;
        }
    }
}
@
