head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2006.09.15.10.41.33;	author heller;	state Exp;
branches;
next	;


desc
@@


1.1
log
@*** empty log message ***
@
text
@//$Header: /mec_as2/de/mendelson/comm/as2/cert/CertificateManager.java 2     28.06.06 11:01 Heller $
package de.mendelson.comm.as2.cert;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.util.MecResourceBundle;
import de.mendelson.util.security.BCCryptoHelper;
import de.mendelson.util.security.KeyStoreUtil;
import java.io.File;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Helper class to store
 * @@author S.Heller
 * @@version $Revision: 2 $
 */
public class CertificateManager{
    
    private Logger logger = Logger.getLogger( "de.mendelson.as2" );
    
    private PreferencesAS2 preferences = new PreferencesAS2();
    
    private ArrayList<KeystoreCertificate> keyStoreCertificateList
            = new ArrayList<KeystoreCertificate>();
    
    private KeyStore keystore = null;
    
    private char[] keystorePass = null;
    
    private String keystoreFilename = null;
    
    private KeyStoreUtil keystoreUtil = new KeyStoreUtil();
    
    private KeystoreFileWatcher keystoreFileWatcher = null;
    
    private MecResourceBundle rb = null;
    
    public CertificateManager(){
        //load resource bundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleCertificateManager.class.getName());
        } catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
    }
    
    /**Returns the certificate chain for a special alias
     */
    public Certificate[] getCertificateChain( String alias )throws Exception{
        Certificate[] chain = this.keystore.getCertificateChain( alias );
        return( chain );
    }
    
    /**Returns the X509 certificate assigned to the passed alias
     */
    public X509Certificate getX509Certificate( String alias ) throws Exception{
        X509Certificate certificate = (X509Certificate)this.keystore.getCertificate( alias );
        if( certificate == null )
            throw new Exception( this.rb.getResourceString( "alias.notfound", alias ));
        return( certificate );
    }
    
    /**Returns the private key for an alias. If the assigned certificate
     *does not contain a private key an exception is thrown
     */
    public PrivateKey getPrivateKey( String alias ) throws Exception {
        PrivateKey key = (PrivateKey)this.keystore.getKey(alias, this.keystorePass);
        if(key == null)
            throw new Exception( this.rb.getResourceString( "alias.hasno.privatekey", alias ));
        else
            return( key );
    }
    
    /**Returns the public key or the private key for an alias.
     */
    public Key getKey( String alias ) throws Exception {
        Key key = this.keystore.getKey(alias, this.keystorePass);
        if(key == null)
            throw new Exception( this.rb.getResourceString( "alias.hasno.key", alias ));
        else
            return( key );
    }
    
    
    /**Stores the manages keystore
     */
    public void saveKeystore() throws Exception{
        if( this.getKeystore() == null ){
            //internal error, should not happen
            this.getLogger().severe( "saveKeystore: Unable to save keystore, keystore is not loaded.");
            return;
        }
        KeyStoreUtil keystoreUtil = new KeyStoreUtil();
        keystoreUtil.saveKeyStore( this.getKeystore(), this.getKeystorePass(), this.keystoreFilename );
    }
    
    /**Deletes an entry from the actual keystore*/
    public void deleteKeystoreEntry( String alias ) throws Exception{
        if( this.getKeystore() == null ){
            //internal error, should not happen
            this.getLogger().severe( "deleteKeystoreEntry: Unable to delete entry, keystore is not loaded.");
            return;
        }
        this.getKeystore().deleteEntry( alias );
        this.rereadKeystoreCertificates();
    }
    
    /**Renames an entry in the underlaying keystore
     */
    public void renameAlias( String oldAlias, String newAlias ) throws Exception{
        KeyStoreUtil keystoreUtil = new KeyStoreUtil();
        keystoreUtil.renameEntry( this.keystore, oldAlias, newAlias, null );
        this.rereadKeystoreCertificates();
    }
    
    /**Refreshes the data
     */
    public void rereadKeystoreCertificates() throws Exception{
        HashMap certificates = this.keystoreUtil.getCertificatesFromKeystore(this.getKeystore());
        this.getKeyStoreCertificateList().clear();
        Iterator iterator = certificates.keySet().iterator();
        while( iterator.hasNext() ){
            KeystoreCertificate certificate = new KeystoreCertificate();
            String alias = (String)iterator.next();
            certificate.setAlias( alias );
            X509Certificate foundCertificate
                    = (X509Certificate)certificates.get(alias);
            certificate.setCertificate( foundCertificate );
            try{
                certificate.setIsKeyPair( getKeystore().isKeyEntry(alias));
            } catch( Exception e){
                //no problem, thats what we wanted to know
                certificate.setIsKeyPair( false );
                this.getLogger().warning( e.getMessage() );
            }
            this.getKeyStoreCertificateList().add( certificate );
        }
    }
    
    
    /**Reads the certificates of the actual key store
     */
    public void loadKeystoreCertificates( String keystoreFilename, char[] password ){
        try{
            BCCryptoHelper cryptoHelper = new BCCryptoHelper();
            this.keystore = cryptoHelper.createKeyStoreInstance();
            this.keystoreUtil.loadKeyStore( getKeystore(), keystoreFilename, password );
            this.keystorePass = password;
            this.keystoreFilename = keystoreFilename;
            this.rereadKeystoreCertificates();
            this.logger.info( this.rb.getResourceString( "keystore.loaded",
                    new File( this.keystoreFilename ).getAbsolutePath() ));
            if( this.keystoreFileWatcher != null ){
                this.keystoreFileWatcher.requestStop();
            }
            this.keystoreFileWatcher = new KeystoreFileWatcher( new File( this.keystoreFilename ));
            this.keystoreFileWatcher.setName( "Watching keystore");
            this.keystoreFileWatcher.start();
        } catch( Exception e ){
            this.getLogger().severe( "readKeystoreCertificates: " + e.getMessage() );
        }
    }
    
    public ArrayList<KeystoreCertificate> getKeyStoreCertificateList() {
        return keyStoreCertificateList;
    }
    
    public void setKeyStoreCertificateList(ArrayList keyStoreCertificateList) {
        this.keyStoreCertificateList = keyStoreCertificateList;
    }
    
    public Logger getLogger() {
        return logger;
    }
    
    public void setLogger(Logger logger) {
        this.logger = logger;
    }
    
    public KeyStore getKeystore() {
        return keystore;
    }
    
    public char[] getKeystorePass() {
        return keystorePass;
    }
    
    /**Thread that watches this certificate file size and refreshes the certificates automatically
     */
    public class KeystoreFileWatcher extends Thread{
        private File keystoreFile = null;
        
        private long lastModified;
        
        private boolean requestStop = false;
        
        public KeystoreFileWatcher( File keystoreFile ){
            this.keystoreFile = keystoreFile;
            this.lastModified = this.keystoreFile.lastModified();
        }
        
        public void requestStop(){
            this.requestStop = true;
        }
        
        public void run(){
            while( !this.requestStop ){
                try{
                    this.sleep( 4000 );
                } catch( InterruptedException e ){
                    //nop
                }
                if( this.requestStop ){
                    continue;
                }
                long modificationCheck = this.keystoreFile.lastModified();
                //must not bee younger, user could copy an old certificate store, too
                if( modificationCheck != this.lastModified ){
                    logger.fine( rb.getResourceString( "keystore.modified" ));
                    //set new modification date
                    this.lastModified = modificationCheck;
                    try{
                        rereadKeystoreCertificates();
                    } catch( Exception e ){
                        logger.severe( "rereadKeystore: " + e.getMessage() );
                    }
                }
            }
        }
    }
    
    
}@
