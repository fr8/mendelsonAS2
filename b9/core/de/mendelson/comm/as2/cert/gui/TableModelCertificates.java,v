head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2006.09.15.10.41.34;	author heller;	state Exp;
branches;
next	;


desc
@@


1.1
log
@*** empty log message ***
@
text
@//$Header: /as2/de/mendelson/comm/as2/cert/gui/TableModelCertificates.java 2     15.02.06 14:27 Heller $
package de.mendelson.comm.as2.cert.gui;
import de.mendelson.comm.as2.cert.KeystoreCertificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;
import de.mendelson.util.MecResourceBundle;
import de.mendelson.util.table.AbstractTableModelSortable;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * table model to display a configuration grid
 * @@author  S.Heller
 * @@version $Revision: 2 $
 */
public class TableModelCertificates extends AbstractTableModelSortable{
        
    public static final ImageIcon ICON_CERTIFICATE = 
            new ImageIcon( TableModelCertificates.class.getResource(
                "/de/mendelson/comm/as2/cert/gui/certificate16x16.gif" ));
    public static final ImageIcon ICON_KEY = 
            new ImageIcon( TableModelCertificates.class.getResource(
                "/de/mendelson/comm/as2/cert/gui/key16x16.gif" ));
    
    /*ResourceBundle to localize the headers*/
    private MecResourceBundle rb = null;
        
    private DateFormat format 
            = SimpleDateFormat.getDateInstance( DateFormat.SHORT );
    
    /** Creates new table model
     */
    public TableModelCertificates() {
        //load resource bundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
            ResourceBundleCertificates.class.getName());
        }
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
            + e.getClassName() + " not found." );
        }
    }
    
    /**Passes data to the model and fires a table data update
     *@@param list new data to display
     */
    public void setNewData( ArrayList list ) {
        if( super.getData() != null )
            throw new RuntimeException( 
                    "Fatal: setNewData could only be called once!" );
        else{
            super.setData( list );
        }
        super.resort();
        ((AbstractTableModel)this).fireTableDataChanged();
    }
                        
    /**returns the number of rows in the table*/
    public int getRowCount() {
        if( super.getData() == null )
            return( 0 );
        return( super.getData().size() );
    }
    
    /**returns the number of columns in the table. should be const for a table*/
    public int getColumnCount() {
        return(4);
    }
    
    /**Returns the name of every column
     *@@param col Column to get the header name of
     */
    public String getColumnName( int col ){        
        if( col == 0 )
            return( " ");
        if( col == 1 )
            return( this.rb.getResourceString( "header.alias" ) );
        if( col == 2 )
            return( this.rb.getResourceString( "header.expire" ) );
        if( col == 3 )
            return( this.rb.getResourceString( "header.length" ) );
        //should not happen
        return( "" );
    }
        
    /**Returns the grid value*/
    public Object getValueAt( int row, int col ){
        KeystoreCertificate certificate = (KeystoreCertificate)super.getData().get(row);
        if( col == 0 ){
            if( certificate.getIsKeyPair() )
                return( this.ICON_KEY );
            return( this.ICON_CERTIFICATE );                
        }
        if( col == 1 )
            return( certificate.getAlias() );
        if( col == 2 ){
            return( this.format.format( certificate.getExpireDate()));
        }
        if( col == 3 ){
            return( String.valueOf(certificate.getPublicKeyLength()));
        }
        return( "" );
    }
    
    /**Swing GUI checks which cols are editable.
     */
    public boolean isCellEditable(int row, int col) {
        return( false );
    }
        
    /**Set how to display the grid elements
     * @@param col requested column
     */
    public Class getColumnClass( int col ){
        return(new Class[]{
            ImageIcon.class,
            String.class,
            String.class,
            String.class                            
        }[col] );
    }
    
    /**Indicates which rows are sortable*/
    public boolean isSortable( int row ){
        return( true  );
    }

    public int getDefaultSortColumn() {
        return( 1 );
    }

    public boolean getDefaultSortOrderIsAscending() {
        return( true );
    }
    
    /**Returns the certificate at the passed row
     */
    public KeystoreCertificate getParameter( int row ){
        return( (KeystoreCertificate)super.getData().get(row));
    }
    
}
@
