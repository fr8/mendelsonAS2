head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.33.41;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2011.08.08.13.59.02;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b35/de/mendelson/comm/as2/jms/JMSMessageReceiver.java,v 1.1 2011/08/08 13:59:02 heller Exp $
package de.mendelson.comm.as2.jms;

import de.mendelson.util.security.cert.CertificateManager;
import de.mendelson.comm.as2.clientserver.message.RefreshClientMessageOverviewList;
import de.mendelson.comm.as2.message.AS2MDNInfo;
import de.mendelson.comm.as2.message.AS2Message;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.ExecuteShellCommand;
import de.mendelson.comm.as2.message.MessageAccessDB;
import de.mendelson.comm.as2.message.store.MessageStoreHandler;
import de.mendelson.comm.as2.notification.Notification;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.comm.as2.send.HttpConnectionParameter;
import de.mendelson.comm.as2.send.MessageHttpUploader;
import de.mendelson.comm.as2.server.AS2Server;
import de.mendelson.util.MecResourceBundle;
import de.mendelson.util.clientserver.ClientServer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * Listener thread that listens on the message queue to pick up enqueued messages and process them
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class JMSMessageReceiver implements Runnable {

    public static final boolean DEBUG = false;
    /**Server preferences*/
    private PreferencesAS2 preferences = new PreferencesAS2();
    private Logger logger = Logger.getLogger(AS2Server.SERVER_LOGGER_NAME);
    /**Name of the thread, for debugging purpose*/
    private String threadName = null;
    /**Properties for the java directory service*/
    private Properties jndiProperties = null;
    /**Thread will stop if this is no longer set*/
    private boolean runPermission = true;
    /**Needed for refresh*/
    private ClientServer clientserver = null;
    /**Handles messages storage*/
    private MessageStoreHandler messageStoreHandler;
    /**ResourceBundle to localize the output*/
    private MecResourceBundle rb = null;
    //DB connection
    private java.sql.Connection dbConnection;
    //manage the certificate
    private CertificateManager certificateManager;

    /**Stops the listener*/
    public void stopListener() {
        this.runPermission = false;
    }

    public JMSMessageReceiver(ClientServer clientserver, java.sql.Connection dbConnection, String threadName,
            CertificateManager certificateManager) {
        this.dbConnection = dbConnection;
        //Load default resourcebundle
        try {
            this.rb = (MecResourceBundle) ResourceBundle.getBundle(
                    ResourceBundleJMSMessageReceiver.class.getName());
        } //load up resourcebundle
        catch (MissingResourceException e) {
            throw new RuntimeException("Oops..resource bundle " + e.getClassName() + " not found.");
        }
        this.threadName = threadName;
        this.clientserver = clientserver;
        this.messageStoreHandler = new MessageStoreHandler(this.dbConnection);
        this.jndiProperties = this.getDefaultJNDIProperties();
        this.certificateManager = certificateManager;
    }

    /**Sets the directory servive properties, the server will not start up without them
     */
    private Properties getDefaultJNDIProperties() {
        Properties properties = new Properties();
        properties.setProperty("java.naming.factory.initial",
                "fr.dyade.aaa.jndi2.client.NamingContextFactory");
        properties.setProperty("java.naming.factory.host", "localhost");
        properties.setProperty("java.naming.factory.port",
                this.preferences.get(PreferencesAS2.JNDI_PORT));
        return (properties);
    }

    @@Override
    public void run() {
        javax.jms.Queue queue = null;
        QueueConnectionFactory queueFactory = null;
        QueueConnection queueConnection = null;
        QueueSession session = null;
        MessageConsumer receiver = null;
        try {
            Context context = new InitialContext(this.jndiProperties);
            queue = (javax.jms.Queue) context.lookup("as2_messagequeue");
            queueFactory = (QueueConnectionFactory) context.lookup("queueFactory");
            context.close();
            queueConnection = queueFactory.createQueueConnection("as2user", "messagequeuepasswd");
            session = queueConnection.createQueueSession(true, 0);
            receiver = session.createReceiver(queue);
            queueConnection.start();
        } catch (Exception e) {
            logger.warning(threadName + ": " + e.getMessage());
            throw new RuntimeException(e);
        }
        //listen until stop is requested
        while (this.runPermission) {
            try {
                Message message = receiver.receive();
                session.commit();
                MessageAccessDB messageAccess = new MessageAccessDB(this.dbConnection);
                if (message != null && message instanceof ObjectMessage && ((ObjectMessage) message).getObject() instanceof SendOrder) {
                    SendOrder order = (SendOrder) ((ObjectMessage) message).getObject();
                    try {
                        MessageHttpUploader messageUploader = new MessageHttpUploader();
                        messageUploader.setLogger(this.logger);
                        messageUploader.setAbstractServer(this.clientserver);
                        messageUploader.setDBConnection(this.dbConnection);
                        //configure the connection parameters
                        HttpConnectionParameter connectionParameter = new HttpConnectionParameter();
                        connectionParameter.setConnectionTimeoutMillis(this.preferences.getInt(PreferencesAS2.HTTP_SEND_TIMEOUT));
                        connectionParameter.setHttpProtocolVersion( order.getReceiver().getHttpProtocolVersion() );
                        connectionParameter.setProxy( messageUploader.createProxyObjectFromPreferences());
                        connectionParameter.setUseExpectContinue(true);
                        Properties requestHeader = messageUploader.upload(connectionParameter, order.getMessage(), order.getSender(), order.getReceiver());
                        //set error or finish state, remember that this send order could be
                        //also an MDN if async MDN is requested
                        if (order.getMessage().isMDN()) {
                            AS2MDNInfo mdnInfo = (AS2MDNInfo) order.getMessage().getAS2Info();
                            if (mdnInfo.getState() == AS2Message.STATE_FINISHED) {
                                AS2MessageInfo relatedMessageInfo = messageAccess.getLastMessageEntry(mdnInfo.getRelatedMessageId());
                                messageStoreHandler.movePayloadToInbox(relatedMessageInfo.getMessageType(), mdnInfo.getRelatedMessageId(),
                                        order.getSender(), order.getReceiver());
                                //execute a shell command after send SUCCESS
                                ExecuteShellCommand executeCommand = new ExecuteShellCommand(this.dbConnection);
                                //switch sender and receiver because its the MDN sender that is requested, not the message sender
                                executeCommand.executeShellCommandOnReceipt(order.getReceiver(), order.getSender(), relatedMessageInfo);
                            }
                            //set the transaction state to the MDN state
                            messageAccess.setMessageState(mdnInfo.getRelatedMessageId(), mdnInfo.getState());
                        } else {
                            //its a AS2 message
                            AS2MessageInfo messageInfo = (AS2MessageInfo) order.getMessage().getAS2Info();
                            messageAccess.updateFilenames(messageInfo);
                            if (!messageInfo.requestsSyncMDN()) {
                                long endTime = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(preferences.getInt(PreferencesAS2.ASYNC_MDN_TIMEOUT));
                                DateFormat format = SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT,
                                        DateFormat.MEDIUM);
                                logger.log(Level.INFO, rb.getResourceString("async.mdn.wait",
                                        new Object[]{
                                            order.getMessage().getAS2Info().getMessageId(),
                                            format.format(endTime)
                                        }), messageInfo);
                            }                            
                        }
                        this.clientserver.broadcastToClients(new RefreshClientMessageOverviewList());
                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.log(Level.SEVERE, e.getMessage(), order.getMessage().getAS2Info());
                        try {
                            //stores
                            messageStoreHandler.storeSentErrorMessage(
                                    order.getMessage(), order.getSender(), order.getReceiver());
                            if (!order.getMessage().isMDN()) {
                                //message upload failure
                                messageAccess.setMessageState(order.getMessage().getAS2Info().getMessageId(),
                                        AS2Message.STATE_STOPPED);
                                //its important to set the state in the message info, too. An event exec is not performed
                                //for pending messages
                                order.getMessage().getAS2Info().setState(AS2Message.STATE_STOPPED);
                                messageAccess.updateFilenames((AS2MessageInfo) order.getMessage().getAS2Info());
                                //execute a shell command after send ERROR if this is configured (sync MDN, async MDN)
                                ExecuteShellCommand executor = new ExecuteShellCommand(this.dbConnection);
                                executor.executeShellCommandOnSend((AS2MessageInfo) order.getMessage().getAS2Info(), null);
                                //write status file
                                messageStoreHandler.writeOutboundStatusFile((AS2MessageInfo) order.getMessage().getAS2Info());
                            } else {
                                //MDN send failure, e.g. wrong URL for async MDN in message
                                messageAccess.setMessageState(((AS2MDNInfo) order.getMessage().getAS2Info()).getRelatedMessageId(),
                                        AS2Message.STATE_STOPPED);
                            }
                            this.clientserver.broadcastToClients(new RefreshClientMessageOverviewList());
                        } catch (Exception ee) {
                            logger.log(Level.SEVERE, ee.getMessage(), order.getMessage().getAS2Info());
                        }
                    }
                } else {
                    //no ObjectMessage found
                    this.logger.warning(threadName + ": Unknown message type, thrown away.");
                }
            } catch (javax.jms.IllegalStateException ex) {
                //jms connection seems to be down: try to recover it
                try {
                    Context context = new InitialContext(this.jndiProperties);
                    queue = (javax.jms.Queue) context.lookup("as2_messagequeue");
                    queueFactory = (QueueConnectionFactory) context.lookup("queueFactory");
                    context.close();
                    queueConnection = queueFactory.createQueueConnection("as2user", "messagequeuepasswd");
                    session = queueConnection.createQueueSession(true, 0);
                    receiver = session.createReceiver(queue);
                    queueConnection.start();
                } catch (Exception e) {
                    logger.warning(threadName + ": " + e.getMessage());
                    throw new RuntimeException(e);
                }
            } catch (JMSException e) {
                logger.warning("JMSMessageReceiver " + threadName + " [" + e.getClass().getName() + "]" + ": " + e.getMessage()
                        + " [" + e.getLinkedException().getClass().getName() + ": " + e.getLinkedException().getMessage() + "]");
            } catch (Throwable e) {
                logger.warning("JMSMessageReceiver " + threadName + " [" + e.getClass().getName() + "]" + ": " + e.getMessage());
                Notification.systemFailure(dbConnection, e);
            }
        }
        if (queueConnection != null) {
            try {
                queueConnection.close();
            } catch (Exception e) {
                //nop
            }
        }
    }
}
@


1.1
log
@
Committed on the Free edition of March Hare Software CVSNT Server.
Upgrade to CVS Suite for more features and support:
http://march-hare.com/cvsnt/
@
text
@d1 1
a1 1
//$Header: /mec_as2/de/mendelson/comm/as2/jms/JMSMessageReceiver.java 41    20.04.11 15:12 Heller $
d47 1
a47 1
 * @@version $Revision: 41 $
@

