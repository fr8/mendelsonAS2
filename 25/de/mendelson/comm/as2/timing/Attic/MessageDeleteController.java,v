head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2009.03.16.12.10.30;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2008.09.01.15.37.11;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/25/de/mendelson/comm/as2/timing/MessageDeleteController.java,v 1.1 2008/09/01 15:37:11 heller Exp $
package de.mendelson.comm.as2.timing;

import de.mendelson.comm.as2.clientserver.message.RefreshClientMessageOverviewList;
import de.mendelson.comm.as2.log.LogAccessDB;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.MessageAccessDB;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.util.MecResourceBundle;
import de.mendelson.util.clientserver.AbstractServer;
import java.io.File;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Controlles the timed deletion of AS2 entries from the log
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class MessageDeleteController {

    /**Logger to log inforamtion to*/
    private Logger logger = Logger.getLogger("de.mendelson.as2.server");
    private PreferencesAS2 preferences = new PreferencesAS2();
    private MessageDeleteThread deleteThread;
    private AbstractServer abstractServer = null;
    private MecResourceBundle rb = null;

    public MessageDeleteController() {
        this(null);
    }

    public MessageDeleteController(AbstractServer abstractServer) {
        this.abstractServer = abstractServer;
        //Load default resourcebundle
        try {
            this.rb = (MecResourceBundle) ResourceBundle.getBundle(
                    ResourceBundleMessageDeleteController.class.getName());
        } //load up resourcebundle
        catch (MissingResourceException e) {
            throw new RuntimeException("Oops..resource bundle " + e.getClassName() + " not found.");
        }
    }

    /**Starts the embedded task that guards the log
     */
    public void startAutoDeleteControl() {
        this.deleteThread = new MessageDeleteThread();
        this.deleteThread.setName("Contol auto as2 message delete");
        this.deleteThread.start();
    }

    /**Deletes a message entry from the log. Clears all files
     */
    public void deleteMessageFromLog(AS2MessageInfo info) {
        MessageAccessDB messageAccess = null;
        try {
            messageAccess = new MessageAccessDB("localhost");
            //delete all raw files from the disk
            String[] rawfilenames = messageAccess.getRawFilenamesToDelete(info);
            if (rawfilenames != null) {
                for (String rawfilename : rawfilenames) {
                    new File(rawfilename).delete();
                }
            }
            messageAccess.deleteMessage(info);
        } catch (Exception e) {
            this.logger.severe("deleteMessageFromLog: " + e.getMessage());
        } finally {
            if (messageAccess != null) {
                messageAccess.close();
            }
        }
        LogAccessDB logAccess = null;
        try {
            logAccess = new LogAccessDB("localhost");
            logAccess.deleteMessageLog(info.getMessageId());
        } catch (Exception e) {
            this.logger.severe("deleteMessageFromLog: " + e.getMessage());
        } finally {
            if (logAccess != null) {
                logAccess.close();
            }
        }
        if (this.abstractServer != null) {
            this.abstractServer.broadcastToClients(new RefreshClientMessageOverviewList());
        }
    }

    public class MessageDeleteThread extends Thread {

        private boolean stopRequested = false;
        //wait this time between checks, once an hour
        private final long WAIT_TIME = TimeUnit.HOURS.toMillis(1);

        public MessageDeleteThread() {
        }

        @@Override
        public void run() {
            while (!stopRequested) {
                try {
                    this.sleep(WAIT_TIME);
                } catch (InterruptedException e) {
                    //nop
                }
                if (preferences.getBoolean(PreferencesAS2.AUTO_MSG_DELETE)) {
                    MessageAccessDB messageAccess = null;
                    try {
                        messageAccess = new MessageAccessDB("localhost");
                        long olderThan = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(preferences.getInt(PreferencesAS2.AUTO_MSG_DELETE_OLDERTHAN));
                        AS2MessageInfo info[] = messageAccess.getMessagesOlderThan(olderThan, -1);
                        if (info != null) {
                            for (int i = 0; i < info.length; i++) {
                                if (preferences.getBoolean(PreferencesAS2.AUTO_MSG_DELETE_LOG)) {
                                    logger.fine(rb.getResourceString("autodelete",
                                            new Object[]{
                                                info[i].getMessageId(),
                                                String.valueOf(preferences.getInt(PreferencesAS2.AUTO_MSG_DELETE_OLDERTHAN))
                                            }));
                                }
                                deleteMessageFromLog(info[i]);
                            }
                        }
                    } catch (Exception e) {
                        logger.severe("MessageDeleteThread: " + e.getMessage());
                    } finally {
                        if (messageAccess != null) {
                            messageAccess.close();
                        }
                    }
                }
            }
        }
    }
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/timing/MessageDeleteController.java 14    14.08.08 11:48 Heller $
d27 1
a27 1
 * @@version $Revision: 14 $
@

