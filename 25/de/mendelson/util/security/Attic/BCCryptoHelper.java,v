head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2009.03.16.12.10.31;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2008.09.01.15.38.38;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/25/de/mendelson/util/security/BCCryptoHelper.java,v 1.1 2008/09/01 15:38:38 heller Exp $
package de.mendelson.util.security;

import java.io.*;
import java.security.*;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.*;
import org.bouncycastle.asn1.DEREncodableVector;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.smime.SMIMECapabilitiesAttribute;
import org.bouncycastle.asn1.smime.SMIMECapability;
import org.bouncycastle.asn1.smime.SMIMECapabilityVector;
import org.bouncycastle.cms.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.mail.smime.*;
import org.bouncycastle.util.encoders.Base64;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */

/**
 * Utility class to handle bouncycastle cryptography
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class BCCryptoHelper {

    public static final String ALGORITHM_3DES = "3des";
    public static final String ALGORITHM_DES = "des";
    public static final String ALGORITHM_RC2 = "rc2";
    public static final String ALGORITHM_RC4 = "rc4";
    public static final String ALGORITHM_AES_128 = "aes128";
    public static final String ALGORITHM_AES_192 = "aes192";
    public static final String ALGORITHM_AES_256 = "aes256";
    public static final String ALGORITHM_MD5 = "md5";
    public static final String ALGORITHM_SHA1 = "sha1";
    public static final String ALGORITHM_IDEA = "idea";
    public static final String ALGORITHM_CAST5 = "cast5";
    public static final String KEYSTORE_PKCS12 = "PKCS12";
    public static final String KEYSTORE_JKS = "JKS";

    public BCCryptoHelper() {
    }

    public boolean isEncrypted(MimeBodyPart part) throws MessagingException {
        ContentType contentType = new ContentType(part.getContentType());
        String baseType = contentType.getBaseType().toLowerCase();
        if (baseType.equalsIgnoreCase("application/pkcs7-mime")) {
            String smimeType = contentType.getParameter("smime-type");
            return smimeType != null && smimeType.equalsIgnoreCase("enveloped-data");
        } else {
            return false;
        }
    }

    /**Returns the signed part of this container if it exists, else null. If the container itself
     *is signed it is returned. You could use this method if you are not sure if the main container
     *of a message is the signed part or if there are some unused MIME wrappers around it
     *that embedd the signed part
     */
    public Part getSignedEmbeddedPart(Part part) throws MessagingException, IOException {
        if (part.isMimeType("multipart/signed")) {
            return (part);
        }
        if (part.isMimeType("multipart/*")) {
            Multipart multiPart = (Multipart) part.getContent();
            int count = multiPart.getCount();
            for (int i = 0; i < count; i++) {
                BodyPart bodyPart = multiPart.getBodyPart(i);
                Part signedEmbeddedPart = this.getSignedEmbeddedPart(bodyPart);
                if (signedEmbeddedPart != null) {
                    return (signedEmbeddedPart);
                }
            }
        }
        return (null);
    }

    /**Calculates the hash value for a passed byte array, base 64 encoded
     *@@param digestAlgOID digest OID algorithm, e.g. "1.3.14.3.2.26"
     */
    public String calculateMIC(byte[] data, String digestAlgOID) throws GeneralSecurityException, MessagingException, IOException {
        MessageDigest messageDigest = MessageDigest.getInstance(digestAlgOID, "BC");
        InputStream bIn = this.trimCRLFPrefix(data);
        DigestInputStream digestInputStream = new DigestInputStream(bIn, messageDigest);
        for (byte buf[] = new byte[4096]; digestInputStream.read(buf) >= 0;) {
            ;
        }
        byte mic[] = digestInputStream.getMessageDigest().digest();
        digestInputStream.close();
        String micString = new String(Base64.encode(mic));
        return (micString);
    }

    /**Calculates the hash value for a passed body part, base 64 encoded
     *@@param digestAlgOID digest OID algorithm, e.g. "1.3.14.3.2.26"
     */
    public String calculateMIC(Part part, String digestAlgOID, boolean includeHeaders) throws GeneralSecurityException, MessagingException, IOException {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        if (includeHeaders) {
            part.writeTo(bOut);
        } else {
            this.copyStreams(part.getInputStream(), bOut);
        }
        bOut.flush();
        bOut.close();
        byte data[] = bOut.toByteArray();
        return (this.calculateMIC(data, digestAlgOID));
    }

    public MimeBodyPart decrypt(MimeBodyPart part, Certificate cert, Key key) throws GeneralSecurityException, MessagingException, CMSException, IOException, SMIMEException {
        if (!isEncrypted(part)) {
            throw new GeneralSecurityException("Content-Type indicates data isn't encrypted");
        }
        X509Certificate x509Cert = castCertificate(cert);
        SMIMEEnveloped envelope = new SMIMEEnveloped(part);
        RecipientId recId = new RecipientId();
        recId.setSerialNumber(x509Cert.getSerialNumber());
        recId.setIssuer(x509Cert.getIssuerX500Principal().getEncoded());
        RecipientInformation recipient = envelope.getRecipientInfos().get(recId);
        if (recipient == null) {
            throw new GeneralSecurityException("Certificate does not match part signature");
        } else {
            byte decryptedData[] = recipient.getContent(key, "BC");
            return SMIMEUtil.toMimeBodyPart(decryptedData);
        }
    }

    public void deinitialize() {
    }

    /**@@param algorith a algorith alias name, e.g. "3des", wil be translated into the
     *right IOD number internal
     */
    public MimeBodyPart encrypt(MimeMessage part, Certificate cert, String algorithm) throws GeneralSecurityException, SMIMEException {
        X509Certificate x509Cert = castCertificate(cert);
        String encAlg = this.convertAlgorithmNameToOID(algorithm);
        SMIMEEnvelopedGenerator gen = new SMIMEEnvelopedGenerator();
        gen.addKeyTransRecipient(x509Cert);
        MimeBodyPart encData = gen.generate(part, encAlg, "BC");
        return encData;
    }

    /**@@param algorith a algorith alias name, e.g. "3des", will be translated into the
     *right IOD number internal
     */
    public MimeBodyPart encrypt(MimeBodyPart part, Certificate cert, String algorithm) throws GeneralSecurityException, SMIMEException {
        X509Certificate x509Cert = castCertificate(cert);
        String encAlg = this.convertAlgorithmNameToOID(algorithm);
        SMIMEEnvelopedGenerator gen = new SMIMEEnvelopedGenerator();
        gen.addKeyTransRecipient(x509Cert);
        MimeBodyPart encData = gen.generate(part, encAlg, "BC");
        return encData;
    }

    public void initialize() {
        Security.addProvider(new BouncyCastleProvider());
        MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
        mc.addMailcap("application/pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_signature");
        mc.addMailcap("application/pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_mime");
        mc.addMailcap("application/x-pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_signature");
        mc.addMailcap("application/x-pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_mime");
        mc.addMailcap("multipart/signed;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.multipart_signed");
        CommandMap.setDefaultCommandMap(mc);
    }

    /**Create a pkcs7-signature of the passed content and return it
     *@@param chain certificate chain, chain[0] is the signers certificate itself
     *
     */
    public byte[] sign(byte[] content, Certificate[] chain, Key key, String digest) throws Exception {
        X509Certificate x509Cert = this.castCertificate(chain[0]);
        PrivateKey privKey = this.getPrivateKey(key);
        CMSSignedDataGenerator generator = new CMSSignedDataGenerator();
        if (digest.equalsIgnoreCase(ALGORITHM_SHA1)) {
            generator.addSigner(privKey, x509Cert, CMSSignedGenerator.DIGEST_SHA1);
        } else if (digest.equalsIgnoreCase(ALGORITHM_MD5)) {
        } else {
            throw new Exception("Signing digest " + digest + " not supported.");
        }
        //add cert store
        ArrayList certList = new ArrayList(Arrays.asList(chain));
        CertStore certStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(certList), "BC");
        generator.addCertificatesAndCRLs(certStore);
        CMSProcessable processable = new CMSProcessableByteArray(content);
        CMSSignedData signatureData = generator.generate(processable, "BC");
        return (signatureData.getEncoded());
    }

    /**@@param chain certificate chain, chain[0] is the signers certificate itself
     */
    public MimeMultipart sign(MimeBodyPart body, Certificate[] chain, Key key, String digest) throws Exception {
        X509Certificate x509Cert = this.castCertificate(chain[0]);
        PrivateKey privKey = this.getPrivateKey(key);
        SMIMESignedGenerator generator = new SMIMESignedGenerator();
        //add dont know
        DEREncodableVector signedAttrs = new DEREncodableVector();
        SMIMECapabilityVector caps = new SMIMECapabilityVector();
        caps.addCapability(SMIMECapability.dES_EDE3_CBC);
        caps.addCapability(SMIMECapability.rC2_CBC, 128);
        caps.addCapability(SMIMECapability.dES_CBC);
        signedAttrs.add(new SMIMECapabilitiesAttribute(caps));
        if (digest.equalsIgnoreCase(ALGORITHM_SHA1)) {
            generator.addSigner(privKey, x509Cert, SMIMESignedGenerator.DIGEST_SHA1,
                    new AttributeTable(signedAttrs), null);
        } else if (digest.equalsIgnoreCase(ALGORITHM_MD5)) {
            generator.addSigner(privKey, x509Cert, SMIMESignedGenerator.DIGEST_MD5,
                    new AttributeTable(signedAttrs), null);
        } else {
            throw new Exception("Signing digest " + digest + " not supported.");
        }
        //add cert store
        ArrayList certList = new ArrayList(Arrays.asList(chain));
        CertStore certStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(certList), "BC");
        generator.addCertificatesAndCRLs(certStore);

        MimeMultipart signedPart = generator.generate(body, "BC");
        return (signedPart);
    }

    /**@@param chain certificate chain, chain[0] is the signers certificate itself
     */
    public MimeMessage sign(MimeMessage message, Certificate[] chain, Key key, String digest) throws Exception {
        X509Certificate x509Cert = this.castCertificate(chain[0]);
        PrivateKey privKey = this.getPrivateKey(key);
        SMIMESignedGenerator generator = new SMIMESignedGenerator();
        //add dont know
        DEREncodableVector signedAttrs = new DEREncodableVector();
        SMIMECapabilityVector caps = new SMIMECapabilityVector();
        caps.addCapability(SMIMECapability.dES_EDE3_CBC);
        caps.addCapability(SMIMECapability.rC2_CBC, 128);
        caps.addCapability(SMIMECapability.dES_CBC);
        signedAttrs.add(new SMIMECapabilitiesAttribute(caps));
        if (digest.equalsIgnoreCase(ALGORITHM_SHA1)) {
            generator.addSigner(privKey, x509Cert, SMIMESignedGenerator.DIGEST_SHA1,
                    new AttributeTable(signedAttrs), null);
        } else if (digest.equalsIgnoreCase(ALGORITHM_MD5)) {
            generator.addSigner(privKey, x509Cert, SMIMESignedGenerator.DIGEST_MD5,
                    new AttributeTable(signedAttrs), null);
        } else {
            throw new Exception("Signing digest " + digest + " not supported.");
        }
        //add cert store
        ArrayList certList = new ArrayList(Arrays.asList(chain));
        CertStore certStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(certList), "BC");
        generator.addCertificatesAndCRLs(certStore);

        MimeMultipart signedData = generator.generate(message, "BC");
        MimeMessage signedMessage = new MimeMessage(Session.getInstance(System.getProperties(), null));
        signedMessage.setContent(signedData, signedData.getContentType());
        signedMessage.saveChanges();
        return (signedMessage);
    }

    /**Returns the digest OID algorithm from a signature that signes the passed message part
     *The return value for sha1 is e.g. "1.3.14.3.2.26".
     */
    public String getDigestAlgOIDFromSignature(Part part) throws Exception {
        if (part.isMimeType("multipart/signed")) {
            MimeMultipart signedMultiPart = null;
            if (part.getContent() instanceof MimeMultipart) {
                signedMultiPart = (MimeMultipart) part.getContent();
            } else {
                //assuming it is an inputstream now
                signedMultiPart = new MimeMultipart(new ByteArrayDataSource((InputStream) part.getContent(), part.getContentType()));
            }
            SMIMESigned signed = new SMIMESigned(signedMultiPart);
            SignerInformationStore signerStore = signed.getSignerInfos();
            Iterator iterator = signerStore.getSigners().iterator();
            while (iterator.hasNext()) {
                SignerInformation signerInfo = (SignerInformation) iterator.next();
                return (signerInfo.getDigestAlgOID());
            }
            throw new GeneralSecurityException("Unable to identify signature algorithm.");
        }
        throw new GeneralSecurityException("Content-Type indicates data isn't signed");
    }

    /**Returns the digest OID algorithm from a pkcs7 signature
     *The return value for sha1 is e.g. "1.3.14.3.2.26".
     */
    public String getDigestAlgOIDFromSignature(byte[] signature) throws Exception {
        CMSSignedData signedData = new CMSSignedData(signature);
        SignerInformationStore signers = signedData.getSignerInfos();
        Collection signerCollection = signers.getSigners();
        Iterator iterator = signerCollection.iterator();
        while (iterator.hasNext()) {
            SignerInformation signerInfo = (SignerInformation) iterator.next();
            return (signerInfo.getDigestAlgOID());
        }
        throw new GeneralSecurityException("Unable to identify signature algorithm.");
    }

    /**Verifies a signature of a passed content against the passed certificate
     */
    public boolean verify(byte[] content, byte[] signature, Certificate cert) throws Exception {
        X509Certificate x509 = this.castCertificate(cert);
        CMSTypedStream signedContent = new CMSTypedStream(new ByteArrayInputStream(content));
        CMSSignedDataParser dataParser = new CMSSignedDataParser(
                signedContent, new ByteArrayInputStream(signature));
        dataParser.getSignedContent().drain();
        SignerInformationStore signers = dataParser.getSignerInfos();
        Collection signerCollection = signers.getSigners();
        Iterator it = signerCollection.iterator();
        boolean verified = false;
        while (it.hasNext()) {
            SignerInformation signer = (SignerInformation) it.next();
            if (!verified) {
                verified = signer.verify(x509, "BC");
            }
        }
        return (verified);
    }

    /**Verifies a signature against the passed certificate
     *@@param encoding one of 7bit quoted-printable base64 8bit binary
     */
    public MimeBodyPart verify(Part part, Certificate cert) throws Exception {
        return (this.verify(part, null, cert));
    }

    /**Verifies a signature against the passed certificate
     *@@param encoding one of 7bit quoted-printable base64 8bit binary
     */
    public MimeBodyPart verify(Part part, String encoding, Certificate cert) throws Exception {
        if (part.isMimeType("multipart/signed")) {
            MimeMultipart signedMultiPart = (MimeMultipart) part.getContent();
            //possible encoding: 7bit quoted-printable base64 8bit binary
            SMIMESigned signed = null;
            if (encoding == null) {
                //the default encoding in BC is 7bit but the default encoding in AS2 is binary.
                signed = new SMIMESigned(signedMultiPart, "binary");
            } else {
                signed = new SMIMESigned(signedMultiPart, encoding);
            }
            X509Certificate x509Cert = this.castCertificate(cert);
            SignerInformationStore signerStore = signed.getSignerInfos();
            Iterator iterator = signerStore.getSigners().iterator();
            while (iterator.hasNext()) {
                SignerInformation signerInfo = (SignerInformation) iterator.next();
                if (!signerInfo.verify(x509Cert.getPublicKey(), "BC")) {
                    throw new SignatureException("Verification failed");
                }
            }
            return signed.getContent();
        } else {
            throw new GeneralSecurityException("Content-Type indicates data isn't signed");
        }
    }

    private X509Certificate castCertificate(Certificate cert) throws GeneralSecurityException {
        if (cert == null) {
            throw new GeneralSecurityException("Certificate is null");
        }
        if (!(cert instanceof X509Certificate)) {
            throw new GeneralSecurityException("Certificate must be an instance of X509Certificate");
        } else {
            return (X509Certificate) cert;
        }
    }

    private PrivateKey getPrivateKey(Key key) throws GeneralSecurityException {
        if (!(key instanceof PrivateKey)) {
            throw new GeneralSecurityException("Key must implement PrivateKey interface");
        } else {
            return (PrivateKey) key;
        }
    }

    /**Converts the passed algorithm or OID*/
    public String convertAlgorithmNameToOID(String algorithm) throws NoSuchAlgorithmException {
        if (algorithm == null) {
            throw new NoSuchAlgorithmException("Algorithm is null");
        } else if (algorithm.equalsIgnoreCase(ALGORITHM_MD5)) {
            return ("1.2.840.113549.2.5");
        } else if (algorithm.equalsIgnoreCase(ALGORITHM_SHA1)) {
            return ("1.3.14.3.2.26");
        } else if (algorithm.equalsIgnoreCase(ALGORITHM_3DES)) {
            return ("1.2.840.113549.3.7");
        } else if (algorithm.equalsIgnoreCase(ALGORITHM_DES)) {
            return ("1.3.14.3.2.7");
        } else if (algorithm.equalsIgnoreCase(ALGORITHM_CAST5)) {
            return (CMSEnvelopedDataGenerator.CAST5_CBC);
        } else if (algorithm.equalsIgnoreCase(ALGORITHM_IDEA)) {
            return (CMSEnvelopedDataGenerator.IDEA_CBC);
        } else if (algorithm.equalsIgnoreCase(ALGORITHM_RC2)) {
            return (CMSEnvelopedDataGenerator.RC2_CBC);
        } else if (algorithm.equalsIgnoreCase(ALGORITHM_RC4)) {
            return ("1.2.840.113549.3.4");
        } else if (algorithm.equalsIgnoreCase(ALGORITHM_AES_128)) {
            return (CMSEnvelopedDataGenerator.AES128_CBC);
        } else if (algorithm.equalsIgnoreCase(ALGORITHM_AES_192)) {
            return (CMSEnvelopedDataGenerator.AES192_CBC);
        } else if (algorithm.equalsIgnoreCase(ALGORITHM_AES_256)) {
            return (CMSEnvelopedDataGenerator.AES256_CBC);
        } else {
            throw new NoSuchAlgorithmException("Unsupported algorithm: " + algorithm);
        }
    }

    /**Converts the passed algorithm or OID*/
    public String convertOIDToAlgorithmName(String oid) throws NoSuchAlgorithmException {
        try {
            Signature sig = Signature.getInstance(oid, "BC");
            System.out.println(sig.getAlgorithm());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (oid == null) {
            throw new NoSuchAlgorithmException("OID is null");
        } else if (oid.equalsIgnoreCase("1.2.840.113549.2.5")) {
            return (ALGORITHM_MD5);
        } else if (oid.equalsIgnoreCase("1.3.14.3.2.26")) {
            return (ALGORITHM_SHA1);
        } else if (oid.equalsIgnoreCase(CMSEnvelopedDataGenerator.CAST5_CBC)) {
            return (ALGORITHM_CAST5);
        } else if (oid.equalsIgnoreCase(CMSEnvelopedDataGenerator.DES_EDE3_CBC)) {
            return (ALGORITHM_3DES);
        } else if (oid.equalsIgnoreCase("1.3.14.3.2.7")) {
            return (ALGORITHM_DES);
        } else if (oid.equalsIgnoreCase(CMSEnvelopedDataGenerator.IDEA_CBC)) {
            return (ALGORITHM_IDEA);
        } else if (oid.equalsIgnoreCase(CMSEnvelopedDataGenerator.RC2_CBC)) {
            return (ALGORITHM_RC2);
        } else if (oid.equalsIgnoreCase("1.2.840.113549.3.4")) {
            return (ALGORITHM_RC4);
        } else if (oid.equalsIgnoreCase(CMSEnvelopedDataGenerator.AES128_CBC)) {
            return (ALGORITHM_AES_128);
        } else if (oid.equalsIgnoreCase(CMSEnvelopedDataGenerator.AES192_CBC)) {
            return (ALGORITHM_AES_192);
        } else if (oid.equalsIgnoreCase(CMSEnvelopedDataGenerator.AES256_CBC)) {
            return (ALGORITHM_AES_256);
        } else {
            throw new NoSuchAlgorithmException("Unsupported algorithm: OID " + oid);
        }
    }

    private InputStream trimCRLFPrefix(byte data[]) {
        ByteArrayInputStream bIn = new ByteArrayInputStream(data);
        int scanPos = 0;
        int len = data.length;
        while (scanPos < len - 1) {
            if ((new String(data, scanPos, 2)).equals("\r\n")) {
                bIn.read();
                bIn.read();
                scanPos += 2;
            } else {
                return bIn;
            }
        }
        return bIn;
    }

    /**
     * 
     * @@param type Keystore type which should be one of the class constants
     * @@return
     * @@throws java.security.KeyStoreException
     * @@throws java.security.NoSuchProviderException
     */
    public KeyStore createKeyStoreInstance(String type) throws KeyStoreException, NoSuchProviderException {
        if (type.equals(KEYSTORE_PKCS12)) {
            return KeyStore.getInstance(type, "BC");
        } else {
            return KeyStore.getInstance(type);
        }
    }

    /**Copies all data from one stream to another*/
    private void copyStreams(InputStream in, OutputStream out)
            throws IOException {
        BufferedInputStream inStream = new BufferedInputStream(in);
        BufferedOutputStream outStream = new BufferedOutputStream(out);
        //copy the contents to an output stream
        byte[] buffer = new byte[2048];
        int read = 2048;
        //a read of 0 must be allowed, sometimes it takes time to
        //extract data from the input
        while (read != -1) {
            read = inStream.read(buffer);
            if (read > 0) {
                outStream.write(buffer, 0, read);
            }
        }
        outStream.flush();
    }
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /mec_as2/de/mendelson/util/security/BCCryptoHelper.java 30    23.07.08 15:07 Heller $
d42 1
a42 1
 * @@version $Revision: 30 $
@

