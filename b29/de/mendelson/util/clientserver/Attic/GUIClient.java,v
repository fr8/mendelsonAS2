head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.41.13;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2009.10.07.10.08.39;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b29/de/mendelson/util/clientserver/GUIClient.java,v 1.1 2009/10/07 10:08:39 heller Exp $
package de.mendelson.util.clientserver;

import de.mendelson.util.MecResourceBundle;
import de.mendelson.util.clientserver.gui.JDialogLogin;
import de.mendelson.util.clientserver.gui.JDialogSelectServer;
import de.mendelson.util.clientserver.user.User;
import java.net.InetSocketAddress;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.apache.mina.common.IoSession;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * GUI Client root implementation
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */

public class GUIClient extends JFrame implements ClientSessionHandler.Callback{
    
    private Logger logger = Logger.getAnonymousLogger();
    /**Sets the threshold for the logger*/
    private Level loglevelThreshold = Level.SEVERE;
    
    private BaseClient client = null;
    
    private String host = null;
    private int port = -1;
    private boolean useSSL = false;
    
    private MecResourceBundle rb = null;
    
    public GUIClient(){
        //load resource bundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleGUIClient.class.getName());
        } catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
    }
    
    /**Returns if the server runs local 
     */
    public boolean serverRunsLocal(){
        return( this.client.serverRunsLocal() );
    }
    
    /**Logs something to the clients log - but only if the level is higher than the
     *defined loglevelThreshold
     */
    public void log( Level logLevel, String message ){
        if( logLevel.intValue() >= this.loglevelThreshold.intValue() ){
            this.logger.log( logLevel, message );
        }
    }
    
    public void setLogger( Logger logger, Level loglevelThreshold ){
        if( logger != null ){
            this.logger = logger;
        }
        this.loglevelThreshold = loglevelThreshold;
    }
    
    public Logger getLogger(){
        return( this.logger );
    }
    
    private void retryLogin( String user, String loginMessage ){
        JDialogLogin dialog = new JDialogLogin( null, loginMessage );
        dialog.setDefaultUser( user );
        dialog.setVisible( true );
        if( !dialog.isCanceled() ){
            char[] passwd = dialog.getPass();
            String loginUser = dialog.getUser();
            this.performLogin( this.host, this.port, loginUser, passwd, this.useSSL );
        }else{
            //kill VM
            System.exit(0);
        }
    }
    
    /**The server requests a password for the user
     */
    public void loginFailureServerRequestsPassword( String user, String loginMessage ){
        this.log( Level.INFO, this.rb.getResourceString( "password.required", user ));
        this.retryLogin( user, loginMessage );
    }
    
    /**Logs in to the server*/
    public void performLogin( String user, char[] passwd, boolean useSSL ){
        if( this.host == null ){
            JDialogSelectServer dialog = new JDialogSelectServer( null );
            dialog.setDefaultValues( "127.0.0.1", 1234 );
            dialog.setVisible( true );
            if( dialog.isCanceled() ){
                System.exit(0);
            }
            this.host = dialog.getHost();
            this.port = dialog.getPort();
        }
        this.performLogin( this.host, this.port, user, passwd, useSSL );
    }
    
    /**Logs in to the server*/
    public void performLogin( String host, int port, String user, char[] passwd, boolean useSSL ){
        this.useSSL = useSSL;
        this.host = host;
        this.port = port;
        InetSocketAddress address = new InetSocketAddress(host, port);
        ClientSessionHandler handler = new ClientSessionHandler(this);
        this.client = new BaseClient(user, handler);
        this.client.setLogger( this.logger, this.loglevelThreshold );
        if (!client.connect(address, user, passwd, useSSL)) {
            this.log( Level.WARNING, this.rb.getResourceString( "connectionrefused.message", address ));
            JOptionPane.showMessageDialog( null, this.rb.getResourceString( "connectionrefused.message", address ),
                    this.rb.getResourceString( "connectionrefused.title"), JOptionPane.ERROR_MESSAGE );
            System.exit(1);
        }
    }
    
    public void sendAsync( Object message ){
        this.client.sendAsync( message );
    }
        
    public void connected(){
        this.log( Level.INFO, this.rb.getResourceString( "connection.success" ) );
    }
    
    /**Callback if the user has been logged in successfully
     */
    public void loggedIn( User user, String details ){
        this.log( Level.INFO, this.rb.getResourceString( "login.success" ) );
    }
    
    public void loginFailure( String user, String loginMessage ){
        this.log( Level.INFO, this.rb.getResourceString( "login.failure" ));
        JOptionPane.showMessageDialog( null, this.rb.getResourceString( "login.failed.wrong.pass" ),
                this.rb.getResourceString( "login.failure" ), JOptionPane.ERROR_MESSAGE );
        this.retryLogin( user, loginMessage );
    }
        
    public void loggedOut(){
        this.log( Level.INFO, this.rb.getResourceString( "logout.from.server" ) );
    }    
    
    public void disconnected(){
        this.log( Level.WARNING, this.rb.getResourceString( "connection.closed" ) );
        JOptionPane.showMessageDialog( null, this.rb.getResourceString( "connection.closed.message" ),
                this.rb.getResourceString( "connection.closed.title" ), JOptionPane.ERROR_MESSAGE );
        System.exit(1);
    }
    
    /**Overwrite this in the client implementation for user defined processing
     */
    public void messageReceivedFromServer(Object message){
        this.log( Level.INFO, this.rb.getResourceString( "server.broadcast" ));
    }
    
    
    public void error(String message){
        this.log( Level.INFO, this.rb.getResourceString( "error", message ));
    }
    
    /**Performs a logout, closes the actual session
     */
    public void logout(){
        this.client.logout();
    }
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/util/clientserver/GUIClient.java 5     3.04.08 10:09 Heller $
d27 1
a27 1
 * @@version $Revision: 5 $
@

