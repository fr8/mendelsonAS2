head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.41.05;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2009.10.07.10.08.27;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b29/de/mendelson/comm/as2/cert/CertificateManager.java,v 1.1 2009/10/07 10:08:27 heller Exp $
package de.mendelson.comm.as2.cert;

import de.mendelson.util.MecResourceBundle;
import de.mendelson.util.security.BCCryptoHelper;
import de.mendelson.util.security.KeyStoreUtil;
import java.io.File;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.Iterator;
import java.util.logging.Logger;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
import java.util.Vector;

/**
 * Helper class to store
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class CertificateManager {

    private Logger logger = Logger.getLogger("de.mendelson.as2.server");
    private Vector<KeystoreCertificate> keyStoreCertificateList = new Vector<KeystoreCertificate>();
    private KeyStore keystore = null;
    private char[] keystorePass = null;
    private String keystoreFilename = null;
    private KeyStoreUtil keystoreUtil = new KeyStoreUtil();
    private MecResourceBundle rb = null;

    public CertificateManager() {
        //load resource bundle
        try {
            this.rb = (MecResourceBundle) ResourceBundle.getBundle(
                    ResourceBundleCertificateManager.class.getName());
        } catch (MissingResourceException e) {
            throw new RuntimeException("Oops..resource bundle " + e.getClassName() + " not found.");
        }
    }

    /**Returns the certificate chain for a special alias
     */
    public Certificate[] getCertificateChain(String alias) throws Exception {
        Certificate[] chain = this.keystore.getCertificateChain(alias);
        return (chain);
    }

    /**Returns the X509 certificate assigned to the passed alias
     */
    public X509Certificate getX509Certificate(String alias) throws Exception {
        X509Certificate certificate = (X509Certificate) this.keystore.getCertificate(alias);
        if (certificate == null) {
            throw new Exception(this.rb.getResourceString("alias.notfound", alias));
        }
        return (certificate);
    }

    /**Returns the list of available X509 certificates
     */
    public ArrayList<X509Certificate> getX509CertificateList() {
        ArrayList<X509Certificate> certList = new ArrayList<X509Certificate>();
        for (KeystoreCertificate cert : this.keyStoreCertificateList) {
            certList.add(cert.getX509Certificate());
        }
        return (certList);
    }

    /**Returns the private key for an alias. If the assigned certificate
     *does not contain a private key an exception is thrown
     */
    public PrivateKey getPrivateKey(String alias) throws Exception {
        PrivateKey key = (PrivateKey) this.keystore.getKey(alias, this.keystorePass);
        if (key == null) {
            throw new Exception(this.rb.getResourceString("alias.hasno.privatekey", alias));
        } else {
            return (key);
        }
    }

    /**Returns the public key or the private key for an alias.
     */
    public Key getKey(String alias) throws Exception {
        Key key = this.keystore.getKey(alias, this.keystorePass);
        if (key == null) {
            throw new Exception(this.rb.getResourceString("alias.hasno.key", alias));
        } else {
            return (key);
        }
    }

    /**Stores the manages keystore
     */
    public void saveKeystore() throws Exception {
        if (this.getKeystore() == null) {
            //internal error, should not happen
            this.getLogger().severe("saveKeystore: Unable to save keystore, keystore is not loaded.");
            return;
        }
        KeyStoreUtil keystoreUtility = new KeyStoreUtil();
        keystoreUtility.saveKeyStore(this.getKeystore(), this.getKeystorePass(), this.keystoreFilename);
        //refresh the cert list
        this.rereadKeystoreCertificates();
    }

    /**Deletes an entry from the actual keystore*/
    public void deleteKeystoreEntry(String alias) throws Exception {
        if (this.getKeystore() == null) {
            //internal error, should not happen
            this.getLogger().severe("deleteKeystoreEntry: Unable to delete entry, keystore is not loaded.");
            return;
        }
        this.getKeystore().deleteEntry(alias);
    }

    /**Renames an entry in the underlaying keystore. Please remember that PKCS#12 contains no
     * key pair password, pass null in this case
     * 
     */
    public void renameAlias(String oldAlias, String newAlias, char[] keypairPass) throws Exception {
        KeyStoreUtil keystoreUtility = new KeyStoreUtil();
        keystoreUtility.renameEntry(this.keystore, oldAlias, newAlias, keypairPass);
        //rename alias in cert list
        for (KeystoreCertificate cert : this.keyStoreCertificateList) {
            if (cert.getAlias().equals(oldAlias)) {
                cert.setAlias(newAlias);
                break;
            }
        }
    }

    /**Refreshes the data
     */
    public synchronized void rereadKeystoreCertificates() throws Exception {
        this.keystoreUtil.loadKeyStore(getKeystore(), this.keystoreFilename, this.keystorePass);
        HashMap<String,Certificate> certificates = this.keystoreUtil.getCertificatesFromKeystore(this.getKeystore());
        this.keyStoreCertificateList.clear();
        Iterator iterator = certificates.keySet().iterator();
        while (iterator.hasNext()) {
            KeystoreCertificate certificate = new KeystoreCertificate();
            String alias = (String) iterator.next();
            certificate.setAlias(alias);
            X509Certificate foundCertificate = (X509Certificate) certificates.get(alias);
            certificate.setCertificate(foundCertificate);
            try {
                certificate.setIsKeyPair(getKeystore().isKeyEntry(alias));
            } catch (Exception e) {
                //no problem, thats what we wanted to know
                certificate.setIsKeyPair(false);
                this.getLogger().warning(e.getMessage());
            }
            this.keyStoreCertificateList.add(certificate);
        }
    }

    /**Called from external if the certificate storage has been changed. This method
     *calls the normal rereadkeystore method but logs the step
     */
    public void refreshKeystoreCertificates() {
        try {
            this.rereadKeystoreCertificates();
            this.logger.fine(this.rb.getResourceString("keystore.reloaded"));
        } catch (Exception e) {
            this.logger.severe("refreshKeystoreCertificates: " + e.getMessage());
        }
    }

    /**Reads the certificates of the actual key store
     * @@param type keystore typ as defined in the class BCCryptoHelper
     */
    public void loadKeystoreCertificates(String keystoreFilename, String type, char[] password) {
        try {
            BCCryptoHelper cryptoHelper = new BCCryptoHelper();
            this.keystore = cryptoHelper.createKeyStoreInstance(type);
            this.keystorePass = password;
            this.keystoreFilename = keystoreFilename;
            this.rereadKeystoreCertificates();
            this.logger.info(this.rb.getResourceString("keystore.loaded",
                    new File(this.keystoreFilename).getAbsolutePath()));
        } catch (Exception e) {
            this.getLogger().severe("readKeystoreCertificates: " + e.getMessage());
        }
    }

    /**returns null if the alias does not exist*/
    public KeystoreCertificate getKeystoreCertificate(String alias) {
        for (KeystoreCertificate cert : this.keyStoreCertificateList) {
            if (cert.getAlias().equalsIgnoreCase(alias)) {
                return (cert);
            }
        }
        return (null);
    }

    /**Returns the list of certificates*/
    public Vector<KeystoreCertificate> getKeyStoreCertificateList() {
        return keyStoreCertificateList;
    }

    public void setKeyStoreCertificateList(Vector<KeystoreCertificate> keyStoreCertificateList) {
        this.keyStoreCertificateList = keyStoreCertificateList;
    }

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    public KeyStore getKeystore() {
        return keystore;
    }

    public char[] getKeystorePass() {
        return keystorePass;
    }
}@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/cert/CertificateManager.java 14    28.05.09 16:31 Heller $
d32 1
a32 1
 * @@version $Revision: 14 $
@

