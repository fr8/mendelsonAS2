head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.54.32;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2007.05.14.12.51.10;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/17/mendelson/comm/as2/message/MessageAccessDB.java,v 1.1 2007/05/14 12:51:10 heller Exp $
package de.mendelson.comm.as2.message;
import de.mendelson.comm.as2.database.DBDriverManager;
import de.mendelson.comm.as2.message.store.MessageStoreHandler;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.partner.PartnerAccessDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.logging.Logger;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Implementation of a server log for the rosettanet server database
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class MessageAccessDB{
    
    /**Logger to log inforamtion to*/
    private Logger logger = Logger.getLogger( "de.mendelson.as2" );
    
    /**Connection to the database*/
    private Connection connection = null;
    
    /**Host to conenct to*/
    private String host = null;
    
    /** Creates new message I/O log and connects to localhost
     *@@param host host to connect to
     */
    public MessageAccessDB( String host ) throws SQLException{
        this.host = host;
        this.connection = DBDriverManager.getConnectionWithoutErrorHandling( host );
    }
    
    /**Returns the state of the passed message. Will return pending state if the messageid does not exist*/
    public int getMessageState( String messageId ){
        int state = AS2Message.STATE_PENDING;
        try{
            PreparedStatement statement
                    = this.connection.prepareStatement(
                    "SELECT state FROM messages WHERE messageid=?" );
            statement.setEscapeProcessing( true );
            statement.setString( 1, messageId );
            ResultSet result = statement.executeQuery();
            if( result.next() ){
                state = result.getInt( "state" );
            }
            result.close();
            statement.close();
        } catch( Exception e ){
            this.logger.severe( "getMessageState: " + e.getMessage() );
        }
        return( state );
    }
    
    /**Sets the corresponding message status to the new value. This will have only effects if the actual
     * message state is "pending". "Stopped" and "finished" are states that could not be changed.
     *@@param state one of the staes defined in the class AS2Message
     */
    public void setMessageState( String messageId, int state ){
        int oldState = this.getMessageState( messageId );
        if( oldState == AS2Message.STATE_STOPPED || oldState == AS2Message.STATE_FINISHED ){
            return;
        }
        //perform the db update
        try{
            PreparedStatement statement
                    = this.connection.prepareStatement(
                    "UPDATE messages SET state=? WHERE messageid=?" );
            statement.setEscapeProcessing( true );
            statement.setInt( 1, state );
            statement.setString( 2, messageId );
            int rows = statement.executeUpdate();
            statement.close();
            //write the integration state file to the harddisk
            this.storeSentMessageState( messageId, state );
        } catch( Exception e ){
            this.logger.severe( e.getMessage() );
        }
    }
    
    /**Writes the integration state file to the harddisk, a transaction has been finished or has been stopped
     */
    private void storeSentMessageState( String messageId, int state ) throws Exception{
        AS2MessageInfo messageInfo = this.getMessage( messageId );
        AS2Payload[] payload = this.getPayload( messageId );
        //dont write any state file for inbound messages
        if( messageInfo.getDirection() == AS2MessageInfo.DIRECTION_IN ){
            return;
        }
        PartnerAccessDB partnerAccess = new PartnerAccessDB( "localhost" );
        Partner receiver = partnerAccess.getPartner( messageInfo.getReceiverId() );
        partnerAccess.close();
        MessageStoreHandler storeHandler = new MessageStoreHandler();
        storeHandler.storeSentMessageState( messageInfo, payload, receiver, state );
    }
    
    /**Returns information about the payload of a special message*/
    public AS2Payload[] getPayload( String messageId ){
        ArrayList<AS2Payload> payloadList = new ArrayList<AS2Payload>();
        ResultSet result = null;
        try{
            PreparedStatement statement
                    = this.connection.prepareStatement( "SELECT * FROM payload WHERE messageid=?" );
            statement.setEscapeProcessing( true );
            statement.setString( 1, messageId );
            result = statement.executeQuery();
            while( result.next() ){
                AS2Payload payload = new AS2Payload();
                payload.setPayloadFilename( result.getString( "payloadfilename" ));
                payload.setOriginalFilename( result.getString( "originalfilename" ));
                payloadList.add( payload );
            }
            AS2Payload[] payloadArray = new AS2Payload[payloadList.size()];
            payloadList.toArray( payloadArray );
            return( payloadArray );
        } catch( Exception e ){
            this.logger.severe( "MessageAccessDB.getPayload: " + e.getMessage() );
            return( null );
        } finally{
            if( result != null ){
                try{
                    result.close();
                } catch( Exception e ){
                    //nop
                }
            }
        }
    }
    
    
    /**Returns all overview rows from the datase*/
    public AS2MessageInfo[] getMessageDetails( String messageId ){
        ArrayList<AS2MessageInfo> messageList = new ArrayList<AS2MessageInfo>();
        ResultSet result = null;
        try{
            PreparedStatement statement
                    = this.connection.prepareStatement( "SELECT * FROM messages WHERE messageid=? OR relatedmessageid=? ORDER BY id ASC" );
            statement.setEscapeProcessing( true );
            statement.setString( 1, messageId );
            statement.setString( 2, messageId );
            result = statement.executeQuery();
            while( result.next() ){
                AS2MessageInfo info = new AS2MessageInfo();
                info.setMessageDate( result.getTimestamp( "messagedate"));
                info.setEncryptionType( result.getInt( "encryption" ));
                info.setDirection( result.getInt( "direction"));
                info.setRelatedMessageId( result.getString( "relatedmessageid"));
                if( result.wasNull() )
                    info.setRelatedMessageId(null);
                info.setMessageId( result.getString( "messageid"));
                info.setRawFilename( result.getString( "rawfilename"));
                info.setReceiverId( result.getString( "receiverid"));
                info.setSenderId( result.getString( "senderid"));
                info.setSignType( result.getInt( "signature"));
                info.setState( result.getInt( "state"));
                info.setRequestsSyncMDN( result.getInt( "syncmdn") == 1 );
                info.setHeaderFilename( result.getString( "headerfilename" ));
                info.setRawFilenameDecrypted( result.getString( "rawdecryptedfilename" ));
                info.setSenderHost( result.getString( "senderhost" ));
                info.setUserAgent( result.getString( "useragent"));
                info.setStateFilename( result.getString( "statefilename" ));
                messageList.add( info );
            }
            AS2MessageInfo[] messageArray = new AS2MessageInfo[messageList.size()];
            messageList.toArray( messageArray );
            return( messageArray );
        } catch( Exception e ){
            this.logger.severe( e.getMessage() );
            return( null );
        } finally{
            if( result != null ){
                try{
                    result.close();
                } catch( Exception e ){
                    //nop
                }
            }
        }
    }
    
    /**Checks if a passed message id exists
     */
    public boolean messageIdExists( String messageId ){
        AS2MessageInfo info = this.getMessage( messageId );
        return( info != null );
    }
    
    /**Reads information about a specific messageid from the data base*/
    public AS2MessageInfo getMessage( String messageId ){
        ResultSet result = null;
        try{
            PreparedStatement statement
                    = this.connection.prepareStatement( "SELECT * FROM messages WHERE messageid=?" );            
            statement.setEscapeProcessing( true );            
            statement.setString( 1, messageId );
            result = statement.executeQuery();
            if( result.next() ){
                AS2MessageInfo info = new AS2MessageInfo();
                info.setMessageDate( result.getTimestamp( "messagedate"));
                info.setEncryptionType( result.getInt( "encryption" ));
                info.setDirection( result.getInt( "direction"));
                info.setRelatedMessageId( result.getString( "relatedmessageid"));
                if( result.wasNull() )
                    info.setRelatedMessageId(null);
                info.setMessageId( result.getString( "messageid"));
                info.setRawFilename( result.getString( "rawfilename"));
                info.setReceiverId( result.getString( "receiverid"));
                info.setSenderId( result.getString( "senderid"));
                info.setSignType( result.getInt( "signature"));
                info.setState( result.getInt( "state"));
                info.setRequestsSyncMDN( result.getInt( "syncmdn") == 1 );
                info.setHeaderFilename( result.getString( "headerfilename" ));
                info.setRawFilenameDecrypted( result.getString( "rawdecryptedfilename" ));
                info.setSenderHost( result.getString( "senderhost" ));
                info.setUserAgent( result.getString( "useragent"));
                info.setStateFilename( result.getString( "statefilename" ));
                return( info );
            }
        } catch( Exception e ){
            this.logger.severe( e.getMessage() );
            return( null );
        } finally{
            if( result != null ){
                try{
                    result.close();
                } catch( Exception e ){
                    //nop
                }
            }
        }
        return(null);
    }
    
    /**Returns all overview rows from the datase*/
    public AS2MessageInfo[] getMessageOverview( MessageOverviewFilter filter ){
        ArrayList<AS2MessageInfo> messageList = new ArrayList<AS2MessageInfo>();
        ResultSet result = null;
        try{
            Statement statement = this.connection.createStatement();
            statement.setEscapeProcessing( true );
            StringBuffer queryCondition = new StringBuffer();
            if( !filter.isShowFinished() ){
                queryCondition.append( " AND state <>" + AS2Message.STATE_FINISHED );
            }
            if( !filter.isShowPending() ){
                queryCondition.append( " AND state <>" + AS2Message.STATE_PENDING );
            }
            if( !filter.isShowStopped() ){
                queryCondition.append( " AND state <>" + AS2Message.STATE_STOPPED );
            }
            //check if the number of entires have changed since last request
            String query = "SELECT * FROM messages WHERE relatedmessageid IS NULL"
                    + queryCondition.toString()
                    + " ORDER BY id ASC";
            result = statement.executeQuery( query );
            while( result.next() ){
                AS2MessageInfo info = new AS2MessageInfo();
                info.setMessageDate( result.getTimestamp( "messagedate"));
                info.setEncryptionType( result.getInt( "encryption" ));
                info.setDirection( result.getInt( "direction"));
                info.setRelatedMessageId( result.getString( "relatedmessageid"));
                if( result.wasNull() )
                    info.setRelatedMessageId(null);
                info.setMessageId( result.getString( "messageid"));
                info.setRawFilename( result.getString( "rawfilename"));
                info.setReceiverId( result.getString( "receiverid"));
                info.setSenderId( result.getString( "senderid"));
                info.setSignType( result.getInt( "signature"));
                info.setState( result.getInt( "state"));
                info.setRequestsSyncMDN( result.getInt( "syncmdn") == 1 );
                info.setHeaderFilename( result.getString( "headerfilename" ));
                info.setRawFilenameDecrypted( result.getString( "rawdecryptedfilename" ));
                info.setSenderHost( result.getString( "senderhost" ));
                info.setUserAgent( result.getString( "useragent" ));
                info.setStateFilename( result.getString( "statefilename" ));
                messageList.add( info );
            }
            AS2MessageInfo[] messageArray = new AS2MessageInfo[messageList.size()];
            messageList.toArray( messageArray );
            return( messageArray );
        } catch( Exception e ){
            this.logger.severe( e.getMessage() );
            return( null );
        } finally{
            if( result != null ){
                try{
                    result.close();
                } catch( Exception e ){
                    //nop
                }
            }
        }
    }
    
    /**Returns all file names of files that could be deleted for a passed message
     *info*/
    public String[] getRawFilenamesToDelete( AS2MessageInfo info ){
        ArrayList<String> list = new ArrayList<String>();        
        ResultSet result = null;
        try{
            String query = "SELECT * FROM messages WHERE messageid=? OR relatedmessageid=?";
            PreparedStatement statement
                    = this.connection.prepareStatement( query );
            statement.setEscapeProcessing( true );
            statement.setString( 1, info.getMessageId());
            statement.setString( 2, info.getMessageId());
            result = statement.executeQuery();
            while( result.next() ){
                String rawFilename = result.getString( "rawfilename");
                if( !result.wasNull() )
                    list.add( rawFilename );
                String rawFilenameDecrypted = result.getString( "rawdecryptedfilename" );
                if( !result.wasNull() )
                    list.add( rawFilenameDecrypted );
                String headerFilename = result.getString( "headerfilename" );
                if( !result.wasNull() ){
                    list.add( headerFilename  );                
                }
                String stateFilename = result.getString( "statefilename" );
                if( !result.wasNull() ){
                    list.add( stateFilename  );
                }
            }
        } catch( Exception e ){
            this.logger.severe( e.getMessage() );
            return( null );
        } finally{
            if( result != null ){
                try{
                    result.close();
                } catch( Exception e ){
                    //nop
                }
            }
        }
        String[] nameArray = new String[list.size()];
        list.toArray( nameArray );
        return( nameArray );
    }
    
    
    /**Deletes messages and MDNs of the passed id
     */
    public void deleteMessage( AS2MessageInfo info ){
        try{
            PreparedStatement statement
                    = this.connection.prepareStatement( "DELETE FROM messages WHERE messageid=? OR relatedmessageid=?");
            statement.setEscapeProcessing( true );
            statement.setString( 1, info.getMessageId() );
            statement.setString( 2, info.getMessageId() );
            statement.execute();
            statement.close();
            statement = this.connection.prepareStatement( "DELETE FROM payload WHERE messageid=?");
            statement.setEscapeProcessing( true );
            statement.setString( 1, info.getMessageId() );
            statement.execute();
            statement.close();
        } catch( SQLException e ){
            this.logger.severe( e.getMessage() );
        }
    }
    
    /**Updates a message entry in the database, only the filenames
     */
    public void updateFilenames( AS2MessageInfo info ){
        try{
            PreparedStatement statement
                    = this.connection.prepareStatement(
                    "UPDATE messages SET rawfilename=?,headerfilename=?,rawdecryptedfilename=?,statefilename=? WHERE messageid=?");
            statement.setEscapeProcessing( true );
            statement.setString( 1, info.getRawFilename() );
            statement.setString( 2, info.getHeaderFilename() );
            statement.setString( 3, info.getRawFilenameDecrypted() );
            statement.setString( 4, info.getStateFilename() );
            //WHERE
            statement.setString( 5, info.getMessageId() );
            statement.execute();
            statement.close();
        } catch( SQLException e ){
            this.logger.severe( e.getMessage() );
        }
    }
    /**Writes the payload and original filenames to the database, deleting all entries first (only if a payload has been passed)
     */
    public void insertPayloadFilenames( String messageId, AS2Payload[] payload ){
        if( payload == null || payload.length == 0 ){
            return;
        }
        try{
            PreparedStatement statement
                    = this.connection.prepareStatement( "DELETE FROM payload WHERE messageid=?");
            statement.setEscapeProcessing( true );
            statement.setString( 1, messageId );
            statement.execute();
            statement.close();
            //insert
            for( int i = 0; i < payload.length; i++ ){
                statement = this.connection.prepareStatement( "INSERT INTO payload(messageid,originalfilename,payloadfilename)VALUES(?,?,?)");
                statement.setEscapeProcessing( true );
                statement.setString( 1, messageId );
                statement.setString( 2, payload[i].getOriginalFilename() );
                statement.setString( 3, payload[i].getPayloadFilename() );                
                statement.execute();
            }
        } catch( SQLException e ){
            this.logger.severe( e.getMessage() );
        }
    }
    
    
    /**Inserts a new message entry into the database
     */
    public void insertMessage( AS2Message message ){
        AS2MessageInfo info = message.getMessageInfo();
        try{
            PreparedStatement statement
                    = this.connection.prepareStatement(
                    "INSERT INTO messages(messagedate,encryption,direction,messageid,relatedmessageid,rawfilename,receiverid,senderid,signature,state,syncmdn,headerfilename,rawdecryptedfilename,senderhost,useragent,statefilename)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            statement.setEscapeProcessing( true );
            statement.setTimestamp( 1, new java.sql.Timestamp( info.getMessageDate().getTime() ));
            statement.setInt( 2, info.getEncryptionType() );
            statement.setInt( 3, info.getDirection() );
            statement.setString( 4, info.getMessageId() );
            if( info.getRelatedMessageId() == null ){
                statement.setNull( 5, Types.VARCHAR );
            }else{
                statement.setString( 5, info.getRelatedMessageId() );
            }
            statement.setString( 6, info.getRawFilename() );
            statement.setString( 7, info.getReceiverId() );
            statement.setString( 8, info.getSenderId() );
            statement.setInt( 9, info.getSignType() );
            statement.setInt( 10, info.getState() );
            statement.setInt( 11, info.requestsSyncMDN()?1:0 );
            statement.setString( 12, info.getHeaderFilename() );
            statement.setString( 13, info.getRawFilenameDecrypted() );
            statement.setString( 14, info.getSenderHost() );
            statement.setString( 15, info.getUserAgent() );
            statement.setString( 16, info.getStateFilename());
            statement.execute();
            statement.close();
            //insert payload if this is no MDN
            if( !info.isMDN() ){
                this.insertPayloadFilenames( info.getMessageId(), message.getPayloads() );
            }            
        } catch( SQLException e ){
            this.logger.severe( e.getMessage() );
        }
    }
    
    
    /**Closes the internal database connection.*/
    public void close(){
        try{
            this.connection.close();
        } catch( SQLException e ){
            this.logger.severe( e.getMessage() );
        }
    }
    
    /**Returns a list of all PIPs that are older than the passed timestamp
     *@@param state pass -1 for any state else only messages of the requested state are returned
     */
    public AS2MessageInfo[] getMessagesOlderThan( long initTimestamp, int state ){
        ArrayList<AS2MessageInfo> messageList = new ArrayList<AS2MessageInfo>();
        ResultSet result = null;
        try{
            String query = "SELECT * FROM messages WHERE messagedate < ? AND relatedmessageid IS NULL";
            if( state != -1 )
                query = query + " AND state=" + state;
            PreparedStatement statement
                    = this.connection.prepareStatement( query );
            statement.setEscapeProcessing( true );
            statement.setTimestamp( 1, new java.sql.Timestamp( initTimestamp ));
            result = statement.executeQuery();
            while( result.next() ){
                AS2MessageInfo info = new AS2MessageInfo();
                info.setMessageDate( result.getTimestamp( "messagedate"));
                info.setEncryptionType( result.getInt( "encryption" ));
                info.setDirection( result.getInt( "direction"));
                info.setRelatedMessageId( result.getString( "relatedmessageid"));
                if( result.wasNull() )
                    info.setRelatedMessageId(null);
                info.setMessageId( result.getString( "messageid"));
                info.setRawFilename( result.getString( "rawfilename"));
                info.setReceiverId( result.getString( "receiverid"));
                info.setSenderId( result.getString( "senderid"));
                info.setSignType( result.getInt( "signature"));
                info.setState( result.getInt( "state"));
                info.setRequestsSyncMDN( result.getInt( "syncmdn") == 1 );
                info.setHeaderFilename( result.getString( "headerfilename" ));
                info.setRawFilenameDecrypted( result.getString( "rawdecryptedfilename" ));
                info.setSenderHost( result.getString( "senderhost" ));
                info.setUserAgent( result.getString( "useragent" ));
                info.setStateFilename( result.getString( "statefilename" ));
                messageList.add( info );
            }
        } catch( Exception e ){
            this.logger.severe( e.getMessage() );
            return( null );
        } finally{
            if( result != null ){
                try{
                    result.close();
                } catch( Exception e ){
                    //nop
                }
            }
        }
        AS2MessageInfo[] messageArray = new AS2MessageInfo[messageList.size()];
        messageList.toArray( messageArray );
        return( messageArray );
    }
    
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/message/MessageAccessDB.java 31    4.05.07 10:45 Heller $
d26 1
a26 1
 * @@version $Revision: 31 $
@

