head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.54.30;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2007.05.14.12.51.02;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/17/mendelson/comm/as2/jms/JMSMessageReceiver.java,v 1.1 2007/05/14 12:51:02 heller Exp $
package de.mendelson.comm.as2.jms;
import de.mendelson.comm.as2.client.AS2Gui;
import de.mendelson.comm.as2.message.AS2Message;
import de.mendelson.comm.as2.message.MessageAccessDB;
import de.mendelson.comm.as2.message.store.MessageStoreHandler;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.partner.PartnerAccessDB;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.comm.as2.send.MessageHttpUploader;
import de.mendelson.util.MecResourceBundle;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.naming.*;
import javax.jms.*;
import java.util.logging.*;
import java.util.*;
/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Listener thread that listens on the message queue to pick up enqueued messages and process them
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class JMSMessageReceiver extends Thread{
    
    public static final boolean DEBUG = false;
    
    /**Server preferences*/
    private PreferencesAS2 preferences = new PreferencesAS2();
    
    private Logger logger = Logger.getLogger( "de.mendelson.as2" );
    
    /**Name of the thread, for debugging purpose*/
    private String threadName = null;
    
    /**Properties for the java directory service*/
    private Properties jndiProperties = null;
    
    /**Thread will stop if this is no longer set*/
    private boolean runPermission = true;
    
    /**Needed for refresh*/
    private AS2Gui gui = null;
    
    /**Handles messages storage*/
    private MessageStoreHandler messageStoreHandler = new MessageStoreHandler();
    
    /**ResourceBundle to localize the output*/
    private MecResourceBundle rb = null;
    
    /**Stops the listener*/
    public void stopListener(){
        this.runPermission = false;
    }
    
    public JMSMessageReceiver( AS2Gui gui, String threadName ){
        //Load default resourcebundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleJMSMessageReceiver.class.getName());
        }
        //load up resourcebundle
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
        this.threadName = threadName;
        this.gui = gui;
        this.jndiProperties = this.getDefaultJNDIProperties();
    }
    
    /**Sets the directory servive properties, the server will not start up without them
     */
    private Properties getDefaultJNDIProperties(){
        Properties properties = new Properties();
        properties.setProperty( "java.naming.factory.initial",
                "fr.dyade.aaa.jndi2.client.NamingContextFactory" );
        properties.setProperty( "java.naming.factory.host", "localhost" );
        properties.setProperty( "java.naming.factory.port",
                this.preferences.get( PreferencesAS2.JNDI_PORT ));
        return( properties );
    }
    
    
    public void run(){
        javax.jms.Queue queue = null;
        QueueConnectionFactory queueFactory = null;
        try{
            Context context = new InitialContext( this.jndiProperties );
            queue = (javax.jms.Queue)context.lookup("as2_messagequeue");
            queueFactory
                    = (QueueConnectionFactory)context.lookup("queueFactory");
            context.close();
        } catch( Exception e ){
            logger.warning( threadName + ": " + e.getLocalizedMessage() );
            throw new RuntimeException( e );
        }
        //listen until stop is requested
        while( this.runPermission )
            try{
                Message message = this.waitForNewMessage( queueFactory, queue );
                MessageAccessDB messageAccess = new MessageAccessDB( "localhost" );
                if( message != null
                        && message instanceof ObjectMessage
                        && ((ObjectMessage)message).getObject() instanceof SendOrder ){
                    SendOrder order = (SendOrder)((ObjectMessage)message).getObject();
                    try{
                        PartnerAccessDB access = new PartnerAccessDB( "localhost" );
                        Partner localstation = access.getLocalStation();
                        access.close();
                        MessageHttpUploader sender = new MessageHttpUploader( gui );
                        Properties requestHeader
                                = sender.send( order.getMessage(), localstation, order.getReceiver() );
                        messageStoreHandler.storeSentMessage( order.getMessage(), order.getReceiver(),
                                requestHeader );
                        messageAccess.updateFilenames( order.getMessage().getMessageInfo());
                        //set error or finish state, remember that this send order could be
                        //also an MDN if async MDN is requested
                        if( order.getMessage().getMessageInfo().isMDN()){
                            if( order.getMessage().getMessageInfo().getState() == AS2Message.STATE_FINISHED ){
                                messageStoreHandler.movePayloadToInbox( order.getMessage().getMessageInfo().getRelatedMessageId(),
                                        localstation, order.getReceiver() );
                            }
                            messageAccess.setMessageState( order.getMessage().getMessageInfo().getRelatedMessageId(),
                                    order.getMessage().getMessageInfo().getState() );
                        } else if( !order.getMessage().getMessageInfo().requestsSyncMDN() ){
                            long endTime = System.currentTimeMillis()
                            + preferences.getInt( PreferencesAS2.ASYNC_MDN_TIMEOUT )*60000L;
                            DateFormat format = SimpleDateFormat.getDateTimeInstance( DateFormat.SHORT,
                                    DateFormat.MEDIUM );
                            logger.log( Level.INFO, rb.getResourceString( "async.mdn.wait",
                                    new Object[]{
                                order.getMessage().getMessageInfo().getMessageId(),
                                format.format( endTime )
                            }), order.getMessage().getMessageInfo());
                        }
                        gui.refreshMessageOverviewList();
                    } catch( Exception e ){
                        logger.log( Level.SEVERE, e.getMessage(), order.getMessage().getMessageInfo() );
                        e.printStackTrace();
                        try{
                            //stores
                            messageStoreHandler.storeSentErrorMessage(
                                    order.getMessage(), order.getReceiver());
                            if( !order.getMessage().getMessageInfo().isMDN() ){
                                messageAccess.setMessageState( order.getMessage().getMessageInfo().getMessageId(),
                                        AS2Message.STATE_STOPPED );
                                messageAccess.updateFilenames( order.getMessage().getMessageInfo());
                                gui.refreshMessageOverviewList();
                            }
                        } catch( Exception ee ){
                            logger.log( Level.SEVERE, ee.getMessage(), order.getMessage().getMessageInfo() );
                        }
                    }
                } else
                    this.logger.warning( threadName + ": Unknown message type, thrown away." );
                messageAccess.close();
                this.sleep( 500 );
            } catch( Exception e ){
                logger.warning( threadName + ": " + e.getLocalizedMessage() );
            }
    }
    
    
    
    /**Listens to the queue and waits for a new message, asynchronously
     */
    public Message waitForNewMessage( QueueConnectionFactory queueFactory,
            javax.jms.Queue queue ) throws Exception {
        
        QueueConnection connection = queueFactory.createQueueConnection( "as2user", "messagequeuepasswd" );
        QueueSession session = connection.createQueueSession(true, 0);
        QueueReceiver receiver = session.createReceiver(queue);
        
        connection.start();
        Message message = receiver.receive();
        session.commit();
        connection.close();
        return( message );
    }
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/jms/JMSMessageReceiver.java 5     4.05.07 9:59 Heller $
d28 1
a28 1
 * @@version $Revision: 5 $
@

