head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2009.03.16.12.21.46;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2008.05.16.11.08.00;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b23/de/mendelson/comm/as2/message/AS2MessagePacker.java,v 1.1 2008/05/16 11:08:00 heller Exp $
package de.mendelson.comm.as2.message;

import com.sun.mail.util.LineOutputStream;
import de.mendelson.comm.as2.AS2Exception;
import de.mendelson.comm.as2.AS2ServerVersion;
import de.mendelson.comm.as2.cert.CertificateManager;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.partner.PartnerAccessDB;
import de.mendelson.util.MecResourceBundle;
import de.mendelson.util.security.BCCryptoHelper;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import org.bouncycastle.cms.CMSEnvelopedData;
import org.bouncycastle.cms.CMSEnvelopedDataGenerator;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.mail.smime.SMIMECompressedGenerator;
import org.bouncycastle.mail.smime.SMIMEException;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Packs a message with all necessary headers and attachments
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class AS2MessagePacker {

    private Logger logger = Logger.getLogger("de.mendelson.as2.server");
    private MecResourceBundle rb = null;
    private MecResourceBundle rbMessage = null;
    private CertificateManager certificateManager = null;

    public AS2MessagePacker(CertificateManager certificateManager) {
        //Load resourcebundle
        try {
            this.rb = (MecResourceBundle) ResourceBundle.getBundle(
                    ResourceBundleAS2MessagePacker.class.getName());
            this.rbMessage = (MecResourceBundle) ResourceBundle.getBundle(
                    ResourceBundleAS2Message.class.getName());
        } //load up  resourcebundle
        catch (MissingResourceException e) {
            throw new RuntimeException("Oops..resource bundle " + e.getClassName() + " not found.");
        }
        this.certificateManager = certificateManager;
    }

    /**Creates an mdn that could be returned to the sender and indicates that
     *everything is ok
     */
    public AS2Message createMDNProcessed(AS2MessageInfo releatedMessageInfo, String senderId, String receiverId) throws Exception {
        AS2Message mdn = this.createMDN(releatedMessageInfo, senderId, receiverId, "processed", "AS2 message received.");
        mdn.getMessageInfo().setState(AS2Message.STATE_FINISHED);
        return (mdn);
    }

    /**Creates an mdn that could be returned to the sender and indicates an error
     *by processing the message
     */
    public AS2Message createMDNError(AS2Exception exception) throws Exception {
        AS2MessageInfo info = exception.getAS2Message().getMessageInfo();
        AS2Message mdn = this.createMDN(info, info.getReceiverId(),
                info.getSenderId(), "processed/error: " + exception.getErrorType(),
                exception.getMessage());
        this.logger.log(Level.SEVERE, this.rb.getResourceString("mdn.details",
                new Object[]{
            info.getMessageId(),
            exception.getMessage()
        }), info);
        mdn.getMessageInfo().setState(AS2Message.STATE_STOPPED);
        return (mdn);
    }

    /**Creates a MDN to return
     *@@param dispositionState State that will be written into the disposition header
     */
    private AS2Message createMDN(AS2MessageInfo relatedMessageInfo, String senderId,
            String receiverId, String dispositionState, String additionalText) throws Exception {
        AS2Message message = new AS2Message();
        AS2MessageInfo info = message.getMessageInfo();
        info.setMessageId(this.createId(senderId, receiverId));
        info.setSenderId(senderId);
        info.setReceiverId(receiverId);
        info.setSenderEMail(relatedMessageInfo.getSenderEMail());
        info.setRequestsSyncMDN(relatedMessageInfo.requestsSyncMDN());
        info.setAsyncMDNURL(relatedMessageInfo.getAsyncMDNURL());
        info.setSubject("Message Delivery Notification");
        //MDN is not encrypted
        info.setEncryptionType(AS2Message.ENCRYPTION_NONE);
        //indicate that is is an MDN
        info.setRelatedMessageId(relatedMessageInfo.getMessageId());
        try {
            info.setSenderHost(InetAddress.getLocalHost().getCanonicalHostName());
        } catch (UnknownHostException e) {
        //nop
        }
        MimeMultipart multiPart = new MimeMultipart();
        multiPart.addBodyPart(this.createMDNNotesBody(additionalText));
        multiPart.addBodyPart(this.createMDNDispositionBody(relatedMessageInfo, dispositionState));
        multiPart.setSubType("report; report-type=disposition-notification");
        MimeMessage messagePart = new MimeMessage(Session.getInstance(System.getProperties(), null));
        messagePart.setContent(multiPart, MimeUtility.unfold(multiPart.getContentType()));
        messagePart.saveChanges();
        ByteArrayOutputStream memOutUnsigned = new ByteArrayOutputStream();
        //normally the content type header is folded (which is correct but some products are not able to parse this properly)
        //Now take the content-type, unfold it and write it
        Enumeration hdrLines = messagePart.getMatchingHeaderLines(new String[]{"Content-Type"});
        LineOutputStream los = new LineOutputStream(memOutUnsigned);
        while (hdrLines.hasMoreElements()) {
            //requires java mail API >= 1.4
            String nextHeaderLine = MimeUtility.unfold((String) hdrLines.nextElement());
            los.writeln(nextHeaderLine);
        }
        messagePart.writeTo(memOutUnsigned,
                new String[]{"Message-ID", "Mime-Version", "Content-Type"});
        memOutUnsigned.flush();
        memOutUnsigned.close();
        message.setDecryptedRawData(memOutUnsigned.toByteArray());
        //check if authentification of sender is ok, then sign if possible
        PartnerAccessDB partnerAccess = new PartnerAccessDB("localhost");
        Partner sender = partnerAccess.getPartner(senderId);
        partnerAccess.close();
        if (sender != null) {
            MimeMessage signedMessage = this.signMDN(messagePart, sender, message, relatedMessageInfo);
            message.setContentType(MimeUtility.unfold(signedMessage.getContentType()));
            ByteArrayOutputStream memOutSigned = new ByteArrayOutputStream();
            signedMessage.writeTo(memOutSigned,
                    new String[]{"Message-ID", "Mime-Version", "Content-Type"});
            memOutSigned.flush();
            memOutSigned.close();
            message.setRawData(memOutSigned.toByteArray());
        } //there occured an authentification error: the system was unable to authentificate the sender,
        //do not sign MDN
        else {
            ByteArrayOutputStream memOut = new ByteArrayOutputStream();
            messagePart.writeTo(memOut,
                    new String[]{"Message-ID", "Mime-Version", "Content-Type"});
            memOut.flush();
            memOut.close();
            message.getMessageInfo().setSignType(AS2Message.SIGNATURE_NONE);
            message.setContentType(MimeUtility.unfold(messagePart.getContentType()));
            message.setRawData(memOut.toByteArray());
        }
        if (dispositionState.indexOf("error") >= 0) {
            this.logger.log(Level.SEVERE, this.rb.getResourceString("mdn.created",
                    new Object[]{
                info.getMessageId(), dispositionState
            }), info);
        } else {
            this.logger.log(Level.FINE, this.rb.getResourceString("mdn.created",
                    new Object[]{
                info.getMessageId(), dispositionState
            }), info);
        }
        return (message);
    }

    /**Its necessary to transmit additional notes
     */
    private MimeBodyPart createMDNNotesBody(String text) throws MessagingException {
        MimeBodyPart body = new MimeBodyPart();
        body.setText(text);
        body.setHeader("Content-Type", "text/plain");
        body.setHeader("Content-Transfer-Encoding", "7bit");
        return (body);
    }

    /**Creates the MDN body and returns it
     *
     */
    private MimeBodyPart createMDNDispositionBody(AS2MessageInfo relatedMessageInfo, String dispositionState) throws MessagingException {
        MimeBodyPart body = new MimeBodyPart();
        StringBuffer buffer = new StringBuffer();
        buffer.append("Reporting-UA: " + AS2ServerVersion.getProductName() + "\r\n");
        buffer.append("Original-Recipient: rfc822; " + relatedMessageInfo.getReceiverId() + "\r\n");
        buffer.append("Final-Recipient: rfc822; " + relatedMessageInfo.getReceiverId() + "\r\n");
        buffer.append("Original-Message-ID: <" + relatedMessageInfo.getMessageId() + ">\r\n");
        buffer.append("Disposition: automatic-action/MDN-sent-automatically; " + dispositionState + "\r\n");
        if (relatedMessageInfo.getReceivedContentMIC() != null) {
            buffer.append("Received-Content-MIC: " + relatedMessageInfo.getReceivedContentMIC() + "\r\n");
        }
        body.setText(buffer.toString());
        body.setHeader("Content-Type", "message/disposition-notification");
        body.setHeader("Content-Transfer-Encoding", "7bit");
        return (body);
    }

    /**Builds up a new message from the passed message parts
     */
    public AS2Message createMessage(Partner sender, Partner receiver, File payloadFile) throws Exception {
        BCCryptoHelper cryptoHelper = new BCCryptoHelper();
        AS2MessageInfo info = new AS2MessageInfo();
        info.setSenderId(sender.getAS2Identification());
        info.setReceiverId(receiver.getAS2Identification());
        info.setSenderEMail(sender.getEmail());
        info.setMessageId(this.createId(sender.getAS2Identification(), receiver.getAS2Identification()));
        info.setDirection(AS2MessageInfo.DIRECTION_OUT);
        info.setSignType(receiver.getSignType());
        info.setEncryptionType(receiver.getEncryptionType());
        info.setRequestsSyncMDN(receiver.isSyncMDN());
        if (!receiver.isSyncMDN()) {
            info.setAsyncMDNURL(sender.getMdnURL());
        }
        info.setSubject(receiver.getSubject());
        try {
            info.setSenderHost(InetAddress.getLocalHost().getCanonicalHostName());
        } catch (UnknownHostException e) {
        //nop
        }
        //create message object to return
        AS2Message message = new AS2Message(info);
        InputStream inStream = new FileInputStream(payloadFile);
        ByteArrayOutputStream payloadOut = new ByteArrayOutputStream();
        this.copyStreams(inStream, payloadOut);
        inStream.close();
        payloadOut.flush();
        payloadOut.close();
        //add payload
        AS2Payload payload = new AS2Payload();
        payload.setData(payloadOut.toByteArray());
        payload.setOriginalFilename(payloadFile.getName().replace(' ', '_'));
        message.addPayload(payload);
        //no signature, no encryption, raw transmission
        if (info.getSignType() == AS2Message.SIGNATURE_NONE && info.getEncryptionType() == AS2Message.ENCRYPTION_NONE && receiver.getCompressionType() == AS2Message.COMPRESSION_NONE) {
            //payload content type.
            message.setContentType(receiver.getContentType());
            message.setRawData(payloadOut.toByteArray());
            message.setDecryptedRawData(message.getPayload(0).getData());
            this.logger.log(Level.INFO, this.rb.getResourceString("message.notsigned",
                    new Object[]{
                info.getMessageId()
            }), info);
            this.logger.log(Level.INFO, this.rb.getResourceString("message.notencrypted",
                    new Object[]{
                info.getMessageId()
            }), info);
            return (message);
        }
        InternetHeaders headers = new InternetHeaders();
        headers.addHeader("Content-Type", receiver.getContentType());
        headers.addHeader("Content-Transfer-Encoding", "binary");
        headers.addHeader("Content-Disposition", "attachment; filename=" + payloadFile.getName().replace(' ', '_'));
        MimeBodyPart bodyPart = new MimeBodyPart(headers, message.getPayload(0).getData());
        //should the content be compressed and enwrapped or just enwrapped?
        if (receiver.getCompressionType() == AS2Message.COMPRESSION_ZLIB) {
            int uncompressedSize = bodyPart.getSize();
            bodyPart = this.compressPayload(bodyPart);
            int compressedSize = bodyPart.getSize();
            //sometimes size() is unable to determine the size of the compressed body part and will return -1. Dont log the
            //compression ratio in this case.
            if (uncompressedSize == -1 || compressedSize == -1) {
                this.logger.log(Level.INFO, this.rb.getResourceString("message.compressed.unknownration",
                        new Object[]{
                    info.getMessageId()
                }), info);
            } else {
                this.logger.log(Level.INFO, this.rb.getResourceString("message.compressed",
                        new Object[]{
                    info.getMessageId(), AS2Message.getDataSizeDisplay(uncompressedSize),
                    AS2Message.getDataSizeDisplay(compressedSize)
                }), info);
            }
        }
        MimeMessage messagePart = new MimeMessage(Session.getInstance(System.getProperties(), null));
        //sign message if this is requested
        if (info.getSignType() != AS2Message.SIGNATURE_NONE) {
            MimeMultipart signedPart = this.signMessage(bodyPart, sender, receiver);
            this.logger.log(Level.INFO, this.rb.getResourceString("message.signed",
                    new Object[]{
                info.getMessageId(), sender.getSignAlias(),
                this.rbMessage.getResourceString("signature." + receiver.getSignType())
            }), info);
            messagePart.setContent(signedPart);
            messagePart.saveChanges();
        } else {
            //unsigned message
            MimeMultipart unsignedPart = new MimeMultipart();
            unsignedPart.addBodyPart(bodyPart);
            this.logger.log(Level.INFO, this.rb.getResourceString("message.notsigned",
                    new Object[]{
                info.getMessageId()
            }), info);
            messagePart.setContent(unsignedPart);
            messagePart.saveChanges();
        }
        //store signed or unsigned data
        ByteArrayOutputStream signedOut = new ByteArrayOutputStream();
        //normally the content type header is folded (which is correct but some products are not able to parse this properly)
        //Now take the content-type, unfold it and write it
        Enumeration hdrLines = messagePart.getMatchingHeaderLines(new String[]{"Content-Type"});
        LineOutputStream los = new LineOutputStream(signedOut);
        while (hdrLines.hasMoreElements()) {
            //requires java mail API >= 1.4
            String nextHeaderLine = MimeUtility.unfold((String) hdrLines.nextElement());
            //write the line only if the as2 message is encrypted. If the as2 message is unencrypted this header is added later 
            //in the class MessageHttpUploader
            if( info.getEncryptionType() != AS2Message.ENCRYPTION_NONE ){
                los.writeln(nextHeaderLine);
            }
            //store the content line in the as2 message objet, this value is required later in MessageHttpUploader
            message.setContentType(nextHeaderLine.substring( nextHeaderLine.indexOf(':') + 1 ));
        }
        messagePart.writeTo(signedOut,
                new String[]{"Message-ID", "Mime-Version", "Content-Type"});
        signedOut.flush();
        signedOut.close();
        message.setDecryptedRawData(signedOut.toByteArray());
        //encryption
        if (info.getEncryptionType() != AS2Message.ENCRYPTION_NONE) {
            X509Certificate certificate = this.certificateManager.getX509Certificate(receiver.getCryptAlias());
            CMSEnvelopedDataGenerator dataGenerator = new CMSEnvelopedDataGenerator();
            dataGenerator.addKeyTransRecipient(certificate);
            CMSProcessableByteArray byteArray = new CMSProcessableByteArray(message.getDecryptedRawData());
            CMSEnvelopedData envelopedData = null;
            if (info.getEncryptionType() == AS2Message.ENCRYPTION_3DES) {
                envelopedData = dataGenerator.generate(byteArray, CMSEnvelopedDataGenerator.DES_EDE3_CBC, "BC");
            } else if (info.getEncryptionType() == AS2Message.ENCRYPTION_RC2_40) {
                envelopedData = dataGenerator.generate(byteArray, CMSEnvelopedDataGenerator.RC2_CBC, 40, "BC");
            } else if (info.getEncryptionType() == AS2Message.ENCRYPTION_RC2_64) {
                envelopedData = dataGenerator.generate(byteArray, CMSEnvelopedDataGenerator.RC2_CBC, 64, "BC");
            } else if (info.getEncryptionType() == AS2Message.ENCRYPTION_RC2_128) {
                envelopedData = dataGenerator.generate(byteArray, CMSEnvelopedDataGenerator.RC2_CBC, 128, "BC");
            } else if (info.getEncryptionType() == AS2Message.ENCRYPTION_RC2_196) {
                envelopedData = dataGenerator.generate(byteArray, CMSEnvelopedDataGenerator.RC2_CBC, 196, "BC");
            } else if (info.getEncryptionType() == AS2Message.ENCRYPTION_AES_128) {
                envelopedData = dataGenerator.generate(byteArray, CMSEnvelopedDataGenerator.AES128_CBC, "BC");
            } else if (info.getEncryptionType() == AS2Message.ENCRYPTION_AES_192) {
                envelopedData = dataGenerator.generate(byteArray, CMSEnvelopedDataGenerator.AES192_CBC, "BC");
            } else if (info.getEncryptionType() == AS2Message.ENCRYPTION_AES_256) {
                envelopedData = dataGenerator.generate(byteArray, CMSEnvelopedDataGenerator.AES256_CBC, "BC");
            } else if (info.getEncryptionType() == AS2Message.ENCRYPTION_RC4_40) {
                envelopedData = dataGenerator.generate(byteArray,
                        cryptoHelper.convertAlgorithmNameToOID(BCCryptoHelper.ALGORITHM_RC4), 40, "BC");
            } else if (info.getEncryptionType() == AS2Message.ENCRYPTION_RC4_56) {
                envelopedData = dataGenerator.generate(byteArray,
                        cryptoHelper.convertAlgorithmNameToOID(BCCryptoHelper.ALGORITHM_RC4), 56, "BC");
            } else if (info.getEncryptionType() == AS2Message.ENCRYPTION_RC4_128) {
                envelopedData = dataGenerator.generate(byteArray,
                        cryptoHelper.convertAlgorithmNameToOID(BCCryptoHelper.ALGORITHM_RC4), 128, "BC");
            }
            byte[] result = envelopedData.getEncoded();
            this.logger.log(Level.INFO, this.rb.getResourceString("message.encrypted",
                    new Object[]{
                info.getMessageId(), receiver.getCryptAlias(),
                this.rbMessage.getResourceString("encryption." + receiver.getEncryptionType())
            }), info);
            message.setRawData(result);
        } else {
            message.setRawData(message.getDecryptedRawData());
            this.logger.log(Level.INFO, this.rb.getResourceString("message.notencrypted",
                    new Object[]{
                info.getMessageId()
            }), info);
        }
        return (message);
    }

    /**Compresses the payload using the ZLIB algorithm
     */
    private MimeBodyPart compressPayload(MimeBodyPart bodyPart) throws SMIMEException {
        SMIMECompressedGenerator generator = new SMIMECompressedGenerator();
        generator.setContentTransferEncoding("binary");
        return (generator.generate(bodyPart, SMIMECompressedGenerator.ZLIB));
    }

    /**Signs the passed message and returns it
     */
    private MimeMultipart signMessage(MimeBodyPart body, Partner sender, Partner receiver) throws Exception {
        PrivateKey senderKey = this.certificateManager.getPrivateKey(sender.getSignAlias());
        Certificate[] chain = this.certificateManager.getCertificateChain(sender.getSignAlias());
        String digest = null;
        if (receiver.getSignType() == AS2Message.SIGNATURE_SHA1) {
            digest = "sha1";
        } else if (receiver.getSignType() == AS2Message.SIGNATURE_MD5) {
            digest = "md5";
        }
        BCCryptoHelper helper = new BCCryptoHelper();
        return (helper.sign(body, chain, senderKey, digest));
    }

    /**Signs the passed mdn and returns it
     */
    private MimeMessage signMDN(MimeMessage mimeMessage, Partner sender, AS2Message as2Message, AS2MessageInfo relatedMessageInfo) throws Exception {
        if (relatedMessageInfo.getDispositionNotificationOptions().signMDN()) {
            int[] possibleAlgorithm = relatedMessageInfo.getDispositionNotificationOptions().getSignatureAlgorithm();
            boolean sha1Possible = false;
            boolean md5Possible = false;
            for (int i = 0; i < possibleAlgorithm.length; i++) {
                if (possibleAlgorithm[i] == AS2Message.SIGNATURE_MD5) {
                    md5Possible = true;
                } else if (possibleAlgorithm[i] == AS2Message.SIGNATURE_SHA1) {
                    sha1Possible = true;
                }
            }
            String digest = null;
            if (sha1Possible) {
                digest = "sha1";
                as2Message.getMessageInfo().setSignType(AS2Message.SIGNATURE_SHA1);
            } else if (md5Possible) {
                digest = "md5";
                as2Message.getMessageInfo().setSignType(AS2Message.SIGNATURE_MD5);
            }
            if (digest == null) {
                as2Message.getMessageInfo().setSignType(AS2Message.SIGNATURE_NONE);
                this.logger.log(Level.INFO, this.rb.getResourceString("mdn.notsigned",
                        new Object[]{
                    as2Message.getMessageInfo().getMessageId(),
                }), as2Message.getMessageInfo());
                return (mimeMessage);
            }
            PrivateKey senderKey = this.certificateManager.getPrivateKey(sender.getSignAlias());
            Certificate[] chain = this.certificateManager.getCertificateChain(sender.getSignAlias());
            BCCryptoHelper helper = new BCCryptoHelper();
            MimeMessage signedMimeMessage = helper.sign(mimeMessage, chain, senderKey, digest.toUpperCase());
            this.logger.log(Level.INFO, this.rb.getResourceString("mdn.signed",
                    new Object[]{
                as2Message.getMessageInfo().getMessageId(), digest.toUpperCase()
            }), as2Message.getMessageInfo());
            return (signedMimeMessage);
        } else {
            as2Message.getMessageInfo().setSignType(AS2Message.SIGNATURE_NONE);
            this.logger.log(Level.INFO, this.rb.getResourceString("mdn.notsigned",
                    new Object[]{
                as2Message.getMessageInfo().getMessageId(),
            }), as2Message.getMessageInfo());
            return (mimeMessage);
        }
    }

    /**Creates a "world-unique" content id according to RFC 2045
     */
    private String createId(String senderId, String receiverId) {
        StringBuffer idBuffer = new StringBuffer();
        idBuffer.append(AS2ServerVersion.getProductNameShortcut());
        idBuffer.append("-");
        idBuffer.append(String.valueOf(System.currentTimeMillis()));
        idBuffer.append("-");
        idBuffer.append(UniqueId.get());
        idBuffer.append("@@");
        idBuffer.append(senderId);
        idBuffer.append("_");
        idBuffer.append(receiverId);
        return (idBuffer.toString());
    }

    /**Copies all data from one stream to another*/
    private void copyStreams(InputStream in, OutputStream out) throws IOException {
        BufferedInputStream inStream = new BufferedInputStream(in);
        BufferedOutputStream outStream = new BufferedOutputStream(out);
        //copy the contents to an output stream
        byte[] buffer = new byte[1024];
        int read = 1024;
        //a read of 0 must be allowed, sometimes it takes time to
        //extract data from the input
        while (read != -1) {
            read = inStream.read(buffer);
            if (read > 0) {
                outStream.write(buffer, 0, read);
            }
        }
        outStream.flush();
    }
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /mec_as2/de/mendelson/comm/as2/message/AS2MessagePacker.java 61    6.05.08 17:57 Heller $
d53 1
a53 1
 * @@version $Revision: 61 $
@

