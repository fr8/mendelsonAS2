head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2009.03.16.12.15.17;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2007.03.14.11.38.08;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/15/de/mendelson/comm/as2/message/loggui/TableModelMessageOverview.java,v 1.1 2007/03/14 11:38:08 heller Exp $
package de.mendelson.comm.as2.message.loggui;
import de.mendelson.comm.as2.message.AS2Message;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.ResourceBundleAS2Message;
import de.mendelson.comm.as2.partner.Partner;
import javax.swing.*;
import java.util.*;
import java.text.*;
import de.mendelson.util.MecResourceBundle;
import javax.swing.table.AbstractTableModel;


/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */

/**
 * Model to display the message overview
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class TableModelMessageOverview extends AbstractTableModel{
    
    public static final ImageIcon ICON_IN
            = new ImageIcon( TableModelMessageOverview.class.
            getResource( "/de/mendelson/comm/as2/message/loggui/in16x16.gif" ));
    public static final ImageIcon ICON_OUT
            = new ImageIcon( TableModelMessageOverview.class.
            getResource( "/de/mendelson/comm/as2/message/loggui/out16x16.gif" ));
    public static final ImageIcon ICON_PENDING
            = new ImageIcon( TableModelMessageOverview.class.
            getResource( "/de/mendelson/comm/as2/message/loggui/state_pending16x16.gif" ));
    public static final ImageIcon ICON_STOPPED
            = new ImageIcon( TableModelMessageOverview.class.
            getResource( "/de/mendelson/comm/as2/message/loggui/state_stopped16x16.gif" ));
    public static final ImageIcon ICON_FINISHED
            = new ImageIcon( TableModelMessageOverview.class.
            getResource( "/de/mendelson/comm/as2/message/loggui/state_finished16x16.gif" ));
    
    /**ResourceBundle to localize the headers*/
    private MecResourceBundle rb = null;
    /**ResourceBundle to localize the enc/signature stuff*/
    private MecResourceBundle rbMessage = null;
    
    /**Format the date output*/
    private DateFormat format
            = DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.SHORT );
    
    /**Stores all partner ids and the corresponding partner objects*/
    private HashMap<String,Partner> partnerMap = new HashMap<String,Partner>();
    
    /**Data to display*/
    private ArrayList<AS2MessageInfo> data = new ArrayList<AS2MessageInfo>();
    
    /** Creates new LogTableModel
     */
    public TableModelMessageOverview() {
        //load resource bundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleMessageOverview.class.getName());
            this.rbMessage = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleAS2Message.class.getName());
        }
        //load up  resourcebundle
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
    }
    
    /**Passes a list of partner ot this table
     **/
    public void passPartner( HashMap<String,Partner> partnerMap ){
        this.partnerMap.putAll( partnerMap );
        ((AbstractTableModel)this).fireTableDataChanged();
    }
    
    /**Passes data to the model and fires a table data update*/
    public void passNewData( ArrayList newData ){
        this.data.clear();
        this.data.addAll( newData );
        ((AbstractTableModel)this).fireTableDataChanged();
    }
    
    /**Returns the data stored in the specific row
     *@@param row Row to look into*/
    public AS2MessageInfo getRow( int row ){
        //may happen, not synchronized
        if( row > this.data.size()-1 )
            return( null );
        return( this.data.get(row) );
    }
    
    /**Returns the data stored in specific rows
     *@@param row Rows to look into*/
    public AS2MessageInfo[] getRows( int[] row ){
        AS2MessageInfo[] rows = new AS2MessageInfo[row.length];
        for( int i = 0; i < row.length; i++ ){
            rows[i] = this.data.get(row[i]);
        }
        return( rows );
    }
    
    /**Number of rows to display*/
    public int getRowCount(){
        return( this.data.size() );
    }
    
    /**Number of cols to display*/
    public int getColumnCount() {
        return( 9 );
    }
    
    /**Returns a value at a specific position in the grid
     */
    public Object getValueAt( int row, int col){
        AS2MessageInfo overviewRow = this.data.get( row );
        switch( col ){
            case 0: if( overviewRow.getState() == AS2Message.STATE_FINISHED ){
                return( ICON_FINISHED );
            } else if( overviewRow.getState() == AS2Message.STATE_STOPPED ){
                return( ICON_STOPPED );
            }
            return( ICON_PENDING );
            case 1: if( overviewRow.getDirection() == AS2MessageInfo.DIRECTION_IN ){
                return( ICON_IN );
            } else
                return( ICON_OUT );
            case 2: return( this.format.format( overviewRow.getMessageDate() ));
            case 3: if( overviewRow.getDirection() == AS2MessageInfo.DIRECTION_IN ){
                String id = overviewRow.getSenderId();
                Partner sender = this.partnerMap.get(id);
                if( sender != null ){
                    return( sender.getName() );
                } else{
                    return( id );
                }
            } else{
                String id = overviewRow.getReceiverId();
                Partner receiver = this.partnerMap.get(id);
                if( receiver != null ){
                    return( receiver.getName() );
                } else{
                    return( id );
                }
            }
            case 4: return( overviewRow.getMessageId() );
            case 5: if( overviewRow.getOriginalFilename() == null ){
                return( "--");
            } else{
                return( overviewRow.getOriginalFilename() );
            }
            case 6: return( this.rbMessage.getResourceString( "encryption." + overviewRow.getEncryptionType()));
            case 7: return( this.rbMessage.getResourceString( "signature." + overviewRow.getSignType()));
            case 8: if( overviewRow.requestsSyncMDN() ){
                return( "SYNC");
            } else{
                return( "ASYNC");
            }
            
        }
        return( null );
    }
    
    /**Returns the name of every column
     * @@param col Column to get the header name of
     */
    public String getColumnName( int col ){
        switch( col ){
            case 0: return( " " );
            case 1: return( "  " );
            case 2: return( this.rb.getResourceString( "header.timestamp" ));
            case 3: return( this.rb.getResourceString( "header.partner" ) );
            case 4: return( this.rb.getResourceString( "header.messageid" ) );
            case 5: return( this.rb.getResourceString( "header.originalfile"));
            case 6: return( this.rb.getResourceString( "header.encryption" ) );
            case 7: return( this.rb.getResourceString( "header.signature" ) );
            case 8: return( this.rb.getResourceString( "header.mdn" ) );
        }
        return( null );
    }
    
    /**Set how to display the grid elements
     * @@param col requested column
     */
    public Class getColumnClass( int col ){
        return(
                new Class[]{
            ImageIcon.class,
            ImageIcon.class,
            String.class,
            String.class,
            String.class,
            String.class,
            String.class,
            String.class,
            String.class
        }[col] );
    }
    
}@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /mec_as2/de/mendelson/comm/as2/message/loggui/TableModelMessageOverview.java 7     20.02.07 11:25 Heller $
d25 1
a25 1
 * @@version $Revision: 7 $
@

