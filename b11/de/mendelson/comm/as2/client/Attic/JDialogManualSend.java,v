head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.58.50;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2006.12.06.09.06.30;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b11/de/mendelson/comm/as2/client/JDialogManualSend.java,v 1.1 2006/12/06 09:06:30 heller Exp $
package de.mendelson.comm.as2.client;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.partner.PartnerAccessDB;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.util.MecFileChooser;
import de.mendelson.util.MecResourceBundle;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Dialog to send a file to a single partner
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class JDialogManualSend extends JDialog {
    
    /**ResourceBundle to localize the GUI*/
    private MecResourceBundle rb = null;
    
    private Logger logger = Logger.getLogger( "de.mendelson.as2" );
    
    private PreferencesAS2 preferences = new PreferencesAS2();
    
    /** Creates new form JDialogPartnerConfig
     *@@param parameter Parameter to edit, null for a new one
     *@@param parameterList List of available parameter
     */
    public JDialogManualSend(JFrame parent ) {
        super( parent, true );
        //load resource bundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleManualSend.class.getName());
        } catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
        this.setTitle( this.rb.getResourceString( "title" ));
        initComponents();
        this.getRootPane().setDefaultButton( this.jButtonOk );
        //fill in data
        try{
            PartnerAccessDB partnerAccess = new PartnerAccessDB( "localhost" );
            Partner[] partner = partnerAccess.getPartner();
            partnerAccess.close();
            for( int i = 0; i < partner.length; i++ ){
                if( !partner[i].isLocalStation() ){
                    this.jComboBoxPartner.addItem( partner[i]);
                }
            }
        } catch( Exception e ){
            this.logger.severe( e.getMessage() );
        }
        this.setButtonState();
    }
    
    /**Sets the ok and cancel buttons of this GUI*/
    private void setButtonState(){
        this.jButtonOk.setEnabled(
                this.jTextFieldFilename.getText().length() > 0 );
    }
    
    /**Will be executed on click to OK*/
    private void performSend() throws Exception{
        Partner receiver = (Partner)this.jComboBoxPartner.getSelectedItem();
        File outboxDir = new File( new File( this.preferences.get(
                        PreferencesAS2.DIR_MSG )).getAbsolutePath()
                        + File.separator + receiver.getName() 
                        + File.separator + "outbox" );
        if( outboxDir.exists() ){
            outboxDir.mkdirs();
        }
        File sendFile = new File( this.jTextFieldFilename.getText());
        FileInputStream inStream = new FileInputStream( sendFile );
        FileOutputStream outStream = new FileOutputStream( outboxDir
                + File.separator + sendFile.getName());
        this.copyStreams( inStream, outStream );
        outStream.flush();
        outStream.close();
        inStream.close();
    }
    
    /**Copies all data from one stream to another*/
    private void copyStreams( InputStream in, OutputStream out ) throws IOException{
        BufferedInputStream inStream = new BufferedInputStream( in );
        BufferedOutputStream outStream = new BufferedOutputStream( out );
        //copy the contents to an output stream
        byte[] buffer = new byte[1024];
        int read = 1024;
        //a read of 0 must be allowed, sometimes it takes time to
        //extract data from the input
        while( read != -1 ){
            read = inStream.read( buffer );
            if( read > 0 )
                outStream.write( buffer, 0, read );
        }
        outStream.flush();
    }
    
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelEdit = new javax.swing.JPanel();
        jLabelIcon = new javax.swing.JLabel();
        jLabelFilename = new javax.swing.JLabel();
        jTextFieldFilename = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabelPartner = new javax.swing.JLabel();
        jComboBoxPartner = new javax.swing.JComboBox();
        jButtonBrowse = new javax.swing.JButton();
        jPanelButtons = new javax.swing.JPanel();
        jButtonOk = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();

        getContentPane().setLayout(new java.awt.GridBagLayout());

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        jPanelEdit.setLayout(new java.awt.GridBagLayout());

        jPanelEdit.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jLabelIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/send_32x32.gif")));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelEdit.add(jLabelIcon, gridBagConstraints);

        jLabelFilename.setText(this.rb.getResourceString( "label.filename"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelEdit.add(jLabelFilename, gridBagConstraints);

        jTextFieldFilename.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextFieldFilenameKeyReleased(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 10);
        jPanelEdit.add(jTextFieldFilename, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        jPanelEdit.add(jPanel3, gridBagConstraints);

        jLabelPartner.setText(this.rb.getResourceString( "label.partner"));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        jPanelEdit.add(jLabelPartner, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 10);
        jPanelEdit.add(jComboBoxPartner, gridBagConstraints);

        jButtonBrowse.setIcon(new javax.swing.ImageIcon(getClass().getResource("/de/mendelson/comm/as2/client/folder.gif")));
        jButtonBrowse.setToolTipText(this.rb.getResourceString( "button.browse"));
        jButtonBrowse.setMargin(new java.awt.Insets(2, 5, 2, 5));
        jButtonBrowse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBrowseActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 10);
        jPanelEdit.add(jButtonBrowse, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        getContentPane().add(jPanelEdit, gridBagConstraints);

        jPanelButtons.setLayout(new java.awt.GridBagLayout());

        jButtonOk.setText(this.rb.getResourceString( "button.ok" ));
        jButtonOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOkActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelButtons.add(jButtonOk, gridBagConstraints);

        jButtonCancel.setText(this.rb.getResourceString( "button.cancel" ));
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
        jPanelButtons.add(jButtonCancel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        getContentPane().add(jPanelButtons, gridBagConstraints);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-417)/2, (screenSize.height-230)/2, 417, 230);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonBrowseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBrowseActionPerformed
        JFrame parent = (JFrame)SwingUtilities.getAncestorOfClass( JFrame.class, this );
        MecFileChooser chooser = new MecFileChooser( parent, 
                this.rb.getResourceString( "label.selectfile" ));
        chooser.browseFilename( this.jTextFieldFilename );
        this.setButtonState();
    }//GEN-LAST:event_jButtonBrowseActionPerformed
    
    private void jTextFieldFilenameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldFilenameKeyReleased
        this.setButtonState();
    }//GEN-LAST:event_jTextFieldFilenameKeyReleased
    
    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
        this.setVisible( false );
        this.dispose();
    }//GEN-LAST:event_jButtonCancelActionPerformed
    
    private void jButtonOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOkActionPerformed
        try{
            this.performSend();
            JFrame parent = (JFrame)SwingUtilities.getAncestorOfClass( JFrame.class, this );
            this.setVisible( false );
            JOptionPane.showMessageDialog( parent, 
                    this.rb.getResourceString( "send.success" ));
        }
        catch( Exception e ){
            this.setVisible( false );
            this.logger.severe( e.getMessage() );
        }        
        this.dispose();
    }//GEN-LAST:event_jButtonOkActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonBrowse;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonOk;
    private javax.swing.JComboBox jComboBoxPartner;
    private javax.swing.JLabel jLabelFilename;
    private javax.swing.JLabel jLabelIcon;
    private javax.swing.JLabel jLabelPartner;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanelButtons;
    private javax.swing.JPanel jPanelEdit;
    private javax.swing.JTextField jTextFieldFilename;
    // End of variables declaration//GEN-END:variables
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /mec_as2/de/mendelson/comm/as2/client/JDialogManualSend.java 4     31.05.06 11:25 Heller $
d34 1
a34 1
 * @@version $Revision: 4 $
@

