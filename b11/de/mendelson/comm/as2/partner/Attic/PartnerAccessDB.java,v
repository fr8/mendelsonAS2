head	1.2;
access;
symbols;
locks; strict;
comment	@# @;


1.2
date	2016.02.17.14.58.56;	author heller;	state dead;
branches;
next	1.1;

1.1
date	2006.12.06.09.06.38;	author heller;	state Exp;
branches;
next	;


desc
@@


1.2
log
@*** empty log message ***
@
text
@//$Header: /cvsroot/mec-as2/b11/de/mendelson/comm/as2/partner/PartnerAccessDB.java,v 1.1 2006/12/06 09:06:38 heller Exp $
package de.mendelson.comm.as2.partner;
import de.mendelson.comm.as2.database.DBDriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Implementation of a server log for the rosettanet server database
 * @@author S.Heller
 * @@version $Revision: 1.1 $
 */
public class PartnerAccessDB{
    
    /**Logger to log inforamtion to*/
    private Logger logger = Logger.getLogger( "de.mendelson.as2" );
    
    /**Connection to the database*/
    private Connection connection = null;
    
    /**Host to conenct to*/
    private String host = null;
    
    /** Creates new message I/O log and connects to localhost
     *@@param host host to connect to
     */
    public PartnerAccessDB( String host ) throws SQLException{
        this.host = host;
        this.connection = DBDriverManager.getConnectionWithoutErrorHandling( host );
    }
    
    /**Returns all partner stored in the DB, even the local station*/
    public Partner[] getPartner(){
        ArrayList<Partner> partnerList = new ArrayList<Partner>();
        ResultSet result = null;
        try{
            Statement statement = this.connection.createStatement();
            statement.setEscapeProcessing( true );
            //check if the number of entires have changed since last request
            String query = "SELECT * FROM partner";
            result = statement.executeQuery( query );
            while( result.next() ){
                Partner partner = new Partner();
                partner.setAS2Identification( result.getString( "as2ident" ));
                partner.setName( result.getString( "name" ));
                partner.setDBId( result.getInt( "id" ));
                partner.setLocalStation( result.getInt( "islocal") == 1 );
                partner.setCertAlias( result.getString( "certalias"));
                partner.setSignType( result.getInt( "sign"));
                partner.setEncryptionType( result.getInt( "encrypt"));
                partner.setEmail( result.getString( "email" ));
                partner.setURL( result.getString( "url" ));
                partner.setMdnURL( result.getString( "mdnurl" ));
                partner.setSubject( result.getString( "subject" ));
                partner.setContentType( result.getString( "contenttype" ));
                partner.setSyncMDN( result.getInt( "syncmdn" ) == 1 );
                partnerList.add( partner );
            }
            Partner[] partnerArray = new Partner[partnerList.size()];
            partnerList.toArray( partnerArray );
            return( partnerArray );
        } catch( Exception e ){
            this.logger.severe( e.getMessage() );
            return( null );
        } finally{
            if( result != null ){
                try{
                    result.close();
                } catch( Exception e ){
                    //nop
                }
            }
        }
    }
    
    /**receives a partner configuration and updates the database with them
     */
    public void updatePartner( Partner[] newPartner ){
        //first delete all partners that are in the DB but not in the new list
        Partner[] existingPartner = this.getPartner();
        ArrayList<Partner> newPartnerList = new ArrayList( Arrays.asList(newPartner) );
        for( int i = 0; i < existingPartner.length; i++ ){
            if( !newPartnerList.contains(existingPartner[i]))
                this.deletePartner( existingPartner[i] );
        }
        //insert all NEW partners and update the existing
        for( int i = 0; i < newPartner.length; i++ ){
            if( newPartner[i].getDBId() < 0 )
                this.insertPartner( newPartner[i] );
            else
                this.updatePartner( newPartner[i] );
        }
    }
    
    /**Updates a single partner in the db*/
    /**Inserts a new partner into the database
     */
    public void updatePartner( Partner partner ){
        try{
            PreparedStatement statement
                    = this.connection.prepareStatement(
                    "UPDATE partner SET as2ident=?,name=?,islocal=?,sign=?,encrypt=?,email=?,certalias=?,url=?,mdnurl=?,subject=?,contenttype=?,syncmdn=? WHERE id=?");
            statement.setEscapeProcessing( true );
            statement.setString( 1, partner.getAS2Identification() );
            statement.setString( 2, partner.getName() );
            statement.setInt( 3, partner.isLocalStation()?1:0 );
            statement.setInt( 4, partner.getSignType() );
            statement.setInt( 5, partner.getEncryptionType() );
            statement.setString( 6, partner.getEmail() );
            statement.setString( 7, partner.getCertAlias() );
            statement.setString( 8, partner.getURL());
            statement.setString( 9, partner.getMdnURL());
            statement.setString( 10, partner.getSubject());
            statement.setString( 11, partner.getContentType());
            statement.setInt( 12, partner.isSyncMDN()?1:0 );
            statement.setInt( 13, partner.getDBId() );
            statement.execute();
            statement.close();
        } catch( SQLException e ){
            this.logger.severe( "updatePartner: " + e.getMessage() );
        }
    }
    
    /**Deletes a single partner from the database
     */
    /**Updates a single partner in the db*/
    /**Inserts a new partner into the database
     */
    public void deletePartner( Partner partner ){
        try{
            PreparedStatement statement
                    = this.connection.prepareStatement( "DELETE FROM partner WHERE id=?");
            statement.setEscapeProcessing( true );
            statement.setInt( 1, partner.getDBId() );
            statement.execute();
            statement.close();
        } catch( SQLException e ){
            this.logger.severe( e.getMessage() );
        }
    }
    
    
    /**Inserts a new partner into the database
     */
    public void insertPartner( Partner partner ){
        try{
            PreparedStatement statement
                    = this.connection.prepareStatement(
                    "INSERT INTO partner(as2ident,name,islocal,sign,encrypt,email,certalias,url,mdnurl,subject,contenttype,syncmdn)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
            statement.setEscapeProcessing( true );
            statement.setString( 1, partner.getAS2Identification() );
            statement.setString( 2, partner.getName() );
            statement.setInt( 3, partner.isLocalStation()?1:0 );
            statement.setInt( 4, partner.getSignType() );
            statement.setInt( 5, partner.getEncryptionType() );
            statement.setString( 6, partner.getEmail() );
            statement.setString( 7, partner.getCertAlias() );
            statement.setString( 8, partner.getURL());
            statement.setString( 9, partner.getMdnURL());
            statement.setString( 10, partner.getSubject());
            statement.setString( 11, partner.getContentType());
            statement.setInt( 12, partner.isSyncMDN()?1:0 );
            statement.execute();
            statement.close();
        } catch( SQLException e ){
            this.logger.severe( e.getMessage() );
        }
    }
    
    
    /**Loads a specified partner from the DB
     *@@return null if the partner does not exist
     */
    public Partner getPartner( String as2ident ){
        ResultSet result = null;
        try{
            Statement statement = this.connection.createStatement();
            statement.setEscapeProcessing( true );
            //check if the number of entires have changed since last request
            String query = "SELECT * FROM partner WHERE as2ident='" + as2ident + "'";
            result = statement.executeQuery( query );
            if( !result.next() )
                return( null );
            Partner partner = new Partner();
            partner.setAS2Identification( as2ident );
            partner.setName( result.getString( "name" ));
            partner.setDBId( result.getInt( "id" ));
            partner.setLocalStation( result.getInt( "islocal") == 1 );
            partner.setCertAlias( result.getString( "certalias"));
            partner.setSignType( result.getInt( "sign"));
            partner.setEncryptionType( result.getInt( "encrypt"));
            partner.setEmail( result.getString( "email" ));
            partner.setURL( result.getString( "url" ));
            partner.setMdnURL( result.getString( "mdnurl" ));
            partner.setSubject( result.getString( "subject" ));
            partner.setContentType( result.getString( "contenttype" ));
            partner.setSyncMDN( result.getInt( "syncmdn" ) == 1);
            return( partner );
        } catch( Exception e ){
            this.logger.severe( e.getMessage() );
            return( null );
        } finally{
            if( result != null ){
                try{
                    result.close();
                } catch( Exception e ){
                    //nop
                }
            }
        }
    }
    
    /**Returns the station that is defined as local*/
    public Partner getLocalStation(){
        ResultSet result = null;
        try{
            Statement statement = this.connection.createStatement();
            statement.setEscapeProcessing( true );
            String query = "SELECT * FROM partner WHERE islocal=1";
            result = statement.executeQuery( query );
            if( !result.next() )
                return( null );
            Partner partner = new Partner();
            partner.setAS2Identification( result.getString( "as2ident" ) );
            partner.setName( result.getString( "name" ));
            partner.setDBId( result.getInt( "id" ));
            partner.setLocalStation( result.getInt( "islocal") == 1 );
            partner.setCertAlias( result.getString( "certalias"));
            partner.setSignType( result.getInt( "sign"));
            partner.setEncryptionType( result.getInt( "encrypt"));
            partner.setEmail( result.getString( "email" ));
            partner.setURL( result.getString( "url" ));
            partner.setMdnURL( result.getString( "mdnurl" ));
            partner.setSubject( result.getString( "subject" ));
            partner.setContentType( result.getString( "contenttype" ));
            partner.setSyncMDN( result.getInt( "syncmdn" ) == 1);
            return( partner );
        } catch( Exception e ){
            this.logger.severe( "PartnerAccessDB: No local station defined." );
            return( null );
        } finally{
            if( result != null ){
                try{
                    result.close();
                } catch( Exception e ){
                    //nop
                }
            }
        }
    }
    
    /**Closes the internal database connection.*/
    public void close(){
        try{
            this.connection.close();
        } catch( SQLException e ){
            this.logger.severe( e.getMessage() );
        }
    }
    
}
@


1.1
log
@*** empty log message ***
@
text
@d1 1
a1 1
//$Header: /as2/de/mendelson/comm/as2/partner/PartnerAccessDB.java 6     20.02.06 12:32 Heller $
d23 1
a23 1
 * @@version $Revision: 6 $
@

