head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2006.05.18.11.10.48;	author heller;	state Exp;
branches;
next	;


desc
@@


1.1
log
@*** empty log message ***
@
text
@//$Header: /as2/de/mendelson/comm/as2/message/loggui/TableModelMessageOverview.java 4     21.02.06 17:39 Heller $
package de.mendelson.comm.as2.message.loggui;
import de.mendelson.comm.as2.message.AS2Message;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.ResourceBundleAS2Message;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.partner.gui.ResourceBundlePartnerPanel;
import javax.swing.*;
import java.util.*;
import java.text.*;
import de.mendelson.util.MecResourceBundle;
import de.mendelson.util.table.AbstractTableModelSortable;


/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */

/**
 * Model to display the message overview
 * @@author S.Heller
 * @@version $Revision: 4 $
 */
public class TableModelMessageOverview extends AbstractTableModelSortable{
    
    public static final ImageIcon ICON_IN
            = new ImageIcon( TableModelMessageOverview.class.
            getResource( "/de/mendelson/comm/as2/message/loggui/in16x16.gif" ));
    public static final ImageIcon ICON_OUT
            = new ImageIcon( TableModelMessageOverview.class.
            getResource( "/de/mendelson/comm/as2/message/loggui/out16x16.gif" ));
    public static final ImageIcon ICON_PENDING
            = new ImageIcon( TableModelMessageOverview.class.
            getResource( "/de/mendelson/comm/as2/message/loggui/state_pending16x16.gif" ));
    public static final ImageIcon ICON_STOPPED
            = new ImageIcon( TableModelMessageOverview.class.
            getResource( "/de/mendelson/comm/as2/message/loggui/state_stopped16x16.gif" ));
    public static final ImageIcon ICON_FINISHED
            = new ImageIcon( TableModelMessageOverview.class.
            getResource( "/de/mendelson/comm/as2/message/loggui/state_finished16x16.gif" ));
    
    /**ResourceBundle to localize the headers*/
    private MecResourceBundle rb = null;
    /**ResourceBundle to localize the enc/signature stuff*/
    private MecResourceBundle rbMessage = null;
    
    /**Format the date output*/
    private DateFormat format
            = DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.SHORT );
    
    /**Stores all partner ids and the corresponding partner objects*/
    private HashMap<String,Partner> partnerMap = new HashMap<String,Partner>();
    
    /** Creates new LogTableModel
     */
    public TableModelMessageOverview() {
        super( new ArrayList());
        //load resource bundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleMessageOverview.class.getName());
            this.rbMessage = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleAS2Message.class.getName());
        }
        //load up  resourcebundle
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
    }
    
    /**Passes a list of partner ot this table
     **/
    public void passPartner( HashMap<String,Partner> partnerMap ){
        this.partnerMap.putAll( partnerMap );
        this.fireTableDataChanged();
    }
    
    /**Passes data to the model and fires a table data update*/
    public void passNewData( ArrayList newData ){
        if( super.getData() == null ){
            super.setData( newData );
        }
        //ensure to keep the pointer of the interal array for the sort superclass
        else if( newData != super.getData() ){
            super.getData().clear();
            super.getData().addAll( newData );
        }
        super.resort();
    }
    
    /**Returns the data stored in the specific row
     *@@param row Row to look into*/
    public AS2MessageInfo getRow( int row ){
        //may happen, not synchronized
        if( row > super.getData().size()-1 )
            return( null );
        return( (AS2MessageInfo)super.getData().get(row) );
    }
    
    /**Returns the data stored in specific rows
     *@@param row Rows to look into*/
    public AS2MessageInfo[] getRows( int[] row ){
        AS2MessageInfo[] rows = new AS2MessageInfo[row.length];
        for( int i = 0; i < row.length; i++ ){
            rows[i] = (AS2MessageInfo)super.getData().get(row[i]);
        }
        return( rows );
    }
    
    /**Number of rows to display*/
    public int getRowCount(){
        if( super.getData() == null ){
            return( 0 );
        }
        return(super.getData().size() );
    }
    
    /**Number of cols to display*/
    public int getColumnCount() {
        return( 8 );
    }
    
    /**Returns a value at a specific position in the grid
     */
    public Object getValueAt( int row, int col){
        AS2MessageInfo overviewRow = (AS2MessageInfo)super.getData().get( row );
        switch( col ){
            case 0: if( overviewRow.getState() == AS2Message.STATE_FINISHED ){
                return( ICON_FINISHED );
            } else if( overviewRow.getState() == AS2Message.STATE_STOPPED ){
                return( ICON_STOPPED );
            }
            return( ICON_PENDING );
            case 1: if( overviewRow.getDirection() == AS2MessageInfo.DIRECTION_IN ){
                return( ICON_IN );
            } else
                return( ICON_OUT );
            case 2: return( this.format.format( overviewRow.getMessageDate() ));
            case 3: if( overviewRow.getDirection() == AS2MessageInfo.DIRECTION_IN ){
                String id = overviewRow.getSenderId();
                Partner sender = this.partnerMap.get(id);
                if( sender != null ){
                    return( sender.getName() );
                } else{
                    return( id );
                }
            } else{
                String id = overviewRow.getReceiverId();
                Partner receiver = this.partnerMap.get(id);
                if( receiver != null ){
                    return( receiver.getName() );
                } else{
                    return( id );
                }
            }
            case 4: return( overviewRow.getMessageId() );
            case 5: return( this.rbMessage.getResourceString( "signature." + overviewRow.getSignType()));
            case 6: return( this.rbMessage.getResourceString( "encryption." + overviewRow.getEncryptionType()));
            case 7: if( overviewRow.requestsSyncMDN() ){
                return( "SYNC");
            } else{
                return( "ASYNC");
            }
            
        }
        return( null );
    }
    
    /**Returns the name of every column
     * @@param col Column to get the header name of
     */
    public String getColumnName( int col ){
        switch( col ){
            case 0: return( " " );
            case 1: return( "  " );
            case 2: return( this.rb.getResourceString( "header.timestamp" ));
            case 3: return( this.rb.getResourceString( "header.partner" ) );
            case 4: return( this.rb.getResourceString( "header.messageid" ) );
            case 5: return( this.rb.getResourceString( "header.encryption" ) );
            case 6: return( this.rb.getResourceString( "header.signature" ) );
            case 7: return( this.rb.getResourceString( "header.mdn" ) );
        }
        return( null );
    }
    
    /**Set how to display the grid elements
     * @@param col requested column
     */
    public Class getColumnClass( int col ){
        return(
                new Class[]{
            ImageIcon.class,
                    ImageIcon.class,
                    String.class,
                    String.class,
                    String.class,
                    String.class,
                    String.class,
                    String.class,
        }[col] );
    }
    
    
    /**Indicates if the passed column should be sortable*/
    public boolean isSortable(int col) {
        return( true );
    }
    
    
    public int getDefaultSortColumn() {
        return( 2 );
    }
    
    public boolean getDefaultSortOrderIsAscending() {
        return( true );
    }
    
    
}@
