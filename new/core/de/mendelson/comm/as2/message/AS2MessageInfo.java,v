head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2006.05.18.11.10.48;	author heller;	state Exp;
branches;
next	;


desc
@@


1.1
log
@*** empty log message ***
@
text
@//$Header: /as2/de/mendelson/comm/as2/message/AS2MessageInfo.java 15    22.02.06 13:29 Heller $
package de.mendelson.comm.as2.message;
import de.mendelson.util.table.PropertyComparator;
import java.io.Serializable;
import java.util.Date;

/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Stores all information about a rosettanet message
 * @@author S.Heller
 * @@version $Revision: 15 $
 */

public class AS2MessageInfo implements Serializable, PropertyComparator{
    
    public static final int DIRECTION_IN = 1;
    public static final int DIRECTION_OUT = 2;
    
    private String senderId;
    private String receiverId;
    /**Date of this message*/
    private Date messageDate = new Date();
    private String messageId;
    private String senderEMail;
    
    private String rawFilename;
    /**indicates if the message is signed*/
    private int signType = AS2Message.SIGNATURE_NONE;
    /**indicates if the message is encrypted*/
    private int encryptionType = AS2Message.ENCRYPTION_NONE;
    /**this filename origins where the payload is, it is in the pending directory
     *as long as no positive MDN came in
     */
    private String payloadFilename;
    
    private int direction;
    
    private String receivedContentMIC;
    /**Possible are
     * AS2Message.STATE_STATE_FINISHED
     *AS2Message.STATE_STATE_PENDING
     *AS2Message.STATE_STATE_STOPPED
     */
    private int state = AS2Message.STATE_PENDING;
    
    private boolean requestsSyncMDN = true;
    
    private String asyncMDNURL = null;
    
    private String subject;
    
    /**Only filled if this is an MDN, show where it relates to*/
    private String relatedMessageId;
    
    public String getSenderId() {
        return senderId;
    }
    
    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }
    
    public String getReceiverId() {
        return receiverId;
    }
    
    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }
    
    public Date getMessageDate() {
        return messageDate;
    }
    
    public void setMessageDate(Date messageDate) {
        this.messageDate = messageDate;
    }
    
    public String getMessageId() {
        return messageId;
    }
    
    /**Removes braces if they exist
     */
    public void setMessageId(String messageId){
        if( messageId != null && messageId.startsWith( "<" )
        && messageId.endsWith( ">" )){
            messageId = messageId.substring( 1, messageId.length()-1);
        }
        this.messageId = messageId;
    }
    
    public String getRawFilename() {
        return rawFilename;
    }
    
    public void setRawFilename(String rawFilename) {
        this.rawFilename = rawFilename;
    }
    
    public String getSenderEMail() {
        return senderEMail;
    }
    
    public void setSenderEMail(String senderEMail) {
        this.senderEMail = senderEMail;
    }
    
    public int getDirection() {
        return direction;
    }
    
    public void setDirection(int direction) {
        this.direction = direction;
    }
    
    public int getState() {
        return state;
    }
    
    public void setState(int state) {
        this.state = state;
    }
    
    public boolean isMDN() {
        return( this.relatedMessageId != null );
    }
    
    public int getSignType() {
        return signType;
    }
    
    public void setSignType(int signType) {
        this.signType = signType;
    }
    
    public int getEncryptionType() {
        return encryptionType;
    }
    
    public void setEncryptionType(int encryptionType) {
        this.encryptionType = encryptionType;
    }
    
    public String getReceivedContentMIC() {
        return receivedContentMIC;
    }
    
    public void setReceivedContentMIC(String receivedContentMIC) {
        this.receivedContentMIC = receivedContentMIC;
    }

    
    public boolean requestsSyncMDN() {
        return requestsSyncMDN;
    }

    public void setRequestsSyncMDN(boolean requestsSyncMDN) {
        this.requestsSyncMDN = requestsSyncMDN;
    }

    public String getAsyncMDNURL() {
        return asyncMDNURL;
    }

    public void setAsyncMDNURL(String asyncMDNURL) {
        this.asyncMDNURL = asyncMDNURL;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRelatedMessageId() {
        return relatedMessageId;
    }

    public void setRelatedMessageId(String relatedMessageId) {
        if( relatedMessageId != null && relatedMessageId.startsWith( "<" )
        && relatedMessageId.endsWith( ">" )){
            relatedMessageId = relatedMessageId.substring( 1, relatedMessageId.length()-1);
        }
        this.relatedMessageId = relatedMessageId;
    }
    
       public int compare(Object one, Object two){
        return( this.compare( one, two, 1 ));
    }
    
    /**Compares two objects of this class where the index is the property
     *of this class taht should be compared, this is useful to support
     *the JTableSortable class and it's model.
     */
    public int compare(Object one, Object two, int propertyIndex) throws IndexOutOfBoundsException {
        AS2MessageInfo infoOne = (AS2MessageInfo)one;
        AS2MessageInfo infoTwo = (AS2MessageInfo)two;
        if( propertyIndex == 0 ){
            if( infoOne.getState() < infoTwo.getState() )
                return( -1 );
            if( infoOne.getState() == infoTwo.getState() )
                return( 0 );
            if( infoOne.getState() > infoTwo.getState() )
                return( 1 );
        }
        if( propertyIndex == 1 ){
            if( infoOne.getDirection() < infoTwo.getDirection() )
                return( -1 );
            if( infoOne.getDirection() == infoTwo.getDirection() )
                return( 0 );
            if( infoOne.getDirection() > infoTwo.getDirection() )
                return( 1 );
        }
        if( propertyIndex == 2 ){
            return( infoOne.getMessageDate().compareTo(infoTwo.getMessageDate()));
        }
        if( propertyIndex == 3 ){
            String idOne;
            String idTwo;
            if( infoOne.getDirection() == AS2MessageInfo.DIRECTION_IN ){
                idOne = infoOne.getSenderId();
            } else
                idOne = infoOne.getReceiverId();
            if( infoTwo.getDirection() == AS2MessageInfo.DIRECTION_IN ){
                idTwo = infoTwo.getSenderId();
            } else
                idTwo = infoTwo.getReceiverId();
            return( idOne.compareToIgnoreCase( idTwo ));
        }
        if( propertyIndex == 4 ){
            return( infoOne.getMessageId().compareTo(infoTwo.getMessageId()));
        }
        if( propertyIndex == 5 ){
            return( new Integer( infoOne.getEncryptionType()).compareTo( 
                    infoTwo.getEncryptionType()));            
        }
        if( propertyIndex == 6 ){
            return( new Integer( infoOne.getSignType()).compareTo( 
                    infoTwo.getSignType()));            
        }        
        if( propertyIndex == 7 ){
            return( new Boolean( infoOne.requestsSyncMDN()).compareTo(
                    infoTwo.requestsSyncMDN() ));
        }                
        //return equal indicator
        return( 0 );
    }

    public String getPayloadFilename() {
        return payloadFilename;
    }

    public void setPayloadFilename(String payloadFilename) {
        this.payloadFilename = payloadFilename;
    }
    
}
@
