head	1.1;
access;
symbols;
locks; strict;
comment	@# @;


1.1
date	2006.05.18.11.10.54;	author heller;	state Exp;
branches;
next	;


desc
@@


1.1
log
@*** empty log message ***
@
text
@//$Header: /as2/de/mendelson/comm/as2/send/MessageHttpUploader.java 17    23.02.06 16:22 Heller $
package de.mendelson.comm.as2.send;
import de.mendelson.comm.as2.AS2ServerVersion;
import de.mendelson.comm.as2.client.AS2Gui;
import de.mendelson.comm.as2.client.rmi.GenericClient;
import de.mendelson.comm.as2.clientserver.ErrorObject;
import de.mendelson.comm.as2.clientserver.serialize.CommandObjectIncomingMessage;
import de.mendelson.comm.as2.message.AS2Message;
import de.mendelson.comm.as2.message.AS2MessageInfo;
import de.mendelson.comm.as2.message.MessageAccessDB;
import de.mendelson.comm.as2.partner.Partner;
import de.mendelson.comm.as2.preferences.PreferencesAS2;
import de.mendelson.util.MecResourceBundle;
import java.net.URL;
import java.security.Security;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;


/*
 * Copyright (C) mendelson-e-commerce GmbH Berlin Germany
 *
 * This software is subject to the license agreement set forth in the license.
 * Please read and agree to all terms before using this software.
 * Other product and brand names are trademarks of their respective owners.
 */
/**
 * Class to allow HTTP multipart uploads
 * @@author  S.Heller
 * @@version $Revision: 17 $
 */
public class MessageHttpUploader{
    
    private Logger logger = Logger.getLogger( "de.mendelson.as2" );
    
    private PreferencesAS2 preferences = new PreferencesAS2();
    
    /**localisze the GUI*/
    private MecResourceBundle rb = null;
    
    /**The header that has been built fro the request*/
    private Properties requestHeader = new Properties();
    
    /**remote answer*/
    private byte[] responseData = null;
    
    /**remote answer header*/
    private Header[] responseHeader = null;
    
    private AS2Gui gui;
    
    /** Creates new FTP
     * @@param hostname Name of the host to connect to
     * @@param username Name of the user that will connect to the remote ftp server
     * @@param password password to connect to the ftp server
     */
    public MessageHttpUploader( AS2Gui gui ) throws Exception {
        this.gui = gui;
        //Load default resourcebundle
        try{
            this.rb = (MecResourceBundle)ResourceBundle.getBundle(
                    ResourceBundleHttpUploader.class.getName());
        }
        //load up resourcebundle
        catch ( MissingResourceException e ) {
            throw new RuntimeException( "Oops..resource bundle "
                    + e.getClassName() + " not found." );
        }
        System.setProperty("java.protocol.handler.pkgs",
                "com.sun.net.ssl.internal.www.protocol");
        System.setProperty("javax.net.ssl.trustStore",
                this.preferences.get( PreferencesAS2.KEYSTORE_HTTPS_SEND ));
        System.setProperty("javax.net.ssl.trustStorePassword",
                this.preferences.get( PreferencesAS2.KEYSTORE_HTTPS_SEND_PASS ));
        
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
    }
    
    /**Returns the created header for the sent data*/
    public Properties send( AS2Message message, Partner sender, Partner receiver ) throws Exception{
        AS2MessageInfo info = message.getMessageInfo();
        int returnCode = this.performUpload( message, sender, receiver );
        if( returnCode == HttpServletResponse.SC_OK ){
            this.logger.log( Level.INFO,
                    this.rb.getResourceString( "standard.returncode",
                    new Object[]{
                info.getMessageId(),
                        String.valueOf( returnCode )
            }), info );
        }else throw new Exception( info.getMessageId() + ": HTTP " + returnCode );
        MessageAccessDB messageAccess = new MessageAccessDB( "localhost" );
        messageAccess.insertMessage( message.getMessageInfo() );
        messageAccess.close();
        this.gui.refreshMessageOverviewList();
        //inform the server of the result if a sync MDN has been requested
        if( message.getMessageInfo().requestsSyncMDN()){
            GenericClient client = new GenericClient();
            CommandObjectIncomingMessage commandObject = new CommandObjectIncomingMessage();
            commandObject.setMessageData( this.responseData );
            for( int i = 0; i < this.responseHeader.length; i++ ){
                String key = this.responseHeader[i].getName();
                String value = this.responseHeader[i].getValue();
                commandObject.addHeader( key, value);
                if( key.toLowerCase().equals( "content-type" ))
                    commandObject.setContentType( value );
            }
            ErrorObject errorObject = client.send( commandObject );
        }
        return( this.requestHeader );
    }
    
    /**Sets the proxy authentification for the client*/
    private void setProxyAuthentification( HttpClient client ){
        //the proxy authentification
        if( this.preferences.getBoolean( PreferencesAS2.AUTH_PROXY_USE )){
            UsernamePasswordCredentials credentials = null;
            String user = this.preferences.get( PreferencesAS2.AUTH_PROXY_USER );
            String pass = this.preferences.get( PreferencesAS2.AUTH_PROXY_PASS );
            String host = this.preferences.get( PreferencesAS2.AUTH_PROXY_HOST );
            if( user != null && user.length() > 0 ){
                credentials = new UsernamePasswordCredentials( user, pass );
            }
            client.getState().setProxyCredentials( AuthScope.ANY_REALM, host, credentials );
        }
    }
    
    
    /**Uploads the data, returns the HTTP result code*/
    private int performUpload( AS2Message message, Partner sender, Partner receiver ){
        PostMethod filePost = new PostMethod(receiver.getURL());
        int status = -1;
        try {
            filePost.addRequestHeader( "AS2-Version", "1.1" );
            filePost.addRequestHeader( "mime-version", "1.0" );
            filePost.addRequestHeader( "User-Agent", AS2ServerVersion.getProductName() + " " + AS2ServerVersion.getVersion() );
            filePost.addRequestHeader( "recipient-address", receiver.getURL() );
            filePost.addRequestHeader( "message-id", "<" + message.getMessageInfo().getMessageId() + ">");
            filePost.addRequestHeader( "as2-from", sender.getAS2Identification() );
            filePost.addRequestHeader( "as2-to", receiver.getAS2Identification() );
            filePost.addRequestHeader( "subject", message.getMessageInfo().getSubject() );
            filePost.addRequestHeader( "from", sender.getEmail() );
            filePost.addRequestHeader( "connection","close, TE" );
            DateFormat format = new SimpleDateFormat( "EE, dd MMM yyyy HH:mm:ss zz");
            filePost.addRequestHeader( "date", format.format( new Date() ));
            if( message.getMessageInfo().getEncryptionType() != AS2Message.ENCRYPTION_NONE ){
                filePost.addRequestHeader( "Content-Type", "application/pkcs7-mime; smime-type=enveloped-data; name=smime.p7m" );                            
            } else{
                filePost.addRequestHeader( "Content-Type", message.getContentType() );
            }
            URL url = null;
            //MDN header, this is always the way for async MDNs
            if( message.getMessageInfo().isMDN()){
                this.logger.log( Level.INFO,
                        this.rb.getResourceString( "sending.mdn.async",
                        new Object[]{
                    message.getMessageInfo().getMessageId(),
                            message.getMessageInfo().getAsyncMDNURL()
                }), message.getMessageInfo() );
                url = new URL( message.getMessageInfo().getAsyncMDNURL());
                filePost.addRequestHeader( "Server", AS2ServerVersion.getProductName());
            } else{
                //message header
                if( message.getMessageInfo().requestsSyncMDN()){
                    this.logger.log( Level.INFO,
                            this.rb.getResourceString( "sending.msg.sync",
                            new Object[]{
                        message.getMessageInfo().getMessageId(),
                                receiver.getURL()
                    }), message.getMessageInfo() );
                } else{
                    this.logger.log( Level.INFO,
                            this.rb.getResourceString( "sending.msg.async",
                            new Object[]{
                        message.getMessageInfo().getMessageId(),
                                receiver.getURL()
                    }), message.getMessageInfo() );
                }
                url = new URL( receiver.getURL());
                if( !message.getMessageInfo().requestsSyncMDN()){
                    filePost.addRequestHeader( "receipt-delivery-option", message.getMessageInfo().getAsyncMDNURL() );
                }
                filePost.addRequestHeader( "disposition-notification-to", sender.getMdnURL() );
                filePost.addRequestHeader( "disposition-notification-options", "signed-receipt-protocol=optional, pkcs7-signature;signed-receipt-micalg=optional, sha1" );
                if( message.getMessageInfo().getSignType() != AS2Message.SIGNATURE_NONE ){
                    filePost.addRequestHeader( "content-disposition", "attachment; filename=\"smime.p7m\"" );
                }
            }
            int port = url.getPort();
            if( port == -1 )
                port = url.getDefaultPort();
            filePost.addRequestHeader( "host", url.getHost() + ":" + port );
            filePost.setRequestEntity( new ByteArrayRequestEntity( message.getRawData() ));
            HttpClient client = new HttpClient();
            this.setProxyAuthentification(client);
            client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
            status = client.executeMethod(filePost);
            this.responseData = filePost.getResponseBody();
            this.responseHeader = filePost.getResponseHeaders();
            Header[] requestHeader = filePost.getRequestHeaders();
            for( int i = 0; i < requestHeader.length; i++ ){
                this.requestHeader.setProperty( requestHeader[i].getName(), requestHeader[i].getValue() );
            }
            if( status != HttpServletResponse.SC_OK ){
                this.logger.severe( filePost.getStatusText());
            }
        } catch (Exception ex) {
            this.logger.severe( ex.getClass().getName() + ": " + ex.getMessage());
        } finally {
            filePost.releaseConnection();
        }
        return( status );
    }
    
    /**Returns the version of this class*/
    public static String getVersion(){
        String revision = "$Revision: 17 $";
        return( revision.substring( revision.indexOf( ":" )+1,
                revision.lastIndexOf( "$" ) ).trim());
    }
}@
